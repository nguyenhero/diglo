import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AddOrderModule } from './productDetails/addOrder.module';
import { MyOrderModule } from './my-account/myorder.module';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap/src/bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FreeCatalogRequestModal } from './free-catalog/free-catalog-request/free-catalog-request.component';
import { LoginModal } from './account/login/login.component';
import { RegisterModal } from './account/register/register.component';
import { ForgotPasswordModal } from './account/forgot-password/forgot-password.component';
import { ChangePasswordModal } from './account/change-password/change-password.component';
import { AppComponent } from '../app.component';
import { HomeComponent } from './home/home.component';
import { FooterMenuComponent } from './footer/footer.component';
import { BannerComponent } from './banner/banner.component';
import { CategoryMenuComponent } from './category/category.component';
import { GuaranteedMenuComponent } from './guaranteed/guaranteed.component';
import { TestimonialsMenuComponent } from './testimonials/testimonials.component';
import { FreeCatalogMenuComponent } from './free-catalog/free-catalog.component';
import { HeaderMenuComponent } from './header/header.component';
import { MainMenuComponent } from './mainmenu/mainmenu.component';
import { ClassItemComponent } from './mainmenu/menu-item.component';
import { OffSaleComponent } from './offsale/offsale.component';
import { NewsletterMenuComponent } from './newsletter/newsletter.component';
import { ValidationService } from '../shared/services/api/services/validation.service';
import { UserService } from '../shared/services/api/services/user.service';
import { AuthenticationService } from '../shared/services/api/services/authentication.service';
import { AppState } from '../shared/services/api/services/appState.service';
import { CustomerCatService } from '../shared/services/api/services/customer.cat.service';
import { StateService } from '../shared/services/api/services/state.service';
import { NewsletterService } from '../shared/services/api/services/newsletter.service';
import { ProductService } from '../shared/services/api/services/product.service';
import { ProductDetailsService } from '../shared/services/api/services/productDetails.service';
import { ProductReviewService } from '../shared/services/api/services/productReview.service';
import { CardProductService } from '../shared/services/api/services/cardProduct.service';
import { OrderService } from '../shared/services/api/services/order.service';
import { PromoCodeService } from '../shared/services/api/services/promoCode.service';
import { ShippingChargeService } from '../shared/services/api/services/shippingCharge.service';
import { MenuService } from '../shared/services/api/services/menu.service';
import { CardWishProductsService } from '../shared/services/api/services/cardWishProducts.service';
import { ShippingAddressService } from '../shared/services/api/services/shippingAddress.service';
import { BillingAddressService } from '../shared/services/api/services/billingAddress.service';
import { WishListService } from '../shared/services/api/services/wishList.service';
import { ProductAdditionInfoService } from '../shared/services/api/services/productAdditionInfo.service';
import { OrderStorageService } from '../shared/services/api/services/orderStorage.service';
import { OrderLogicService } from '../shared/services/api/services/orderLogic.service';
import { FilterGroupService } from '../shared/services/api/services/filterGroup.service';

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        AddOrderModule,
        MyOrderModule,
        ModalModule.forRoot(),
        BootstrapModalModule,
    ],
    declarations: [
        LayoutComponent,
        FreeCatalogRequestModal,
        LoginModal,
        RegisterModal,
        ForgotPasswordModal,
        ChangePasswordModal,
        HomeComponent,
        FooterMenuComponent,
        BannerComponent,
        CategoryMenuComponent,
        GuaranteedMenuComponent,
        TestimonialsMenuComponent,
        FreeCatalogMenuComponent,
        HeaderMenuComponent,
        MainMenuComponent,
        ClassItemComponent,
        OffSaleComponent,
        NewsletterMenuComponent
    ],
    providers: [
        ValidationService,
        UserService,
        AuthenticationService,
        AppState,
        CustomerCatService,
        StateService,
        NewsletterService,
        ProductService,
        ProductDetailsService,
        ProductReviewService,
        CardProductService,
        OrderService,
        PromoCodeService,
        ShippingChargeService,
        MenuService,
        CardWishProductsService,
        ShippingAddressService,
        BillingAddressService,
        WishListService,
        ProductAdditionInfoService,
        OrderStorageService,
        OrderLogicService,
        FilterGroupService,
    ],
})
export class LayoutModule { }
