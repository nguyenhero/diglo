﻿import {Component} from '@angular/core';
import {Router} from '@angular/router';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
@Component({
    selector: 'privacy',
    templateUrl: './privacy.component.html',
    styleUrls: ['./privacy.component.scss']
})
export class PrivacyComponent {
    constructor(private router: Router, public appState: AppState) {
        window.scrollTo(0, 0);
        appState.publish(this.router.url);
    }
}