import {NgModule, Component} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HelpfulResourcesComponent} from './helpful-resources.component';
@NgModule({
  declarations: [HelpfulResourcesComponent],
  imports: [
    RouterModule.forChild([
      { path: '', component: HelpfulResourcesComponent, pathMatch: 'full' }
    ])
  ]
})
export class HelpfulResourcesModule {}
