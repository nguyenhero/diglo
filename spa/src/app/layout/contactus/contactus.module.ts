import {NgModule, Component} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ContactUsComponent} from './contactus.component';
@NgModule({
  declarations: [ContactUsComponent],
  imports: [
    RouterModule.forChild([
      { path: 'contactus', component: ContactUsComponent, pathMatch: 'full' }
    ])
  ]
})
export class ContactUsModule {}
