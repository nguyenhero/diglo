import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'home'
            },
            { path: 'home', component: HomeComponent },
            { path: 'allproducts', loadChildren: './products/products.module#ProductsModule' },
            { path: 'productDetails', loadChildren: './productDetails/productDetails.module#ProductDetailsModule' },
            { path: 'place-order-successfully', loadChildren: './order/placeOrderCuccessfully.module#PlaceOrderCuccessfullyModule' },
            { path: 'checkout-order', loadChildren: './order/orders.module#OrdersModule' },
            { path: 'helpful-resources', loadChildren: './helpful-resources/helpful-resources.module#HelpfulResourcesModule' },
            { path: 'my-account', loadChildren: './my-account/myaccount.module#MyAccountModule' },
            { path: 'search', loadChildren: './search/search.module#SearchModule' },
            { path: 'about-us', loadChildren: './about/about.module#AboutModule' },
            { path: 'contactus', loadChildren: './contactus/contactus.module#ContactUsModule' },
            { path: 'condition', loadChildren: './condition/condition.module#ConditionModule' },
            { path: 'privacy', loadChildren: './privacy/privacy.module#PrivacyModule' },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            },
            {
                path: 'components',
                loadChildren:
                    './material-components/material-components.module#MaterialComponentsModule'
            },
            {
                path: 'forms',
                loadChildren: './forms/forms.module#FormsModule'
            },
            {
                path: 'grid',
                loadChildren: './grid/grid.module#GridModule'
            },
            {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            },
            {
                path: 'blank-page',
                loadChildren: './blank-page/blank-page.module#BlankPageModule'
            }
        ]
   }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
