import {NgModule, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ConditionComponent} from './condition.component';
// import { OffSaleComponent } from './../OffSale/offsale.component';
@NgModule({
  declarations: 
  [ConditionComponent,
    // OffSaleComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: ConditionComponent, pathMatch: 'full' }
    ])
  ]
})
export class ConditionModule {}
