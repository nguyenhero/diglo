﻿import { Component, Inject} from '@angular/core';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { MyOrdersDetailsModal, MyOrdersDetailsContext } from './my-orders-details.component';
import { Router, ActivatedRoute} from '@angular/router';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { CardProductService } from 'src/app/shared/services/api/services/cardProduct.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { User }           from 'src/app/shared/services/api/models/user';
import { OrderHead } from 'src/app/shared/services/api/models/orderHead';
import { DOCUMENT } from '@angular/platform-browser';    //fixed popup scroll
import { OrderService } from 'src/app/shared/services/api/services/order.service';
export class MyRecentsOrdersModalContext extends BSModalContext {
    constructor(public link: string = '') {
        super();
    }
}
@Component({
    selector: 'my-recents-orders-modal',
    templateUrl: './my-recents-orders-modal.component.html',
    styleUrls: ['./my-recents-orders.component.scss']
})
export class MyRecentsOrdersModalComponent implements CloseGuard, ModalComponent<MyRecentsOrdersModalContext> {
    monthCount: string = '';
    orders: OrderHead[] = [];
    context: MyRecentsOrdersModalContext;
    constructor(
        public dialog: DialogRef<MyRecentsOrdersModalContext>,
        public modal: Modal,
        private router: Router, private appState: AppState,
        private _routeParams: ActivatedRoute,
        private orderService: OrderService,
        private cardProductService: CardProductService,
        @Inject(DOCUMENT) private document,
        private authenticationService: AuthenticationService) {
        this.getOrder();
    }
    getOrder() {
        let currentUser = this.authenticationService.getCurrentUser();
        let operation = this.orderService.getOrder(currentUser.custNo.toString(), this.monthCount);
        operation.subscribe(result => {
            this.orders = result;
        });
    }
    getAllOrder() {
        this.monthCount = '-1';
        this.getOrder();
    }
    showOrderDetailsDialog(order) {
        let context = new MyOrdersDetailsContext(order);
        context.dialogClass = 'my-orders-details';
        context.isBlocking = true;
        this.modal.open(MyOrdersDetailsModal, { context: context });
    }
    reOrder(order) {
        let orderOperation = this.orderService.getOrderDetails(order.orderID.toString());
        orderOperation.subscribe(result => {
            let currentUser = this.authenticationService.getCurrentUser();
            if (currentUser) {
                result.forEach(orderDetail => {
                    let product = orderDetail.product;
                    product.qty = orderDetail.quantity;
                    this.cardProductService.addProduct(product);
                });
                let queryParams = { orderId: order.orderID };
                this.router.navigate(['checkout-order'], { queryParams: queryParams });
            }
        });
    }
    close() {
        this.dialog.close();
        this.document.body.classList.remove('modal-open'); //fixed popup scroll
    }
    beforeDismiss(): boolean {
        return true;
    }
    beforeClose(): boolean {
        return false;
    }
}

