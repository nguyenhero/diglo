import { NgModule, Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MyAccountOverralComponent } from './my-account-overral.component';
import { ShareWishlistModal } from './share-wish-list.component';
import { MyAccountComponent } from './my-account.component';
import { ChangePasswordComponent } from './change-password.component';
import { MyAddressBookComponent } from './my-address-book.component';
import { MyWishListComponent } from './my-wish-list.component';
import { MyOrdersComponent } from './my-orders.component';
import { MyRecentsOrdersComponent } from './my-recents-orders.component';
@NgModule({
  declarations: [
    MyAccountOverralComponent,
    ShareWishlistModal,
    MyOrdersComponent,
    MyAccountComponent,
    ChangePasswordComponent,
    MyRecentsOrdersComponent,
    MyAddressBookComponent,
    MyWishListComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: MyAccountOverralComponent, pathMatch: 'full' }
    ])
  ],
})
export class MyAccountModule {}
