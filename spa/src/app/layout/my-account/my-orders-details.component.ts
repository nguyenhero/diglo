﻿import { Component, Inject, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { ShareWishlistModal, ShareWishlistContext } from './share-wish-list.component';
import { DOCUMENT } from '@angular/platform-browser';    //fixed popup scroll
import { Router, ActivatedRoute} from '@angular/router';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { WishListService } from 'src/app/shared/services/api/services/wishList.service';
import { StateService } from 'src/app/shared/services/api/services/state.service';
import { OrderService } from 'src/app/shared/services/api/services/order.service';
import { CardProductService } from 'src/app/shared/services/api/services/cardProduct.service';
import { OrderHead } from 'src/app/shared/services/api/models/orderHead';
import { OrderDetail } from 'src/app/shared/services/api/models/orderDetail';
import { Product } from 'src/app/shared/services/api/models/product';
import { User }           from 'src/app/shared/services/api/models/user';
export class MyOrdersDetailsContext extends BSModalContext {
    constructor(public order: OrderHead) {
        super();
    }
}
@Component({
    selector: 'my-orders-details',
    templateUrl: './my-orders-details.component.html',
    styleUrls: ['./my-orders-details.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class MyOrdersDetailsModal implements CloseGuard, ModalComponent<MyOrdersDetailsContext> {
    context: MyOrdersDetailsContext;
    order: OrderHead;
    orderDetails: OrderDetail[];
    states = [];
    shippingStates = [];
    billingStates = [];
    subTotal: number;
    shippingOrderPanel: string;
    constructor(public dialog: DialogRef<MyOrdersDetailsContext>,
        private formBuilder: FormBuilder,
        @Inject(DOCUMENT) private document,
        public modal: Modal, private router: Router,
        public appState: AppState, private _routeParams: ActivatedRoute,
        private stateService: StateService,
        private orderService: OrderService,
        private cardProductService: CardProductService,
        public authenticationService: AuthenticationService,
        public wishListService: WishListService
    ) {
        this.order = dialog.context.order;
        let shippingStateOperation = this.stateService.get(this.order.shippingCountry);
        shippingStateOperation.subscribe(result => {
            this.shippingStates = result;

        });

        let billingStateOperation = this.stateService.get(this.order.billingCountry);
        billingStateOperation.subscribe(result => {
            this.billingStates = result;

        });

        let orderOperation = this.orderService.getOrderDetails(this.order.orderID.toString());
        orderOperation.subscribe(result => {
            this.orderDetails = result;
            this.getProductsPriceTotals();
        });
        dialog.setCloseGuard(this);
    }
    getCountry(country) {
        switch (country) {
            case 'US':
                return 'United States';
            case 'CA':
                return 'Canada';
        }
    }
    getShippingState(state) {
        for (let i = 0; i < this.shippingStates.length; i++) {
            if (this.shippingStates[i].stateID.toString() == state) {
                return this.shippingStates[i].stateDesc;
            }
        }
    }
    getBillingState(state) {
        for (let i = 0; i < this.billingStates.length; i++) {
            if (this.billingStates[i].stateID.toString() == state) {
                return this.billingStates[i].stateDesc;
            }
        }
    }
    getShipFee(order) {
        return order.shipFee;
    }
    getProductsPriceTotals() {
        let total = 0;
        if (this.orderDetails) {
            for (let i = 0; i < this.orderDetails.length; i++) {
                if (this.orderDetails[i].cartPrice >= 0) {
                    total += this.orderDetails[i].cartPrice * this.orderDetails[i].quantity;
                } else {
                    total += this.orderDetails[i].webPrice * this.orderDetails[i].quantity;
                }
            }
        }
        this.subTotal = total;
    }
    print() {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }
    reOrder() {
        this.close();
        let order = this.order;
        let orderOperation = this.orderService.getOrderDetails(order.orderID.toString());
        orderOperation.subscribe(result => {
            let currentUser = this.authenticationService.getCurrentUser();
            if (currentUser) {
                result.forEach(orderDetail => {
                    let product = orderDetail.product;
                    product.qty = orderDetail.quantity;
                    this.cardProductService.addProduct(product);
                });
                let queryParams = { orderId: order.orderID };
                this.router.navigate(['checkout-order'], { queryParams: queryParams });
            }

        });
    }
    close() {
        this.dialog.close();
        this.document.body.classList.remove('modal-open'); 
    }
    beforeDismiss(): boolean {
        return true;
    }
    beforeClose(): boolean {
        return false;
    }
}
