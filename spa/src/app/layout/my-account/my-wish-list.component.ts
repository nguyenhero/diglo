﻿import { Component, Inject} from '@angular/core';
import { Modal, } from 'angular2-modal/plugins/bootstrap';
import { ShareWishlistModal, ShareWishlistContext } from './share-wish-list.component';
import { DOCUMENT } from '@angular/platform-browser';    //fixed popup scroll
import { WishList } from 'src/app/shared/services/api/models/wishList';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { WishListService } from 'src/app/shared/services/api/services/wishList.service';
import { OrderLogicService } from 'src/app/shared/services/api/services/orderLogic.service';
@Component({
    selector: 'my-wish-list',
    templateUrl: './my-wish-list.component.html',
    styleUrls: ['./my-wish-list.component.scss']
})
export class MyWishListComponent {
    wishLists: WishList[];
    constructor(public appState: AppState, private router: Router, public modal: Modal,
        private authenticationService: AuthenticationService,
        private wishListService: WishListService,
        @Inject(DOCUMENT) private document,
        private orderLogicService: OrderLogicService
    ) {
        let currentUser = this.authenticationService.getCurrentUser();
        let operation = this.wishListService.get(currentUser.custNo.toString());
        operation.subscribe(
            result => {
                this.wishLists = result;
            }
        );
    }
    changeQty(wishList, value) {
        wishList.wishItemQty = parseInt(value);
    }
    remove(wishList: WishList, index: number) {
        this.wishLists.splice(index, 1);
        let operation = this.wishListService.deleteByWishListId(wishList.wishID);
        operation.subscribe(
            result => {
                let msg = result > 0
                    ? `You WishList has been removed.`
                    : `You WishList has been removed unsuccessfully`;
                this.modal.alert()
                    .size('sm')
                    .isBlocking(true)
                    .showClose(true)
                    .body(msg)
                    .open().then(x=>{
                        this.document.body.classList.remove('modal-open');
                    });
            }
        );
    }
    addToCart(wishList: WishList, index) {        
        wishList.product.qty = wishList.wishItemQty;
        this.orderLogicService.addToCart(wishList.product);
        let operation = this.wishListService.deleteByWishListId(wishList.wishID);
        operation.subscribe(
            result => {
                this.wishLists.splice(index, 1);
                let msg = result > 0
                    ? `You WishList has been sent to cart.`
                    : `You WishList has been sent unsuccessfully`;
                this.modal.alert()
                    .size('sm')
                    .isBlocking(true)
                    .showClose(true)
                    .body(msg)
                    .open().then(x=>{
                        this.document.body.classList.remove('modal-open');
                    });
            }
        );
       
    }
    continueShopping() {
        this.router.navigate(['all-products']);
    }
    addAllToCart() {
        let currentUser = this.authenticationService.getCurrentUser();
        this.wishLists.forEach(wishList => {
            wishList.product.qty = wishList.wishItemQty;
            this.orderLogicService.addToCart(wishList.product);
        });
        this.wishLists = [];
        let operation = this.wishListService.deleteByCustNo(currentUser.custNo.toString());
        operation.subscribe(
            result => {
                let msg = result > 0
                    ? `You WishList has been sent to cart.`
                    : `You WishList has been sent unsuccessfully`;
                this.modal.alert()
                    .size('sm')
                    .isBlocking(true)
                    .showClose(true)
                    .body(msg)
                    .open().then(x=>{
                        this.document.body.classList.remove('modal-open');
                    });
            }
        );
    }
    shareWishList() {
        this.showShareWishListDialog();
    }
    showShareWishListDialog() {
        let context = new ShareWishlistContext(this.wishLists);
        context.dialogClass = 'share-wish-list';
        context.isBlocking = true;
        this.modal.open(ShareWishlistModal, { context: context });
    }
}