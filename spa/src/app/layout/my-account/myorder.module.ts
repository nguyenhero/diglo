import { NgModule, Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { MyOrdersDetailsModal } from './my-orders-details.component';
import { MyAddressBookModalComponent } from './my-address-book-modal.component';
import { MyWishListModalComponent } from './my-wish-list-modal.component';
import { ChangePasswordModalComponent } from './change-password-modal.component';
import { MyOrdersModalComponent } from './my-orders-modal.component';
import { MyRecentsOrdersModalComponent } from './my-recents-orders-modal.component';
import { MyAccountModalComponent } from './my-account-modal.component';
@NgModule({
  declarations: [
    MyOrdersDetailsModal,
    MyAddressBookModalComponent,
    MyWishListModalComponent,
    ChangePasswordModalComponent,
    MyOrdersModalComponent,
    MyRecentsOrdersModalComponent,
    MyAccountModalComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ModalModule.forRoot(),
    BootstrapModalModule   
  ],
  providers: [
  ],
  entryComponents: [
    MyOrdersDetailsModal,
    ChangePasswordModalComponent,
    MyAddressBookModalComponent,
    MyOrdersModalComponent,
    MyRecentsOrdersModalComponent,
    MyWishListModalComponent,
    MyAccountModalComponent
  ]
})
export class MyOrderModule {
}
