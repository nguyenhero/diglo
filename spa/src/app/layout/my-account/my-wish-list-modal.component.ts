﻿import { Component , Inject} from '@angular/core';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { DOCUMENT } from '@angular/platform-browser';    //fixed popup scroll
import { WishList } from 'src/app/shared/services/api/models/wishList';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { WishListService } from 'src/app/shared/services/api/services/wishList.service';
import { OrderLogicService } from 'src/app/shared/services/api/services/orderLogic.service';
import { ShareWishlistContext, ShareWishlistModal } from './share-wish-list.component';
export class MyWishListModalContext extends BSModalContext {
    constructor(public link: string = '') {
        super();
    }
}
@Component({
    selector: 'my-wish-list-modal',
    templateUrl: './my-wish-list-modal.component.html',
    styleUrls: ['./my-wish-list.component.scss']
})
export class MyWishListModalComponent implements CloseGuard, ModalComponent<MyWishListModalContext> {
    context: MyWishListModalContext;
    wishLists: WishList[];
    constructor(
        public dialog: DialogRef<MyWishListModalContext>,
        public appState: AppState, private router: Router, public modal: Modal,
        private authenticationService: AuthenticationService,
        private wishListService: WishListService,
        @Inject(DOCUMENT) private document,
        private orderLogicService: OrderLogicService
    ) {
        let currentUser = this.authenticationService.getCurrentUser();
        let operation = this.wishListService.get(currentUser.custNo.toString());
        operation.subscribe(
            result => {
                this.wishLists = result;
            }
        );
    }
    changeQty(wishList, value) {
        wishList.wishItemQty = parseInt(value);
    }
    remove(wishList: WishList, index: number) {
        this.wishLists.splice(index, 1);
        let operation = this.wishListService.deleteByWishListId(wishList.wishID);
        operation.subscribe(
            result => {
                let msg = result > 0
                    ? `You WishList has been removed.`
                    : `You WishList has been removed unsuccessfully`;
                this.modal.alert()
                    .size('sm')
                    .isBlocking(true)
                    .showClose(true)
                    .body(msg)
                    .open().then(x=>{
                        this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
                    });
            }
        );
    }
    addToCart(wishList: WishList, index) {        
        wishList.product.qty = wishList.wishItemQty;
        this.orderLogicService.addToCart(wishList.product);
        let operation = this.wishListService.deleteByWishListId(wishList.wishID);
        operation.subscribe(
            result => {
                this.wishLists.splice(index, 1);
                let msg = result > 0
                    ? `You WishList has been sent to cart.`
                    : `You WishList has been sent unsuccessfully`;
                this.modal.alert()
                    .size('sm')
                    .isBlocking(true)
                    .showClose(true)
                    .body(msg)
                    .open().then(x=>{
                        this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
                    });
            }
        );
    }
    continueShopping() {
        this.router.navigate(['all-products']);
    }
    addAllToCart() {
        let currentUser = this.authenticationService.getCurrentUser();
        this.wishLists.forEach(wishList => {
            wishList.product.qty = wishList.wishItemQty;
            this.orderLogicService.addToCart(wishList.product);
        });
        this.wishLists = [];
        let operation = this.wishListService.deleteByCustNo(currentUser.custNo.toString());
        operation.subscribe(
            result => {
                let msg = result > 0
                    ? `You WishList has been sent to cart.`
                    : `You WishList has been sent unsuccessfully`;
                this.modal.alert()
                    .size('sm')
                    .isBlocking(true)
                    .showClose(true)
                    .body(msg)
                    .open().then(x=>{
                        this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
                    });
            }
        );
    }
    shareWishList() {
        this.showShareWishListDialog();
    }
    showShareWishListDialog() {
        let context = new ShareWishlistContext(this.wishLists);
        context.dialogClass = 'share-wish-list';
        context.isBlocking = true;
        this.modal.open(ShareWishlistModal, { context: context });
    }
    close() {
        this.dialog.close();
        this.document.body.classList.remove('modal-open'); //fixed popup scroll
    }
    beforeDismiss(): boolean {
        return true;
    }
    beforeClose(): boolean {
        return false;
    }
}