﻿import { Component, ViewEncapsulation, Output, EventEmitter , Inject} from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { DOCUMENT } from '@angular/platform-browser';   
import { User } from 'src/app/shared/services/api/models/user';
import { BillingAddress } from 'src/app/shared/services/api/models/billingAddress';
import { ShippingAddress } from 'src/app/shared/services/api/models/shippingAddress';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { StateService } from 'src/app/shared/services/api/services/state.service';
import { OrderService } from 'src/app/shared/services/api/services/order.service';
import { BillingAddressService } from 'src/app/shared/services/api/services/billingAddress.service';
import { ShippingAddressService } from 'src/app/shared/services/api/services/shippingAddress.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
export class MyAddressBookModalContext extends BSModalContext {
    constructor(public link: string = '') {
        super();
    }
}
@Component({
    selector: 'my-address-book-modal',
    templateUrl: './my-address-book-modal.component.html',
    styleUrls: ['./my-address-book.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class MyAddressBookModalComponent implements CloseGuard, ModalComponent<MyAddressBookModalContext> {
    currentUser: User;
    billingOrderForm: any;
    shippingOrderForm: any;
    states = [];
    shippingStates = [];
    addrID: number = 0;
    billingAddresses: BillingAddress[] = [];
    currentBillingAddresses: BillingAddress;
    showBillingAddresses: boolean = false;
    shippingOrderPanel: string;
    shippingAddresses: ShippingAddress[] = [];
    currentShippingAddresses: ShippingAddress;
    showShippingddresses: boolean = false;
    context: MyAddressBookModalContext;
    constructor(public dialog: DialogRef<MyAddressBookModalContext>,
        private formBuilder: FormBuilder,
        private modal: Modal,
        private appState: AppState,
        private stateService: StateService,
        private orderService: OrderService,
        @Inject(DOCUMENT) private document,
        private billingAddressService: BillingAddressService,
        private shippingAddressService: ShippingAddressService,
        private authenticationService: AuthenticationService) {
        this.context = dialog.context;
        dialog.setCloseGuard(this);
        this.createBillingOrderForm();
        this.getBillingAddresses();
        this.getShippingAddresses();
    }
    getState(state) {
        for (let i = 0; i < this.states.length; i++) {
            if (this.states[i].stateID.toString() == state) {
                return this.states[i].stateDesc;
            }
        }
    }
    getCountry(country) {
        switch (country) {
            case 'US':
                return 'United States';
            case 'CA':
                return 'Canada';
        }
    }
    getShippingState(state) {
                for (let i = 0; i < this.shippingStates.length; i++) {
            if (this.shippingStates[i].stateID.toString() == state) {
                return this.shippingStates[i].stateDesc;
            }
        }
    }
    createBillingOrderForm() {
        this.billingOrderForm = this.formBuilder.group({
            'firstName': ['', Validators.required],
            'lastName': ['', Validators.required],
            'phone': ['', [Validators.required, ValidationService.telephoneValidator]],
            'country': ['', Validators.required],
            'address': ['', Validators.required],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zip': ['', Validators.required]
        });
        this.shippingOrderForm = this.formBuilder.group({
            'firstName': ['', Validators.required],
            'lastName': ['', Validators.required],
            'phone': ['', [Validators.required, ValidationService.telephoneValidator]],
            'country': ['', Validators.required],
            'address': ['', Validators.required],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zip': ['', Validators.required]
        });
    }
    changeCountry(value) {
        if (value) {
            let stateOperation = this.stateService.get(value);
            stateOperation.subscribe(result => {
                this.states = result;
            });
        }
    }
    changeShippingCountry(value) {
        if (value) {
            let stateOperation = this.stateService.get(value);
            stateOperation.subscribe(result => {
                this.shippingStates = result;
            });
        }
    }
    changeBillingAddress(addrID) {
        this.findBillingAddress(addrID);
        if (this.showBillingAddresses) {
            this.setDataToBillingOrderForm(this.currentBillingAddresses);
        }
    }
    findBillingAddress(addrID) {
        for (let i = 0; i < this.billingAddresses.length; i++) {
            if (this.billingAddresses[i].addrID.toString() === addrID) {
                this.currentBillingAddresses = this.billingAddresses[i];
                return;
            }
        }
    }
    getBillingAddresses() {
        let self = this;
        this.currentUser = this.authenticationService.getCurrentUser();
        if (this.currentUser) {
            let billingAddressOperation = this.orderService.getBillingAddress(this.currentUser.custNo.toString());
            billingAddressOperation.subscribe(
                data => {
                    if (data.length > 0) {
                        //this.showBillingAddresses = true;
                        self.billingAddresses = data;
                        self.currentBillingAddresses = data[0];
                        self.changeCountry(data[0].country);
                    }
                });
        }
    }
    getShippingAddresses() {
        let self = this;
        this.currentUser = this.authenticationService.getCurrentUser();
        if (this.currentUser) {
            let self = this;
            let shippingAddressOperation = this.orderService.getshippingAddress(this.currentUser.custNo.toString());
            shippingAddressOperation.subscribe(
                data => {
                    if (data.length > 0) {
                        self.shippingAddresses = data;
                        self.currentShippingAddresses = data[0];
                        self.changeShippingCountry(data[0].country);
                    }

                });
        }
    }
    findShippingAddress(addrID) {
        for (let i = 0; i < this.shippingAddresses.length; i++) {
            if (this.shippingAddresses[i].addrID.toString() === addrID) {
                this.currentShippingAddresses = this.shippingAddresses[i];
                return;
            }
        }
    }
    UpdateBillingAddresses() {
        this.showBillingAddresses = true;
        this.setDataToBillingOrderForm(this.currentBillingAddresses);
    }
    UpdateShippingAddresses() {
        this.showShippingddresses = true;
        this.setDataToShippingOrderForm(this.currentShippingAddresses);
    }
    changeShippingAddress(addrID) {
        this.findShippingAddress(addrID);
        if (this.showShippingddresses) {
            this.setDataToShippingOrderForm(this.currentShippingAddresses);
        }
    }
    setDataToBillingOrderForm(data) {
        let controls = this.billingOrderForm.controls;
        controls['firstName'].setValue(data.firstName);
        controls['lastName'].setValue(data.lastName);
        controls['address'].setValue(data.address);
        controls['city'].setValue(data.city);
        controls['zip'].setValue(data.zip);
        controls['phone'].setValue(data.phone);
        controls['country'].setValue(data.country);
        let stateOperation = this.stateService.get(data.country);
        stateOperation.subscribe(result => {
            this.states = result;
            controls['state'].setValue(data.state);
        });
    }
    setDataToShippingOrderForm(data) {
        let controls = this.shippingOrderForm.controls;
        controls['firstName'].setValue(data.firstName);
        controls['lastName'].setValue(data.lastName);
        controls['address'].setValue(data.address);
        controls['city'].setValue(data.city);
        controls['zip'].setValue(data.zip);
        controls['phone'].setValue(data.phone);
        controls['country'].setValue(data.country);
        let stateOperation = this.stateService.get(data.country);
        stateOperation.subscribe(result => {
            this.shippingStates = result;
            controls['state'].setValue(data.state);
        });
    }
    validateAllFormFields(formGroup: FormGroup) {
        if (formGroup && formGroup.controls) {
            Object.keys(formGroup.controls).forEach(field => {
                const control = formGroup.get(field);
                if (control) {
                    if (control instanceof FormControl) {             //{4}
                        control.markAsTouched({ onlySelf: true });
                    } else if (control instanceof FormGroup) {        //{5}
                        this.validateAllFormFields(control);            //{6}
                    }
                }
            });
        }
    }
    setDataByUser(self, data) {
        let controls = self.billingOrderForm.controls;
        controls['firstName'].setValue(data.custFirstName);
        controls['lastName'].setValue(data.custLastName);
        controls['email'].setValue(data.custEmail);
    }
    chooseSameAsShippingAddress() {
        this.currentShippingAddresses.addrID = 0;
        this.currentShippingAddresses.firstName = this.billingOrderForm.value.firstName;
        this.currentShippingAddresses.lastName = this.billingOrderForm.value.lastName;
        this.currentShippingAddresses.address = this.billingOrderForm.value.address;
        this.currentShippingAddresses.city = this.billingOrderForm.value.city;
        this.currentShippingAddresses.state = this.billingOrderForm.value.state;
        this.currentShippingAddresses.zip = this.billingOrderForm.value.zip;
        this.currentShippingAddresses.country = this.billingOrderForm.value.country;
        this.currentShippingAddresses.phone = this.billingOrderForm.value.phone;
        this.setDataToShippingOrderForm(this.currentShippingAddresses);
    }
    resetBillingAddress() {
        this.getBillingAddresses();
        this.showBillingAddresses = false;
    }
    resetShippingAddress() {
        this.getShippingAddresses();
        this.showShippingddresses = false;
    }
    saveShippingAddress(notShowDialog = false) {
        if (this.shippingOrderForm.valid) {
            if (this.currentShippingAddresses &&
                this.currentShippingAddresses.addrID > 0) {
                let updateOperation: Observable<number>;
                this.shippingOrderForm.value.updateUser = this.currentUser.custNo;
                updateOperation = this.shippingAddressService.update(this.currentShippingAddresses.addrID, this.shippingOrderForm.value);
                updateOperation.subscribe(
                    result => {
                        let msg = result == 1
                            ? `<span class='successfully-message'> You have changed successfully shipping address information.Thank you.</span>`
                            : `<span class='successfully-message'> You have changed unSuccessfully shipping address information.Thank you.</span>`
                        this.modal.alert()
                            .size('sm')
                            .showClose(true)
                            .isBlocking(true)
                            .body(msg)
                            .open().then(x => {
                                this.document.body.classList.remove('modal-open');
                            });
                        this.resetShippingAddress();
                    });
            } else {
                this.currentUser = this.authenticationService.getCurrentUser();
                this.shippingOrderForm.value.custNo = this.currentUser.custNo;

                let addOperation: Observable<number>;
                addOperation = this.shippingAddressService.add(this.shippingOrderForm.value);
                addOperation.subscribe(
                    result => {
                        if (!notShowDialog) {
                            let msg = result == 1
                                ? `<span class='successfully-message'> You have added successfully shipping address information.Thank you.</span>`
                                : `<span class='successfully-message'> You have added unSuccessfully shipping address information.Thank you.</span>`
                            this.modal.alert()
                                .size('sm')
                                .showClose(true)
                                .isBlocking(true)
                                .body(msg)
                                .open().then(x => {
                                    this.document.body.classList.remove('modal-open');
                                });
                        }
                        this.resetShippingAddress();
                    });
            }
        } else {
            this.validateAllFormFields(this.shippingOrderForm);
        }
    }
    saveBillingAddress() {
        if (this.billingOrderForm.valid) {
            if (this.currentBillingAddresses &&
                this.currentBillingAddresses.addrID > 0) {
                let updateOperation: Observable<number>;
                this.billingOrderForm.value.updateUser = this.currentUser.custNo;
                updateOperation = this.billingAddressService.update(this.currentBillingAddresses.addrID, this.billingOrderForm.value);
                updateOperation.subscribe(
                    result => {
                        let msg = result == 1
                            ? `<span class='successfully-message'> You have changed successfully billing address information.Thank you.</span>`
                            : `<span class='successfully-message'> You have changed unSuccessfully billing address information.Thank you.</span>`
                        this.modal.alert()
                            .size('sm')
                            .showClose(true)
                            .isBlocking(true)
                            .body(msg)
                            .open().then(x => {
                                this.document.body.classList.remove('modal-open');
                            });
                        this.resetBillingAddress();
                    });
            } else {
                this.currentUser = this.authenticationService.getCurrentUser();
                this.billingOrderForm.value.custNo = this.currentUser.custNo;
                let addOperation: Observable<number>;
                addOperation = this.billingAddressService.add(this.billingOrderForm.value);
                addOperation.subscribe(
                    result => {
                        let msg = result == 1
                            ? `<span class='successfully-message'> You have added successfully billing address information.Thank you.</span>`
                            : `<span class='successfully-message'> You have added unSuccessfully billing address information.Thank you.</span>`
                        if (result == 1) {
                            this.saveShippingAddress(true);
                        }
                        this.modal.alert()
                            .size('sm')
                            .showClose(true)
                            .isBlocking(true)
                            .body(msg)
                            .open().then(x => {
                                this.document.body.classList.remove('modal-open');
                            });
                        this.resetBillingAddress();
                    });
            }
        } else {
            this.validateAllFormFields(this.billingOrderForm);
        }
    }
    addShippingddress() {
        this.showShippingddresses = true;
        if (this.currentShippingAddresses.addrID) {
            this.currentShippingAddresses.addrID = null;
        }
    }
    addBillingAddress() {
        this.showBillingAddresses= true;
        if (this.currentBillingAddresses.addrID) {
            this.currentBillingAddresses.addrID = null;
        }
    }
    close() {
        this.dialog.close();
        this.document.body.classList.remove('modal-open'); //fixed popup scroll
    }
    beforeDismiss(): boolean {
        return true;
    }
    beforeClose(): boolean {
        return false;
    }
}
