import { NgModule, Component, Pipe, PipeTransform, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './../search/searchresults.component';
import { PaginationDirective } from 'src/app/shared/directives/pagination.directive.search';

@Pipe({ name: 'rateNumber' })
export class RateNumber implements PipeTransform {
    transform(value, args: string[]): any {
        let res = [];
        let decimal: number = 0;
        value = value + '';
        if (value) {
            var nindex = value.indexOf("."),
                result = "0." + (nindex > -1 ? value.substring(nindex + 1) : "0");
            if (value) {
                decimal = parseFloat(result);
            }

            for (let i = 0; i < value; i++) {
                res.push(i);
            }

            if (decimal <= 0.5 && decimal >= 0.25) {
                res[res.length - 1] = 0.5;
            } else if (decimal > 0 && decimal < 0.25) {
                res.pop();
            }
            let length = res.length;
            for (let i = length + 1; i <= 5; i++) {
                res.push(-1);
            }
        }
        return res;
    }
}

@NgModule({
  declarations: [
    SearchComponent,
    RateNumber,
    PaginationDirective
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: SearchComponent, pathMatch: 'full' }
    ])
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class SearchModule {

}
