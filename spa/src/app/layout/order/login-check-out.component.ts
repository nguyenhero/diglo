﻿import {Component, ViewEncapsulation, Inject} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {DialogRef, ModalComponent, CloseGuard} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';  
import {DOCUMENT} from '@angular/platform-browser';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
export class LoginCheckOutModalContext extends BSModalContext {
    constructor(public email: string = '') {
        super();
    }
}
@Component({
    selector: 'login-check-out-modal',
    templateUrl: './login-check-out.component.html',
    styleUrls: ['./login-check-out.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class LoginCheckOutModal implements CloseGuard, ModalComponent<LoginCheckOutModalContext> {
    userForm: any;
    context: LoginCheckOutModalContext;
    public errorMsg = '';
    isExistEmail = true;
    isExistPassword = true;
    email: string = '';
    showEmail: number = 0;
    constructor(public dialog: DialogRef<LoginCheckOutModalContext>, private formBuilder: FormBuilder, public modal: Modal,
        private authenticationService: AuthenticationService, private router: Router, public appState: AppState,
        @Inject(DOCUMENT) private document) {
        this.context = dialog.context;
        dialog.setCloseGuard(this);
        this.userForm = this.formBuilder.group({
            'email': ['', [Validators.required, ValidationService.emailValidator]],
            'password': ['', [Validators.required, Validators.minLength(6), ValidationService.passwordValidator]]
        });
        this.userForm.controls['email'].setValue(this.context.email);
        this.userForm.valueChanges.subscribe((value) => {
            this.isExistEmail = true;
            this.isExistPassword = true;
        });
    }
    login() {
        if (this.userForm.dirty && this.userForm.valid) {
            this.authenticationService.login(this.userForm.value)
                .subscribe(
                    data => {
                        let currentUser = null;
                        if (data) {
                            if (data.errorNumber == 100) this.isExistEmail = false;
                            else if (data.errorNumber == 101) this.isExistPassword = false;
                            else if (data.custEmail && data.custPassword) {
                                currentUser = data;
                                this.appState.publish('login');
                                this.dialog.close(currentUser);
                                this.document.body.classList.remove('modal-open');    //popup log scroll fixed
                            }
                        }
                    },
                    error => {
                        //this.alertService.error(error);
                        //this.loading = false;
                    });
        } else {
            this.validateAllFormFields(this.userForm);
        }
    }
    secureDifferentUser() {
        this.dialog.close(1);
        this.document.body.classList.remove('modal-open');    //popup log scroll fixed
    }
    continueToCheckout() {
        this.dialog.close(2);
        this.document.body.classList.remove('modal-open');  //popup log scroll fixed
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
    close() {
        this.dialog.close();
        this.document.body.classList.remove('modal-open');     //popup log scroll fixed
    }
    beforeDismiss(): boolean {
        return true;
    }
    beforeClose(): boolean {
        return false;
    }
}