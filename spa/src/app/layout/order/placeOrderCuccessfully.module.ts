import {NgModule, Component} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {PlaceOrderSuccessfullyComponent} from './place-order-successfully.component';
@NgModule({
  declarations: [
    PlaceOrderSuccessfullyComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: PlaceOrderSuccessfullyComponent, pathMatch: 'full' }
    ])
  ],
  entryComponents: [
  ]
})
export class PlaceOrderCuccessfullyModule {}
