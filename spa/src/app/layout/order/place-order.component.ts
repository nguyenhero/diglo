﻿declare var affirm: any;
declare var paypal: any;
import {Component, Inject, HostListener, ViewEncapsulation, Input, Output, EventEmitter, ChangeDetectorRef  } from '@angular/core';  //fromchechout
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {DOCUMENT} from '@angular/platform-browser';  //Added for popupscroll fixed
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BillingAddress } from 'src/app/shared/services/api/models/billingAddress';
import { ShippingAddress } from 'src/app/shared/services/api/models/shippingAddress';
import { ShipProvider } from 'src/app/shared/services/api/models/shipProvider';
import { CreditCard } from 'src/app/shared/services/api/models/creditCard';
import { Payflow } from 'src/app/shared/services/api/models/payflow';
import { OrderDetail } from 'src/app/shared/services/api/models/orderDetail';
import { OrderHead } from 'src/app/shared/services/api/models/orderHead';
import { Product } from 'src/app/shared/services/api/models/product';
import { User } from 'src/app/shared/services/api/models/user';
import { OrderStorage } from 'src/app/shared/services/api/models/orderStorage';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { CardProductService } from 'src/app/shared/services/api/services/cardProduct.service';
import { OrderService } from 'src/app/shared/services/api/services/order.service';
import { OrderLogicService } from 'src/app/shared/services/api/services/orderLogic.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
 
@Component({
    selector: 'place-order',
    templateUrl: './place-order.component.html',
    styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent {
    @Input()
    paymentType: number;
    @Input()
    billingAddress: BillingAddress;
    @Input()
    shippingAddress: ShippingAddress;
    @Input()
    showPass: number = 0;
    @Input()
    shipFee: number;
    @Input()
    shipProvider: ShipProvider;
    @Input()
    creditCard: CreditCard;
    @Input()
    payflow: Payflow;
    @Input()
    taxResponse: any;
    @Input()
    isExistEmail = false;
    @Input()
    comments: string;
    @Input()
    email: string;
    placeOrderForm: any;
    states = [];
    orderDetails: OrderDetail[] = [];
    orderHead: OrderHead;
    products: Product[];
    currentUser: User;
    agrreeTerm: number = -1;
    subcribeNewletter: number = 0;
    order: OrderStorage;
    moneyOver = 5000;
    private PAY_PAL_COMPLETED: string = 'PayPalCompleted';
    private CREDIT_CARD_COMPLETED: string = 'CreditCardCompleted';
    private PAY_PAL_IN_PROGRESS: string = 'PayPalInProgress';
    constructor(private formBuilder: FormBuilder,
        public appState: AppState,
        @Inject(DOCUMENT) private document,  //Added for popupscroll fixed
        private modal: Modal,
        private router: Router,
        private authenticationService: AuthenticationService,
        private cartProductService: CardProductService,
        private orderService: OrderService,
        private orderLogicService: OrderLogicService) {
        this.placeOrderForm = this.formBuilder.group({
            'password': ['', [Validators.required, Validators.minLength(6), ValidationService.passwordValidator]]
        });
        this.order = this.orderLogicService.getOrder();
        if (this.order) {
            this.products = this.order.products;
        }
    }
    agrreeTermClick() {
        if (this.agrreeTerm <= 0) {
            this.agrreeTerm = 1;
        } else {
            this.agrreeTerm = 0;
        }
    }
    subcribeNewletterClick() {
        if (this.subcribeNewletter == 0) {
            this.subcribeNewletter = 1;
        } else {
            this.subcribeNewletter = 0;
        }
    }
    createOrder() {
        this.currentUser = this.authenticationService.getCurrentUser();
        this.products = this.orderLogicService.getProducts();
        if (this.products) {
            let order = null, billingAddress = null, shippingAddress = null;
            let creditCard = this.creditCard ? this.creditCard : null;
            let custEmail = '',  custPassword ='',  custNo = -1;
            custEmail = this.isExistEmail ? this.email : '';
            this.createOrderDetails();
            this.createOrderHead();
            if (this.currentUser) {
                this.billingAddress.custNo = this.currentUser.custNo;
                this.shippingAddress.custNo = this.currentUser.custNo;
                this.orderHead.custNo = this.currentUser.custNo;
                billingAddress = this.billingAddress;
                shippingAddress = this.shippingAddress;
                custEmail = this.currentUser.custEmail;
                custPassword = this.currentUser.custPassword;
                custNo = this.currentUser.custNo;
            } else {
                this.orderHead.custNo = -1;
                if (this.showPass == 1) {
                    billingAddress = this.billingAddress;
                    shippingAddress = this.shippingAddress;
                    custPassword = this.placeOrderForm.value.password;
                }
            }
            let data = {
                'isExistEmail': this.isExistEmail,
                'subcribeNewletter': this.subcribeNewletter,
                'user': {'custEmail': custEmail, 'custPassword': custPassword, 'custNo': custNo },
                'billingAddress': billingAddress,
                'shippingAddress': shippingAddress,
                'orderDetails': this.orderDetails,
                'orderHead': this.orderHead,
                'creditCard': this.creditCard
            }
            return data;
        }
        else {
            let msg = `Please choose products`;
            this.modal.alert()
                .size('sm')
                .isBlocking(true)
                .showClose(true)
                .body(msg)
                .open().then(x=>{
                    this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
                });
        }
        return null;
    }
    createOrderHead() {
        let orderHead = new OrderHead();
        this.createOrderHeadBilling(orderHead);
        this.createOrderHeadShipping(orderHead);
        this.createOrderHeadPayment(orderHead);
        this.orderHead = orderHead;
    }
    createOrderHeadBilling(orderHead: OrderHead) {
        orderHead.billingAddress = this.billingAddress.address;
        orderHead.billingCity = this.billingAddress.city;
        orderHead.billingCompany = this.billingAddress.company ? this.billingAddress.company : '';
        orderHead.billingCountry = this.billingAddress.country;
        orderHead.billingEmail = this.currentUser ? this.currentUser.custEmail : this.email;
        orderHead.billingPhone = this.billingAddress.phone;
        orderHead.billingFax = this.billingAddress.phone;
        orderHead.billingFirstName = this.billingAddress.firstName;
        orderHead.billingLastName = this.billingAddress.lastName;
        orderHead.billingFullName = this.billingAddress.firstName + ' ' + this.billingAddress.lastName;
        orderHead.billingState = this.billingAddress.state;
        orderHead.billingZip = this.billingAddress.zip;
        orderHead.shipProvider = '';
    }
    createOrderHeadShipping(orderHead: OrderHead) {
        orderHead.shippingAddress = this.shippingAddress.address;
        orderHead.shippingCity = this.shippingAddress.city;
        orderHead.shippingCompany = this.shippingAddress.company ? this.shippingAddress.company : '';
        orderHead.shippingCountry = this.shippingAddress.country;
        orderHead.shippingEmail = this.currentUser ? this.currentUser.custEmail : this.email;
        orderHead.shippingPhone = this.shippingAddress.phone;
        orderHead.shippingFax = this.shippingAddress.phone;
        orderHead.shippingFirstName = this.shippingAddress.firstName;
        orderHead.shippingLastName = this.shippingAddress.lastName;
        orderHead.shippingFullName = this.shippingAddress.firstName + ' ' + this.shippingAddress.lastName;
        orderHead.shippingState = this.shippingAddress.state;
        orderHead.shippingZip = this.shippingAddress.zip;
        orderHead.shipFee = this.shipFee;
        orderHead.shipProvider = this.shipProvider.shipProvider;
        orderHead.shipType = this.shipProvider.shipType;
        orderHead.shipDesc = this.comments;
    }
    createOrderHeadPayment(orderHead: OrderHead) {
        let subTotal = this.orderLogicService.getSubTotal();
        orderHead.paymentType = this.billingAddress.paymentType;
        orderHead.shipFee = this.shipFee;
        orderHead.shipProvider = this.shipProvider.shipType ? 'Fedex' : '';
        orderHead.shipProviderCode = this.orderLogicService.getShipProviderCode(this.shipProvider.shipType);
        orderHead.tax = (this.taxResponse && this.taxResponse.rate) ? Math.round(this.taxResponse.rate * subTotal * 100) / 100 : 0;
        orderHead.total = this.getTotals();
        orderHead.promoCode = this.order && this.order.promoCode ? this.order.promoCode : '';
    }
    getTotals() {
        let subTotal = this.shippingAddress.country == 'CA'
            ? this.orderLogicService.getSubTotalShipToCa()
            : this.orderLogicService.getSubTotal();
        let tax = 0;
        if (this.taxResponse && this.taxResponse.rate) {
            let tax = parseFloat(this.taxResponse.rate);
        }
        return Math.round((subTotal + this.shipFee + (tax * subTotal)) * 100) / 100;
    }
    createOrderDetails() {
        let self = this;
        self.orderDetails = [];
        this.products
            .forEach(item => {
                if (item.qty > 0) {
                    if (this.shippingAddress.country == 'CA') {
                        if (item.shipToCa && item.shipToCa == 'Y') {
                            let orderDetail = this.createOrderDetail(item);
                            self.orderDetails.push(orderDetail);
                        }
                    } else {
                        let orderDetail = this.createOrderDetail(item);
                        self.orderDetails.push(orderDetail);
                    }
                }
            });
    }
    createOrderDetail(item: Product) {
        let orderDetail = new OrderDetail();
        orderDetail.productSKU = item.productSKU;
        orderDetail.webPrice = item.webPrice;
        orderDetail.cartPrice = this.order && this.order.promoCode && item.cartPrice > 0 ? item.cartPrice : -1.00;
        orderDetail.tax = 0;
        orderDetail.quantity = item.qty;
        orderDetail.productDesc = item.productDesc;
        return orderDetail;
    }
    updateOrderDetails() {
        if (this.taxResponse && this.taxResponse.breakdown && this.taxResponse.breakdown.line_items) {
            for (let i = 0; i < this.taxResponse.breakdown.line_items.lengh; i++) {
                if (this.taxResponse.breakdown.line_items[i].county_tax_rate) {
                    let tax = parseFloat(this.taxResponse.breakdown.line_items[i].county_tax_rate);
                    this.orderDetails[i].tax = Math.round(tax * 100) / 100;
                }
            }
        }
    }
    showShippingChargesOver(total) {
        if (total > this.moneyOver) {
            let msg = `Please call for orders outside the U.S. or Canada.
                   Shipping charges for Internet orders over USD $5, 000 will be Actual Shipping`;
            this.modal.alert()
                .size('sm')
                .isBlocking(true)
                .showClose(true)
                .body(msg)
                .open().then(x=>{
                    this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
                });
        }
    }
    save() {
        if (this.agrreeTerm == -1) {
            this.agrreeTerm = 0;
            return;
        }
        if (this.validateOrder() == false) return;
        if (this.getTotals() > 0) {

            if (this.products) {
                this.products.forEach(item => {
                    if (item.qty <= 0) {
                        this.orderLogicService.remove(item);
                    }
                });
            }
            let products = this.orderLogicService.getProducts();
            if (products && products.length > 0) {
                if (this.shipToCaProduct(products)) {
                    if (this.agrreeTerm == 1) {
                        let total = this.getTotals();
                        if (total <= this.moneyOver) {
                            this.saveOrder();
                        } else this.showShippingChargesOver(total);
                    }
                } else this.showMsgShipToCa();
            } else this.showMsgNoProduct();
        } else {
            let msg = `Please enter your quatity.`;
            this.modal.alert()
                .size('sm')
                .isBlocking(true)
                .showClose(true)
                .body(msg)
                .open().then(x=>{
                    this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
                });
        }
    }
    saveOrder() {
        let order = this.createOrder();
        if (order) {
            let operation = this.orderService.addOrder(order);
            operation.subscribe(
                data => {
                    if (data.result > 0) {
                        this.orderLogicService.removeAll();
                        let order = { orderNumber: data.result };
                        this.router.navigate(['place-order-successfully', order]);
                    } else {
                        if (data.result == 0 && !data.errorMsg && data.securetokenId && data.securetoken) {
                            this.createForm(data.securetokenId, data.securetoken);
                        } else {
                            let msg = data.result == 0
                                ? `You have not successfully order. Thank you.`
                                : data.errorMsg;
                            this.showUnSuccessfullyOrder(msg);
                        }
                    }
                });
        }
    }
    payPalExpressCheckout() {
        let order = this.createOrder();
        let request = {
            'shippingAddress': order.shippingAddress,
            'amount': order.orderHead.total
        };
        if (this.products) {
            this.products.forEach(item => {
                if (item.qty <= 0) {
                    this.orderLogicService.remove(item);
                }
            });
        }
        let peration = this.orderService.getPalPalExpressCheckoutTokenByRequest(request);
        peration.subscribe(data => {
            if (data.result == 0) {
                this.createForm(data.url, data.secureToken);
            } else {
                this.modal.alert()
                    .size('sm')
                    .isBlocking(true)
                    .showClose(true)
                    .body(data.errorMsg)
                    .open().then(x=>{
                        this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
                    });
            }
        });
    }
    createForm(url, secureToken) {
        var form = document.createElement("form");
        var cmd = document.createElement("input");
        var token = document.createElement("input");
        form.action = url;
        cmd.value = "_express-checkout";
        cmd.name = "cmd";
        form.appendChild(cmd);
        token.value = secureToken;
        token.name = "token";
        form.appendChild(token);
        document.body.appendChild(form);
        form.submit();
    }
    saveAffirm() {
        if (this.agrreeTerm == -1) {
            this.agrreeTerm = 0;
            return;
        }
        if (this.validateOrder() == false) return;
        if (this.getTotals() > 0) {
            if (this.products) {
                this.products.forEach(item => {
                    if (item.qty <= 0) {
                        this.orderLogicService.remove(item);
                    }
                });
            }
            let products = this.orderLogicService.getProducts();
            if (products && products.length > 0) {
                if (this.shipToCaProduct(products)) {
                    if (this.agrreeTerm == 1) {
                        let total = this.getTotals();
                        if (total <= this.moneyOver) {
                            this.createSaveAffirm();
                        } else this.showShippingChargesOver(total);
                    }
                } else this.showMsgShipToCa();
            } else this.showMsgNoProduct();
        } else {
            let msg = `Please enter your quatity.`;
            this.modal.alert()
                .size('sm')
                .isBlocking(true)
                .showClose(true)
                .body(msg)
                .open().then(x=>{
                    this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
                });
        }
    }
    showMsgShipToCa() {
        let msg = 'We cannot ship this item out Canada';
        this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .body(msg)
            .open().then(x=>{
                this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
            });
    }
    showMsgNoProduct() {
        let msg = 'No item in cart.';
        this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .body(msg)
            .open().then(x=>{
                this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
            });
    }
    createSaveAffirm() {
        let order = this.createOrder();
        if (order) {
            let operation = this.orderService.addAffirmOrder(order);
            operation.subscribe(
                data => {
                    if (data.orderId > 0) {
                        //this.orderLogicService.removeAll();
                        this.checkOutAffirm(data);
                    } else this.showUnSuccessfullyOrder();
                });
        }

    }
    showUnSuccessfullyOrder(msg = null) {
        msg = msg ? msg : 'You have not successfully order. Thank you';
        this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .body(msg)
            .open().then(x=>{
                this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
            });
    }
    checkOutAffirm(checkoutOject: any) {
        let checkoutObject = {
            "config": {
                "financial_product_key": checkoutOject.financialProductKey
            },
            "merchant": {
                "user_confirmation_url": checkoutOject.confirmationUrl,
                "user_cancel_url": checkoutOject.cancelUrl,
                "user_confirmation_url_action": "POST",
                "name": checkoutOject.billingAddress.fullName
            },
            "items": this.createAffimItems(checkoutOject.orderDetails),
            "order_id": checkoutOject.orderId,
            "metadata": {
                "mode": "redirect"
            },
            "billing": {
                "name": {
                    "first": checkoutOject.billingAddress.firstName,
                    "last": checkoutOject.billingAddress.lastName,
                },
                "address": {
                    "line1": checkoutOject.billingAddress.address,
                    "line2": checkoutOject.billingAddress.address,
                    "city": checkoutOject.billingAddress.city,
                    "state": checkoutOject.billingAddress.state,
                    "zipcode": checkoutOject.billingAddress.zip,
                },
                "phone_number": checkoutOject.billingAddress.phone,
                "email": checkoutOject.billingAddress.email,
            },
            "shipping": {
                "name": {
                    "first": checkoutOject.shippingAddress.firstName,
                    "last": checkoutOject.shippingAddress.lastName,
                },
                "address": {
                    "line1": checkoutOject.shippingAddress.address,
                    "line2": checkoutOject.shippingAddress.address,
                    "city": checkoutOject.shippingAddress.city,
                    "state": checkoutOject.shippingAddress.state,
                    "zipcode": checkoutOject.shippingAddress.zip,
                },
                "phone_number": checkoutOject.shippingAddress.phone,
                "email": checkoutOject.shippingAddress.email,
            },
            "shipping_amount": checkoutOject.orderHead.shipFee,
            "tax_amount": checkoutOject.orderHead.tax,
            "total": checkoutOject.orderHead.total,
        };
        affirm.checkout(checkoutObject);
        affirm.checkout.post();
    }
    createAffimItems(orderDetails: OrderDetail[]) {
        let items = [];
        if (orderDetails && orderDetails.length > 0) {
            orderDetails.forEach(orderDetail => {
                let item = {
                    "display_name": orderDetail.productSKU,
                    "sku": orderDetail.productSKU,
                    "unit_price": orderDetail.cartPrice > 0 ? orderDetail.cartPrice : orderDetail.webPrice,
                    "qty": orderDetail.quantity
                };
                items.push(item);
            });
        }
        return items;
    }
    shipToCaProduct(products) {
        if (products && products.length == 1) {
            let product = products[0];
            if (this.shippingAddress.country == 'CA' && product.shipToCa == 'N') return false;
        }
        return true;
    }
    validateOrder() {
        return this.getTotals() > 0 && this.orderLogicService.getSubTotal() > 0;
    }
}
