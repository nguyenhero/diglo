﻿ 
import {Component, Inject, Input, Output, OnChanges, SimpleChanges, EventEmitter} from '@angular/core';
import {DOCUMENT} from '@angular/platform-browser'; 
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {FedExRateOptionsModal, FedExRateOptionsModalContext} from './fedExRateOptions.component'; 
import { BillingAddress } from 'src/app/shared/services/api/models/billingAddress';
import { ShippingAddress } from 'src/app/shared/services/api/models/shippingAddress';
import { ShipProvider } from 'src/app/shared/services/api/models/shipProvider';
import { OrderStorage } from 'src/app/shared/services/api/models/orderStorage';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { StateService } from 'src/app/shared/services/api/services/state.service';
import { OrderService } from 'src/app/shared/services/api/services/order.service';
import { CardProductService } from 'src/app/shared/services/api/services/cardProduct.service';
import { ShippingChargeService } from 'src/app/shared/services/api/services/shippingCharge.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { OrderLogicService } from 'src/app/shared/services/api/services/orderLogic.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
 
@Component({
    selector: 'shipping-order',
    templateUrl: './shipping-order.component.html',
    styleUrls: ['./shipping-order.component.scss']
})
export class ShippingOrderComponent implements OnChanges {
    CA_FEE: number = 25.00;
    @Input()
    paymentType: number;
    @Input()
    billingAddress: BillingAddress;
    @Input()
    shippingAddress: ShippingAddress;
    @Input()
    totals: number;
    @Output('shippingOrderFormFinished') shippingOrderFormFinished: EventEmitter<Object> = new EventEmitter<Object>();
    @Output('changeFeesFinished') changeFeesFinished: EventEmitter<Object> = new EventEmitter<Object>();
    isExistEmail = false;
    shippingOrderForm: any;
    states = [];
    rates  = []; // 5/4/2018 will be empty
    addrID: number = 0;
    sameAsBillingAddress: number = 1;  // 5/4/2018 change to 1 from -1
    requiredShippingOrder: boolean = false;
    showComments: boolean = false;
    shippingAddresses: ShippingAddress[];
    shipProvider: ShipProvider;
    shipFee: number;
    rate: any;
    showShippingOrder: string = 'hide-class';
    order: OrderStorage;
    stateList: string[] = ['AK', 'HI', 'PR'];
    serviceType: string = '';
    shipCountry: string = '';
    disabled = true;
    isChangeShippingAddress: boolean = false;
    constructor(private formBuilder: FormBuilder,
        @Inject(DOCUMENT) private document,
        private modal: Modal,
        private appState: AppState,
        private stateService: StateService,
        private orderService: OrderService,
        private cardProductService: CardProductService,
        private shippingChargeService: ShippingChargeService,
        private authenticationService: AuthenticationService,
        private orderLogicService: OrderLogicService) {
        this.init();
    }
    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            let change = changes[propName];
            if (change) {
                if (propName === 'billingAddress') {
                    let curVal = JSON.stringify(change.currentValue);
                    if (curVal && this.sameAsBillingAddress == 1) {
                        this.chooseSameAsBillingAddress();
                    }
                }
                else if (propName === 'shippingAddress' &&  this.sameAsBillingAddress == -1) {
                    let curVal = JSON.stringify(change.currentValue);
                    if (curVal) {
                        this.isChangeShippingAddress = true;
                        let shippingAddress = change.currentValue;
                        this.disabled = true;
                        this.shipProvider.shipType = '';
                        this.shipFee = 0;
                        this.sameAsBillingAddress = 0;
                        this.showComments = true;
                        this.shippingOrderForm.reset();
                        if (!this.shippingAddresses) {
                            let currentUser = this.authenticationService.getCurrentUser();
                            let shippingAddressOperation = this.orderService.getshippingAddress(currentUser.custNo.toString());
                            shippingAddressOperation.subscribe(
                                data => {
                                    if (data.length > 0) {
                                        this.showShippingOrder = 'show-class';
                                        this.shipCountry = shippingAddress.country;
                                        if (this.shipCountry) {
                                            this.shippingAddresses = [];
                                            data.forEach(x => {
                                                if (x.country == this.shipCountry) {
                                                    this.shippingAddresses.push(x);
                                                }
                                            });
                                        }
                                        this.setShipingAddress(shippingAddress);
                                    }
                                });
                        } else {
                            this.setShipingAddress(shippingAddress);
                        }
                    }
                }
            }
        }
    }
    setShipingAddress(shippingAddress) {
        for (let i = 0; i < this.shippingAddresses.length; i++) {
            let item = this.shippingAddresses[i];
            item.company = item.company ? item.company : '';
            shippingAddress.company = shippingAddress.company ? shippingAddress.company : '';
            if (item.firstName == shippingAddress.firstName &&
                item.lastName == shippingAddress.lastName &&
                item.address == shippingAddress.address &&
                item.city == shippingAddress.city &&
                item.country == shippingAddress.country &&
                item.state == shippingAddress.state &&
                item.zip == shippingAddress.zip &&
                item.phone == shippingAddress.phone &&
                item.company == shippingAddress.company) {
                this.shippingOrderForm.controls['shippingAddress'].setValue(item.addrID);
                this.setDataByEmail(this, item);
                break;
            }
        }
    }
    createShippingOrderForm() {
        this.shippingOrderForm = this.formBuilder.group({
            'shippingAddress': [''],
            'firstName': ['', Validators.required],
            'lastName': ['', Validators.required],
            'phone': ['', [Validators.required, ValidationService.telephoneValidator]],
            'company': [''],
            'country': ['', Validators.required],
            'address': ['', Validators.required],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zip': ['', Validators.required],
            'comments': ['']
        });
    }
    init() {
        this.order = this.orderLogicService.getOrder();
        this.createShippingOrderForm();
        this.createShipProvider();
    }
    createShipProvider() {
        this.shipProvider = new ShipProvider();
        this.shipProvider.shipProvider = 'None';
        this.shipProvider.shipType = '';
    }
    onChangeCountry(value) {
        if (value) {
            this.shipCountry = value;
            this.showMsgShipToCa();
            let stateOperation = this.stateService.get(value);
            stateOperation.subscribe(result => {
                this.states = result;
            });
            this.shippingOrderForm.reset();
            this.shippingOrderForm.controls['country'].setValue(value);
            this.shippingOrderForm.controls['state'].setValue(-1);
            this.shippingOrderForm.controls['shippingAddress'].setValue(-1);
            this.shipFee = value == 'US' ? 0 : this.CA_FEE;
            this.createChangeFeesFinished();
            this.showShippingAddresses();
        }
    }
    onChangeState(value) {
        if (value) {
            let country = this.shippingOrderForm.controls['country'].value;
            if (country == 'US') {
                this.shippingOrderForm.controls['state'].setValue(value);
                this.getRates(value);
            } else {
                this.shipFee = this.CA_FEE;
                this.createChangeFeesFinished();
            }
        }
    }
    onChangeZip(value) {
        if (value) {
            let country = this.shippingOrderForm.controls['country'].value;
            let state = this.shippingOrderForm.controls['state'].value;
            if (country == 'US') {
                this.getRates(state);
            } else {
                this.shipFee = this.CA_FEE;
                this.createChangeFeesFinished();
            }
        }
    }
    chooseSameAsBillingAddress() {
        this.sameAsBillingAddress = 1;
        this.showComments = true;
        this.showShippingOrder = 'hide-class';
        let country = this.billingAddress.country;
        let state = this.billingAddress.state;
        this.shipCountry = country;
        this.showMsgShipToCa();
        if (country == 'US') {
            this.getRates(state);
        } else {
            this.shipFee = this.CA_FEE;
            this.createChangeFeesFinished();
        }
    }
    chooseDifferentBillingAddress() {
        this.disabled = true;
        this.shipProvider.shipType = '';
        this.shipFee = 0;
        this.sameAsBillingAddress = 0;
        this.showComments = true;
        this.shippingOrderForm.reset();
        this.shippingOrderForm.controls['country'].setValue(-1);
        this.showShippingAddresses();
        this.createChangeFeesFinished();
    }
    showShippingAddresses() {
        let currentUser = this.authenticationService.getCurrentUser();
        if (currentUser && currentUser.custNo && !this.shippingAddresses) {
            let self = this;
            let shippingAddressOperation = this.orderService.getshippingAddress(currentUser.custNo.toString());
            shippingAddressOperation.subscribe(
                data => {
                    if (data.length > 0) {
                        if (this.shipCountry) {
                            self.shippingAddresses = [];
                            data.forEach(x => {
                                if (x.country == this.shipCountry) {
                                    self.shippingAddresses.push(x);
                                }
                            });
                        } else {
                            self.shippingAddresses = data;
                            this.shippingOrderForm.controls['state'].setValue(-1);
                            this.shippingOrderForm.controls['shippingAddress'].setValue(-1);
                        }
                        if (self.shippingAddresses && self.shippingAddresses.length > 0) {
                            self.showShippingOrder = 'hide-class';
                        } else {
                            self.showShippingOrder = 'show-class';
                        }

                    } else {
                        self.showShippingOrder = 'show-class';
                        self.disabled = false;
                    }

                });
        } else {
            this.showShippingOrder = 'show-class';
            this.disabled = false;
        }

    }
    changeShippingAddress(addrID) {
        this.addrID = addrID;
        this.showShippingOrder = 'show-class';
        //this.showContinueShippingOrder = 'show-class';
        if (addrID) {
            this.findShippingAddress(addrID);
        } else {
            this.addrID = 0;
            let controls = this.shippingOrderForm.controls;
            this.shippingOrderForm.reset();
            this.shippingOrderForm.controls['country'].setValue(this.shipCountry);
            this.shippingOrderForm.controls['shippingAddress'].setValue('');
            this.shippingOrderForm.controls['state'].setValue(-1);
            this.createChangeFeesFinished();
        }
    }
    findShippingAddress(addrID) {
        for (let i = 0; i < this.shippingAddresses.length; i++) {
            if (this.shippingAddresses[i].addrID.toString() === addrID) {
                let item = this.shippingAddresses[i];
                this.setDataByEmail(this, item);
                break;
            }
        }
    }
    setDataByEmail(self, data) {
        let controls = self.shippingOrderForm.controls;
        controls['firstName'].setValue(data.firstName);
        controls['lastName'].setValue(data.lastName);
        controls['company'].setValue(data.company);
        controls['address'].setValue(data.address);
        controls['city'].setValue(data.city);
        controls['zip'].setValue(data.zip);
        controls['phone'].setValue(data.phone);
        controls['country'].setValue(data.country);
        let stateOperation = self.stateService.get(data.country);
        stateOperation.subscribe(result => {
            self.states = result;
            controls['state'].setValue(data.state);
            if (data.country == 'US') {
                this.getRates(data.state);
            } else {
                this.shipFee = this.CA_FEE;
                this.createChangeFeesFinished();
            }
        });
    }
    validateShippingOrderForm() {
        let shippingAddress = new ShippingAddress();
        if (this.sameAsBillingAddress == 1) {
            this.disabled = false;
            shippingAddress = this.billingAddress;
            this.createShippingOrderFormFinished();
        }
        else {
            if (!this.shippingOrderForm.valid) {
                this.validateAllFormFields(this.shippingOrderForm);
            } else {
                this.createShippingOrderFormFinished();
            }
        }
    }
    getRatesDialog() {
        this.order = this.orderLogicService.getOrder();
        if (this.order) {
            let weightTotal = 0;
            for (let i = 0; i < this.order.products.length; i++) {
                weightTotal = weightTotal + this.order.products[i].qty * this.order.products[i].weight;
            }
            let shippingAddress =
                this.sameAsBillingAddress == 1
                    ? this.billingAddress
                    : this.shippingOrderForm.value;

            let data = {
                'shippingAddress': shippingAddress, 'weightTotal': weightTotal
            };

            // 5/4/2018 this will change to manually put Rates
            // this.orderService.getRates(data).subscribe(
            //     result => {
            //         if (result) {
            //             this.showRatesDialog(result, shippingAddress.state);
            //         }
            //     });
            
            // this add manually rate
            this.showRatesDialog(this.rates, shippingAddress.state);
        }
    }
    getRates(state) {
            this.order = this.orderLogicService.getOrder();
            if (this.order) {
                if (this.stateList.indexOf(state) == -1) {
                    this.shipProvider.shipType = '';
                    this.shipFee = 0;
                    this.createChangeFeesFinished();
                    this.disabled = false;
                    return;
                }
                this.disabled = true;
                let weightTotal = 0;
                for (let i = 0; i < this.order.products.length; i++) {
                    weightTotal = weightTotal + this.order.products[i].qty * this.order.products[i].weight;
                }

                let shippingAddress =
                    this.sameAsBillingAddress == 1
                        ? this.billingAddress
                        : (this.shippingOrderForm.controls['country'].value
                            && this.shippingOrderForm.controls['state'].value
                            ? this.shippingOrderForm.value : null);
                let data = {
                    'shippingAddress': shippingAddress,
                    'weightTotal': weightTotal,
                    'serviceType': 'FEDEX_GROUND'
                };
                if (shippingAddress) {
                    this.shipFee = 0;
                    this.createChangeFeesFinished();
                    //this.orderService.getRates(data).subscribe(
                    //    result => {
                            // let noAvailable = false;
                            // if (result && result.rateOptions && result.rateOptions.length > 0) {
                            //     result.rateOptions.forEach(item => {
                            //         if (item.serviceTypeCode === 'FEDEX_GROUND') {
                            //             this.disabled = false;
                            //             noAvailable = true;
                            //             this.shipProvider.shipType = item.serviceTypeCode;
                            //             this.shipFee = item.totalNetCharge;
                            //             this.createChangeFeesFinished();
                            //             return;
                            //         }
                            //     });
                            // }

                            // if (!noAvailable) {
                            //     this.shipProvider.shipType = '';
                            //     this.shipFee = 0;
                            //     this.createChangeFeesFinished();
                            //     this.modal.alert()
                            //         .size('sm')
                            //         .isBlocking(true)
                            //         .showClose(true)
                            //         .body('FEDEX_GROUND services is not available')
                            //         .open().then(x=>{
                            //             this.document.body.classList.remove('modal-open');
                            //         });;
                            // }
                    // });
                }
            }      
    }
    showRatesDialog(rates, state) {
        let context = new FedExRateOptionsModalContext(rates, state, this.shipProvider);
        context.dialogClass = 'fedExRateOptions';
        context.isBlocking = true;
        let dialog = this.modal.open(FedExRateOptionsModal, { context: context });
        dialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                if (result && result.totalNetCharge) {
                    this.shipProvider.shipType = result.serviceTypeCode;
                    this.shipFee = result.totalNetCharge;
                } else {
                    this.shipProvider.shipProvider = '';
                    this.shipProvider.shipType = '';
                    this.shipFee = 0;
                }
                this.createChangeFeesFinished();
            })
        });
    }
    createChangeFeesFinished() {
        let address = this.sameAsBillingAddress == 1
            ? this.billingAddress
            : this.shippingOrderForm.valid
                ? this.shippingOrderForm.value
                : (this.shippingAddress ? this.shippingAddress : null);
        this.changeFeesFinished.emit({
            'shipCountry': this.shipCountry,
            'shippingAddress': address,
            'shipFee': this.shipFee
        }
        );
    }
    createShippingOrderFormFinished() {
        let shippingAddress = (this.sameAsBillingAddress == 1) ? this.billingAddress : this.shippingOrderForm.value;
        this.shippingOrderFormFinished.emit({
            'comments': this.shippingOrderForm.value.comments,
            'sameAsBillingAddress': this.sameAsBillingAddress,
            'shippingAddress': shippingAddress,
            'shipFee': this.shipFee,
            'shipProvider': this.shipProvider
        }
        );
    }
    showMsgShipToCa() {
        this.disabled = false;
        let products = this.orderLogicService.getProducts();
        if (products && products.length == 1 && this.shipCountry == 'CA'
            && (!products[0].shipToCa || products[0].shipToCa == 'N')) {
            this.disabled = true;
            let msg = 'We cannot ship this item out Canada';
            this.modal.alert()
                .size('sm')
                .isBlocking(true)
                .showClose(true)
                .body(msg)
                .open().then(x=>{
                    this.document.body.classList.remove('modal-open');
                });;
        }
    }
    validateAllFormFields(formGroup: FormGroup) {
        if (formGroup && formGroup.controls) {
            Object.keys(formGroup.controls).forEach(field => {
                const control = formGroup.get(field);
                if (control) {
                    if (control instanceof FormControl) {
                        control.markAsTouched({ onlySelf: true });
                    } else if (control instanceof FormGroup) {
                        this.validateAllFormFields(control);
                    }
                }
            });
        }
    }
}
