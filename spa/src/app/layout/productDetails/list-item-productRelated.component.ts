﻿import {Component, Input, NgZone, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {Product} from 'src/app/shared/services/api/models/product';
import {Router} from '@angular/router';
@Component({
    selector: 'list-item-product-related',
    templateUrl: './list-item-productRelated.component.html',
    styleUrls: ['./../products/list-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListItemProductRelatedComponent {
    @Input()
    item: Product;
    productRelated: number;
    constructor(private router: Router, private zone: NgZone, private changeDetectorRef: ChangeDetectorRef) {
        this.router = router;
        this.productRelated = this.router.url.indexOf('productDetails') > -1 ? 1 : 0;     
    }
    selectDetails(item) {
        let queryParams = { productSKU: item.productSKU, parent: item.color ? true : false };
        this.router.navigate(['productDetails'], { queryParams: queryParams });
        return false;
    }
}
