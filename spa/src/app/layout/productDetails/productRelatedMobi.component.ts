﻿import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { Product } from 'src/app/shared/services/api/models/product';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { ProductService } from 'src/app/shared/services/api/services/product.service';
@Component({
    selector: 'productRelatedMobi',
    templateUrl: './productRelatedMobi.component.html',
    styleUrls: ['./productRelatedMobi.component.scss']
})
export class ProductRelatedMobiComponent {
    productSKU: string;
    productRelateds: Product[] = [];
    public bufferSize: number = 4;
    public number: number = 0;
    constructor(private router: Router,
        public appState: AppState,
        private _routeParams: ActivatedRoute,
        private productService: ProductService) {
        this._routeParams.queryParams.subscribe(params => {
            this.productSKU = params['productSKU'];
            let operatetion = this.productService.getProductRelated(this.productSKU);
            operatetion.subscribe(result => {
                if (result) {
                    this.productRelateds = result;
                }
            }
            );
        });
    }
    chunks(size) {
        this.number = Math.ceil(this.productRelateds.length / size);
        return this.createRange(this.number);
    };
    createRange(number) {
        var items: number[] = [];
        for (var i = 1; i <= number; i++) {
            items.push(i);
        }
        return items;
    }
    prev() {
        if (this.bufferSize > 0 && this.bufferSize < this.number - 1) {
            this.bufferSize--;
        } else {
            this.bufferSize = 0;
        }
    }
    next() {
        if (this.bufferSize < this.number -3) {
            this.bufferSize++;
        } else {
            this.bufferSize = 0;
        }
    }
}