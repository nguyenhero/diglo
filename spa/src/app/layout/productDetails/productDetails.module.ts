import {NgModule, Component, Pipe, PipeTransform} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ModalModule} from 'angular2-modal';
import {BootstrapModalModule} from 'angular2-modal/plugins/bootstrap';
import {ProductDetailsComponent} from './products-details.component';
import {HammerGestureConfig, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {ListItemProductRelatedComponent} from './list-item-productRelated.component';
import {ProductRelatedMobiComponent} from './productRelatedMobi.component';
import {ProductRelatedComponent} from './productRelated.component';
import {ProductsReviewListComponent} from './products-review-list.component';
import {ProductsReviewComponent} from './products-review.component';
import {ProductAdditionInfoComponent} from './product-addition-info.component';
import {ProductsImageMobi} from './products-image-mobi.component';
import {AddOrderModule} from './addOrder.module';
export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    'swipe': { velocity: 0.4, threshold: 20 } // override default settings
  }
}
@Pipe({ name: 'safeHtml' })
export class SafeHtmlPipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer) { }
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}
@Pipe({ name: 'rateNumber' })
export class RateNumber implements PipeTransform {
    transform(value, args: string[]): any {
        let res = [];
        let decimal: number = 0;
        value = value + '';
        if (value) {
            var nindex = value.indexOf("."),
                result = "0." + (nindex > -1 ? value.substring(nindex + 1) : "0");
            if (value) {
                decimal = parseFloat(result);
            }
            for (let i = 0; i < value; i++) {
                res.push(i);
            }
            if (decimal <= 0.5 && decimal >= 0.25) {
                res[res.length - 1] = 0.5;
            } else if (decimal > 0 && decimal < 0.25) {
                res.pop();
            }
            let length = res.length;
            for (let i = length + 1; i <= 5; i++) {
                res.push(-1);
            }
        }
        return res;
    }
}
@NgModule({
  declarations: [
    ProductDetailsComponent,
    SafeHtmlPipe,
    RateNumber,
    ListItemProductRelatedComponent,
    ProductRelatedComponent,
    ProductsReviewListComponent,
    ProductsReviewComponent,
    ProductAdditionInfoComponent,
    ProductsImageMobi,
    ProductRelatedMobiComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    AddOrderModule,
    RouterModule.forChild([
      { path: '', component: ProductDetailsComponent, pathMatch: 'full' }
    ])
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    }
  ],
  entryComponents: [
    ProductsImageMobi
  ]
})
export class ProductDetailsModule {}