﻿import { Component, ViewContainerRef, ViewEncapsulation, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { LoginModalContext, LoginModal } from './../account/login/login.component';
import { FreeCatalogRequestModal, FreeCatalogRequestModalContext } from './../free-catalog/free-catalog-request/free-catalog-request.component';
import { Observable } from 'rxjs';
import { DOCUMENT } from '@angular/platform-browser';  
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { NewsletterService } from 'src/app/shared/services/api/services/newsletter.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
import { Newsletter } from 'src/app/shared/services/api/models/newsletter';
@Component({
    selector: 'newsletter',
    templateUrl: './newsletter.component.html',
    styleUrls: ['./newsletter.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class NewsletterMenuComponent implements OnInit {
    newsletterClass: string = 'col-xs-12 col-sm-12';
    freeProductCatalogsClass: string = 'visible-md-block visible-lg-block';
    newsletterForm: any;
    currentUser: any;
    isLogin: boolean = false;
    constructor(public modal: Modal, private formBuilder: FormBuilder, public appState: AppState,  @Inject(DOCUMENT) private document,
        private authenticationService: AuthenticationService, private newsletterService: NewsletterService) {
        appState.event.subscribe((data) => {
            if (data && data != '/home') {
                this.newsletterClass = 'visible-md-block visible-lg-block';
                this.freeProductCatalogsClass = 'col-xs-12 col-sm-12';
            } else {
                this.freeProductCatalogsClass = 'visible-md-block visible-lg-block';
                this.newsletterClass = 'col-xs-12 col-sm-12';
            }
        });
    }
    ngOnInit() {
        this.newsletterForm = this.formBuilder.group({
            'email': ['', [Validators.required, ValidationService.emailValidator]]
        });
    }
    show_request_login() {
        let context = new LoginModalContext('free-catalog-request');
        context.dialogClass = 'login-modal';
        context.isBlocking = true; // now its blocking.        
        let dialog = this.modal.open(LoginModal, { context: context });
        dialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                if (result) {
                    //localStorage.setItem('currentUser', JSON.stringify(result));
                    this.isLogin = true;
                    this.appState.publish('login');
                    this.validate_catalog_dialog(result);
                }
            });
        });
    }
    show_catalog_request() {
        let currentUser = this.authenticationService.getCurrentUser();
        if (currentUser) {
            this.validate_catalog_dialog(currentUser);
        } else {
            this.show_request_login();
        }
    }
    validate_catalog_dialog(currentUser) {
        if (currentUser.custCatStatus == 1) {
            this.show_message_requested_catalog();
        } else {
            let context = new FreeCatalogRequestModalContext(currentUser);
            context.isBlocking = true; // now its blocking.
            this.modal.open(FreeCatalogRequestModal, { context: context });
        }
    }
    show_message_requested_catalog() {
        this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .body(`<span class='successfully-message'> You already requested a catalog. For multiple catalog requests, email <a href="mailto:"><strong>info@harriscomm.com</strong></a>.<br /><br /> Thank you.</span>`)
            .open().then(x=>{
                this.document.body.classList.remove('modal-open');  // scroll
            });
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
    subscriber(email, flagg) {
        if (this.newsletterForm.dirty && this.newsletterForm.valid) {
            let subscriberOperation: Observable<Newsletter>;
            subscriberOperation = this.newsletterService.subscribers(email, flagg);
            // Subscribe to observable
            subscriberOperation.subscribe(
                result => {
                    let msg = '';
                    if (result && result.EmailID && result.Status == 'Active') {
                        msg = `<div class='successfully-message'> 
                           Email Subscription Request Submitted.<br /><br />
                           Thank you for your request!
                            <br /><br />
                           We’ve sent a confirmation email to the address you’ve submitted.
                           We will activate your subscription once you click the confirmation link inside the email. 
                           <br /><br />
                           If you haven’t received it, make sure you check your junk mail folder, it might be hiding there.
                           <br /><br />
                           Ready our <a href ="/privacy"> Privacy Policy</a> 
                           </div>`; 
                    } else if (result && result.EmailID && result.Status == 'Removed') {
                        msg = 'You have successfully unsubscribed from all mailing lists';
                    } else {
                        msg = result.ErrorCode + ':' + result.Message;
                    }
                    this.modal.alert()
                        .size(result && result.EmailID && result.Status == 'Active' ? 'lg' : 'sm')
                        .isBlocking(true)
                        .showClose(true)
                        .body(msg)
                        .open().then(x=>{
                            this.document.body.classList.remove('modal-open');  // scroll
                        });
                });
        } else {
            this.validateAllFormFields(this.newsletterForm);
        }
    }
}
