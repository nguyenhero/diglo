﻿import { DOCUMENT } from '@angular/platform-browser';
import { Component, Inject, NgZone, ChangeDetectorRef, OnDestroy, HostListener } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/shared/services/api/models/product';
import { FilterGroup } from 'src/app/shared/services/api/models/filterGroup';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { ProductService } from 'src/app/shared/services/api/services/product.service';
import { FilterGroupService } from 'src/app/shared/services/api/services/filterGroup.service';
import { Subscription } from 'rxjs';
import * as rxjsNumeric from "rxjs/util/isNumeric"

@Component({
    selector: 'products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnDestroy {
    private subscription: Subscription;
    public currentPage: number = 1;
    public lastPage: number = 0;
    public totalItems: number = 0;
    public maxSize: number = 5;
    protected buffer: Product[];
    public bufferSize: number = 12;
    loading: boolean = false;
    lastScrollTop: number = 0;
    start: number = 0;
    blockHeight: number = 0;
    direction: string = '';
    sortBy: number = 4;
    sortByDropdown: string = 'hidden';
    sortByDropdownMenu: string = 'hidden';
    filter: string = 'hidden';
    departmentId: string;
    classId: string;
    subClassId: string;
    filterId: string = '';
    filterVal: string = '';
    filter12IDs: number[] = [12121, 12122];
    filter14IDs: number[] = [14141, 14142, 14143, 14144];
    q: string = '';
    newArrivalInd = '';
    fromPrice: number = 0;
    toPrice: number = 0;

    filterGroups: FilterGroup[];
    departments: any = [];
    tempGroups: any = [];
    filterGroupId: number = 0;
    filterDetails: any = [];
    tickFilterGroup: string = 'hidden';
    page: number = 1;
    limit: number = 5;
    title: string = "All Products";
    category: boolean = false;
    showPrice: boolean = false;
    showFilterSignaler: boolean = true;
    showProducts: boolean = true;
    filterSignalerGroup: any;
    signalerType = 'Signaler Type';
    signalerSystem = 'Signaler System';
    filterSignalerTitle: string = '';
    displayFilter: String = '';
    showMenuFilter: boolean = false;

    constructor(private zone: NgZone,
        private cdr: ChangeDetectorRef,
        private router: Router,
        private routeParams: ActivatedRoute,
        public appState: AppState,
        @Inject(DOCUMENT) private document,
        private productService: ProductService,
        private filterGroupService: FilterGroupService) {
        appState.publish(this.router.url);
        this.buffer = [];
        this.start = 0;
        this.sortBy = 0;
        this.subscription = this.routeParams.queryParams.subscribe(
            (queryParam: any) => {
                this.buffer = [];
                this.start = 0;
                this.sortBy = 0;
                this.filterDetails = [];
                this.filterSignalerGroup = [];
                this.showPrice = false;
                this.departmentId = queryParam['departmentId'];
                this.classId = queryParam['classId'];
                this.subClassId = queryParam['subClassId'];
                this.filterId = '';
                this.filterVal = '';
                this.q = queryParam['q'];
                this.title = queryParam['title'] ? queryParam['title'] : 'All Products';
                this.filterSignalerTitle = this.title;
                this.newArrivalInd = queryParam['newArrivalInd'] ? 'new'
                    : (queryParam['clearance'] ? 'clearance' : '');
                if (!queryParam['category']) {
                    this.departments = [];
                }
                if (!this.classId || (this.classId != '12' && this.classId != '14')) {
                    this.loadData();
                    this.getCategory();
                }
                this.getFilterGroup();
                this.showProducts = this.classId && (this.classId == '12' || this.classId == '14') ? false : true;
                this.showFilterSignaler = !this.showProducts;
            }
        );
    }

    subclassFillter(departmentID, classItem, subclass) {
        this.departmentId = departmentID;
        this.classId = classItem.classID;
        this.subClassId = subclass.subclassID;
        this.link(subclass);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    public setPage(pageNo: number): void {
        this.currentPage = pageNo;
    }

    public pageChanged(event: any): void {
        if (event.direction) {
            this.direction = event.direction;
        }
        let request = this.createRequest();
        this.get(request);

    }

    public get(request) {
        this.loading = true;
        let operatetion = this.productService.get(request, this.filterDetails);
        operatetion.subscribe(result => {
            this.zone.run(() => {
                if (result) {
                    if (this.classId && (this.classId == '12' || this.classId == '14')) {
                        this.showProducts = true;
                        this.buffer = result;
                        this.start = 0;
                        window.scroll(0, 500);
                    } else {
                        this.buffer = this.buffer.concat(result);
                        this.start = this.buffer.length;
                    }
                    this.loading = false;
                    this.cdr.detectChanges();
                }
            });
        });
        let getCountOpt = this.productService.getCount(request, this.filterDetails);
        getCountOpt.subscribe(result => {
            this.zone.run(() => {
                this.totalItems = result;
                this.loading = false;
            });
        });
    }

    public getFilterGroup() {
        let operatetion = this.filterGroupService.get(this.departmentId, this.classId, this.subClassId, this.newArrivalInd, this.filterId, this.q);
        operatetion.subscribe(result => {
            this.zone.run(() => {
                if (result) {
                    this.filterGroups = result;
                    if (this.filterGroups && this.classId && (this.classId == '12' || this.classId == '14') && this.filterId == '') {
                        for (let i = 0; i < this.filterGroups.length; i++) {
                            if (this.filterGroups[i].filterGroupName == this.signalerType ||
                                this.filterGroups[i].filterGroupName == this.signalerSystem) {
                                this.filterSignalerGroup = this.filterGroups[i];
                            }
                        }
                    }
                    this.cdr.detectChanges();
                }
            });
        });

    }

    public getCategory() {
        if (!this.departmentId && !this.classId && !this.subClassId) {
            let operatetion = this.productService.getCategory();
            operatetion.subscribe(result => {
                if (result) {
                    this.departments = result;
                }
            });
        }

    }

    loadMore(value) {
        this.limit = this.limit + 5;
    }

    chooseFilter(item) {
        this.filterGroups.forEach(x => {
            x.active = false;
        });
        let filterGroupId = item.filterGroupId;
        this.filterGroupId = this.filterGroupId == filterGroupId ? 0 : filterGroupId;
        item.active = !item.active;
    }

    view(filterDetail) {
        this.title = filterDetail.filterDetailName;
        this.filterDetails = [];
        this.filterDetails.push(filterDetail);
        this.loadData();
        this.filterId = filterDetail.filterDetailId.toString();
        this.filterVal = filterDetail.filterDetailValue;
        this.getFilterGroup();
    }

    filterTick(filterDetail) {
        this.fromPrice = 0;
        this.toPrice = 0;
        let add = false;
        if (this.filterDetails != null && this.filterDetails.length > 0) {
            for (let i = 0; i < this.filterDetails.length; i++) {
                let item = this.filterDetails[i];
                if (item.filterDetailId == filterDetail.filterDetailId) {
                    this.filterDetails.splice(i, 1);
                    add = true;
                    break;
                }
            }
        }
        if (!add) {
            if (this.filter12IDs.indexOf(filterDetail.filterDetailId) > -1) {
                filterDetail.searchcolumn = ' ClassID = 12 AND ' + filterDetail.searchcolumn;
            } else if (this.filter14IDs.indexOf(filterDetail.filterDetailId) > -1) {
                filterDetail.searchcolumn = ' ClassID = 14 AND ' + filterDetail.searchcolumn;
            }
            this.filterDetails.push(filterDetail);
        }
        this.buffer = [];
        this.start = 0;
        this.loadData();
    }

    filterMobiTick(filterDetail) {
        this.fromPrice = 0;
        this.toPrice = 0;

        let add = false;
        if (this.filterDetails != null && this.filterDetails.length > 0) {
            for (let i = 0; i < this.filterDetails.length; i++) {
                let item = this.filterDetails[i];
                if (item.filterDetailId == filterDetail.filterDetailId) {
                    this.filterDetails.splice(i, 1);
                    add = true;
                    break;
                }
            }
        }
        if (!add) {
            if (this.filter12IDs.indexOf(filterDetail.filterDetailId) > -1) {
                filterDetail.searchcolumn = ' ClassID = 12 AND ' + filterDetail.searchcolumn;
            } else if (this.filter14IDs.indexOf(filterDetail.filterDetailId) > -1) {
                filterDetail.searchcolumn = ' ClassID = 14 AND ' + filterDetail.searchcolumn;
            }
            this.filterDetails.push(filterDetail);
        }

    }

    clearFilterByGroupId(filterGroup, moblie = false) {
        this.fromPrice = 0;
        this.toPrice = 0;
        this.showPrice = false;
        filterGroup.filterDetails.forEach((item, index) => {
            item.show = false;
        });
        if (this.filterDetails != null && this.filterDetails.length > 0) {
            var i = this.filterDetails.length;
            while (i--) {
                if (this.filterDetails[i].filterGroupId == filterGroup.filterGroupId)
                    this.filterDetails.splice(i, 1);
            }
        }
        if (!moblie) {
            this.buffer = [];
            this.start = 0;
            this.loadData();
        }
    }

    goPriceFilter(filterGroup, fromPrice, toPrice) {
        this.displayFilter = 'none';

        filterGroup.filterDetails.forEach((item, index) => {
            item.show = false;
        });
        this.filterDetails = [];
        if (!rxjsNumeric.isNumeric(fromPrice) || !rxjsNumeric.isNumeric(toPrice)) {
            alert('Please enter number!');
            return;
        }
        let filterDetail = {
            filterGroupId: filterGroup.filterGroupId,
            filterDetailId: 0,
            tableName: 'Products',
            searchcolumn: 'webprice',
            operation: 'between',
            filterDetailValue: fromPrice + ' and ' + toPrice
        };
        let add = false;
        if (this.filterDetails != null && this.filterDetails.length > 0) {
            for (let i = 0; i < this.filterDetails.length; i++) {
                let item = this.filterDetails[i];
                if (item.filterDetailId == filterDetail.filterDetailId) {
                    item.tableName = filterDetail.tableName;
                    item.searchcolumn = filterDetail.searchcolumn;
                    item.operation = filterDetail.operation;
                    item.filterDetailValue = filterDetail.filterDetailValue;
                    add = true;
                    break;
                }
            }
        }
        if (!add) {
            if (this.classId == '12') {
                filterDetail.searchcolumn = ' Filter LIKE \'%' + this.filterVal + '%\' AND ' + filterDetail.searchcolumn;
            } else if (this.classId == '14') {
                filterDetail.searchcolumn = ' Filter LIKE \'%' + this.filterVal + '%\' AND  ' + filterDetail.searchcolumn;
            }
            this.filterDetails.push(filterDetail);
        }

        this.buffer = [];
        this.start = 0;
        this.loadData();
        this.filterDetails = [];
        
        if (this.document.getElementById('mainMenuFilter') &&
        this.document.getElementById('mainMenuFilter').className.indexOf('main-menu-scroll') < 0) {
        this.document.getElementById('mainMenuFilter').classList.add("main-menu-scroll");            
        this.showMenuFilter = true;
    };
    }

    viewProducts() {
        this.buffer = [];
        this.start = 0;
        this.displayFilter = 'none';
        this.loadData();
        if (this.document.getElementById('mainMenuFilter') &&
            this.document.getElementById('mainMenuFilter').className.indexOf('main-menu-scroll') < 0) {
            this.document.getElementById('mainMenuFilter').classList.add("main-menu-scroll");            
            this.showMenuFilter = true;
        };
    }

    loadData() {
        let request = this.createRequest();
        this.get(request);
    }

    changeSortBy(value) {
        this.sortBy = value;
        this.start = 0;
        this.buffer = [];
        this.totalItems = 0;
        let request = this.createRequest();
        this.sortByDropdown = 'hidden';
        this.sortByDropdownMenu = 'hidden';
        this.get(request);
    }

    createRequest() {
        let loadIteamTotal = this.bufferSize * this.maxSize;
        let request =
            {
                'sortBy': this.sortBy,
                'start': this.start,
                'end': loadIteamTotal,
                'departmentId': this.departmentId,
                'classId': this.classId,
                'subClassId': this.subClassId,
                'q': this.q,
                'newArrivalInd': this.newArrivalInd
            };

        return request;
    }


    openFilter() {
        this.displayFilter = this.displayFilter == 'block' ? 'none' : 'block';
        if (this.filterGroups && this.filterGroups.length > 0) {
            this.filterGroups[0].active = false;
        }

        if (this.document.getElementById('mainMenuFilter')) {
            this.document.getElementById('mainMenuFilter').classList.remove("main-menu-scroll");
            this.document.getElementById('mainMenu').classList.remove("main-menu-scroll");
            this.showMenuFilter = false;
        }
    }

    showSortByDropdown() {
        if (this.sortByDropdown == 'hidden') {
            this.sortByDropdown = 'visible';
            this.filter = 'hidden';
        } else {
            this.sortByDropdown = 'hidden';
        }
    }

    showSortByDropdownMenu() {
        if (this.sortByDropdownMenu == 'hidden') {
            this.sortByDropdownMenu = 'visible';
            this.filter = 'hidden';
        } else {
            this.sortByDropdownMenu = 'hidden';
        }
    }

    showFilter() {
        if (this.filter == 'hidden') {
            this.filter = 'visible';
            this.sortByDropdown = 'hidden';
        } else {
            this.filter = 'hidden';
        }
    }

    clear() {
        if (this.filterGroups && this.filterGroups.length > 0) {
            this.filterGroups.forEach((item, index) => {
                this.clearFilterByGroupId(item, true);
            });
        }
    }

    link(subclass) {
        if (subclass.subclassDesc == 'Clearance') {
            this.clearanceProductLink(subclass);
        }
        else {
            this.productLink(subclass);
        }
    }

    newProductLink(departmentID, subclass) {
        let queryParams = {
            departmentId: departmentID,
            title: subclass.subclassDesc,
            category: true,
            newArrivalInd: 'Y'
        };
        this.router.navigate(['allproducts'], { queryParams: queryParams });
    }

    clearanceProductLink(subclass) {
        let queryParams = {
            departmentId: this.departmentId,
            title: subclass.subclassDesc,
            category: true,
            clearance: 'Y'
        };
        this.router.navigate(['allproducts'], { queryParams: queryParams });
    }

    productLink(subclass) {
        let queryParams = {};
        if (subclass.subclassDesc == 'All Phone' ||
            subclass.subclassDesc == 'All Education' ||
            subclass.subclassDesc == 'All Alarms' ||
            subclass.subclassDesc == 'All Devices') {
            queryParams = {
                departmentId: this.departmentId,
                title: subclass.subclassDesc,
                category: true
            };

        } else {
            queryParams = {
                departmentId: this.departmentId,
                classId: subclass.classID,
                subClassId: subclass.subclassID,
                title: subclass.subclassDesc,
                category: true
            };

        }
        this.router.navigate(['allproducts'], { queryParams: queryParams });
    }

    getParameterByName(name) {
        let url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    @HostListener("window:scroll", ['$event'])
    onWindowScroll($event: Event) {
        if (window.pageYOffset > 0) {
            if (this.document.getElementById('mainMenuFilter') &&
                (!this.document.getElementById('filter') || this.document.getElementById('filter').style.display != "block" )&&
                this.document.getElementById('mainMenuFilter').className.indexOf('main-menu-scroll') < 0) {
                this.document.getElementById('mainMenuFilter').classList.add("main-menu-scroll");
                this.showMenuFilter = true;
            };

        } else {
            if ((!this.document.getElementById('filter') || this.document.getElementById('filter').style.display == "block") || (this.document.getElementById('mainMenuFilter') &&
                this.document.getElementById('mainMenuFilter').className.indexOf('main-menu-scroll') > 0)) {
                this.document.getElementById('mainMenuFilter').classList.remove("main-menu-scroll");
                this.showMenuFilter = false;
            };
        }
    }
}

