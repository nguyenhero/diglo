﻿import {Component, Input, NgZone, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import {Router} from '@angular/router';
import { Product } from 'src/app/shared/services/api/models/product';
@Component({
    selector: 'list-item',
    templateUrl: './list-item.component.html',
    styleUrls: ['./list-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListItemComponent {
    @Input()
    item: Product;
    productRelated: number;
    constructor(private router: Router, private zone: NgZone, private changeDetectorRef: ChangeDetectorRef) {
        this.router = router;
        this.productRelated = this.router.url.indexOf('product-details') > -1 ? 1 : 0;     
    }
    selectDetails(item) {
        let queryParams = { productSKU: item.productSKU, parent: item.color ? true : false };
        this.router.navigate(['productDetails'], { queryParams: queryParams });
        return false;
    }
}
