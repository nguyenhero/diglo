﻿// import { Component, ViewEncapsulation} from '@angular/core';
// import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
// import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
// import { Product }           from 'src/app/shared/services/api/models/product';
// import { ProductDetailsService } from 'src/app/shared/services/api/services/productDetails.service';

// export class ProductsImageMobiContext extends BSModalContext {
//     constructor(public product: Product, public products: Product[]) {
//         super();
//     }
// }

// @Component({
//     selector: 'products-image-mobi',
//     template: require('./products-image-mobi.component.html'),
//     styles: [require('./products-image-mobi.component.scss')],
//     providers: [Modal],
//     encapsulation: ViewEncapsulation.None
// })

// export class ProductsImageMobi implements CloseGuard, ModalComponent<ProductsImageMobiContext> {
//     userForm: any;
//     context: ProductsImageMobiContext;
//     public errorMsg = '';
//     isExistEmail = true;
//     protected product: Product;
//     protected products: Product[];
//     SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
//     avatars = [];

//     constructor(public dialog: DialogRef<ProductsImageMobiContext>, public modal: Modal) {
//         this.context = dialog.context;
//         this.product = this.context.product;
//         this.products = this.context.products;
//         this.createAvatars();
//         dialog.setCloseGuard(this);
//     }
//     close() {
//         this.dialog.close();
//     }

//     beforeDismiss(): boolean {
//         return true;
//     }

//     beforeClose(): boolean {
//         return false;
//     }

//     swipe(currentIndex: number, action = this.SWIPE_ACTION.RIGHT) {
    
//         if (currentIndex > this.avatars.length || currentIndex < 0) return;

//         let nextIndex = 0;

//         // next
//         if (action === this.SWIPE_ACTION.LEFT) {
//             const isLast = currentIndex === this.avatars.length - 1;
//             nextIndex = isLast ? 0 : currentIndex + 1;
//         }

//         // previous
//         if (action === this.SWIPE_ACTION.RIGHT) {
//             const isFirst = currentIndex === 0;
//             nextIndex = isFirst ? this.avatars.length - 1 : currentIndex - 1;
//         }

//         // toggle avatar visibility
//         this.avatars.forEach((x, i) => x.visible = (i === nextIndex));
//     }
//     changeProductMobile(item) {
//         this.product = item;
//         this.avatars = [];
//         this.createAvatars();
//     }
//     createAvatars(){
//         if (this.product.image1) {
//             let image1 = {
//                 name: 'image1',
//                 image: this.product.image1,
//                 visible: true
//             }
//             this.avatars.push(image1);
//         }
//         if (this.product.image2) {
//             let image2 = {
//                 name: 'image2',
//                 image: this.product.image2,
//                 visible: false
//             }
//             this.avatars.push(image2);
//         }
//         if (this.product.image3) {
//             let image3 = {
//                 name: 'image3',
//                 image: this.product.image3,
//                 visible: false
//             }
//             this.avatars.push(image3);
//         }
//         if (this.product.image4) {
//             let image4 = {
//                 name: 'image4',
//                 image: this.product.image4,
//                 visible: false
//             }
//             this.avatars.push(image4);
//         }

//     }

// }