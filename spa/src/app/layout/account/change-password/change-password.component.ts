﻿import {Component, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {DialogRef, ModalComponent, CloseGuard} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import { Observable } from 'rxjs';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { UserService } from 'src/app/shared/services/api/services/user.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
import { PasswordValidation } from '../../directives/password-validation';
export class ChangePasswordModalContext extends BSModalContext {
    constructor(public customerNo: string = '' ) {
        super();
    }
}
@Component({
    selector: 'change-password-modal',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class ChangePasswordModal implements CloseGuard, ModalComponent<ChangePasswordModalContext>  {
    userForm: any;
    context: ChangePasswordModalContext;
    customerNo: string;
    requestAt: Date;

    constructor(private router: Router, public dialog: DialogRef<ChangePasswordModalContext>, private formBuilder: FormBuilder, public modal: Modal,
        private userService: UserService) {
        this.context = dialog.context;
        this.customerNo = this.context.customerNo;
        dialog.setCloseGuard(this);

        this.userForm = this.formBuilder.group({
            'password': ['', [Validators.required, Validators.minLength(6), ValidationService.passwordValidator]],
            'confirmPassword': ['', Validators.required]
        }, {
                validator: PasswordValidation.MatchPassword // your validation method
            });
    }

    submit() {
        if (this.userForm.dirty && this.userForm.valid) {
            this.userForm.value.customerNo = this.customerNo;
            let changePasswordOperation: Observable<number>;
            changePasswordOperation = this.userService.resetPassword(this.userForm.value);

            // Subscribe to observable
            changePasswordOperation.subscribe(
                register => {
                    let msg = '';
                    if (register == 0) {
                        msg = `<span class='successfully- message'>You have successfully reset password and logged in. Thank you.</span>`;
                    } else if (register == 100) {
                        msg = `<span class='successfully- message'>Customer Does Not Exist.</span>`;
                    }
                    else msg = 'You have not successfully reset password';
                    this.modal.alert()
                        .size('sm')
                        .showClose(true)
                        .isBlocking(true)
                        .body(msg)
                        .open().then((resultPromise) => {
                            this.close();
                        });
                },
                err => { console.log(err); });
        } else {
            this.validateAllFormFields(this.userForm);
        }
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
    close() {
        this.dialog.close();
    }

    beforeDismiss(): boolean {
        return true;
    }

    beforeClose(): boolean {
        return false;
    }
}

