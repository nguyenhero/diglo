﻿import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {DOCUMENT} from '@angular/platform-browser';    //fixed popup scroll
import {DialogRef, ModalComponent, CloseGuard} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap'; 
import {PasswordValidation} from './../../directives/password-validation'; 
import { UserService } from 'src/app/shared/services/api/services/user.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
export class RegisterModalContext extends BSModalContext {
   constructor() {
       super();
   }
}
@Component({
   selector: 'register-modal',
   templateUrl: './register.component.html',
   styleUrls: ['./register.component.scss']
})
export class RegisterModal implements CloseGuard, ModalComponent<RegisterModalContext> {
   userForm: any;
   context: RegisterModalContext;
   isExistEmail = false;
   sendEmail: any;
   public wrongAnswer: boolean;
   constructor(public modal: Modal, public dialog: DialogRef<RegisterModalContext>, private formBuilder: FormBuilder, private userService: UserService,  @Inject(DOCUMENT) private document, private authenticationService: AuthenticationService, public appState: AppState) {
       this.context = dialog.context;
       dialog.setCloseGuard(this);
       this.userForm = this.formBuilder.group({
           'firstName': ['', Validators.required],
           'lastName': ['', Validators.required],
           'email': ['', [Validators.required, ValidationService.emailValidator]],
           'password': ['', [Validators.required, Validators.minLength(6), ValidationService.passwordValidator]],
           'confirmPassword': ['', Validators.required]
       }, {
               validator: PasswordValidation.MatchPassword // your validation method
           });
       this.userForm.valueChanges.subscribe((value) => {
           this.isExistEmail = false;
       });
   }
   saveUser() {
       if (this.userForm.dirty && this.userForm.valid) {
           /*alert(`Name: ${this.userForm.value.name} Email: ${this.userForm.value.email}`);*/
           // Variable to hold a reference of addComment/updateComment
           let registerOperation: Observable<number>;
           registerOperation = this.userService.addAccount(this.userForm.value);
           // Subscribe to observable
           registerOperation.subscribe(
               register => {
                   if (register == 0) {
                       this.modal.alert()
                           .size('sm')
                           .showClose(true)
                           .isBlocking(true)
                           .body(`<span class='successfully-message'>You have successfully registered and logged in. Thank you.</span>`)
                           .open().then((resultPromise) => {
                               resultPromise.result.then((result) => {
                                   this.authenticationService.login(this.userForm.value)
                                       .subscribe(
                                       data => {
                                           if (data) {
                                               this.dialog.close(data);
                                               this.document.body.classList.remove('modal-open'); 
                                           }
                                       },
                                       error => {
                                           //this.alertService.error(error);
                                           //this.loading = false;
                                       });
                               },
                                   () => { });
                           });
                   }
                   else if (register == 100) {
                       this.isExistEmail = true;
                   }
               },
               err => { console.log(err); });
       } else {
           this.validateAllFormFields(this.userForm);
       }
   }
   validateAllFormFields(formGroup: FormGroup) {
       Object.keys(formGroup.controls).forEach(field => {
           const control = formGroup.get(field);
           if (control instanceof FormControl) {             //{4}
               control.markAsTouched({ onlySelf: true });
           } else if (control instanceof FormGroup) {        //{5}
               this.validateAllFormFields(control);            //{6}
           }
       });
   }
   updateSelectedSendEmail($event){}
   openLogin() {
       this.dialog.close();
       this.appState.publish('login');
   }
   close() {
       this.dialog.close();
       this.document.body.classList.remove('modal-open'); 
   }
   beforeDismiss(): boolean {
       return true;
   }
   beforeClose(): boolean {
       return false;
   }
}