﻿import {Component, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {DialogRef, ModalComponent, CloseGuard} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {ValidationService} from 'src/app/shared/services/api/services/validation.service';
import {UserService} from 'src/app/shared/services/api/services/user.service';
import {User} from 'src/app/shared/services/api/models/user';
import {AppState} from 'src/app/shared/services/api/services/appState.service';
import {RegisterModalContext, RegisterModal} from './../../account/register/register.component';
export class ForgotPasswordModalContext extends BSModalContext {
    constructor(public errorMsg: string = '') {
        super();
    }
}
@Component({
    selector: 'forgot-password-modal',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class ForgotPasswordModal implements CloseGuard, ModalComponent<ForgotPasswordModalContext> {
    userForm: any;
    context: ForgotPasswordModalContext;
    public errorMsg = '';
    isExistEmail = true;
    constructor(public dialog: DialogRef<ForgotPasswordModalContext>, private formBuilder: FormBuilder, public modal: Modal,
        private userService: UserService, private router: Router, public appState: AppState) {
        this.context = dialog.context;
        this.errorMsg = this.context.errorMsg;
        dialog.setCloseGuard(this);
        this.userForm = this.formBuilder.group({
            'email': ['', [Validators.required, ValidationService.emailValidator]]
        });
        this.userForm.valueChanges.subscribe((value) => {
            this.isExistEmail = true;
        });
    }
    submit() {
        if (this.userForm.dirty && this.userForm.valid) {
            this.userService.getByEmail(this.userForm.value.email)
                .subscribe(
                data => {
                    if (data == 0) {
                        this.isExistEmail = false;
                    } else {
                        this.dialog.close();
                        var messeage = data == 1 ?
                            '<span>The change password mail has been sent successfully. Please check your email.This link is only valid for <strong>4 hours.</strong></span>' :
                            '<span>The change password mail has not been sent successfully</span>';
                        this.modal.alert()
                            .size('sm')
                            .showClose(true)
                            .isBlocking(true)
                            .body(messeage)
                            .open();
                    }
                },
                error => {
                    //this.alertService.error(error);
                    //this.loading = false;
                });
        } else {
            this.validateAllFormFields(this.userForm);
        }
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
    keyDownFunction(event) {        
        if (event.keyCode == 13) {
            this.userForm.controls["email"]._touched = true;
        }
    }
    openRegister() {
        this.dialog.close();
        let registerDialog = this.modal.open(RegisterModal);
        registerDialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                if (result) {
                    this.appState.publish('login');
                }
            });
        });
    }
    close() {
        this.dialog.close();
    }
    beforeDismiss(): boolean {
        return true;
    }
    beforeClose(): boolean {
        return false;
    }
}