import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../shared/services/storage/storage.service';
import { ApiInterceptor } from '../shared/services/api-interceptor';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers:[]
})
export class LoginComponent implements OnInit {
    constructor(private router: Router,
        private storageService: StorageService) {}

    ngOnInit() {}

    onLogin() {
        this.storageService.setItem('isLoggedin', 'true');
        this.router.navigate(['/dashboard']);
    }
}
