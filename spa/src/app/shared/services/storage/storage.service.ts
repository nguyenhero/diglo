import { Injectable } from '@angular/core';
import {  Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  public notify = new Subject<any>();
  public storage = {};
  /**
  * Observable string streams
  */
  notifyObservable$ = this.notify.asObservable();

  constructor() { }
  /**
   * clear
   */
  public clear() {
    this.storage = {};
  }
  /**
   * setItem
   */
  public setItem(key: string, value: string) {
    this.storage[key] = value;
  }
  /**
   * getItem
   */
  public getItem(key: string) {
    return this.storage[key];
  }
  /**
   * removeItem
   */
  public removeItem(key: string) {
     this.storage[key] = null;
  }
  public notifyOther(data: any) {
    if (data) {
      this.notify.next(data);
    }
  }
}
