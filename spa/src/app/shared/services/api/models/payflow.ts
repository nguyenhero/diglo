﻿export class Payflow {
    url: string;
    securetoken: string;
    securetokenId: string;
    mode: string;
}
