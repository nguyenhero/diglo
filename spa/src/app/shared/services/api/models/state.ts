﻿export class State {
    constructor(
        public stateID: string,
        public stateDesc: string,
        public countryID: string
    ) {}
}
