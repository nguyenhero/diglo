﻿import {Product} from './product';
export class WishList {
    constructor() {}
    public wishID: number;
    public custNo: number;
    public productSKU: string;
    public wishItemQty: number;
    public status: string;
    public createDatetime: string;
    public sharedDatetime: string;
    public email: string;
    public product: Product;
}
