﻿import {Subclass} from './subclass';
export class AddOrderResponse {
    constructor(
    ) {}
    public result: number;
    public errorMsg: string;
    public securetoken: string;
    public securetokenId: string;
}
