﻿export class OrderHead {
    constructor(
    ) {}
    public orderID: number;
    public custNo: number;
    public billingFirstName: string;
    public billingLastName: string;
    public billingFullName: string;
    public billingAddress: string;
    public billingCity: string;
    public billingState: string;
    public billingZip: string;
    public billingCountry: string;
    public billingPhone: string;
    public billingEmail: string;
    public billingFax: string;
    public billingCompany: string;
    public shippingFirstName: string;
    public shippingLastName: string;
    public shippingFullName: string;
    public shippingAddress: string;
    public shippingCity: string;
    public shippingState: string;
    public shippingZip: string;
    public shippingCountry: string;
    public shippingPhone: string;
    public shippingEmail: string;
    public shippingFax: string;
    public shippingCompany: string;
    public shipLocFee: number;
    public shipProvider: string;
    public shipProviderCode: string;
    public shipType: string;
    public shipFee: number;
    public shipFeeTotal: number;
    public shipDesc: string;
    public createDateTime: Date;
    public shipDateTime: Date;
    public paymentType: string;
    public promoCode: string;
    public tax: number;
    public isexported: boolean;
    public total: number;
    public status: string;
}
