﻿export class BillingAddress {
    constructor() {}
    public addrID: number;
    public custNo: number;
    public primaryAddress: string;
    public firstName: string;
    public lastName: string;
    public fullName: string;
    public company: string;
    public address: string;
    public city: string;
    public state: string;
    public zip: string;
    public country: string;
    public phone: string;
    public email: string;
    public paymentType: string;
    public createDatetime: Date;
    public updateDatetime: Date;
    public updateUser: string;
}
