﻿export class ProductReview {
    constructor(    
    ) {}
    public revID: number;
    public productSKU: string;
    public revName: string;
    public revComments: string;
    public revRate: number;
    public createDatetime: Date;
    public overallCount: number;
}

