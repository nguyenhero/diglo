﻿export class PromoCode {
    constructor(
        public code: string,
        public valid: string
    ) {}
}
