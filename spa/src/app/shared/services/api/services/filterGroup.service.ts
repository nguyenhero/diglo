﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import {FilterGroup} from '../models/filterGroup';
@Injectable()
export class FilterGroupService {
    private filterGroupUrl = environment.apiUrl +'filterGroup';
    constructor(private http: Http) { }
    get(departmentId: string, classId: string, subclassId: string, kind: string, filterId: string, q: string): Observable<FilterGroup[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("departmentId", departmentId ? departmentId : '');
        params.set("classId", classId ? classId : '');
        params.set("subclassId", subclassId ? subclassId : '');
        params.set("kind", kind ? kind : '');
        params.set("filterId", filterId ? filterId : '');
        params.set("q", q ? q : '');
        return this.http.get(this.filterGroupUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}