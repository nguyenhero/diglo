﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Product} from '../models/product';
import { environment } from 'src/environments/environment';
@Injectable()
export class ProductDetailsService {
    private productUrl = environment.apiUrl +'productDetails';
    constructor(private http: Http) { }
    get(productSKU: string, parent: string): Observable<Product[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("productSKU", productSKU);
        params.set("parent", parent);
        return this.http.get(this.productUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}