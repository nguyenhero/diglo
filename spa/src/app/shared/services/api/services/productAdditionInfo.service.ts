﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {ProductAdditionInfo} from '../models/productAdditionInfo';
import { environment } from 'src/environments/environment';
@Injectable()
export class ProductAdditionInfoService {
    private productAdditionInfoUrl = environment.apiUrl +'productAdditionInfo';
    constructor(private http: Http) { }
    get(productSKU: string): Observable<ProductAdditionInfo[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("productSKU", productSKU);
        return this.http.get(this.productAdditionInfoUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}