﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {State} from '../models/state';
import { environment } from 'src/environments/environment';
@Injectable()
export class StateService {
    private stateUrl = environment.apiUrl +'state';
    constructor(private http: Http) { }
    get(countryId: string): Observable<State[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("countryId", countryId);
        return this.http.get(this.stateUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}