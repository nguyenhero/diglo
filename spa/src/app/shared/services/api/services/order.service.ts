﻿import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {User}           from '../models/user';
import {BillingAddress} from '../models/billingAddress';
import {ShippingAddress} from '../models/shippingAddress';
import {AddOrderResponse} from '../models/addOrderResponse';
import {Payflow} from '../models/payflow';
import {ExpressCheckoutDetails} from '../models/expressCheckoutDetails';
import {OrderHead} from '../models/orderHead';
import {OrderDetail} from '../models/orderDetail';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable()
export class OrderService {
    user: User;
    private paypalUrl = environment.apiUrl +'paypal';
    private paypalByShippingAddressUrl = environment.apiUrl +'paypalByShippingAddress';
    private orderUrl = environment.apiUrl +'order';
    private addAffirmOrderUrl = environment.apiUrl +'addAffirmOrder';
    private affirmUrl = environment.apiUrl +'affirm';
    private orderTaxUrl = environment.apiUrl +'tax';
    private orderDetailsUrl = environment.apiUrl +'orderDetails';
    private orderReviewUrl = environment.apiUrl +'orderReview';
    private billingAddressUrl = environment.apiUrl +'billingAddress';
    private shippingAddressUrl = environment.apiUrl +'shippingAddress';
    private fedExRateServiceUrl = environment.apiUrl +'fedExRateService';
    constructor(private http: Http) { }
    addOrder(body: Object): Observable<AddOrderResponse> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.orderUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    addAffirmOrder(body: Object): Observable<any> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON       
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.addAffirmOrderUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }    
    checkout(body: Object)
        : Observable<any> {
        let bodyString = this.getFormUrlEncoded(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post("https://sandbox.affirm.com/api/v2/checkout", bodyString, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }    
    getFormUrlEncoded(toConvert) {
        const formBody = [];
        for (const property in toConvert) {
            const encodedKey = encodeURIComponent(property);
            const encodedValue = encodeURIComponent(toConvert[property]);
            formBody.push(encodedKey + '=' + encodedValue);
        }
        return formBody.join('&');
    }
    getOrderTax(body: Object): Observable<any> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.orderTaxUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    getOrder(custNo: string, monthCount: string, orderId: string = null): Observable<OrderHead[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("custNo", custNo);
        params.set("monthCount", monthCount);
        if (orderId) params.set("orderId", orderId);
        return this.http.get(this.orderUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            ); 
    }
    getOrderDetails(orderId: string): Observable<OrderDetail[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("orderId", orderId);
        return this.http.get(this.orderDetailsUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            ); 
    }
    getRates(body: Object): Observable<any> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.fedExRateServiceUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    getBillingAddress(custNo: string): Observable<BillingAddress[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("custNo", custNo);
        return this.http.get(this.billingAddressUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            ); 
    }
    getshippingAddress(custNo: string): Observable<ShippingAddress[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("custNo", custNo);
        return this.http.get(this.shippingAddressUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            ); 
    }
    getPayFlowToken(body: Object): Observable<Payflow> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post('api/payFlowPro', body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    getPalPalExpressCheckoutToken(): Observable<any> {
        return this.http.get(this.paypalUrl) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    getPalPalExpressCheckoutTokenByRequest(body: Object): Observable<any> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.paypalByShippingAddressUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    getPalPalExpressCheckoutDetails(token: string): Observable<ExpressCheckoutDetails> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("token", token);
        return this.http.get(this.orderReviewUrl, { search: params }) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    doExpressCheckoutPayment(body: Object): Observable<AddOrderResponse> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.orderReviewUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    public setPayPalOrder(payPalOrder, token) {
        localStorage.removeItem('payPalOrder');
        let data = {
            'payPalOrder': payPalOrder,
            'token': token
        };
        localStorage.setItem('payPalOrder', JSON.stringify(data));
    }
    public getPayPalOrder(token) {
        if (token) {
            let data = JSON.parse(localStorage.getItem('payPalOrder'));
            if (data.token == token) {
                return data.payPalOrder;
            } else {
                localStorage.removeItem('payPalOrder');
            }
        } else {
            localStorage.removeItem('payPalOrder');
        }
    }
    public removePayPalOrder(token) {
        if (token) {
            let data = JSON.parse(localStorage.getItem('payPalOrder'));
            if (localStorage.getItem('payPalOrder') != null && data.token == token) {
                localStorage.removeItem('payPalOrder');
            }
        }
    }
}