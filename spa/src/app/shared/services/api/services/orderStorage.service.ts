import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {Product} from './../models/product';
import {OrderStorage} from './../models/orderStorage';
import {Token} from '../models/token';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Router} from '@angular/router';
import {AppState} from './appState.service';
@Injectable()
export class OrderStorageService {
    private CART_ORDER: string = 'CART_ORDER';
    private ADD_ORDER_CARD: string = 'ADD_ORDER_CARD';
    constructor(private http: Http, private router: Router, public appState: AppState) { }
    addToCart(custNo: string, product: Product): Product[] {
        let products = [];
        if (product) {
            let order = this.get(custNo);
            if (order) {
                this.update(custNo, product, true);
            } else {
                products.push(product);
                order = this.create(custNo, products);
                this.add(order);
            }
        }
        return products;
    }
    add(order: OrderStorage) {
        if (localStorage.getItem(this.CART_ORDER)) {
            localStorage.removeItem(this.CART_ORDER);
        }
        localStorage.setItem(this.CART_ORDER, JSON.stringify(order));
        this.appState.publish(this.ADD_ORDER_CARD);
    }
    create(custNo: string, products: Product[]): OrderStorage {
        let order = new OrderStorage();
        order.custNo = custNo;
        order.products = products;
        order.expires_at = this.getExpiresDate();
        return order;
    }
    update(custNo: string, product: Product, add: boolean = false) {
        let order = this.get(custNo);
        if (order && order.products) {
            let hasProduct = false;
            order.products.forEach((item) => {
                if (item.productSKU == product.productSKU) {
                    if (add == false) {
                        item.qty = product.qty;
                    } else {
                        item.qty += product.qty;
                    }
                    item.validQty = product.validQty;
                    item.required = product.required;
                    hasProduct = true;
                    return;
                }
            });
            if (hasProduct == false) {
                order.products.push(product);
            }
            this.create(custNo, order.products);
            this.add(order);
        }
        this.appState.publish(this.ADD_ORDER_CARD);
    }
    updatePromoCode(custNo: string, promoCode: string) {
        let order = this.get(custNo);
        if (order) {
            order.promoCode = promoCode;
            this.add(order);
        }
    }
    get(custNo: String): OrderStorage {
        if (localStorage.getItem(this.CART_ORDER)) {
            let order = JSON.parse(localStorage.getItem(this.CART_ORDER))
            if (this.isAuthenticated(order.expires_at)) {
                if (order.custNo == custNo) {
                    return order;
                }
            }
            this.remove();
            return null;
        }
        return null;
    }
    getAll(): OrderStorage {
        if (localStorage.getItem(this.CART_ORDER)) {
            let order = JSON.parse(localStorage.getItem(this.CART_ORDER))
            if (this.isAuthenticated(order.expires_at)) {
                return order;
            }
            this.remove();
            return null;
        }
        return null;
    }
    remove(): void {
        if (localStorage.getItem(this.CART_ORDER)) {
            localStorage.removeItem(this.CART_ORDER);
            this.appState.publish(this.ADD_ORDER_CARD);
        }
    }
    getExpiresDate(): string {
        var expiresIn = (30 * 60*24);  // default: seconds for 1 day 
        const expiresAt = JSON.stringify((expiresIn * 1000) + new Date().getTime());
        return expiresAt;
    }
    isAuthenticated(expires_at): boolean {
        const expiresAt = parseInt(expires_at);
        return new Date().getTime() < expiresAt;
    }
}
