import { Injectable} from '@angular/core';
//import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
//import { LoginModalContext, LoginModal } from './../components/account/login/login.component';
import { User }           from '../models/user';
import { AppState } from './appState.service';

@Injectable()
export class AuthGuard implements CanActivate {
  currentUser: {};

  constructor() { }

    //constructor(public modal: Modal, private router: Router, public appState: AppState) {
    //}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }
        //// not logged in so redirect to login page with the return url
        //this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        this.openLogin();
        return false;
    }

    openLogin() {
        //let dialog = this.modal.open(LoginModal, new LoginModalContext());
        //dialog.then((resultPromise) => {
        //    return resultPromise.result.then((result) => {
        //        this.currentUser = result;
        //        if (this.currentUser) {
        //            this.appState.publish('login');
        //            this.router.navigate(['/about-us']);
        //        }
        //    });
        //});
    }
}
