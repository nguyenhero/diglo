export class ValidationService {
    static getValidatorErrorMessage(validatorName: string, controlName: string, validatorValue?: any) {
        let config = {
            'required': 'Please enter your ' + controlName + '.',
            'invalidCreditCard': 'Is invalid credit card number',
            'invalidTelephone': 'Is invalid telephone number',
            'invalidEmailAddress': 'Invalid email address',
            'invalidPassword': 'Invalid password. Password must be at least 6 characters long, and contain a number.',
            'minlength': `Minimum length ${validatorValue.requiredLength}`
        };
        return config[validatorName];
    }
    static creditCardValidator(control) {
        if (control.value) {
            // Visa, MasterCard, American Express, Diners Club, Discover, JCB
            if (control.value.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
                return null;
            } else {
                return { 'invalidCreditCard': true };
            }
        }
        return null;
    }
    static telephoneValidator(control) {
        if (control.value) {
            if (control.value.match(/^\d{3}-\d{3}-\d{4}$/) || control.value.match(/^\d{1}-\d{3}-\d{3}-\d{4}$/)) {
                return null;
            } else {
                return { 'invalidTelephone': true };
            }
        }
        return null;
    }
    static qtyValidator(control) {
        if (control.value && control.value < 0) {
            return { 'invalidQty': true };
        }
        return null;
    }
    static emailUpperValidator(control) {
        if (!control.value.match(/^[A-Z]/)) {
            return null;
        } else {
            return { 'invalidEmailUpperValidator': true };
        }
    }
    static emailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value) {
            if (control.value.match(/[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/)) {
                return null;
            } else {
                return { 'invalidEmailAddress': true };
            }
        }
        return null;
    }
    static passwordValidator(control) {
        // {6,100}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number
        if (control.value) {
            if (control.value.match(/^(?=.*)[a-zA-Z0-9!@#$%^&*]{6,25}$/)) {
                return null;
            } else {
                return { 'invalidPassword': true };
            }
        }
        //return null;
    }
}
