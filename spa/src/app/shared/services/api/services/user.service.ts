﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {User} from '../models/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable()
export class UserService {
    user: User;
    private userUrl = environment.apiUrl +'user';    
    private customerUrl = environment.apiUrl +'customer';
    private changePasswordUrl = environment.apiUrl +'changePassword';
    constructor(private http: Http) { }
    addAccount(body: Object): Observable<number> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.userUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    updateAccount(custNo: number, body: Object): Observable<number> {
        const url = `${this.userUrl}/${custNo}`;
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.put(url, bodyString, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    resetPassword(body: Object): Observable<number> {
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.put(this.userUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    getByEmail(email: string): Observable<number> {
    let params: URLSearchParams = new URLSearchParams();
        params.set("email", email);
        return this.http.get(this.userUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    getCustomerByEmail(email: string): Observable<number> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("email", email);
        return this.http.get(this.customerUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}