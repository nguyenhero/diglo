﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {ProductReview} from '../models/productReview';
import { environment } from 'src/environments/environment';
@Injectable()
export class ProductReviewService {
    private productReviewsUrl = environment.apiUrl +'productReviews';
    constructor(private http: Http) { }
    get(productSKU: string, start: number, bufferSize: number): Observable<ProductReview[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("productSKU", productSKU);
        params.set("start", start.toString());
        params.set("end", bufferSize.toString());
        return this.http.get(this.productReviewsUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    save(body: Object): Observable<number> {
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.productReviewsUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }   
}