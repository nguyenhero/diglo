import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {Product} from './../models/product';
import {PromoCode} from './../models/promoCode';
import {Token} from '../models/token';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Router} from '@angular/router';
import {AppState} from './appState.service';
import { environment } from 'src/environments/environment';
@Injectable()
export class CardWishProductsService {
    private tokeyKey = "token";
    private token: string;
    private registerUrl: string = environment.apiUrl +'authenticate';
    constructor(private http: Http, private router: Router, public appState: AppState) {}
    addProduct(product: Product) {
        let products = null;
        let hasProduct = false;
        if (!this.isAuthenticated()) {
            localStorage.removeItem('wishProducts');
        } else {
            products = JSON.parse(localStorage.getItem('wishProducts'));
        }
        if (!products) {
            products = [];
        } else {
            for (let i = 0; i < products.length; i++) {
                if (products[i].productSKU == product.productSKU) {
                    let qty = product.qty + products[i].qty;
                    products[i].qty = qty;
                    hasProduct = true;
                    break;
                }
            }
        }
        if (!hasProduct) {
            products.push(product);
        }
        localStorage.removeItem('wishProducts');
        localStorage.setItem('wishProducts', JSON.stringify(products));
        this.setSession();
        this.appState.publish('addWishProducts');
    }
    changeQty(products) {
        localStorage.removeItem('wishProducts');
        localStorage.setItem('wishProducts', JSON.stringify(products));
        this.appState.publish('addWishProducts');
    }
    remove() {
        localStorage.removeItem('wishProducts');
        this.appState.publish('addWishProducts');
    }
    removeProduct(product: Product) {
        let products = this.getProducts();
        if (products) {
            for (let i = 0; i < products.length; i++) {
                if (products[i].productSKU == product.productSKU) {
                    products.splice(i, 1);
                    break;
                }
            }
            localStorage.removeItem('wishProducts');
            localStorage.setItem('wishProducts', JSON.stringify(products));
            this.appState.publish('addWishProducts');
            return products;
        }
    }
    getProducts(): Product[] {
        if (!this.isAuthenticated()) {
            localStorage.removeItem('wishProducts');
            return null;
        } else {
            return JSON.parse(localStorage.getItem('wishProducts'));
        }
    }
    countProducts() {
        let products = this.getProducts();
        let count = 0;
        if (products) {
            for (let i = 0; i < products.length; i++) {
                count += products[i].qty;
            }
        }
        return count;
    }
    getProductsPriceSubTotals(products) {
        let total = 0;
        if (products) {
            for (let i = 0; i < products.length; i++) {
                total += products[i].webPrice * products[i].qty;
            }
        }
        return total;
    }
    setSession(): void {
        // Set the time that the access token will expire at
        var expiresIn = (30 * 60);  // default: seconds for 1 day 
        const expiresAt = JSON.stringify((expiresIn * 1000) + new Date().getTime());
        localStorage.setItem('wish_products_expires_at', expiresAt);
    }
    isAuthenticated(): boolean {
        // Check whether the current time is past the
        // access token's expiry time
        const expiresAt = JSON.parse(localStorage.getItem('wish_products_expires_at'));
        return new Date().getTime() < expiresAt;
    }
}
