﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {User} from '../models/user';
import {Product} from './../models/product';
import {OrderStorage} from './../models/orderStorage';
import {OrderStorageService} from './orderStorage.service';
import {OrderService} from './order.service';
import {AuthenticationService} from './authentication.service';
import {ShippingChargeService} from './shippingCharge.service';
@Injectable()
export class OrderLogicService {
    user: User;
    constructor(
        private authenticationService: AuthenticationService,
        private orderStorageService: OrderStorageService,
        private shippingChargeService: ShippingChargeService,
        private orderService: OrderService) {}
    addCartToUser() {
        let custNo = this.getCustNo();
        if (custNo != '-1') {
            let order = this.orderStorageService.getAll();
            if (order && order.custNo && order.custNo == '-1') {
                order.custNo = custNo;
                this.orderStorageService.add(order);
            }
        }
    }
    addToCart(product: Product) {
        this.orderStorageService.addToCart(this.getCustNo(), product);
    }
    updateToCart(product: Product) {
        this.orderStorageService.update(this.getCustNo(), product);
    }
    updatePromoCode(promoCode: string) {
        this.orderStorageService.updatePromoCode(this.getCustNo(), promoCode);
    }
    removeAll() {
        this.orderStorageService.remove();
    }
    remove(product: Product) {
        let custNo = this.getCustNo();
        let order = this.orderStorageService.get(custNo);
        if (order) {
            var index = -1;
            var i = -1;
            order.products.forEach((item) => {
                i++;
                if (item.productSKU == product.productSKU) {
                    index = i;
                    return;
                }
            });
            if (index > -1) {
                order.products.splice(index, 1);
                this.orderStorageService.remove();
                if (order.products && order.products.length > 0) {
                    this.orderStorageService.add(order);
                }
            }
        }
    }
    countProducts(): number {
        let count = 0;
        let order = this.orderStorageService.get(this.getCustNo());
        if (order) {
            order.products.forEach((item) => {
                count += item.qty;
            })
        }
        return count;
    }
    getOrder(): OrderStorage {
        let order = this.orderStorageService.get(this.getCustNo());
        return order;
    }
    getProducts(): Product[] {
        let order = this.orderStorageService.get(this.getCustNo());
        if (order) return order.products;
        else { this.removeAll(); return []; }
    }
    getSubTotal(order: OrderStorage = null): number {
        order = this.getOrder();
        if (order) {
            let total = 0;
            order.products.forEach(product => {
                if (order.promoCode) {
                    if (product.cartPrice.toString() == '-1.00') {
                        total += parseFloat(product.webPrice.toString()) * product.qty;
                    } else {
                        total += parseFloat(product.cartPrice.toString()) * product.qty;
                    }
                } else {
                    total += parseFloat(product.webPrice.toString()) * product.qty;
                }
            });
            return total;
        } else this.removeAll();
        return 0;
    }
    getSubTotalShipToCa(order: OrderStorage = null): number {
        order = this.getOrder();
        if (order) {
            let total = 0;
            order.products.forEach(product => {
                if (product.shipToCa && product.shipToCa == 'Y') {
                    if (order.promoCode) {
                        if (product.cartPrice.toString() == '-1.00') {
                            total += parseFloat(product.webPrice.toString()) * product.qty;
                        } else {
                            total += parseFloat(product.cartPrice.toString()) * product.qty;
                        }
                    } else {
                        total += parseFloat(product.webPrice.toString()) * product.qty;
                    }
                }

            });
            return total;
        }
        return 0;
    }
    getCustNo(): string {
        let user = this.authenticationService.getUser();
        let custNo = user && user.custNo ? user.custNo : -1;
        return custNo.toString();
    }
    getTax(billingAddress, shippingAddress, orderDetails, sub_totals, shipFee): number {
        let data = {
            'billingaddress': billingAddress,
            'shippingaddress': shippingAddress,
            'orderdetails': orderDetails,
            'excludingshippingtotal': sub_totals,
            'shipfeetotal': shipFee
        };
        let operation = this.orderService.getOrderTax(data);
        operation.subscribe(
            result => {
                if (result.errors) {
                    alert(result.errors);
                }
                else {
                    return result.taxable_amount;
                }

            });
        return 0;
    }
    getShipProviderCode(shipType: string): string {
        let shipProviderCode = shipType;
        switch (shipType) {
            case 'FIRST_OVERNIGHT':
                shipProviderCode = 'FEA';
                break;
            case 'PRIORITY_OVERNIGHT':
                shipProviderCode = 'FEP';
                break;
            case 'STANDARD_OVERNIGHT':
                shipProviderCode = 'FE1';
                break;
            case 'FEDEX_2_DAY':
                shipProviderCode = 'FE2';
                break;
            case 'FEDEX_3_DAY':
                shipProviderCode = 'FE3';
                break;
            case 'FEDEX_GROUND':
                shipProviderCode = 'FE6';
        }
        return shipProviderCode;
    }
    createForm(url, secureToken) {
        var form = document.createElement("form");
        var cmd = document.createElement("input");
        var token = document.createElement("input");
        form.action = url;
        cmd.value = "_express-checkout";
        cmd.name = "cmd";
        form.appendChild(cmd);
        token.value = secureToken;
        token.name = "token";
        form.appendChild(token);
        document.body.appendChild(form);
        form.submit();
    }
}