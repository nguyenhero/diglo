import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {User}           from '../models/user';
import {Token} from '../models/token';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Router} from '@angular/router';
import {AppState} from './appState.service';
import {OrderStorageService} from './orderStorage.service';
import { environment } from 'src/environments/environment';
@Injectable()
export class AuthenticationService {
    private tokeyKey = "token";
    private token: string;
    private registerUrl: string =  environment.apiUrl +'authenticate';
    constructor(private http: Http,
        private router: Router, 
        public appState: AppState,
        private orderStorageService: OrderStorageService) { }
    login(body: Object): Observable<User> {
        let bodyString = JSON.stringify(body);
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.registerUrl, body, options) // ...using post request
            .pipe(map((res: Response) => {
                let user = res.json();
                //if (user && user.token) {
                if (user) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes                   
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    //sessionStorage.setItem("token", user.token);
                    this.setSession();
                }
                return user;
            }) // ...and calling .json() on the response to return data
            );
    }
    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('expires_at');
        localStorage.removeItem('currentUser');
        this.orderStorageService.remove();
    }
    getCurrentUser(): User {
        if (!this.isAuthenticated()) {
            localStorage.removeItem('currentUser');
            //this.router.navigate(['/home']);
            return null;
        } else {
            return JSON.parse(localStorage.getItem('currentUser'));
        }
    }
    getUser(): User {
        if (localStorage.getItem('currentUser')) {
            return JSON.parse(localStorage.getItem('currentUser'));
        }
        return null;
    }
    updateCurrentUser(user: User) {
        localStorage.removeItem('currentUser');
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.appState.publish('updateCurrentUser');
    }
    updateUserPassword(user: User) {
        localStorage.removeItem('currentUser');
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.appState.publish('logout');
        //this.router.navigate(['/home']);
    }
    setSession(): void {
        // Set the time that the access token will expire at
        var expiresIn = (24*60 * 60);  // default: seconds for 1 day   (30 * 60);  expirationMin * 60 * 1000;
        const expiresAt = JSON.stringify((expiresIn * 1000) + new Date().getTime());
        localStorage.setItem('expires_at', expiresAt);
    }
    isAuthenticated(): boolean {
        // Check whether the current time is past the
        // access token's expiry time
        const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        return new Date().getTime() < expiresAt;
    }
    private getLocalToken(): string {
        if (!this.token) {
            this.token = localStorage.getItem(this.tokeyKey);
        }
        return this.token;
    }
    public initAuthHeaders(): Headers {
        let token = this.getLocalToken();
        if (token == null) throw "No token";
        var headers = new Headers();
        headers.append("Authorization", "Bearer " + token);
        return headers;
    }
}
