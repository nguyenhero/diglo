﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Department} from '../models/department';
import { environment } from 'src/environments/environment';
@Injectable()
export class ShippingChargeService {
    private shippingChargeUrl = environment.apiUrl +'shippingCharge';
    constructor(private http: Http) { }
    getFee(country: string, state: string, total: string): Observable<number> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("country", country);
        params.set("state", state);
        params.set("total", total);
        return this.http.get(this.shippingChargeUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}