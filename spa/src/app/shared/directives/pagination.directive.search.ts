﻿import {Component, Inject, HostListener, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges} from "@angular/core";
import {NgModel, ControlValueAccessor} from "@angular/forms";
import {DOCUMENT} from '@angular/platform-browser';
@Component({
    selector: 'ng-pagination[ngModel]',
    //  directives: [NgIf, NgFor, NgClass],
    template: `
              <ul class="pagination" >                  
                  <li> <a *ngIf="previousItemValid" (click)="previousPage(currentpage, true)" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a> </li>
                  <li *ngFor="let pageNo of pageList" [ngClass]="{'active':currentpage === pageNo}">
                      <a (click)="setCurrentPage(pageNo, true)">{{pageNo}}</a>
                  </li>                
                  <li> <a  *ngIf="nextItemValid" (click)="nextPage(nextItem, true)" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a> </li>                 
                </ul>`,
    styles: [`
        li {
            display: block!important;
        }
  .pagination >.active > a {
              background-color: #719F0A;
              border-color: #719F0A;
              color:white;
        }
    .pagination > li:first-child  > a ,
    .pagination > li:last-child  > a
        {
            color: #719F0A;
            margin-left: -1px;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }
     .pagination > li > a
       {    padding: 2px 5px;
            color: black;
       }
    .pagination 
       {    margin-top: 120px;
       }
  `]
})
export class PaginationDirective implements ControlValueAccessor, OnInit {
    @Input("previous-text") previousText: string;
    @Input("next-text") nextText: string;
    @Input("first-text") firstText: string;
    @Input("last-text") lastText: string;
    @Input("totalItems") totalItems: number;
    @Input("maxSize") pageSize: number;
    @Input("bufferSize") bufferSize: number;
    @Input("boundaryLinks") boundaryLinks: boolean;
    @Input("loading") loading: boolean;
    @Input("lastScrollTop") lastScrollTop: number = 0;
    @Output("pageChanged") pageChanged = new EventEmitter();
    currentpage: number;
    pageList: Array<number> = [];
    private onChange: Function;
    private onTouched: Function;
    private seletedPage: number;
    private nextItem: number;
    private previousItem: number;
    public nextItemValid: boolean;
    public previousItemValid: boolean;
    private blockHeight: number = 0;
    private mutationObserver: MutationObserver;
    private lastPosition: number = 0;
    private heightFirst: number = 0;
    private heightLast: number = 0;
    direction: string = "";
    hasNextPage: boolean = false;
    isScrolled: boolean = false;
    constructor( @Inject(DOCUMENT) private document, private pageChangedNgModel: NgModel) {
        this.pageChangedNgModel.valueAccessor = this;
    }
    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            let change = changes[propName];
            if (propName === 'loading') {
                this.loading = change.currentValue;
                if (this.hasNextPage) {
                    //this.scrollY(this.currentpage);
                    this.hasNextPage = false;
                }
            }
        }
    }
    ngOnInit() {
        this.seletedPage = 1;
        this.currentpage = 1;
    }
    doPaging() {
        this.pageList = [];
        var i: number, count: number;
        var remaining = this.totalItems % this.bufferSize;
        var totalSize = ((this.totalItems - remaining) / this.bufferSize) + (remaining === 0 ? 0 : 1);
        for (i = (this.currentpage), count = 0; i <= totalSize && count < this.pageSize; i++ , count++) {
            this.pageList.push(i);
        }
        //next validation
        if (i - 1 < totalSize) {
            this.nextItemValid = true;
            this.nextItem = i;
        } else {
            this.nextItemValid = false;
        }
        //previous validation
        if ((this.currentpage) >= 1) {
            this.previousItemValid = true;
            this.previousItem = (this.currentpage * this.pageSize) - 1;
        } else {
            this.previousItemValid = false;
        }
    }
    setCurrentPage(pageNo: number, click: boolean = false) {
        this.currentpage = pageNo;
        this.pageChangedNgModel.viewToModelUpdate(pageNo);
        if (click) {
            if (pageNo == 1) {
                //this.document.body.scrollTop = 0;
                this.isScrolled = true;
                window.scroll(0, 0);

            } else {
                this.scrollY(pageNo);
            }
        } else {}
    }
    firstPage() {
        this.currentpage = 1;
        this.pageChangedNgModel.viewToModelUpdate(1);
        this.pageChageListner();
        this.doPaging()
    }
    lastPage() {
        var totalPages = (this.totalItems / this.pageSize);
        var lastPage = (totalPages) - (totalPages % this.pageSize === 0 ? this.pageSize : totalPages % this.pageSize) + 1;
        this.currentpage = lastPage;
        this.pageChangedNgModel.viewToModelUpdate(lastPage);
        this.pageChageListner();
        this.doPaging()
    }
    nextPage(pageNo: number, click: boolean = false) {
        var totalPages = Math.ceil(this.totalItems / this.bufferSize);
        this.hasNextPage = true;
        if (pageNo <= totalPages) {
            this.currentpage = pageNo;
            this.pageChangedNgModel.viewToModelUpdate(pageNo);
            let content = this.document.getElementsByClassName('list-item');
            if (content.length < this.totalItems) {
                this.pageChageListner();
            } else {
                this.scrollY(pageNo);
            }
            this.doPaging();
        }
    }
    scrollY(pageNo) {
        this.isScrolled = true;
        window.scroll(0, this.getFirstItemScrollTop());
    }
    previousPage(pageNo: number, click: boolean = false) {
        if (pageNo > 0) {
            this.currentpage = click && pageNo > 1 ? pageNo - 1 : pageNo;
            this.pageChangedNgModel.viewToModelUpdate(this.currentpage);
            this.doPaging();
            if (this.currentpage == 1) {
                window.scroll(0, 0);
                window.scrollTo(0, 0);
            } else {
                this.scrollY(pageNo);
            }
        }
    }
    writeValue(value: string): void {
        if (!value && value != '0') return;
        this.setValue(value);
    }
    registerOnChange(fn: (_: any) => {}): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: (_: any) => {}): void {
        this.onTouched = fn;
    }
    setValue(currentValue: any) {
        this.currentpage = currentValue;
        this.doPaging();
    }
    pageChageListner() {

        this.pageChanged.emit({
            itemsPerPage: this.currentpage,
            direction: this.direction
        });
    }
    getLastItemScrollTop() {
        var totalPages = Math.ceil(this.totalItems / this.bufferSize);
        let content = this.document.getElementsByClassName('list-item');
        let length = content ? content.length : 0;
        let currentItemIndex = this.currentpage == totalPages
            ? length - 1
            : (this.currentpage * this.bufferSize) - 1;
        return content && length > 0 && content[currentItemIndex]
            ? content[currentItemIndex].offsetTop : 0;
    }
    getFirstItemScrollTop() {
        let content = this.document.getElementsByClassName('list-item');
        let length = content ? content.length : 0;
        let currentItemIndex = (this.currentpage - 1) * this.bufferSize;
        return content && length > 0 && content[currentItemIndex]
            ? content[currentItemIndex].offsetTop : 0;
    }
    getScrollPosition() {
        return (window.pageYOffset !== undefined)
            ? window.pageYOffset :
            (document.documentElement || document.body).scrollTop;
    }
    @HostListener("window:scroll", ['$event'])
    onWindowScroll($event: Event) {
        var position = this.getScrollPosition();
        if (position > 0) {
            var totalPages = Math.ceil(this.totalItems / this.bufferSize);
            if (!this.loading && !this.isScrolled) {
                if (position > this.lastScrollTop) {
                    if (position > this.getLastItemScrollTop()) {
                        if (this.currentpage > 0 && this.currentpage < totalPages) {
                            if (this.currentpage == this.pageList[this.pageList.length - 1]) {
                                this.seletedPage = this.seletedPage + 1;
                                this.nextPage(this.currentpage + 1);
                            } else if (this.currentpage >= this.pageList[0]) {
                                this.direction = 'down';
                                this.setCurrentPage(this.currentpage + 1);
                            }
                        }
                    }
                    if (this.currentpage == totalPages && 
                        this.document.getElementById("scrollable") && 
                        this.document.getElementById("scrollable").style.visibility != 'hidden') {

                        let content = this.document.getElementsByClassName('list-item');
                        if (content && content.length > 0) {
                            let lastContent = content[content.length - 1].offsetTop + content[content.length - 1].clientHeight;
                            if (position > lastContent && this.document.getElementById("scrollable")) {
                                this.document.getElementById("scrollable").style.visibility = "hidden";
                            }
                        }
                    }
                } else {
                    if (this.document.getElementsByClassName('list-item') &&
                        this.document.getElementsByClassName('list-item').lengh > 0 &&
                        position <= this.document.getElementsByClassName('list-item')[0].offsetTop) {
                        if (this.document.getElementById("scrollable")) {
                            this.document.getElementById("scrollable").style.visibility = "visible";
                        }
                        if (this.currentpage == this.pageList[0]) {
                            this.previousPage(1);
                        } else {
                            this.setCurrentPage(1);
                        }
                    } else {

                        if (this.currentpage == totalPages &&
                            this.document.getElementById("scrollable") &&
                            this.document.getElementById("scrollable").style.visibility != 'visible') {
                            this.document.getElementById("scrollable").style.visibility = "visible";
                        }

                        if (this.currentpage > 1 && this.currentpage <= totalPages &&
                            position <= this.getFirstItemScrollTop()) {
                            this.direction = 'up';
                            if (this.currentpage > this.pageList[0]) {
                                this.setCurrentPage(this.currentpage - 1);
                            } else if (this.currentpage == this.pageList[0]) {
                                this.previousPage(this.currentpage - 1);
                            }
                        }
                    }
                }
            }
        } else {
            this.currentpage = 1;
            this.previousPage(1);
            if (this.document.getElementById("scrollable")) {
                this.document.getElementById("scrollable").style.visibility = "visible";
            }
        }
        this.lastScrollTop = position;
        this.isScrolled = false;
    }
}