﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Web.Helpers
{
    public  static class DateTimeHelper
    {
        private static readonly long DatetimeMinTimeTicks = new DateTime(1970, 1, 1, 0, 0, 0).Ticks;
        /// Gets the milliseconds value of a c# DateTime (can be used to convert to a JavaScript Date() in our front-end
        public static long ToJavaScriptMilliseconds(DateTime dt)
        {
            return (long)((dt.Ticks - DatetimeMinTimeTicks) / 10000);
        }
    }
}

