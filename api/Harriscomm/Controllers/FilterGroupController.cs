﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class FilterGroupController
    {
        private readonly IFilterGroupManager m_FilterGroupManager;
        private readonly IProductManager m_ProductManager;

        public FilterGroupController(IFilterGroupManager filterGroupManager, IProductManager productManager)
        {
            m_FilterGroupManager = filterGroupManager;
            m_ProductManager = productManager;
        }

        [HttpGet]
        public IList<FilterGroup> Get(string departmentId, string classId, string subclassId, string kind, string filterId, string q)
        {
            List<FilterGroup> filterGroups = new List<FilterGroup>();

            var result = m_FilterGroupManager.Get(departmentId, classId, subclassId, kind, filterId, q);
            if (result != null && result.Count() > 0)
            {
                filterGroups = result.ToList();
            };

            return filterGroups;
        }
    }
}
