﻿
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class OrderDetailsController : Controller
    {
        private readonly IOrderDetailsManager m_OrderDetailsManager;

        public OrderDetailsController(IOrderDetailsManager orderDetailsManager)
        {
            m_OrderDetailsManager = orderDetailsManager;
        }

        [HttpGet]
        public IList<OrderDetail> Get(int orderId)
        {
            return m_OrderDetailsManager.Get(orderId);
        }
    }
}
