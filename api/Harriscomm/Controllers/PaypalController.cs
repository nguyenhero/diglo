﻿using EDWrapper;
using EDWrapper.Internals;
using EDWrapper.Models;
using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class PayPalController : Controller
    {
        const string PAY_PAL_COMPLETED = "PayPalCompleted";

        private readonly AppSettings m_AppSettings;
        private readonly IOrderManager m_OrderManager;
        private readonly ICustomerManager m_CustomerManager;

        public PayPalController(IOrderManager orderManager,
            ICustomerManager customerManager,
            IOptions<AppSettings> appSettings)
        {
            m_OrderManager = orderManager;
            m_AppSettings = appSettings.Value;
        }

        [HttpGet]
        public async Task<RedirectResult> PayPalBack(string token, string PayerID)
        {
            AddOrderResponse response = new AddOrderResponse();
            ExpressCheckoutDetails expressCheckoutDetails = new ExpressCheckoutDetails();

            var dict = await PayPalProvider.GetExpressCheckoutDetails(m_AppSettings, token);
            if (dict["ACK"] == "Success")
            {
                int orderNumber = 0;
                if (dict["INVNUM"] != null)
                {
                    orderNumber = Convert.ToInt32(dict["INVNUM"].ToString());
                    var updateResult = m_OrderManager.UpdateStatusByOrderId(orderNumber, PAY_PAL_COMPLETED);
                    response.Result = updateResult > 0 ? orderNumber : 0;

                }

                string url = string.Format("{0}?orderNumber={1}",
                     m_AppSettings.ExpressCheckoutSetting.ReturnUrl2, orderNumber);
                return RedirectPermanent(url);
            }
            else
            {
                string url = string.Format("{0}?orderNumber={1}",
                  m_AppSettings.ExpressCheckoutSetting.ReturnUrl2, 0);
                return RedirectPermanent(url);
            }
        }

        [HttpPost]
        public AddOrderRequest Post()
        {
            if (HttpContext.Request.Form != null && HttpContext.Request.Form.Count() > 0
                && !string.IsNullOrEmpty(HttpContext.Request.Form["RESULT"]))
            {
                AddOrderResponse addOrderResponse = new AddOrderResponse();
                addOrderResponse.Result = Convert.ToInt32(HttpContext.Request.Form["RESULT"]);
                addOrderResponse.ErrorMsg = HttpContext.Request.Form["RESPMSG"];
                addOrderResponse.SecuretokenId = HttpContext.Request.Form["SECURETOKENID"];
                addOrderResponse.Securetoken = HttpContext.Request.Form["SECURETOKEN"];

                AddOrderRequest request = new AddOrderRequest();
                request.BillingAddress = new BillingAddress()
                {
                    FirstName = HttpContext.Request.Form["BILLTOFIRSTNAME"],
                    LastName = HttpContext.Request.Form["BILLTOLASTNAME"],
                    Address = HttpContext.Request.Form["BILLTOSTREET"],
                    City = HttpContext.Request.Form["BILLTOCITY"],
                    State = HttpContext.Request.Form["BILLTOSTATE"],
                    Zip = HttpContext.Request.Form["BILLTOZIP"],
                    Country = HttpContext.Request.Form["BILLTOCOUNTRY"]
                };
                request.ShippingAddress = new ShippingAddress()
                {
                    FirstName = HttpContext.Request.Form["SHIPTOFIRSTNAME"],
                    LastName = HttpContext.Request.Form["BILLTOLASTNAME"],
                    Address = HttpContext.Request.Form["SHIPTOSTREET"],
                    City = HttpContext.Request.Form["SHIPTOCITY"],
                    State = HttpContext.Request.Form["SHIPTOSTATE"],
                    Zip = HttpContext.Request.Form["SHIPTOZIP"],
                    Country = HttpContext.Request.Form["SHIPTOCOUNTRY"]

                };

                return request;
            }
            return null;
        }
    }
}
