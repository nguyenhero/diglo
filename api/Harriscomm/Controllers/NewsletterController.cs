using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Harriscomm.Common;
using Microsoft.Extensions.Options;
using EDWrapper;
using EDWrapper.Models;
using Newtonsoft.Json;
using EDWrapper.Internals;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class NewsletterController : Controller
    {
        private readonly AppSettings m_AppSettings;
        public NewsletterController(IOptions<AppSettings> appSettings)
        {
            m_AppSettings = appSettings.Value;
            RequestOptions.ApiKey = m_AppSettings.ApiKeyCampaigner;
            RequestOptions.Endpoint = m_AppSettings.EndPointCampaigner;
        }
        [HttpPost]
        public async Task<string> Subscriber(string email, string flagg)
        {
            string body = string.Empty;
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(flagg))
            {
                string path = flagg.Equals("Add") ? "/Subscribers" : "/Subscribers/Remove";
                SubscriberDetails subscriber = new SubscriberDetails()
                {
                    EmailAddress = email
                };
                var subscribers = new SubscriberAdd(subscriber);
                body = serializeObject(subscribers);
                RequestHelper requestHelper = new RequestHelper();
                string content = await requestHelper.Post(path, body);
                return content;
            }
            return null;
        }
        private static string serializeObject(object payload)
        {
            if (payload == null)
                return string.Empty;
            return JsonConvert.SerializeObject(payload, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}
