﻿using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class PayFlowProController : Controller
    {
        private readonly AppSettings m_AppSettings;
        public PayFlowProController(IOptions<AppSettings> appSettings)
        {
            m_AppSettings = appSettings.Value;
          
        }
        [HttpPost]
        public async Task<PayFlowProResponse> GetPayFlowToken([FromBody] AddOrderRequest request)
        {
            PayFlowProResponse payFlowProResponse = new PayFlowProResponse();
            payFlowProResponse.Url = m_AppSettings.PayPalSetting.UrlPayPalLink;

            AddOrderResponse addOrderResponse = await PayFlowProvider.SendPayFlowLink(m_AppSettings, request);
            payFlowProResponse.Securetoken = addOrderResponse.Securetoken;
            payFlowProResponse.SecuretokenId = addOrderResponse.SecuretokenId;

            return payFlowProResponse;
        }
        public class PayFlowProResponse        {
            public string Url { get; set; }
            public string SecuretokenId { get; set; }
            public string Securetoken { get; set; }
        }
    }
}
