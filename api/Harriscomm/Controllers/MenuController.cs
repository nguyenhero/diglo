﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class MenuController : Controller
    {
        private readonly IDepartmentManager m_DepartmentManager;
        public MenuController(IDepartmentManager departmentManager)
        {
            m_DepartmentManager = departmentManager;
        }

        [HttpGet]
        public IList<Department> Get()
        {
            IList<Department> departments = new List<Department>();

            departments = m_DepartmentManager.Get();
            return departments;
        }
    }
}
