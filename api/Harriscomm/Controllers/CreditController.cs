using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Utilities;
using Harriscomm.Web.Helpers;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class CreditController : Controller
    {
        private readonly ICustomerManager m_CustomerManager;
        private readonly AppSettings m_AppSettings;
        public CreditController(ICustomerManager customerManager, IOptions<AppSettings> appSettings)
        {
            m_CustomerManager = customerManager;
            m_AppSettings = appSettings.Value;
        }
        [HttpPut]
        public int checkCredit([FromBody]CreateCreditRequest request)
        {
            request.creditCardNumber = Cryptography.Encrypt(request.creditCardNumber);
            request.cardVerifyNumber = Cryptography.Encrypt(request.cardVerifyNumber);
            return m_CustomerManager.CheckCredit(request);          
        }
    }
}
