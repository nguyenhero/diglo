﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class ShippingChargeController : Controller
    {
        private readonly IShippingChargeManager m_ShippingChargeManager;

        public ShippingChargeController(IShippingChargeManager shippingChargeManager)
        {
            m_ShippingChargeManager = shippingChargeManager;
        }

        [HttpGet]
        public decimal Get(string country, string state, decimal total)
        {
            ShippingCharge request = new ShippingCharge()
            {
                CountryID = country,
                StateID = state,
                Total = total
            };
            return m_ShippingChargeManager.GetFee(request);
        }
    }
}
