﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class SuggestionSearchController
    {
        private readonly ISuggestionSearchManager m_SuggestionSearchManager;

        public SuggestionSearchController(ISuggestionSearchManager suggestionSearchManager)
        {
            m_SuggestionSearchManager = suggestionSearchManager;
        }

        [HttpGet]
        public List<string> Get(string querySearch)
        {
            List<string> itemSuggestions = new List<string>();

            var result = m_SuggestionSearchManager.Get(querySearch);
            if (result != null && result.Count() > 0)
            {
                itemSuggestions = result.ToList();
            };

            return itemSuggestions;
        }
    }
}
