﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Linq;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class ProductAdditionInfoController : Controller
    {
        private readonly IProductManager m_ProductManager;

        public ProductAdditionInfoController(IProductManager productManager)
        {
            m_ProductManager = productManager;
        }

        [HttpGet]
        public IEnumerable<ProductAdditionInfo> Get(string productSKU)
        {
            var result = m_ProductManager.GetProductAdditionInfo(productSKU);     
            if (result != null && result.Count() > 0)
            {
                result = m_ProductManager.GetProductAdditionInfo(productSKU);
            }
            return result;
        }
      
    }
}
