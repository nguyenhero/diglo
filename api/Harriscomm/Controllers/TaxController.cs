﻿using EDWrapper;
using EDWrapper.Internals;
using EDWrapper.Models;
using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class TaxController : Controller
    {
        private readonly AppSettings m_AppSettings;
        public TaxController(IOptions<AppSettings> appSettings)
        {
            m_AppSettings = appSettings.Value;

            RequestOptions.ApiKey = m_AppSettings.ApiKeyCampaigner;
            RequestOptions.Endpoint = m_AppSettings.EndPointCampaigner;
        }

        [HttpPost]
        public async Task<TaxResponse> Post([FromBody]TaxOrdertRequest request)
        {
            try
            {
                var taxResponse = await TaxProvider.GetTax(m_AppSettings, request);
                return taxResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
