﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Linq;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class ProductCountController : Controller
    {
        private readonly IProductManager m_ProductManager;

        public ProductCountController(IProductManager productManager)
        {
            m_ProductManager = productManager;
        }

        [HttpGet]
        public int Get(GetProductRequest request)
        {
            return m_ProductManager.GetCount(request);
        }
    }
}
