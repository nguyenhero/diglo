using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Utilities;
using Harriscomm.Web.Helpers;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class CartController : Controller
    {
        private readonly ICustomerManager m_CustomerManager;
        private readonly AppSettings m_AppSettings;
        private IHostingEnvironment m_HostingEnvironment;
        public CartController(ICustomerManager customerManager, IOptions<AppSettings> appSettings, IHostingEnvironment environment)
        {
            m_CustomerManager = customerManager;
            m_AppSettings = appSettings.Value;
            m_HostingEnvironment = environment;
        }
        [HttpPost]
        public int CreateCart([FromBody]CreateCartDetailRequest request)
        {
            int createCart  = m_CustomerManager.CreateCart(request);
            return createCart;
        }
        [HttpPut("{custNo}")]
        public IList<ProductCart> getCart(int custNo,string request)
        {
            List<ProductCart> products = new List<ProductCart>();
            var result = m_CustomerManager.GetCart(custNo);
            if (result != null && result.Count() > 0)
            {
                products = result.ToList() ;
            }
            return products;
        }

        [HttpPut]
        public int delCart([FromBody]DelCartRequest request)
        {
            return m_CustomerManager.DelCart(request);
        }
        
    }
}

