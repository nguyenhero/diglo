﻿using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Utilities;
using Harriscomm.Web.Helpers;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly ICustomerManager m_CustomerManager;
        private readonly AppSettings m_AppSettings;
        private IHostingEnvironment m_HostingEnvironment;
        public UserController(ICustomerManager customerManager, IOptions<AppSettings> appSettings, IHostingEnvironment environment)
        {
            m_CustomerManager = customerManager;
            m_AppSettings = appSettings.Value;
            m_HostingEnvironment = environment;
        }
        [HttpPost]
        public int CreateUser([FromBody]CreateUserRequest request)
        {
            UserTransalator.UpdateCreateUserRequest(request);
            int createUser  = m_CustomerManager.CreateUser(request);
            return createUser;
        }
        [HttpPut("{id}")]
        public int UpdateUser(int id, [FromBody]UpdateUserRequest request)
        {
            request.CustNo = id;
            int createUser = m_CustomerManager.UpdateUser(request);
            return createUser;
        }
        [HttpPut]
        public int ResetPassword([FromBody]ResetPasswordRequest request)
        {
            request.Password = Cryptography.Encrypt(request.Password);
            return m_CustomerManager.UpdateUserByCustomerNo(request);
        }
        [HttpGet]
        public int SendEmailToUser(string email)
        {
            int errorNumber = 0;
            Customer customer = GetByEmail(email);

            if (customer == null) return errorNumber;
            string body = CreateEmailBody(customer);
            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = body;
            bool sendEmail = SendEmailProvider.SendEmail(m_AppSettings, customer.CustEmail, customer.CustFullName, bodyBuilder);
            return errorNumber = sendEmail ? 1 : 2;
        }
        private string CreateEmailBody(Customer customer)
        {
            var path = System.IO.Path.Combine("reset-password-email-template.html");
            if (!string.IsNullOrEmpty(m_HostingEnvironment.WebRootPath))
                path = System.IO.Path.Combine(m_HostingEnvironment.WebRootPath, "reset-password-email-template.html"); 
            string resetPasswordUrl = Request.HttpContext.Request.Scheme + "://" +
             Request.HttpContext.Request.Host.Value +
             "/reset-password/" + customer.CustNo +
             "/" + DateTime.Now.AddHours(4).ToString("yyyyMMddHHmmss");
            string homeUrl = Request.HttpContext.Request.Scheme + "://" +
             Request.HttpContext.Request.Host.Value + "/home";

            string logoUrl = Request.HttpContext.Request.Scheme + "://" +
            Request.HttpContext.Request.Host.Value + "/images/logo.png";
            using (StreamReader reader = System.IO.File.OpenText(path))
            {
                string body = reader.ReadToEnd();
                if (body != null && body != "")
                {
                    body = body.Replace("{{LogoUrl}}", logoUrl);
                    body = body.Replace("{{CustLFullName}}", customer.CustLFullName);
                    body = body.Replace("{{HomeUrl}}", homeUrl);
                    body = body.Replace("{{ResetPasswordUrl}}", resetPasswordUrl);
                    return body;
                }
            }
            return string.Empty;               
        }	
        private Customer GetByEmail(string email)
        {
            IEnumerable<Customer> customers = m_CustomerManager.GetByEmail(email);

            if (customers != null && customers.Count() > 0)
            {
                return customers.FirstOrDefault();
            }
            return null;
        }
    }
}
