﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class StateController : Controller
    {
        private readonly IStateManager m_StateManager;

        public StateController(IStateManager customerCatManager)
        {
            m_StateManager = customerCatManager;
        }

        [HttpGet]
        public IList<State> Get(string countryId)
        {
            return m_StateManager.Get(countryId);
        }
    }
}
