﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Model
{
    public class SendContactUs
    {
        public string ToEmail { get; set; }
        public string ToName { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string Telephone { get; set; }
        public string Comment { get; set; }
    }
}
