﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Model
{
    public class LineItems
    {
        public int id { get; set; }
        public int quantity { get; set; }
        public decimal unit_price { get; set; }
        public int product_tax_code { get; set; }
    }
}