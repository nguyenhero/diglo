﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Model
{
    public class CheckoutPayPalRequest
    {
        public ShippingAddress ShippingAddress { get; set; }
        public decimal Amount { get; set; }
         public string OrderId { get; set; }
    }
}
