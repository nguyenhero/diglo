﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Web.Model
{
    public class ExpressCheckoutDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Countrycode { get; set; }
        public string Email { get; set; }
        public string ShipToName { get; set; }
        public string ShipToStreet { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToState { get; set; }
        public string ShipToZip{ get; set; }
        public string ShipToCountryCode { get; set; }
        public string Token { get; set; }
        public string PayerId { get; set; }
        public string Amt { get; set; }
        public string MsgError { get; set; }
    }
}
