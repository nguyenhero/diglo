﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Model
{
    public class AddOrderResponse
    {
        public int Result { get; set; }
        public string ErrorMsg { get; set; }
        public string SecuretokenId { get; set; }
        public string Securetoken { get; set; }
    }
}
