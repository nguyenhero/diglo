﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Web.Model
{
    public class AddOrderModel
    {
        public BillingAddress BillingAddress { get; set; }
        public ShippingAddress ShippingAddress { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public OrderHead OrderHead { get; set; }
        public Customer User { get; set; }
        public int SubcribeNewletter { get; set; }
    }
}
