﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Model
{
    public class ShareWishRequest
    {
        public IList<WishList> WishList { get; set; }
        public string FromEmails { get; set; }
        public string Emails { get; set; }
        public string Messages { get; set; }
    }
}
