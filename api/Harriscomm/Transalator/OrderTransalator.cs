﻿using Harriscomm.Common;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using Harriscomm.Entities.Request;
using Harriscomm.Utilities;
using Harriscomm.Web.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Web.Transalator
{
    public static class OrderTransalator
    {
        public static AddOrderResponse CreateAddOrderResponse(string response)
        {
            AddOrderResponse addOrderResponse = new AddOrderResponse();
            var dict = new Dictionary<string, string>();
            foreach (string nvp in response.Split('&'))
            {
                string[] keys = nvp.Split('=');
                dict.Add(keys[0], keys[1]);
            }
            if (dict["RESULT"] == "0" && dict["RESPMSG"] == "Approved")
            {
                addOrderResponse.Result = 1;
                addOrderResponse.ErrorMsg = string.Empty;
                if (dict["SECURETOKEN"] != null)
                {
                    addOrderResponse.Securetoken = dict["SECURETOKEN"];
                    addOrderResponse.SecuretokenId = dict["SECURETOKENID"];
                }
            }
            else
            {
                addOrderResponse.Result = -1;
                addOrderResponse.ErrorMsg = dict["RESPMSG"];
            }
            return addOrderResponse;
        }
        public static void CreateOrderHeaderTotal(AddOrderRequest request)
        {
            decimal total = 0;

            foreach (var detail in request.OrderDetails)
            {
                if (!string.IsNullOrEmpty(request.OrderHead.PromoCode) && detail.CartPrice != -1)
                {
                    detail.Price = detail.CartPrice;
                }
                else
                {
                    detail.Price = detail.WebPrice;
                }
                total += detail.Price * detail.Quantity + detail.SpecialFee;
            }
            request.OrderHead.ExcludingShippingTotal = total;
            total += request.OrderHead.ShipFee + request.OrderHead.ShipLocFee + request.OrderHead.Tax;
            request.OrderHead.Total = total;
        }
        public static void CreateUser(AddOrderRequest request)
        {
            CreateUserRequest user = new CreateUserRequest();
            user.FirstName = request.OrderHead.BillingFirstName;
            user.LastName = request.OrderHead.BillingLastName;
            user.Email = request.OrderHead.BillingEmail;

            user.Password = request.User.CustPassword;
            UserTransalator.UpdateCreateUserRequest(user);
            request.CreateUserRequest = user;
        }
        public static ShippingCharge CreateShippingCharge(AddOrderRequest request)
        {
            return new ShippingCharge()
            {
                CountryID = request.ShippingAddress.Country,
                StateID = request.ShippingAddress.State,
                Total = request.OrderHead.Total

            };
        }
        public static Dictionary<string, string> CreatePayPalRequest(PayPalSetting payPalSetting, AddOrderRequest request)
        {
            var values = new Dictionary<string, string>()
            {
                {"PARTNER",payPalSetting.Partner},                         // You'll want to change these 4
                {"VENDOR", payPalSetting.Vendor},           // To use your own credentials
                {"USER", payPalSetting.User},
                {"PWD", payPalSetting.Pwd},
                {"TENDER", payPalSetting.Tender},
                {"TRXTYPE", payPalSetting.Trxtype},
                {"CREATESECURETOKEN", "Y"},
                {"SECURETOKENID", GenId()},
                {"ACCT", request.CreditCard.Number},
                {"EXPDATE", string.Format("{0}{1}",
                    request.CreditCard.ExpireMonth,
                    request.CreditCard.ExpireYear)},
                {"CVV2", request.CreditCard.Cvv2},
                {"AMT", request.OrderHead.Total.ToString()},
                { "BILLTOFIRSTNAME", request.BillingAddress.FirstName},
                { "BILLTOLASTNAME", request.BillingAddress.LastName},
                { "BILLTOSTREET", request.BillingAddress.Address},
                { "BILLTOCITY",  request.BillingAddress.City},
                { "BILLTOSTATE",  request.BillingAddress.State},
                { "BILLTOZIP",  request.BillingAddress.Zip},
                { "BILLTOCOUNTRY",  request.BillingAddress.Country},
                { "SHIPTOFIRSTNAME", request.ShippingAddress.FirstName},
                { "SHIPTOLASTNAME", request.ShippingAddress.LastName},
                { "SHIPTOSTREET", request.ShippingAddress.Address},
                { "SHIPTOCITY", request.ShippingAddress.City},
                { "SHIPTOSTATE", request.ShippingAddress.State},
                { "SHIPTOZIP", request.ShippingAddress.Zip},
                { "SHIPTOCOUNTRY", request.ShippingAddress.Country}
            };
            return values;
        }
        public static Dictionary<string, string> CreatePayFlowLinkRequest(PayPalSetting payPalSetting, AddOrderRequest request)
        {
            var values = new Dictionary<string, string>()
            {
                {"PARTNER",payPalSetting.Partner},                         // You'll want to change these 4
                {"VENDOR", payPalSetting.Vendor},           // To use your own credentials
                {"USER", payPalSetting.User},
                {"PWD", payPalSetting.Pwd},
                {"TENDER", "C"},
                {"TRXTYPE", "S"},
                {"CREATESECURETOKEN", "Y"},
                {"SECURETOKENID", GenId()},
                {"AMT", request.OrderHead.Total.ToString()},
                { "BILLTOFIRSTNAME", request.BillingAddress.FirstName},
                { "BILLTOLASTNAME", request.BillingAddress.LastName},
                { "BILLTOSTREET", request.BillingAddress.Address},
                { "BILLTOCITY",  request.BillingAddress.City},
                { "BILLTOSTATE",  request.BillingAddress.State},
                { "BILLTOZIP",  request.BillingAddress.Zip},
                { "BILLTOCOUNTRY",  request.BillingAddress.Country},
                { "SHIPTOFIRSTNAME", request.ShippingAddress.FirstName},
                { "SHIPTOLASTNAME", request.ShippingAddress.LastName},
                { "SHIPTOSTREET", request.ShippingAddress.Address},
                { "SHIPTOCITY", request.ShippingAddress.City},
                { "SHIPTOSTATE", request.ShippingAddress.State},
                { "SHIPTOZIP", request.ShippingAddress.Zip},
                { "SHIPTOCOUNTRY", request.ShippingAddress.Country}
            };
            return values;
        }
        public static Dictionary<string, string> AuthenticationRequest(PayPalSetting payPalSetting, AddOrderRequest request)
        {
            var values = new Dictionary<string, string>()
            {
                {"PARTNER",payPalSetting.Partner},                         // You'll want to change these 4
                {"VENDOR", payPalSetting.Vendor},           // To use your own credentials
                {"USER", payPalSetting.User},
                {"PWD", payPalSetting.Pwd},
                {"TENDER", payPalSetting.Tender},
                {"TRXTYPE", "A"},
                {"CREATESECURETOKEN", "Y"},
                {"SECURETOKENID", GenId()},
                {"AMT", request.OrderHead.Total.ToString()}
            };
            return values;
        }
        private static string GenId()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 16)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return "MySecTokenID-" + result; //add a prefix to avoid confusion with the "SECURETOKEN"
        }
        public static string CreateTaxRequest(TaxOrdertRequest request)
        {
            var lineItems = CreateListItems(request);
            var values = new Dictionary<string, object>()
            {
                { "from_street",  request.BillingAddress.Address},
                { "from_city",  request.BillingAddress.City},
                { "from_state", request.BillingAddress.State},
                { "from_zip", request.BillingAddress.Zip},
                { "from_country", request.BillingAddress.Country},
                { "line_items", lineItems },
                { "to_street", request.ShippingAddress.Address},
                { "to_city", request.ShippingAddress.City},
                { "to_state", request.ShippingAddress.State},
                { "to_zip", request.ShippingAddress.Zip},
                { "to_country", request.ShippingAddress.Country},
                { "amount", request.ExcludingShippingTotal.ToString()},
                { "shipping", request.ShipFeeTotal.ToString() },
            };
            return serializeObject(values);
        }
        private static List<LineItems> CreateListItems(TaxOrdertRequest request)
        {
            int count = 0;
            if (request.OrderDetails != null)
            {
                return request.OrderDetails.Select(x =>
                    new LineItems
                    {
                        id = count++,
                        quantity = x.Quantity,
                        unit_price = x.WebPrice > 0 ? x.WebPrice : x.CartPrice
                    }).ToList();
            }
            return null;
        }
        private static string serializeObject(object payload)
        {
            if (payload == null)
                return string.Empty;

            return JsonConvert.SerializeObject(payload, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}
