using Harriscomm.Entities.Enum;
using Harriscomm.Entities.Request;
using Harriscomm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Web.Transalator
{
    public static class CustomerTransalator
    {
        public static void UpdateCreateCreditRequest(CreateCreditRequest request)
        {
            request.cardVerifyNumber = Cryptography.Encrypt(request.cardVerifyNumber);
            request.creditCardNumber = Cryptography.Encrypt(request.creditCardNumber);
        }
    }
}