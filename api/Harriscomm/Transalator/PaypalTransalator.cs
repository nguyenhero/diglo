﻿using Harriscomm.Common;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using Harriscomm.Entities.Request;
using Harriscomm.Utilities;
using Harriscomm.Web.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Web.Transalator
{
    public static class PaypalTransalator
    {
        public static Dictionary<string, string> CreateSetExpressCheckout(ExpressCheckoutSetting setting, CheckoutPayPalRequest request)
        {
            var values = new Dictionary<string, string>()
            {
                    {"USER", setting.User },
                    {"PWD", setting.Pwd},
                    {"SIGNATURE", setting.Signature},
                    {"METHOD", "SetExpressCheckout"},
                    {"VERSION", setting.Version},
                    {"PAYMENTREQUEST_0_PAYMENTACTION", "SALE"},
                    {"PAYMENTREQUEST_0_AMT", request.Amount.ToString()},
                    {"PAYMENTREQUEST_0_CURRENCYCODE", "USD"},
                    {"CANCELURL", setting.ReturnUrl}
            };
            if (!string.IsNullOrEmpty(request.OrderId))
            {
                values.Add("PAYMENTREQUEST_0_INVNUM", request.OrderId);
            }
            if (request.ShippingAddress != null)
            {
                values.Add("ReturnUrl", setting.ReturnUrl1);
                values.Add("PAYMENTREQUEST_0_SHIPTONAME", string.Format("{0} {1}", request.ShippingAddress.FirstName, request.ShippingAddress.LastName));
                values.Add("PAYMENTREQUEST_0_SHIPTOSTREET", request.ShippingAddress.Address);
                values.Add("PAYMENTREQUEST_0_SHIPTOCITY", request.ShippingAddress.City);
                values.Add("PAYMENTREQUEST_0_SHIPTOSTATE", request.ShippingAddress.State);
                values.Add("PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE", request.ShippingAddress.Country);
                values.Add("PAYMENTREQUEST_0_SHIPTOZIP", request.ShippingAddress.Zip);
                values.Add("PAYMENTREQUEST_0_EMAIL", request.ShippingAddress.Email);
                values.Add("PAYMENTREQUEST_0_SHIPTOPHONENUM", request.ShippingAddress.Phone);
            }
            else
            {
                values.Add("ReturnUrl", setting.ReturnUrl);
            }
            return values;
        }
        public static Dictionary<string, string> CreateGetExpressCheckoutDetails(string token, ExpressCheckoutSetting setting)
        {
            var values = new Dictionary<string, string>()
            {
                    {"USER", setting.User },                         // You'll want to change these 4
                    {"PWD", setting.Pwd},           // To use your own credentials
                    {"SIGNATURE", setting.Signature},
                    {"VERSION", setting.Version},
                    {"METHOD", "GetExpressCheckoutDetails"},
                    {"TOKEN", token}
            };
            return values;
        }
        public static Dictionary<string, string> CreateDoExpressCheckoutPayment(AddReviewOrdertRequest request, ExpressCheckoutSetting setting)
        {
            var values = new Dictionary<string, string>()
            {
                    {"USER", setting.User },                         // You'll want to change these 4
                    {"PWD", setting.Pwd},           // To use your own credentials
                    {"SIGNATURE", setting.Signature},
                    {"VERSION", setting.Version},
                    {"PAYMENTREQUEST_0_PAYMENTACTION", "Sale"},
                    {"PAYERID", request.PayerId},
                    {"TOKEN", request.Token},
                    {"PAYMENTREQUEST_0_AMT", request.OrderHead.Total.ToString()},
                    {"METHOD", "DoExpressCheckoutPayment"}
            };
            return values;
        }
    }
}
