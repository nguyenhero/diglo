﻿using Harriscomm.Common;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Transalator;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Harriscomm.Web.Provider
{
    public static class PayPalProvider
    {
        public static async Task<Dictionary<string, string>> SetExpressCheckout(AppSettings appSettings, CheckoutPayPalRequest request)
        {
            HttpClient client = new HttpClient();
            var values = PaypalTransalator.CreateSetExpressCheckout(appSettings.ExpressCheckoutSetting, request);
            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync(appSettings.ExpressCheckoutSetting.UrlApi, content);
            var responseString = await response.Content.ReadAsStringAsync();
            string responseEncode = System.Net.WebUtility.UrlDecode(responseString);

            var dict = new Dictionary<string, string>();
            foreach (string nvp in responseEncode.Split('&'))
            {
                string[] keys = nvp.Split('=');
                dict.Add(keys[0], keys[1]);
            }

            return dict;

        }
        public static async Task<Dictionary<string, string>> GetExpressCheckoutDetails(AppSettings appSettings, string token)
        {
            HttpClient client = new HttpClient();
            var values = PaypalTransalator.CreateGetExpressCheckoutDetails(token, appSettings.ExpressCheckoutSetting);
            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync(appSettings.ExpressCheckoutSetting.UrlApi, content);
            var responseString = await response.Content.ReadAsStringAsync();

            string responseEncode = System.Net.WebUtility.UrlDecode(responseString);
            var dict = new Dictionary<string, string>();
            foreach (string nvp in responseEncode.Split('&'))
            {
                string[] keys = nvp.Split('=');
                dict.Add(keys[0], keys[1]);
            }
            return dict;

        }
        public static async Task<Dictionary<string, string>> DoExpressCheckoutPayment(AppSettings appSettings, AddReviewOrdertRequest request)
        {
            HttpClient client = new HttpClient();
            var values = PaypalTransalator.CreateDoExpressCheckoutPayment(request, appSettings.ExpressCheckoutSetting);
            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync(appSettings.ExpressCheckoutSetting.UrlApi, content);
            var responseString = await response.Content.ReadAsStringAsync();

            string responseEncode = System.Net.WebUtility.UrlDecode(responseString);
            var dict = new Dictionary<string, string>();
            foreach (string nvp in responseEncode.Split('&'))
            {
                string[] keys = nvp.Split('=');
                dict.Add(keys[0], keys[1]);
            }
            return dict;

        }
    }
}

