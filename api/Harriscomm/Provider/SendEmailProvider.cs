﻿using Harriscomm.Common;
using Harriscomm.Data.Entities;
using Harriscomm.Web.Model;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Web.Provider
{
    public static class SendEmailProvider
    {
        public static bool SendEmail(AppSettings appSettings, string email, string name, BodyBuilder bodyBuilder)
        {
            try
            {
                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(appSettings.NameEmailForm, appSettings.EmailFrom));
                mimeMessage.To.Add(new MailboxAddress(name, email));
                mimeMessage.Subject = appSettings.Subject;
                mimeMessage.Body = bodyBuilder.ToMessageBody();
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(appSettings.SmtpConfig, System.Int32.Parse(appSettings.Port), false);
                    client.AuthenticationMechanisms.Remove(appSettings.AuthenticationMechanisms);
                    client.Authenticate(appSettings.UserName, appSettings.Password);
                    client.Send(mimeMessage);
                    client.Disconnect(true);
                }
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return false;
            }
            return true;
        }
        public static bool SendEmailOrdeCheckout(AppSettings appSettings, string email, string name, BodyBuilder bodyBuilder)
        {
            try
            {
                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(appSettings.NameEmailForm, appSettings.EmailFrom));
                mimeMessage.To.Add(new MailboxAddress(name, email));
                mimeMessage.Subject = appSettings.SubjectOrderCheckout;
                mimeMessage.Body = bodyBuilder.ToMessageBody();
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(appSettings.SmtpConfig, System.Int32.Parse(appSettings.Port), false);
                    client.AuthenticationMechanisms.Remove(appSettings.AuthenticationMechanisms);
                    client.Authenticate(appSettings.UserName, appSettings.Password);
                    client.Send(mimeMessage);
                    client.Disconnect(true);
                }
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return false;
            }
            return true;
        }
        public static bool SendEmailContactUs(AppSettings appSettings, string email, string name, BodyBuilder bodyBuilder)
        {
            try
            {
                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(appSettings.NameEmailForm, appSettings.EmailFrom));
                mimeMessage.To.Add(new MailboxAddress(name, email));
                mimeMessage.Subject = appSettings.SubjectContactUs;
                mimeMessage.Body = bodyBuilder.ToMessageBody();
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(appSettings.SmtpConfig, System.Int32.Parse(appSettings.Port), false);
                    client.AuthenticationMechanisms.Remove(appSettings.AuthenticationMechanisms);
                    client.Authenticate(appSettings.UserName, appSettings.Password);
                    client.Send(mimeMessage);
                    client.Disconnect(true);
                }
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return false;
            }
            return true;
        }
        public static bool SendEmailShareWishList(AppSettings appSettings, ShareWishRequest request, Customer customer, BodyBuilder bodyBuilder)
        {
            try
            {
                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(customer.CustFullName, customer.CustEmail));
                mimeMessage.To.Add(new MailboxAddress(string.Empty, request.Emails));
                mimeMessage.Subject = "Share Wish List";
                mimeMessage.Body = bodyBuilder.ToMessageBody();
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(appSettings.SmtpConfig, System.Int32.Parse(appSettings.Port), false);
                    client.AuthenticationMechanisms.Remove(appSettings.AuthenticationMechanisms);
                    client.Authenticate(appSettings.UserName, appSettings.Password);
                    client.Send(mimeMessage);
                    client.Disconnect(true);
                }
            }
            catch (System.Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}

