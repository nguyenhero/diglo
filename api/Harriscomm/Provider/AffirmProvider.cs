﻿using Harriscomm.Common;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Transalator;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
namespace Harriscomm.Web.Provider
{
    public static class AffirmProvider
    {
        public static async Task<HttpResponseMessage> Checkout(AppSettings appSettings, string token)
        {
            string apiKeys = string.Format("{0}:{1}",
                appSettings.AffirmSetting.PublicKey,
                appSettings.AffirmSetting.PrivateKey
                );
            var values = new Dictionary<string, string>()
            {
                    {"checkout_token", token }
            };
            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes(apiKeys);
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            var content = new StringContent(serializeObject(values), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(appSettings.AffirmSetting.UrlApi, content);
            return response;
        }
        private static string serializeObject(object payload)
        {
            if (payload == null)
                return string.Empty;
            return JsonConvert.SerializeObject(payload, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}
