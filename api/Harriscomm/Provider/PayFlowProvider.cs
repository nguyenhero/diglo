﻿using Harriscomm.Common;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Transalator;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Harriscomm.Web.Provider
{
    public static class PayFlowProvider
    {
        public static async Task<AddOrderResponse> SendPayFlow(AppSettings appSettings, AddOrderRequest request)
        {
            AddOrderResponse addOrderResponse = new AddOrderResponse();
            addOrderResponse = await AuthenticationRequest(appSettings, request);
            if (addOrderResponse.Result != -1)
            {
                HttpClient client = new HttpClient();

                var values = OrderTransalator.CreatePayPalRequest(appSettings.PayPalSetting, request);
                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync(appSettings.PayPalSetting.UrlEndpoint, content);
                var responseString = await response.Content.ReadAsStringAsync();
                 addOrderResponse = OrderTransalator.CreateAddOrderResponse(responseString);
            }
            return addOrderResponse;
        }

        public static async Task<AddOrderResponse> AuthenticationRequest(AppSettings appSettings, AddOrderRequest request)
        {
            AddOrderResponse addOrderResponse = new AddOrderResponse();
            HttpClient client = new HttpClient();

            var values = OrderTransalator.AuthenticationRequest(appSettings.PayPalSetting, request);
            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync(appSettings.PayPalSetting.UrlEndpoint, content);
            var responseString = await response.Content.ReadAsStringAsync();
            var dict = new Dictionary<string, string>();

            foreach (string nvp in responseString.Split('&'))
            {
                string[] keys = nvp.Split('=');
                dict.Add(keys[0], keys[1]);
            }
            addOrderResponse = OrderTransalator.CreateAddOrderResponse(responseString);
            return addOrderResponse;
        }

        public static async Task<AddOrderResponse> SendPayFlowLink(AppSettings appSettings, AddOrderRequest request)
        {
            AddOrderResponse addOrderResponse = new AddOrderResponse();
            HttpClient client = new HttpClient();

            var values = OrderTransalator.CreatePayFlowLinkRequest(appSettings.PayPalSetting, request);
            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync(appSettings.PayPalSetting.UrlEndpoint, content);
            var responseString = await response.Content.ReadAsStringAsync();
            var dict = new Dictionary<string, string>();

            foreach (string nvp in responseString.Split('&'))
            {
                string[] keys = nvp.Split('=');
                dict.Add(keys[0], keys[1]);
            }
            addOrderResponse = OrderTransalator.CreateAddOrderResponse(responseString);
            return addOrderResponse;
        }
    }
}

