using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Harriscomm.Common;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Queries;
using Harriscomm.Data.DataLayer;
using Harriscomm.Data.Business;
using Harriscomm.Data.Repository;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
namespace Harriscomm
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public IConfigurationRoot Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddCors();
            services.AddOptions();  // Setup options with DI
            services.Configure<AppSettings>(options => Configuration.GetSection("AppSettings").Bind(options));
            // Add application services.
            services.AddTransient<Harriscomm.Common.Interfaces.IConfiguration, Configuration>();
            services.AddTransient<IConnectionFactory, ConnectionFactory>();
            services.AddTransient<IDepartmentQueries, DepartmentQueries>();
            services.AddTransient<IDepartmentDataLayer, DepartmentDataLayer>();
            services.AddTransient<IDepartmentManager, DepartmentManager>();
            services.AddTransient<IUserQueries, UserQueries>();
            services.AddTransient<IUserDataLayer, UserDataLayer>();
            services.AddTransient<IUserManager, UserManager>();
            services.AddTransient<ICustomerQueries, CustomerQueries>();
            services.AddTransient<ICustomerDataLayer, CustomerDataLayer>();
            services.AddTransient<ICustomerManager, CustomerManager>();
            services.AddTransient<ICustomerCatQueries, CustomerCatQueries>();
            services.AddTransient<ICustomerCatDataLayer, CustomerCatDataLayer>();
            services.AddTransient<ICustomerCatManager, CustomerCatManager>();
            services.AddTransient<IStateQueries, StateQueries>();
            services.AddTransient<IStateDataLayer, StateDataLayer>();
            services.AddTransient<IStateManager, StateManager>();
            services.AddTransient<IProductQueries, ProductQueries>();
            services.AddTransient<IProductDataLayer, ProductDataLayer>();
            services.AddTransient<IProductManager, ProductManager>();
            services.AddTransient<IProductReviewsQueries, ProductReviewsQueries>();
            services.AddTransient<IProductReviewsDataLayer, ProductReviewsDataLayer>();
            services.AddTransient<IProductReviewsManager, ProductReviewsManager>();
            services.AddTransient<IOrderQueries, OrderQueries>();
            services.AddTransient<IOrderDataLayer, OrderDataLayer>();
            services.AddTransient<IOrderManager, OrderManager>();
            services.AddTransient<IShippingAddressQueries, ShippingAddressQueries>();
            services.AddTransient<IShippingAddressDataLayer, ShippingAddressDataLayer>();
            services.AddTransient<IShippingAddressManager, ShippingAddressManager>();
            services.AddTransient<IBillingAddressQueries, BillingAddressQueries>();
            services.AddTransient<IBillingAddressDataLayer, BillingAddressDataLayer>();
            services.AddTransient<IBillingAddressManager, BillingAddressManager>();
            services.AddTransient<IShippingChargeQueries, ShippingChargeQueries>();
            services.AddTransient<IShippingChargeDataLayer, ShippingChargeDataLayer>();
            services.AddTransient<IShippingChargeManager, ShippingChargeManager>();
            services.AddTransient<IWishListQueries, WishListQueries>();
            services.AddTransient<IWishListDataLayer, WishListDataLayer>();
            services.AddTransient<IWishListManager, WishListManager>();
            services.AddTransient<IOrderDetailsQueries, OrderDetailsQueries>();
            services.AddTransient<IOrderDetailsDataLayer, OrderDetailsDataLayer>();
            services.AddTransient<IOrderDetailsManager, OrderDetailsManager>();
            services.AddTransient<IFilterGroupQueries, FilterGroupQueries>();
            services.AddTransient<IFilterGroupDataLayer, FilterGroupDataLayer>();
            services.AddTransient<IFilterGroupManager, FilterGroupManager>();
            services.AddTransient<ISuggestionSearchQueries, SuggestionSearchQueries>();
            services.AddTransient<ISuggestionSearchDataLayer, SuggestionSearchDataLayer>();
            services.AddTransient<ISuggestionSearchManager, SuggestionSearchManager>();
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseStatusCodePages();
            app.UseDeveloperExceptionPage();
            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404
                    && !Path.HasExtension(context.Request.Path.Value))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
            app.UseMvc(routes =>
            {
                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Fallback", action = "Index" }
                );
            });
        }
    }
}
