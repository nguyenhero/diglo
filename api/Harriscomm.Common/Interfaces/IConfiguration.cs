﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Common.Interfaces
{
    public interface IConfiguration
    {
        string ConnectionString { get; }
    }
}
