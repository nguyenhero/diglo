﻿namespace Harriscomm.Common
{
    public class AppSettings
    {
        public string ConnectionString { get; set; }
        public string Subject { get; set; }
        public string EmailFrom { get; set; }
        public string SubjectContactUs { get; set; }
        public string SubjectOrderCheckout { get; set; }
        public string NameEmailForm { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string NameEmailTo { get; set; }
        public string SmtpConfig { get; set; }
        public string Port { get; set; }
        public string AuthenticationMechanisms { get; set; }
        public string ApiKeyCampaigner { get; set; }
        public string EndPointCampaigner { get; set; }
        public string FedExKey { get; set; }
        public string FedExPassword { get; set; }
        public string FedExAccountNumber { get; set; }
        public string FedExMeterNumber { get; set; }
        public string FedExHubId { get; set; }
        public string FedExUseProduction { get; set; }
        public string UpsUsername { get; set; } //UPS
        public string UpsPassword { get; set; }
        public string UpsAccessLicenseNumber { get; set; }
        public string HarrisAddress { get; set; }
        public string HarrisCity { get; set; }
        public string HarrisState { get; set; }
        public string HarrisZip { get; set; }
        public string HarrisCountry { get; set; }
        public AffirmSetting AffirmSetting { get; set; }
        public PayPalSetting PayPalSetting { get; set; }
        public TaxSetting TaxSetting { get; set; }
        public ExpressCheckoutSetting ExpressCheckoutSetting { get; set; }
    }
    public class TaxSetting
    {
        public string UrlEndpoint { get; set; }
        public string AccessToken { get; set; }
    }
    public class PayPalSetting
    {
        public string UrlEndpoint { get; set; }
        public string UrlPayPalLink { get; set; }
        public string Partner { get; set; }
        public string Vendor { get; set; }
        public string User { get; set; }
        public string Pwd { get; set; }
        public string Tender { get; set; }
        public string Trxtype { get; set; }
    }
    public class ExpressCheckoutSetting
    {
        public string UrlEndpoint { get; set; }
        public string UrlApi{ get; set; }
        public string User { get; set; }
        public string Pwd { get; set; }
        public string Signature { get; set; }
        public string Version { get; set; }
        public string ReturnUrl { get; set; }
        public string ReturnUrl1 { get; set; }
        public string ReturnUrl2 { get; set; }
        public string CancelUrl { get; set; }
        
    }
    public class AffirmSetting
    {
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
        public string FinancialProductKey { get; set; }
        public string UrlApi { get; set; }
        public string ReturnUrl { get; set; }
        public string CancelUrl { get; set; }
        public string CheckoutUrlApi { get; set; }

    }
}
