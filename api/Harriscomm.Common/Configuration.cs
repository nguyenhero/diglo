﻿using Harriscomm.Common.Interfaces;
using Microsoft.Extensions.Options;

namespace Harriscomm.Common
{
    public class Configuration : IConfiguration
    {
        private readonly AppSettings m_AppSettings;
        public Configuration(IOptions<AppSettings> appSettings)
        {
            m_AppSettings = appSettings.Value;

            Initialize();
        }

        private void Initialize()
        {
            ConnectionString = m_AppSettings.ConnectionString;
        }
        public string ConnectionString { get; private set; }
    }
}
