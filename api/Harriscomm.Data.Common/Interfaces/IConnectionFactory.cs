﻿using System.Data;

namespace Harriscomm.Data.Common
{
    public interface IConnectionFactory
    {
        IDbConnection CreateConnection();
    }
}
