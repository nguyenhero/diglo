﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class AddOrderRequest
    {
        public bool IsExistEmail { get; set; }
        public BillingAddress BillingAddress { get; set; }
        public ShippingAddress ShippingAddress { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public OrderHead OrderHead { get; set; }
        public Customer User { get; set; }
        public int SubcribeNewletter { get; set; }
        public CreateUserRequest CreateUserRequest { get; set; }
        public ShippingCharge CreateShippingCharge { get; set; }
        public CreditCard CreditCard { get; set; }
        public string Token { get; set; }
    }
}
