﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class AddWishListRequest
    {
        public int CustNo { get; set; }
        public string ProductSKU { get; set; }
        public decimal WishItemQty { get; set; }
        public decimal Status { get; set; }
        public string Email { get; set; }
        public DateTime CreateDatetime { get; set; }
        public DateTime UpdateDatetime { get; set; }
        public string UpdateUser { get; set; }
    }
}
