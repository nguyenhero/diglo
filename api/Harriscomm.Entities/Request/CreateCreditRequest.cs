using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class CreateCreditRequest
    {
        public string cardVerifyNumber { get; set; }
		public string creditCardNumber { get; set; }
        public int CustNo { get; set; }
        public string dateM { get; set; }
        public string dateY { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }
}