﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class AddReviewOrdertRequest
    {
        public string PayerId { get; set; }
        public string Token { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public OrderHead OrderHead { get; set; }
        public ShippingCharge CreateShippingCharge { get; set; }
    }
}
