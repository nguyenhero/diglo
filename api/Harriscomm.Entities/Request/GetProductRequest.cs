﻿using Harriscomm.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class GetProductRequest
    {
        public SortBy SortBy { get; set; }
        public int Start { get; set; }
        public int End { get; set; }
        public int DepartmentId { get; set; }
        public int ClassId { get; set; }
        public int SubClassId { get; set; }
        public string NewArrivalInd { get; set; }
        public string[] SearchColumns { get; set; }
        public string[] Operations { get; set; }
        public string[] FilterDetailValues { get; set; }
        public int[] FilterGroupIds { get; set; }
        // begin search
        public string q { get; set; }
        // end search
    }
}
