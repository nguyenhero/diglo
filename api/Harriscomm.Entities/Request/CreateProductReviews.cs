﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class CreateProductReviews
    {
        public string productSKU { get; set; }
        public int Star { get; set; }
        public string NickName { get; set; }
        public string Review { get; set; }
        public string CustNo { get; set; }
    }
}

