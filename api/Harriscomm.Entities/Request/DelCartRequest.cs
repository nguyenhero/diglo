using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class DelCartRequest
    {
        public int custNo { get; set; }
        public int productID { get; set; }
    }
}