﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class UpdateUserRequest
    {
        public int CustNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime CustModified { get; set; }
    }
}
