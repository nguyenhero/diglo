﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class CreateUserRequest
    {
        public string CustNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Group { get; set; }
        public string Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public char Status { get; set; }
        public int CatStatus { get; set; }
        public int IsExported { get; set; }
        public string CustLastLogin { get; set; }
    }
}
