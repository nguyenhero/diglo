﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class UpdateUserPasswordRequest
    {
        public int CustNo { get; set; }
        public string Password { get; set; }
        public DateTime CustModified { get; set; }
    }
}
