﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class ResetPasswordRequest
    {
        public string CustomerNo { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
