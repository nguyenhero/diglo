﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Request
{
    public class GetFedExRatesRequest
    {     
        public ShippingAddress ShippingAddress { get; set; }
        public decimal WeightTotal { get; set; }
        public string ServiceType { get; set; }
    }
}
