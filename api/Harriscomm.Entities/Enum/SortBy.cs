﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Enum
{
    public enum SortBy
    {
       NewAndBestselling,
       AvgCustomerRating,
       PriceLowToHigh,
       PriceHighToLow,
       NewestToOldest
    }
}
