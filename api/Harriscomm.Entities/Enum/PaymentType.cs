﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Entities.Enum
{
    public enum PaymentType
    {
        Paypal = 33,
        Affirm = 25,
        CreditCard = 24
    }
}
