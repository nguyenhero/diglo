﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class CustomerCatManager : ICustomerCatManager
    {
        private readonly ICustomerCatDataLayer m_CustomerCatDataLayer;
        public CustomerCatManager(ICustomerCatDataLayer customerCatDataLayer)
        {
            m_CustomerCatDataLayer = customerCatDataLayer;
        }
        public int CreateFreeCatalog(CreateFreeCatalogRequest request)
        {
            return m_CustomerCatDataLayer.CreateFreeCatalog(request);
        }
    }
}
