﻿using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business.Interfaces
{
    public class DepartmentManager : IDepartmentManager
    {
        private readonly IDepartmentDataLayer m_DepartmentDataLayer;
        public DepartmentManager(IDepartmentDataLayer departmentDataLayer)
        {
            m_DepartmentDataLayer = departmentDataLayer;
        }
        public IList<Department> Get()
        {
            return m_DepartmentDataLayer.Get();
        }
    }
}
