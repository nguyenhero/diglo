﻿using Harriscomm.Data.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.DataLayer.Interfaces;

namespace Harriscomm.Data.Business
{
    public class FilterGroupManager : IFilterGroupManager
    {
        private readonly IFilterGroupDataLayer m_FilterGroupDataLayer;
        public FilterGroupManager(IFilterGroupDataLayer filterGroupDataLayer)
        {
            m_FilterGroupDataLayer = filterGroupDataLayer;
        }

        public IList<FilterGroup> Get(string departmentId, string classId, string subclassId, string kind, string filterId, string q)
        {
            return m_FilterGroupDataLayer.Get(departmentId, classId, subclassId, kind, filterId, q);
        }
    }
}
