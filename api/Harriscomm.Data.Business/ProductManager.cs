﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class ProductManager : IProductManager
    {
        private readonly IProductDataLayer m_ProductDataLayer;
        public ProductManager(IProductDataLayer productDataLayer)
        {
            m_ProductDataLayer = productDataLayer;
        }

        public IEnumerable<Product> Get(GetProductRequest request)
        {
            return m_ProductDataLayer.Get(request);
        }
        public int GetCount(GetProductRequest request)
        {
            return m_ProductDataLayer.GetCount(request);
        }
        public IEnumerable<Product> GetByProductSKU(string productSKU, bool parent)
        {
            return m_ProductDataLayer.GetByProductSKU(productSKU, parent);
        }
        public IEnumerable<ProductAdditionInfo> GetProductAdditionInfo(string productSKU, int class_subclassid)
        {
            return m_ProductDataLayer.GetProductAdditionInfo(productSKU, class_subclassid);
        }
        public IEnumerable<ProductCount> GetProductCount()
        {
            return m_ProductDataLayer.GetProductCount();
        }
        public IEnumerable<Product> GetProductRelated(string productSKU)
        {
            return m_ProductDataLayer.GetProductRelated(productSKU);
        }
    }
}
