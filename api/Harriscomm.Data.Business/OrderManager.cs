﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class OrderManager : IOrderManager
    {
        private readonly IOrderDataLayer m_OrderDataLayer;
        public OrderManager(IOrderDataLayer orderDataLayer)
        {
            m_OrderDataLayer = orderDataLayer;
        }

        public int CheckPromoCode(string promoCode)
        {
            return m_OrderDataLayer.CheckPromoCode(promoCode);
        }
        public int Add(AddOrderRequest request)
        {
            return m_OrderDataLayer.Add(request);
        }

        public int AddReviewOrder(AddReviewOrdertRequest request)
        {
            return m_OrderDataLayer.AddReviewOrder(request);
        }

        public int AddShippingAddress(ShippingAddress request)
        {
            return m_OrderDataLayer.AddShippingAddress(request);
        }
        public int AddBillingAddress(BillingAddress request)
        {
            return m_OrderDataLayer.AddBillingAddress(request);
        }
        public BillingAddress GetBillingAddressByEmail(string email)
        {
            return m_OrderDataLayer.GetBillingAddressByEmail(email);
        }
        public IList<ShippingAddress> GetShippingAddressByCustNo(string custNo)
        {
            return m_OrderDataLayer.GetShippingAddressByCustNo(custNo);
        }

        public IList<BillingAddress> GetBillingAddressByCustNo(string custNo)
        {
            return m_OrderDataLayer.GetBillingAddressByCustNo(custNo);
        }
        public IList<OrderHead> Get(string custNo, string monthCount, string orderId)
        {
            return m_OrderDataLayer.Get(custNo, monthCount, orderId);

        }
        public int UpdateStatusByOrderId(int orderId, string status)
        {
            return m_OrderDataLayer.UpdateStatusByOrderId(orderId, status);
        }
    }
}
