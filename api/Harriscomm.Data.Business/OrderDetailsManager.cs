﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class OrderDetailsManager : IOrderDetailsManager
    {
        private readonly IOrderDetailsDataLayer m_OrderDetailsDataLayer;
        public OrderDetailsManager(IOrderDetailsDataLayer orderDetailsDataLayer)
        {
            m_OrderDetailsDataLayer = orderDetailsDataLayer;
        }
        public IList<OrderDetail> Get(int orderId)
        {
            return m_OrderDetailsDataLayer.Get(orderId);
        }
    }
}
