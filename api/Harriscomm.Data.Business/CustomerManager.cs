﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class CustomerManager : ICustomerManager
    {
        private readonly ICustomerDataLayer m_CustomerDataLayer;
        public CustomerManager(ICustomerDataLayer customerDataLayer)
        {
            m_CustomerDataLayer = customerDataLayer;
        }
        public Customer Get(string email, string password)
        {
            return m_CustomerDataLayer.Get(email, password);
        }
        public IEnumerable<Customer> GetByEmail(string email)
        {
            return m_CustomerDataLayer.GetByEmail(email);
        }
        public IEnumerable<ProductCart> GetCart(int custNo)
        {
            return m_CustomerDataLayer.GetCart(custNo);
        }
        public IEnumerable<Product> GetProdRecent(int custNo)
        {
            return m_CustomerDataLayer.GetProdRecent(custNo);
        }
        public IEnumerable<CreditCards> GetCredit(int custNo)
        {
            return m_CustomerDataLayer.GetCredit(custNo);
        }
        public int CreateCredit(CreateCreditRequest request)
        {
            return m_CustomerDataLayer.CreateCredit(request);
        }
        public int DelCredit(CreateCreditRequest request)
        {
            return m_CustomerDataLayer.DelCredit(request);
        }
        public int CheckCredit(CreateCreditRequest request)
        {
            return m_CustomerDataLayer.CheckCredit(request);
        }
        public int CreateUser(CreateUserRequest request)
        {
            return m_CustomerDataLayer.CreateUser(request);
        }
        public int CreateCart(CreateCartDetailRequest request)
        {
            return m_CustomerDataLayer.CreateCart(request);
        }
        public int CreateProdRecent(CreateProdRecentRequest request)
        {
            return m_CustomerDataLayer.CreateProdRecent(request);
        }
        public int DelCart(DelCartRequest request)
        {
            return m_CustomerDataLayer.DelCart(request);
        }
        public int UpdateUser(UpdateUserRequest request)
        {
            return m_CustomerDataLayer.UpdateUser(request);
        }

        public int UpdateUserPassword(UpdateUserPasswordRequest request)
        {
            return m_CustomerDataLayer.UpdateUserPassword(request);
        }
        public int UpdateUserByCustomerNo(ResetPasswordRequest request)
        {
            return m_CustomerDataLayer.UpdateUserByCustomerNo(request);
        }
    }
}
