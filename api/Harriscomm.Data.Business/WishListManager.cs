﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class WishListManager : IWishListManager
    {
        private readonly IWishListDataLayer m_WishListDataLayer;
        public WishListManager(IWishListDataLayer WishListDataLayer)
        {
            m_WishListDataLayer = WishListDataLayer;
        }
        public IList<WishList> Get(string countryId)
        {
            return m_WishListDataLayer.Get(countryId);
        }
        public int  Add(AddWishListRequest request)
        {
            return m_WishListDataLayer.Add(request);
        }

        public int DeleteWishListByCustNo(string custNo)
        {
            return m_WishListDataLayer.DeleteWishListByCustNo(custNo);
        }

        public int DeleteById(int id)
        {
            return m_WishListDataLayer.DeleteById(id);
        }
    }
}
