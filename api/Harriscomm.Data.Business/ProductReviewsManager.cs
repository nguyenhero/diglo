﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class ProductReviewsManager : IProductReviewsManager
    {
        private readonly IProductReviewsDataLayer m_ProductReviewsDataLayer;
        public ProductReviewsManager(IProductReviewsDataLayer productReviewsDataLayer)
        {
            m_ProductReviewsDataLayer = productReviewsDataLayer;
        }

        public IEnumerable<ProductReviews> Get(string productSKU, int start, int end)
        {
            return m_ProductReviewsDataLayer.Get(productSKU, start, end);
        }
        public int Create(CreateProductReviews request)
        {
            return m_ProductReviewsDataLayer.Create(request);
        }
    }
}
