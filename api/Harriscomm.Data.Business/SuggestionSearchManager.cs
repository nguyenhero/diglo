﻿using Harriscomm.Data.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.DataLayer.Interfaces;

namespace Harriscomm.Data.Business
{
    public class SuggestionSearchManager : ISuggestionSearchManager
    {
        private readonly ISuggestionSearchDataLayer m_SuggestionSearchDataLayer;
        public SuggestionSearchManager(ISuggestionSearchDataLayer suggestionSearchDataLayer)
        {
            m_SuggestionSearchDataLayer = suggestionSearchDataLayer;
        }

        public List<string> Get(string querySearch)
        {
            return m_SuggestionSearchDataLayer.Get(querySearch);
        }
    }
}
