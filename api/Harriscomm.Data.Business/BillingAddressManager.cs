﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class BillingAddressManager : IBillingAddressManager
    {
        private readonly IBillingAddressDataLayer m_BillingAddressDataLayer;
        public BillingAddressManager(IBillingAddressDataLayer billingAddressDataLayer)
        {
            m_BillingAddressDataLayer = billingAddressDataLayer;
        }
        public int Update(BillingAddress request)
        {
            return m_BillingAddressDataLayer.Update(request);
        }
        public int Add(BillingAddress request)
        {
            return m_BillingAddressDataLayer.Add(request);
        }
    }
}
