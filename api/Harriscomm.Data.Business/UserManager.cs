﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class UserManager : IUserManager
    {
        private readonly IUserDataLayer m_UserDataLayer;
        public UserManager(IUserDataLayer userDataLayer)
        {
            m_UserDataLayer = userDataLayer;
        }
        public IEnumerable<User> Get()
        {
            return m_UserDataLayer.Get();
        }
    }
}
