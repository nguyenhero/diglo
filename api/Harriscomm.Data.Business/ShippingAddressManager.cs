﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class ShippingAddressManager : IShippingAddressManager
    {
        private readonly IShippingAddressDataLayer m_ShippingAddressDataLayer;
        public ShippingAddressManager(IShippingAddressDataLayer shippingAddressDataLayer)
        {
            m_ShippingAddressDataLayer = shippingAddressDataLayer;
        }
        public int Update(ShippingAddress request)
        {
            return m_ShippingAddressDataLayer.Update(request);
        }
        public int Add(ShippingAddress request)
        {
            return m_ShippingAddressDataLayer.Add(request);
        }
    }
}
