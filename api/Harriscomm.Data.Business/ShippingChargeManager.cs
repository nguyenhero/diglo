﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class ShippingChargeManager : IShippingChargeManager
    {
        private readonly IShippingChargeDataLayer m_ShippingChargeDataLayer;
        public ShippingChargeManager(IShippingChargeDataLayer shippingChargeDataLayer)
        {
            m_ShippingChargeDataLayer = shippingChargeDataLayer;
        }

        public decimal GetFee(ShippingCharge request)
        {
            return m_ShippingChargeDataLayer.GetFee(request);
        }
    }
}
