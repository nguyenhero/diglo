﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business
{
    public class StateManager : IStateManager
    {
        private readonly IStateDataLayer m_StateDataLayer;
        public StateManager(IStateDataLayer stateDataLayer)
        {
            m_StateDataLayer = stateDataLayer;
        }
        public IList<State> Get(string countryId)
        {
            return m_StateDataLayer.Get(countryId);
        }
    }
}
