﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class ProductReviews
    {
        public int RevID { get; set; }
        public string ProductSKU { get; set; }
        public string RevComments { get; set; }
        public string RevName { get; set; }
        public int RevRate { get; set; }
        public DateTime CreateDatetime { get; set; }
        public int OverallCount { get; set; }

    }
}
