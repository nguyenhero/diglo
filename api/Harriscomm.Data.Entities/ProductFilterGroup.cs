﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class ProductFilterGroup
    {
        public int Id { get; set; }
        public int FilterGroupId { get; set; }
        public int DepartmentID { get; set; }
        public int ClassID { get; set; }
        public int SubclassID { get; set; }
    }
}
