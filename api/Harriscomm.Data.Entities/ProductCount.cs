﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class ProductCount
    {
        public int DepartmentID { get; set; }
        public string DepartmentDesc { get; set; }
        public int DeptProdCnt { get; set; }
        public int DeptCLCnt { get; set; }
        public int DeptOnSalCnt { get; set; }
        public int DeptNewCnt { get; set; }
        public int ClassID { get; set; }
        public string ClassDesc { get; set; }
        public int ClassProdCnt { get; set; }
        public int ClassCLCnt { get; set; }
        public int ClassOnSalCnt { get; set; }
        public int ClassNewCnt { get; set; }
        public int SubclassID { get; set; }
        public string SubclassDesc { get; set; }
        public int SubProdCnt { get; set; }
        public int SubCLCnt { get; set; }
        public int SubOnSalCnt { get; set; }
        public int SubNewCnt { get; set; }
    }
}
