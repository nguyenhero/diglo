using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{

        public class ProductCart 
        {
            public int CustCartNo { get; set; } 
            public string firstName { get; set; } 
            public string lastName { get; set; } 
            public string email { get; set; } 
            public int productID { get; set; } 
            public string productSKU { get; set; } 
            public string productDesc { get; set; } 
            public int qty { get; set; } 
            public decimal mSRPPrice { get; set; } 
            public decimal webPrice { get; set; } 
            public decimal cartPrice { get; set; } 
            public string availability { get; set; } 
            public decimal sub_total { get; set; } 
            public decimal subTotal { get; set; } 
            public int weight { get; set; } 
            public string departmentDesc { get; set; } 
            public string classDesc { get; set; } 
            public string subclassDesc  { get; set; } 
            public int departmentID  { get; set; } 
            public int classID { get; set; } 
            public int subclassID { get; set; }
            public string filter { get; set; } 
            public int pageID { get; set; } 
            public string metaDescription { get; set; } 
            public string metaKeyword  { get; set; } 
            public string metaTitle  { get; set; } 
            public string productLongDesc { get; set; } 
            public string productShortDesc { get; set; } 
            public string productSpec { get; set; } 
            public int reOderqty { get; set; } 
            public string imageMain { get; set; } 
            public string image1 { get; set; } 
            public string image2 { get; set; } 
            public string image3 { get; set; } 
            public string image4 { get; set; } 
            public string crossoffInd { get; set; }
            public string isFreeShip { get; set; }
            public int specialShipFee { get; set; }
            public string media { get; set; }
            public string productDocument1 { get; set; }
            public string productDocument2 { get; set; }
            public int overallCount { get; set; }
            public string color { get; set; }
            public string reviewRate { get; set; }
            public string newArrivalInd { get; set; }
            public string clearanceInd { get; set; }
            public int reviewCount { get; set; }
            public int flag_in { get; set; }
            public string shipToCa { get; set; }
            public bool required { get; set; }
            public bool validQty { get; set; }

        }
}