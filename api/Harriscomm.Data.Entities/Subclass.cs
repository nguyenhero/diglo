﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class Subclass
    {
        public int SubclassID { get; set; }
        public int ClassID { get; set; }
        public string SubclassDesc { get; set; }
        public string Url { get; set; }
        public bool Show { get; set; }
        public int Count { get; set; }
        public int ClearanceCount { get; set; }
        public int NewArrivalsCount { get; set; }
    }
}

