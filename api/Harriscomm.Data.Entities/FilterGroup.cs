﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class FilterGroup
    {
        public int Id { get; set; }
        public int FilterGroupId { get; set; }
        public string FilterGroupName { get; set; }
        public int OrderNo { get; set; }
        public Boolean Active { get; set; }
        public List<FilterDetail> FilterDetails { get; set; }
    }
}
