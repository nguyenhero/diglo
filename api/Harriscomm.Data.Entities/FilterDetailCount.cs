﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class FilterDetailCount
    {
        public int Id { get; set; }
        public int SubClassID { get; set; }
        public int FilterGroupId { get; set; }
        public int FilterDetailId { get; set; }
        public int FilterCount { get; set; }
    }
}
