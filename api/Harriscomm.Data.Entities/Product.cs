﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class Product
    {
        public int PageID { get; set; }
        public int ProductID { get; set; }
        public string ProductSKU { get; set; }
        public string ProductParent { get; set; }
        public string ProductDesc { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaTitle { get; set; }
        public string ProductLongDesc { get; set; }
        public string ProductShortDesc { get; set; }
        public string ProductSpec { get; set; }
        public int Qty { get; set; }
        public string ImageMain { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string ReviewRate { get; set; }
        public string MSRPPrice { get; set; }
        public string WebPrice { get; set; }
        public string CartPrice { get; set; }
        public string CrossoffInd { get; set; }
        public string IsFreeShip { get; set; }
        public decimal SpecialShipFee { get; set; }
        public string Availability { get; set; }
        public string Media { get; set; }
        public string ProductDocument1 { get; set; }
        public string ProductDocument2 { get; set; }
        public string Color { get; set; }
        public int OverallCount { get; set; }
        public decimal Weight { get; set; }
        public string NewArrivalInd { get; set; }
        public string ClearanceInd { get; set; }
        public int ReviewCount { get; set; }

        public string DepartmentDesc { get; set; }
        public string ClassDesc { get; set; }
        public string SubclassDesc { get; set; }
        public string Filter { get; set; }
        public string ShipToCa { get; set; }
        public int DepartmentID { get; set; }
        public int ClassID { get; set; }
        public int SubclassID { get; set; }

        public bool Required { get; set; }
        public bool ValidQty { get; set; }
    }
}
