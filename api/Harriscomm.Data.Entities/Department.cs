﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class Department
    {
        public Department()
        {
            Classes = new List<Class>();
        }
        public int DepartmentID { get; set; }
        public int Left { get; set; }
        public string DepartmentDesc { get; set; }
        public string ClassHtml { get; set; }
        public int Count { get; set; }
        public bool Show { get; set; }
        public List<Class> Classes { get; set; }
    }
}
