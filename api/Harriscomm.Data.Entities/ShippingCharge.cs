﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class ShippingCharge
    {
        public int ID { get; set; }
        public string CountryID { get; set; }
        public string StateID { get; set; }
        public decimal ChargeFee { get; set; }
        public decimal Total { get; set; }


    }
}
