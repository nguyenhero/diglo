﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class OrderDetail
    {
        public int OrderDetailID { get; set; }
        public int OrderID { get; set; }
        public string ProductSKU { get; set; }
        public decimal Weight { get; set; }
        public int Quantity { get; set; }
        public decimal WebPrice { get; set; }
        public decimal CartPrice { get; set; }
        public decimal SpecialFee { get; set; }        
        public decimal Tax { get; set; }
        public decimal Price { get; set; }
        public Product Product { get; set; }
    }
}