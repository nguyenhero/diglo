﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class FilterDetail
    {
        public int Id { get; set; }
        public int FilterGroupId { get; set; }
        public int FilterDetailId { get; set; }
        public string FilterDetailName { get; set; }
        public int OrderNo { get; set; }
        public string TableName { get; set; }
        public string Searchcolumn { get; set; }
        public string Operation { get; set; }
        public string FilterDetailValue { get; set; }
        public int ScrollView { get; set; }
        public int Limit { get; set; }
        public int FilterCount { get; set; }
        public bool Show { get; set; }

        public string FilterDetailDesc { get; set; }
    }
}
