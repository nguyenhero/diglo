﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class ProductAdditionInfo
    {
        public int ID { get; set; }
        public string ProductSKU { get; set; }
        public string Class_SubclassName { get; set; }
        public string AdditionName { get; set; }
        public string AdditionValue { get; set; }
        public int OrderNo { get; set; }
    }
}