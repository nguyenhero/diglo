﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class State
    {
        public string StateID { get; set; }
        public string StateDesc { get; set; }
        public string CountryID { get; set; }
    }
}
