﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Data.Entities
{
    public class WishList
    {
        public int WishID { get; set; }
        public int CustNo { get; set; }
        public string ProductSKU { get; set; }
        public decimal WishItemQty { get; set; }
        public decimal Status { get; set; }
        public decimal TotalStatus { get; set; }
        public DateTime CreateDatetime { get; set; }        
        public DateTime SharedDatetime { get; set; }
        public string Email { get; set; }
        public Product Product { get; set; }
    }
}