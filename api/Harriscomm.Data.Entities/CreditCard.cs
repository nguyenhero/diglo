﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class CreditCard
    {
        public string Number { get; set; }
        public string Type { get; set; }
        public decimal ExpireMonth { get; set; }
        public string ExpireYear { get; set; }
        public string Cvv2 { get; set; }
    }
}
