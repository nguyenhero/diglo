﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class Class
    {
        public Class()
        {
            Subclasses = new List<Subclass>();
        }

        public int ClassID { get; set; }
        public int DepartmentID { get; set; }
        public string ClassDesc { get; set; }
        public int Count { get; set; }
        public List<Subclass> Subclasses { get; set; }
        public bool Show { get; set; }
    }
}
