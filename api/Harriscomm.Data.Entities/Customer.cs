﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class Customer : Error
    {
        public int CustID { get; set; }
        public int CustNo { get; set; }
        public string CustFirstName { get; set; }
        public string CustLastName { get; set; }
        public string CustFullName { get; set; }
        public string CustLFullName { get; set; }        
        public string CustAddress { get; set; }
        public string CustApt { get; set; }
        public string CustCity { get; set; }
        public string CustZip { get; set; }
        public string CustCountry { get; set; }
        public string CustPhone { get; set; }
        public string CustState { get; set; }
        public string CustEmail { get; set; }
        public string Company { get; set; }
        public string CustPassword { get; set; }
        public DateTime CustCreated { get; set; }
        public string CustType { get; set; }
        public DateTime CustLastLogin { get; set; }
        public DateTime CustModified { get; set; }
        public string CustCreator { get; set; }
        public string CustGroup { get; set; }
        public string CustStatus { get; set; }
        public int CustCatStatus { get; set; }
        public int IsExported { get; set; }
        public string Token { get; set; }
    }
}