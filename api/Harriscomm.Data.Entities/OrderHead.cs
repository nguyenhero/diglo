﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class OrderHead
    {
        public int OrderID { get; set; }
        public string CustNo { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingFullName { get; set; }
        public string BillingAddress { get; set; }
        public string BillingApt { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
        public string BillingCountry { get; set; }
        public string BillingPhone { get; set; }
        public string BillingEmail { get; set; }
        public string BillingFax { get; set; }
        public string BillingCompany { get; set; }
        public string ShippingFirstName { get; set; }
        public string ShippingLastName { get; set; }
        public string ShippingFullName { get; set; }
        public string ShippingAddress { get; set; }
        public string ShippingApt { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingZip { get; set; }
        public string ShippingCountry { get; set; }
        public string ShippingPhone { get; set; }
        public string ShippingEmail { get; set; }
        public string ShippingFax { get; set; }
        public string ShippingCompany { get; set; }
        public decimal ShipLocFee { get; set; }
        public string ShipProvider { get; set; }
        public string ShipProviderCode { get; set; }
        public string ShipType { get; set; }
        public decimal ShipFee { get; set; }
        public decimal ShipFeeTotal { get; set; }
        public string ShipDesc { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime ShipDateTime { get; set; }
        public string PaymentType { get; set; }
        public string PromoCode { get; set; }
        public decimal ExcludingShippingTotal { get; set; }
        public decimal Total { get; set; }
        public decimal Tax { get; set; }
        public string Status { get; set; }
        public string OrderStatus { get; set; }
        public int Isexported { get; set; }
    }
}