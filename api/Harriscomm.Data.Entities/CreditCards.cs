using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Entities
{
    public class CreditCards
    {
        public string expirationDateM { get; set; }
        public string expirationDateY { get; set; }
        public string cardVerifyNumber { get; set; }
        public string creditCardNumber { get; set; }
    }
}