﻿using Harriscomm.Utilities.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Utilities
{
    public interface IPager
    {
        IEnumerablePage<T> GetPage<T>(IEnumerable<T> collection, int pageNumber, int pageSize);

        IEnumerable<T> GetPlainPage<T>(IEnumerable<T> collection, int pageNumber, int pageSize);

        IEnumerablePage<T> CreatePage<T>(IEnumerable<T> dataPage, int pageNumber, int pageSize, int totalCount);

        int GetPageCount(int pageSize, int collectionSize);
    }
}
