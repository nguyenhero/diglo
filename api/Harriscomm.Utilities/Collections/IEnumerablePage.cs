﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Utilities.Collections
{
    public interface IEnumerablePage<T>
    {
        IEnumerable<T> PageData { get; }
        int TotalCount { get; }
        int TotalPageCount { get; }
        int PageNumber { get; }
        int PageSize { get; }
    }
}
