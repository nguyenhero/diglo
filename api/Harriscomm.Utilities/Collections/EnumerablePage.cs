﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Utilities.Collections
{
    public class EnumerablePage<T> : IEnumerablePage<T>
    {
        public IEnumerable<T> PageData { get; set; }

        public int TotalCount { get; set; }

        public int TotalPageCount { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }
    }
}
