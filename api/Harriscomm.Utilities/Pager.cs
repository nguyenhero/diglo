﻿using Harriscomm.Utilities.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Utilities
{
    public class Pager : IPager
    {
        public IEnumerable<T> GetPlainPage<T>(IEnumerable<T> collection, int pageNumber, int pageSize)
        {
            if (pageSize != 0)
            {
                return collection.Skip(pageNumber * pageSize).Take(pageSize);
            }

            return collection;
        }

        public IEnumerablePage<T> GetPage<T>(IEnumerable<T> collection, int pageNumber, int pageSize)
        {
            List<T> input = collection.ToList();

            return new EnumerablePage<T>
            {
                PageData = GetPlainPage(input, pageNumber, pageSize),
                PageNumber = pageNumber,
                PageSize = pageSize,
                TotalCount = input.Count,
                TotalPageCount = GetPageCount(pageSize, input.Count)
            };
        }

        public IEnumerablePage<T> CreatePage<T>(IEnumerable<T> dataPage, int pageNumber, int pageSize, int totalCount)
        {
            return new EnumerablePage<T>
            {
                PageData = dataPage,
                PageNumber = pageNumber,
                PageSize = pageSize,
                TotalCount = totalCount,
                TotalPageCount = GetPageCount(pageSize, totalCount)
            };
        }

        public int GetPageCount(int pageSize, int collectionSize)
        {
            if (pageSize == 0)
            {
                return 1;
            }
            int pageCount = collectionSize / pageSize + (collectionSize % pageSize == 0 ? 0 : 1);
            return pageCount;
        }
    }
}
