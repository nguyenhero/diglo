﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries.Interface
{
    public interface IShippingAddressQueries
    {
        int Update(ShippingAddress request);
        int Add(ShippingAddress request);
    }
}
