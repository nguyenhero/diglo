﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries.Interface
{
    public interface IUserQueries
    {
        IEnumerable<User> Get();
    }
}
