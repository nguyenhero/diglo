﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries.Interface
{
    public interface IProductReviewsQueries
    {
        IEnumerable<ProductReviews> Get(string productSku, int start, int end);
        int Create(CreateProductReviews request);
    }
}
