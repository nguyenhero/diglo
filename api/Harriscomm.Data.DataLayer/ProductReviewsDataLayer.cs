﻿using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer.Interfaces
{
    public class ProductReviewsDataLayer : IProductReviewsDataLayer
    {
        private readonly IProductReviewsQueries m_ProductReviewsQueries;
        public ProductReviewsDataLayer(IProductReviewsQueries productReviews)
        {
            m_ProductReviewsQueries = productReviews;
        }
        public IEnumerable<ProductReviews> Get(string productSKU, int start, int end)
        {
            return m_ProductReviewsQueries.Get(productSKU, start, end);
        }
        public int Create(CreateProductReviews request)
        {
            return m_ProductReviewsQueries.Create(request);
        }
    }
}
