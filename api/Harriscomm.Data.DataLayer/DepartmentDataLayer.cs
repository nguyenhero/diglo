﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Entities.Request;

namespace Harriscomm.Data.DataLayer
{
    public class DepartmentDataLayer : IDepartmentDataLayer
    {
        private readonly IDepartmentQueries m_DepartmentQueries;
        public DepartmentDataLayer(IDepartmentQueries departmentQueries)
        {
            m_DepartmentQueries = departmentQueries;
        }

        public IList<Department> Get()
        {
            return m_DepartmentQueries.Get();
        }
    }
}
