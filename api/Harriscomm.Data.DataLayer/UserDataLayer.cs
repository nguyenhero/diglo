﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;

namespace Harriscomm.Data.DataLayer
{
    public class UserDataLayer : IUserDataLayer
    {
        private readonly IUserQueries m_UserQueries;
        public UserDataLayer(IUserQueries userQueries)
        {
            m_UserQueries = userQueries;
        }

        public IEnumerable<User> Get()
        {
            return m_UserQueries.Get();
        }
    }
}
