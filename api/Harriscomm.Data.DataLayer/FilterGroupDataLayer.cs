﻿using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer
{
    public class FilterGroupDataLayer: IFilterGroupDataLayer
    {
        private readonly IFilterGroupQueries m_FilterGroupQueries;
        public FilterGroupDataLayer(IFilterGroupQueries filterGroupQueries)
        {
            m_FilterGroupQueries = filterGroupQueries;
        }

        public IList<FilterGroup> Get(string departmentId, string classId, string subclassId, string kind, string filterId, string q)
        {
            return m_FilterGroupQueries.Get(departmentId, classId, subclassId, kind, filterId, q);
        }
    }
}
