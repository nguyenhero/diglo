﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Entities.Request;

namespace Harriscomm.Data.DataLayer
{
    public class OrderDataLayer : IOrderDataLayer
    {
        private readonly IOrderQueries m_OrderQueries;
        public OrderDataLayer(IOrderQueries orderQueries)
        {
            m_OrderQueries = orderQueries;
        }
        public int CheckPromoCode(string promoCode)
        {
            return m_OrderQueries.CheckPromoCode(promoCode);
        }
        public int Add(AddOrderRequest request)
        {
            return m_OrderQueries.Add(request);
        }
        public int AddReviewOrder(AddReviewOrdertRequest request)
        {
            return m_OrderQueries.AddReviewOrder(request);
        }
        public int AddShippingAddress(ShippingAddress request)
        {
            return m_OrderQueries.AddShippingAddress(request);
        }
        public int AddBillingAddress(BillingAddress request)
        {
            return m_OrderQueries.AddBillingAddress(request);
        }
        public BillingAddress GetBillingAddressByEmail(string email)
        {
            return m_OrderQueries.GetBillingAddressByEmail(email);
        }
        public IList<ShippingAddress> GetShippingAddressByCustNo(string custNo)
        {
            return m_OrderQueries.GetShippingAddressByCustNo(custNo);
        }

        public IList<BillingAddress> GetBillingAddressByCustNo(string custNo)
        {
            return m_OrderQueries.GetBillingAddressByCustNo(custNo);
        }
        public IList<OrderHead> Get(string custNo, string monthCount, string orderId)
        {
            return m_OrderQueries.Get(custNo, monthCount, orderId);
        }
        public int UpdateStatusByOrderId(int orderId, string status)
        {
            return m_OrderQueries.UpdateStatusByOrderId(orderId, status);
        }
    }
}
