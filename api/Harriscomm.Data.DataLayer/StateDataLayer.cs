﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Entities.Request;

namespace Harriscomm.Data.DataLayer
{
    public class StateDataLayer : IStateDataLayer
    {
        private readonly IStateQueries m_StateQueries;
        public StateDataLayer(IStateQueries stateQueries)
        {
            m_StateQueries = stateQueries;
        }        
        public IList<State> Get(string countryId)
        {
            return m_StateQueries.Get(countryId);
        }
    }
}