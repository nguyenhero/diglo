﻿using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer
{
    public class SuggestionSearchDataLayer : ISuggestionSearchDataLayer
    {
        private readonly ISuggestionSearchQueries m_SuggestionSearchQueries;
        public SuggestionSearchDataLayer(ISuggestionSearchQueries suggestionSearchQueries)
        {
            m_SuggestionSearchQueries = suggestionSearchQueries;
        }

        public List<string> Get(string querySearch)
        {
            return m_SuggestionSearchQueries.Get(querySearch);
        }
    }
}
