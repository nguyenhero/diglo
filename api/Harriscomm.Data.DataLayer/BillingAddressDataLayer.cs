﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;

namespace Harriscomm.Data.DataLayer
{
    public class BillingAddressDataLayer : IBillingAddressDataLayer
    {
        private readonly IBillingAddressQueries m_BillingAddressQueries;
        public BillingAddressDataLayer(IBillingAddressQueries billingAddressQueries)
        {
            m_BillingAddressQueries = billingAddressQueries;
        }
        public int Update(BillingAddress request)
        {
            return m_BillingAddressQueries.Update(request);
        }
        public int Add(BillingAddress request)
        {
            return m_BillingAddressQueries.Add(request);
        }

    }
}
