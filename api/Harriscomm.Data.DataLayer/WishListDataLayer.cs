﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Entities.Request;

namespace Harriscomm.Data.DataLayer
{
    public class WishListDataLayer : IWishListDataLayer
    {
        private readonly IWishListQueries m_WishListQueries;
        public WishListDataLayer(IWishListQueries WishListQueries)
        {
            m_WishListQueries = WishListQueries;
        }        
        public IList<WishList> Get(string countryId)
        {
            return m_WishListQueries.Get(countryId);
        }

        public int Add(AddWishListRequest request)
        {
            return m_WishListQueries.Add(request);
        }


        public int DeleteWishListByCustNo(string custNo)
        {
            return m_WishListQueries.DeleteWishListByCustNo(custNo);
        }

        public int DeleteById(int id)
        {
            return m_WishListQueries.DeleteById(id);
        }
    }
}