﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;

namespace Harriscomm.Data.DataLayer
{
    public class ShippingAddressDataLayer : IShippingAddressDataLayer
    {
        private readonly IShippingAddressQueries m_ShippingAddressQueries;
        public ShippingAddressDataLayer(IShippingAddressQueries shippingAddressQueries)
        {
            m_ShippingAddressQueries = shippingAddressQueries;
        }
        public int Update(ShippingAddress request)
        {
            return m_ShippingAddressQueries.Update(request);
        }
        public int Add(ShippingAddress request)
        {
            return m_ShippingAddressQueries.Add(request);
        }
    }
}
