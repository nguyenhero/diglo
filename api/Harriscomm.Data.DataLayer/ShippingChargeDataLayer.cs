﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Entities.Request;

namespace Harriscomm.Data.DataLayer
{
    public class ShippingChargeDataLayer : IShippingChargeDataLayer
    {
        private readonly IShippingChargeQueries m_ShippingChargeQueries;
        public ShippingChargeDataLayer(IShippingChargeQueries shippingChargeQueries)
        {
            m_ShippingChargeQueries = shippingChargeQueries;
        }
        public decimal GetFee(ShippingCharge request)
        {
            return m_ShippingChargeQueries.GetFee(request);
        }
    }
}
