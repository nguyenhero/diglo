﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Entities.Request;

namespace Harriscomm.Data.DataLayer
{
    public class OrderDetailsDataLayer : IOrderDetailsDataLayer
    {
        private readonly IOrderDetailsQueries m_OrderDetailsQueries;
        public OrderDetailsDataLayer(IOrderDetailsQueries OrderDetailsQueries)
        {
            m_OrderDetailsQueries = OrderDetailsQueries;
        }

        public IList<OrderDetail> Get(int orderId)
        {
           return m_OrderDetailsQueries.Get(orderId);
        }
    }
}