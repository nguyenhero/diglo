﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Entities.Request;

namespace Harriscomm.Data.DataLayer
{
    public class CustomerDataLayer : ICustomerDataLayer
    {
        private readonly ICustomerQueries m_CustomerQueries;
        public CustomerDataLayer(ICustomerQueries customerQueries)
        {
            m_CustomerQueries = customerQueries;
        }

        public Customer Get(string email, string password)
        {
            int errorNumber = 0;
            Customer customer = new Customer();

            IEnumerable<Customer> customers = m_CustomerQueries.Get(email, password, ref errorNumber);
            if (customers != null && customers.Count() > 0)
            {
                customer = customers.FirstOrDefault();
            }
            else customer.ErrorNumber = errorNumber;

            return customer;
        }
        public IEnumerable<ProductCart> GetCart(int custNo)
        {
            return m_CustomerQueries.GetCart(custNo);
        }
        public IEnumerable<Product> GetProdRecent(int custNo)
        {
            return m_CustomerQueries.GetProdRecent(custNo);
        }
        public IEnumerable<CreditCards> GetCredit(int custNo)
        {
            return m_CustomerQueries.GetCredit(custNo);
        }    
        public IEnumerable<Customer> GetByEmail(string email)
        {
            return m_CustomerQueries.GetByEmail(email);
        }
        public int CreateCredit(CreateCreditRequest request)
        {
            return m_CustomerQueries.CreateCredit(request);
        }
        public int CreateUser(CreateUserRequest request)
        {
            return m_CustomerQueries.CreateUser(request);
        }
        public int CreateCart(CreateCartDetailRequest request)
        {
            return m_CustomerQueries.CreateCart(request);
        }
        public int CreateProdRecent(CreateProdRecentRequest request)
        {
            return m_CustomerQueries.CreateProdRecent(request);
        }
        public int DelCredit(CreateCreditRequest request)
        {
            return m_CustomerQueries.DelCredit(request);
        }
        public int CheckCredit(CreateCreditRequest request)
        {
            return m_CustomerQueries.CheckCredit(request);
        }
        public int DelCart(DelCartRequest request)
        {
            return m_CustomerQueries.DelCart(request);
        }
        public int UpdateUser(UpdateUserRequest request)
        {
            return m_CustomerQueries.UpdateUser(request);
        }

        public int UpdateUserPassword(UpdateUserPasswordRequest request)
        {
            return m_CustomerQueries.UpdateUserPassword(request);
        }

        public int UpdateUserByCustomerNo(ResetPasswordRequest request)
        {
            return m_CustomerQueries.UpdateUserByCustomerNo(request);
        }
    }
}
