﻿using Harriscomm.Data.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Entities.Request;

namespace Harriscomm.Data.DataLayer
{
    public class CustomerCatDataLayer : ICustomerCatDataLayer
    {
        private readonly ICustomerCatQueries m_CustomerCatQueries;
        public CustomerCatDataLayer(ICustomerCatQueries customerCatQueries)
        {
            m_CustomerCatQueries = customerCatQueries;
        }        
        public int CreateFreeCatalog(CreateFreeCatalogRequest request)
        {
            return m_CustomerCatQueries.CreateFreeCatalog(request);
        }
    }
}