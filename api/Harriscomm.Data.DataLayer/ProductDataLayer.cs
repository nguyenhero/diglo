﻿using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer.Interfaces
{
    public class ProductDataLayer : IProductDataLayer
    {
        private readonly IProductQueries m_ProductQueries;
        public ProductDataLayer(IProductQueries productQueries)
        {
            m_ProductQueries = productQueries;
        }
        public IEnumerable<Product> Get(GetProductRequest request)
        {
            return m_ProductQueries.Get(request);
        }
        public int GetCount(GetProductRequest request)
        {
            return m_ProductQueries.GetCount(request);
        }
        public IEnumerable<Product> GetByProductSKU(string productSKU, bool parent)
        {
            return m_ProductQueries.GetByProductSKU(productSKU, parent);
        }
        public IEnumerable<ProductAdditionInfo> GetProductAdditionInfo(string productSKU, int class_subclassid)
        {
            return m_ProductQueries.GetProductAdditionInfo(productSKU,class_subclassid);
        }
        public IEnumerable<ProductCount> GetProductCount()
        {
            return m_ProductQueries.GetProductCount();
        }
        public IEnumerable<Product> GetProductRelated(string productSKU)
        {
            return m_ProductQueries.GetProductRelated(productSKU);
        }
    }
}
