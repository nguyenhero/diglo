﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class BillingAddressQueries : ConnectionBase, IBillingAddressQueries
    {
        public BillingAddressQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public int Update(BillingAddress request)
        {
            request.UpdateDatetime = DateTime.Now;
            string sql = OrderTransalator.UpdateBillingAdress();
            return Execute(connection =>
            {
                return connection.Execute(sql, request);
            });
        }
        public int Add(BillingAddress request)
        {
            request.CreateDatetime = DateTime.Now;
            string sql = OrderTransalator.CreateBillingAdress();
            return Execute(connection =>
            {
                int result = 0;
                string sqlCount = "SELECT count(1) FROM [harriswebdb].[BillingAddress] WHERE " +
                                    "[CustNo] = @CustNo AND " +
                                    "[BillingFirstName] = @FirstName AND " +
                                    "[BillingLastName] = @LastName AND " +
                                    "[BillingCompany] = @Company AND " +
                                    "[BillingAddress] = @Address AND " +
                                    "[BillingApt] = @Apt AND " +
                                    "[BillingCity] = @City AND " +
                                    "[BillingState] = @State AND " +
                                    "[BillingZip] = @Zip AND " +
                                    "[BillingCountry] = @Country AND " +
                                    "[BillingPhone] = @Phone AND " +
                                    "[BillingEmail] = @Email ";
                var count = connection.ExecuteScalar<int>(sqlCount, request);
                if (count == 0)
                {
                    sql = OrderTransalator.CreateBillingAdress();
                    result = connection.Execute(sql, request);
                }
                if (result == 1) {
                    sql = OrderTransalator.CreateShippingAdress();
                    result = connection.Execute(sql, CreateShippingAddress(request));
                }
                return result;
            });
        }
        public ShippingAddress CreateShippingAddress(BillingAddress request)
        {
            ShippingAddress shippingAddress = new ShippingAddress();
            shippingAddress.Address = request.Address;
            shippingAddress.Apt = request.Apt;
            shippingAddress.AddrID = request.AddrID;
            shippingAddress.City = request.City;
            shippingAddress.Company = request.Company;
            shippingAddress.Country = request.Country;
            shippingAddress.CreateDatetime = DateTime.Now;
            shippingAddress.CustNo = request.CustNo;
            shippingAddress.Email = request.Email;
            shippingAddress.FirstName = request.FirstName;
            shippingAddress.FullName = request.FullName;
            shippingAddress.LastName = request.LastName;
            shippingAddress.Phone = request.Phone;
            shippingAddress.PrimaryAddress = request.PrimaryAddress;
            shippingAddress.State = request.State;
            shippingAddress.Zip = request.Zip;
            return shippingAddress;
        }
    }
}

