﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class UserQueries : ConnectionBase, IUserQueries
    {
        public UserQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public IEnumerable<User> Get()
        {
            return Execute(connection =>
               connection.Query<User>(Procedures.CustomerGet,
                   commandType: CommandType.Text));
        }
    }
}
