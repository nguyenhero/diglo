﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class WishListQueries : ConnectionBase, IWishListQueries
    {
        public WishListQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public int Add(AddWishListRequest request)
        {
            request.CreateDatetime = DateTime.Now;
            return Execute(connection =>
            {
                int result = 0;
                string sqlCount = "SELECT count(1) FROM [harriswebdb].[WishList] WHERE " +
                                     "[CustNo] = @CustNo AND " +
                                     "[ProductSKU] = @ProductSKU ";
                var count = connection.ExecuteScalar<int>(sqlCount, request);
                string sql = (count == 0)
                        ? WishListTransalator.CreateWishList()
                        : WishListTransalator.UpdateWishList();
                result = connection.Execute(sql, request);
                return result;
            });
        }
        public IList<WishList> Get(string custNo)
        {
            string sql = WishListTransalator.GetWishList();
            return Execute(connection =>
            {
                var result = connection.Query<WishList, Product, WishList>
                (sql, (wishList, product) => { wishList.Product = product; return wishList; },
                new { CustNo = custNo }, splitOn: "ProductSKU");
                if (result != null && result.Count() > 0)
                {
                    return result.ToList();
                }
                return null;
            });
        }
        public int DeleteWishListByCustNo(string custNo)
        {
            string sql = WishListTransalator.DeleteWishListByCustNo();
            return Execute(connection =>
            {
                return connection.Execute(sql, new { CustNo = custNo});
            });
        }
        public int DeleteById(int id)
        {
            string sql = WishListTransalator.DeleteById();
            return Execute(connection =>
            {
                return connection.Execute(sql, new { WishID = id });
            });
        }
    }
}

