﻿using Dapper;
using Harriscomm.Entities.Enum;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Data.Queries
{
    public static class WishListTransalator
    {
        public static string GetWishList()
        {
            string sqlWishList =
                "SELECT * " +
                "FROM [harriswebdb].[WishList] w " +
                "JOIN[harriswebdb].[Products] p ON w.[ProductSKU] = p.[ProductSKU]";
            return sqlWishList;
        }
        public static string DeleteById()
        {
            string sqlWishList =
              "DELETE  " +
              "FROM [harriswebdb].[WishList] " +
              "WHERE [WishID] = @WishID";
            return sqlWishList;
        }
        public static string DeleteWishListByCustNo()
        {
            string sqlWishList =
                "DELETE  " +
                "FROM [harriswebdb].[WishList] " +
                " WHERE " +
                "[CustNo] = @CustNo";
            return sqlWishList;
        }
        public static string CreateWishList()
        {
            string sqlWishList =
                "INSERT INTO [harriswebdb].[WishList]" +
                "([CustNo]" +
                ",[ProductSKU]" +
                ",[WishItemQty]" +
                ",[Status]" +
                ",[CreateDatetime]" +
                ",[Email])" +
                " VALUES" +
                "(@CustNo" +
                ",@ProductSKU" +
                ",@WishItemQty" +
                ",@Status" +
                ",@CreateDatetime" +
                ",@Email)";
            return sqlWishList;
        }
        public static string UpdateWishList()
        {
            string sqlWishList =
                "UPDATE [harriswebdb].[WishList] " +
                "SET " +
                " [WishItemQty] = [WishItemQty] + @WishItemQty, " +
                " [UpdateDatetime] = @UpdateDatetime, " +
                " [UpdateUser] = @UpdateUser " +
                " WHERE " +
                 "[CustNo] = @CustNo AND " +
                 "[ProductSKU] = @ProductSKU ";
            return sqlWishList;
        }
    }
}
