﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Data.Queries
{
    public static class ProductTransalator
    {
        public static string CreateTempaleSortBy(GetProductRequest request)
        {
            string sqlWhere = "[ProductParent] IS NULL AND IsDelete = 0 AND Status = 1 ", sqlSearch = "",
                orderBy = "", sqlMenu = "", sqlFilter = "";
            switch (request.SortBy)
            {
                case SortBy.NewAndBestselling:
                    orderBy = " NewArrivalInd DESC, CreateDate DESC, TotalOrder DESC, ReviewRate DESC, ";
                    break;
                case SortBy.AvgCustomerRating:
                    orderBy = " ReviewRate DESC, ";
                    break;
                case SortBy.PriceLowToHigh:
                    orderBy = " WebPrice ASC, ";
                    break;
                case SortBy.PriceHighToLow:
                    orderBy = " WebPrice DESC, ";
                    break;
                case SortBy.NewestToOldest:
                    orderBy = " CreateDatetime DESC, ";
                    break;
                default:
                    break;
            }
            orderBy += "ProductID DESC ";
            sqlSearch = CreateRequestSearch(request);
            string sql = string.Empty;
            if (request.ClassId == 12 || request.ClassId == 14)
            {
                sqlFilter = CreateFilters(request, true);
                sql =
                "SELECT *, " +
                "CASE NewArrivalInd WHEN 'Y' THEN  CreateDatetime  ELSE  GETDATE() END AS CreateDate " +
                "FROM (SELECT *, " + request.SubClassId + " AS URLID FROM [harriswebdb].[Products] ) r " +
                "WHERE URLID IN (SELECT * FROM dbo.splitstring(URL)) AND " +
                sqlWhere +
                sqlSearch +
                sqlFilter +
                " ORDER BY " + orderBy ;
            }
            else
            {
                sqlMenu = CreateTempaleByMenu(request);
                sqlFilter = CreateFilters(request);
                sql =
                    "SELECT p.*, " +
                    "CASE NewArrivalInd WHEN 'Y' THEN  CreateDatetime  ELSE  GETDATE() END AS CreateDate " +
                    "FROM [harriswebdb].[Products] p " +
                    "inner join [harriswebdb].[MenuProducts] m on p.ProductID = m.ProductID " +
                    "WHERE " +
                    sqlWhere +
                    sqlSearch +
                    sqlMenu +
                    sqlFilter +
                    " ORDER BY " + orderBy ;
            }
            return sql;
        }
        public static string CreateTempaleByMenu(GetProductRequest request)
        {
            string sql = " ";
            sql += request.DepartmentId > 0 ? " AND m.DepartmentID = " + request.DepartmentId : string.Empty;
            sql += request.ClassId > 0 ? " AND m.ClassID = " + request.ClassId : string.Empty;
            sql += request.SubClassId > 0 ? " AND m.SubclassID = " + request.SubClassId : string.Empty;
            sql += !string.IsNullOrEmpty(request.NewArrivalInd) && request.NewArrivalInd.Equals("new")
                    ? " AND NewArrivalInd = 'Y'" : string.Empty;
            sql += !string.IsNullOrEmpty(request.NewArrivalInd) && request.NewArrivalInd.Equals("clearance")
                    ? " AND ClearanceInd = 'Y'" : string.Empty;
            return sql;
        }
        public static string CreateFilters(GetProductRequest request, bool signalers = false)
        {
            GetProductRequest[] f;
            string whereSql = string.Empty;//string.Format("{0} {1} {2}", filterDetails);
            string whereSqlG1 = "1=1"; 
            string whereSqlG2 = "1=1"; 
            string whereSqlG3 = "1=1";  
            string whereSqlG4 = "1=1"; 
            string whereSqlG5 = "1=1"; 
            string whereSqlG6 = "1=1";  
            string whereSqlG7 = "1=1"; 
            string whereSqlG8 = "1=1";  
            string whereSqlG9 = "1=1"; 
            string whereSqlG10 = "1=1";  
            string whereSqlG11 = "1=1";  
            string whereSqlG12 = "1=1";  
            string whereSqlG14 = "1=1"; 
            int G1 = 0; 
            int G2 = 0;
            int G3 = 0; 
            int G4 = 0;
            int G5 = 0; 
            int G6 = 0;
            int G7 = 0; 
            int G8 = 0;
            int G9 = 0; 
            int G10 = 0;
            int G11 = 0; 
            int G12 = 0;      
            int G14 = 0;                                                   
            if (request.SearchColumns != null && request.SearchColumns.Count() > 0)
            {
                for (int i = 0; i < request.SearchColumns.Count(); i++)
                {
                    var value = request.Operations[i].ToLower() == "like"
                            ? string.Format("'%{0}%'", request.FilterDetailValues[i])
                            : request.FilterDetailValues[i];
                    if (request.FilterGroupIds[i] == 1)  {
                       if (G1 == 0) {
                           whereSqlG1 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G1 = 1;
                        }
                        else whereSqlG1 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    } 
                    else if (request.FilterGroupIds[i] == 2) {
                        if (G2 == 0) {
                           whereSqlG2 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G2 = 1;
                        }
                        else whereSqlG2 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 3) {
                        if (G3 == 0) {
                           whereSqlG3 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G3 = 1;
                        }
                        else whereSqlG3 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 4) {
                        if (G4 == 0) {
                           whereSqlG4 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G4 = 1;
                        }
                        else whereSqlG4 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 5) {
                        if (G5 == 0) {
                           whereSqlG5 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G5 = 1;
                        }
                        else whereSqlG5 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 6) {
                        if (G6 == 0) {
                           whereSqlG6 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G6 = 1;
                        }
                        else whereSqlG6 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 7) {
                        if (G7 == 0) {
                           whereSqlG7 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G7 = 1;
                        }
                        else whereSqlG7 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 8) {
                        if (G8 == 0) {
                           whereSqlG8 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G8 = 1;
                        }
                        else whereSqlG8 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 9) {
                        if (G9 == 0) {
                           whereSqlG9 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G9 = 1;
                        }
                        else whereSqlG9 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 10) {
                        if (G10 == 0) {
                           whereSqlG10 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G10 = 1;
                        }
                        else whereSqlG10 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 11) {
                        if (G11 == 0) {
                           whereSqlG11 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G11 = 1;
                        }
                        else whereSqlG11 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 12) {
                        if (G12 == 0) {
                           whereSqlG12 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G12 = 1;
                        }
                        else whereSqlG12 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                    else if (request.FilterGroupIds[i] == 14) {
                        if (G14 == 0) {
                           whereSqlG14 = string.Format("{0} {1} {2} {3}", string.Empty , request.SearchColumns[i], request.Operations[i], value);
                           G14 = 1;
                        }
                        else whereSqlG14 += string.Format("{0} {1} {2} {3}", " OR " , request.SearchColumns[i], request.Operations[i], value);
                    }
                }
                whereSql = "(" + whereSqlG1 + ") AND (" + whereSqlG2 + ") AND (" + whereSqlG3 + ") AND (" + whereSqlG4 + ") AND (" + whereSqlG5 + ") AND (" + whereSqlG6 + ") AND (" + whereSqlG7 + ") AND (" + whereSqlG8 + ") AND (" + whereSqlG9 + ") AND (" + whereSqlG10 + ") AND (" + whereSqlG11 + ") AND (" + whereSqlG12 + ") AND (" + whereSqlG14 + ")" ;
            }
            return !string.IsNullOrEmpty(whereSql) ? string.Format(" {0} ({1})", " AND ", whereSql) : string.Empty;
        }
        public static string CreateTempaleCountSortBy(GetProductRequest request)
        {            
            string sqlWhere = "[ProductParent] IS NULL AND IsDelete = 0 AND Status = 1", sqlSearch = "", sqlMenu = "", sqlFilter = "";
            sqlSearch = CreateRequestSearch(request);
            string sql = string.Empty;
            if (request.ClassId == 12 || request.ClassId == 14)
            {
                sqlFilter = CreateFilters(request, true);
                sql =
                "SELECT count(1) " +
                "FROM (SELECT *, " + request.SubClassId + " AS URLID FROM [harriswebdb].[Products] ) r " +
                "WHERE URLID IN (SELECT * FROM dbo.splitstring(URL)) AND " +
                sqlWhere +
                sqlSearch +
                sqlFilter;
            }
            else
            {
                sqlMenu = CreateTempaleByMenu(request);
                sqlFilter = CreateFilters(request);
                sql =
                "SELECT count(1) " +
                "FROM [harriswebdb].[Products] " +
                "WHERE " +
                sqlWhere +
                sqlSearch +
                sqlMenu +
                sqlFilter;
            }
            return sql;
        }
        public static string CreateRequestSearch(GetProductRequest request)
        {
            string strReturn = string.Empty;
            string strReqSearch = request.q;
            if (!(string.IsNullOrEmpty(strReqSearch))) 
            {
                strReturn = " AND ( ProductID IN ( SELECT ProductID FROM [harriswebdb].[ProductGenSearch] WHERE DataSearch LIKE " + string.Format("'%{0}%'", strReqSearch) + " ) ) ";
            }
            return strReturn;
        }
    }
}
