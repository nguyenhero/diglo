﻿using Dapper;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public static class CustomerCatTransalator
    {
        public static DynamicParameters CreateCreateFreeCatalogParameters(CreateFreeCatalogRequest request)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@CustNo", request.CustNo);
            parameters.Add("@Type", request.Type);
            parameters.Add("@CatFirstName", request.FirstName.Trim());
            parameters.Add("@CatLastName", request.LastName.Trim());
            parameters.Add("@CatEmail", request.Email.Trim().ToLower());
            parameters.Add("@CatCompany", request.Company.Trim());
            parameters.Add("@CatCountry", request.Country.Trim());
            parameters.Add("@CatAddress", request.Address.Trim());
            parameters.Add("@CatApt", request.Apt.Trim());
            parameters.Add("@CatCity", request.City.Trim());
            parameters.Add("@CatState", request.State.Trim());
            parameters.Add("@CatZip", request.ZipCode.Trim());
            parameters.Add("@CatCreated", request.CreatedDate);
            parameters.Add("@ErrorNumber", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
    } 
}
