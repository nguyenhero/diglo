﻿using Dapper;
using Harriscomm.Entities.Enum;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public static class OrderTransalator
    {
        public static string CreateCustomerCat()
        {
            string sql =
                "INSERT INTO [harriswebdb].[CustomerCat] " +
                " (" +
                "[CustNo]" +
                ",[CatFirstName]" +
                ",[CatLastName]" +
                ",[CustFullName]" +
                ",[CatEmail]" +
                ",[CatAddress]" +
                ",[CatApt]" +
                ",[CatCompany]" +
                ",[CatCity]" +
                ",[CatState]" +
                ",[CatZip]" +
                ",[CatCountry]" +
                ",[CatCreated]) " +
                "VALUES" +
                "@CustNo" +
                ",@CatFirstName" +
                ",@CatLastName" +
                ",rtrim(@CatFirstName)+' '+rtrim(@CatLastName)" +
                ",@CatEmail" +
                ",@CatAddress" +
                ",@CatApt" +
                ",@CatCompany" +
                ",@CatCity" +
                " ,@CatState" +
                ",@CatZip" +
                ",@CatCountry" +
                ",@CatCreated) ";
            return sql;
        }
        public static string CreateUser()
        {
            string sql =
                "INSERT INTO [harriswebdb].[Customer] " +
                " ( " +
                " CustNo" +
                ",CustFirstName" +
                ",CustLastName" +
                ",CustLFullName" +
                ",CustEmail" +
                ",CustPassword" +
                ",CustCreated" +
                ",CustType " +
                ",CustStatus " +
                ",CustCatStatus) " +
                " VALUES( " +
                "@CustNo " +
                ",@FirstName" +
                ",@LastName" +
                ",rtrim(@FirstName) + ' ' + rtrim(@LastName)" +
                ",@Email" +
                ",@Password" +
                ",@CreatedDate" +
                ",@Type" +
                ",@Status" +
                ",@CatStatus)";
            return sql;
        }
        public static string CreateShippingAdress()
        {
            string sqlShippingAdress =
                "INSERT INTO [harriswebdb].[ShippingAddress]" +
                "([CustNo]" +
                ",[PrimaryAddress]" +
                ",[ShippingFirstName]" +
                ",[ShippingLastName]" +
                ",[ShippingfullName]" +
                ",[ShippingCompany]" +
                ",[ShippingAddress]" +
                ",[ShippingApt]" +
                ",[ShippingCity]" +
                ",[ShippingState]" +
                ",[ShippingZip]" +
                ",[ShippingCountry]" +
                ",[ShippingPhone]" +
                ",[ShippingEmail]" +
                ",[CreateDatetime]" +
                ",[UpdateUser])" +
                " VALUES" +
                "(@CustNo" +
                ",@PrimaryAddress" +
                ",@FirstName" +
                ",@LastName" +
                ", @FullName" +
                ",@Company" +
                ",@Address" +
                ",@Apt" +
                ",@City" +
                ",@State" +
                ",@Zip" +
                ",@Country" +
                ",@Phone" +
                ",@Email" +
                ",@CreateDatetime" +
                ",@UpdateUser)";
            return sqlShippingAdress;
        }
        public static string UpdateShippingAdress()
        {
            string sqlShippingAdress =
                "UPDATE [harriswebdb].[ShippingAddress] " +
                "SET " +
                " [ShippingFirstName] = @FirstName " +
                ",[ShippingLastName] = @LastName " +
                ",[ShippingCompany] = @Company " +
                ",[ShippingAddress] = @Address " +
                ",[ShippingApt] = @Apt " +
                ",[ShippingCity] = @City " +
                ",[ShippingState] = @State " +
                ",[ShippingZip] = @Zip " +
                ",[ShippingCountry] = @Country " +
                ",[ShippingPhone] = @Phone " +
                ",[ShippingEmail] = @Email " +
                ",[UpdateDatetime] = @UpdateDatetime " +
                ",[UpdateUser] = @UpdateUser " +
                 "WHERE ShipAddrID =  @AddrID";
            return sqlShippingAdress;
        }
        public static string UpdateBillingAdress()
        {
            string sqlShippingAdress =
                "UPDATE [harriswebdb].[BillingAddress] " +
                "SET " +
                " [BillingFirstName] = @FirstName " +
                ",[BillingLastName] = @LastName " +
                ",[BillingCompany] = @Company " +
                ",[BillingAddress] = @Address " +
                ",[BillingApt] = @Apt " +
                ",[BillingCity] = @City " +
                ",[BillingState] = @State " +
                ",[BillingZip] = @Zip " +
                ",[BillingCountry] = @Country " +
                ",[BillingPhone] = @Phone " +
                ",[BillingEmail] = @Email " +
               ",[UpdateDatetime] = @UpdateDatetime " +
                ",[UpdateUser] = @UpdateUser " +
                 "WHERE BillAddrID =  @AddrID";
            return sqlShippingAdress;
        }
        public static string CreateBillingAdress()
        {
            string sqlBillingAdress =
                "INSERT INTO [harriswebdb].[BillingAddress]" +
                "([CustNo]" +
                ",[PrimaryAddress]" +
                ",[BillingFirstName]" +
                ",[BillingLastName]" +
                ",[BillingfullName]" +
                ",[BillingCompany]" +
                ",[BillingAddress]" +
                ",[BillingApt]" +
                ",[BillingCity]" +
                ",[BillingState]" +
                ",[BillingZip]" +
                ",[BillingCountry]" +
                ",[BillingPhone]" +
                ",[BillingEmail]" +
                ",[CreateDatetime]" +
                ",[UpdateUser])" +
                "VALUES" +
                "(@CustNo" +
                ", @PrimaryAddress" +
                ", @FirstName" +
                ", @LastName" +
                ", @FullName" +
                ", @Company" +
                ", @Address" +
                ", @Apt" +
                ", @City" +
                ", @State" +
                ", @Zip" +
                ", @Country" +
                ", @Phone" +
                ", @Email" +
                ", @CreateDatetime" +
                ", @UpdateUser)";
            return sqlBillingAdress;
        }
        public static string CreateOrderDetails()
        {
            string sqlOrderDetails =
                "INSERT INTO [harriswebdb].[OrderDetail]" +
                "([OrderID]" +
                ",[ProductSKU]" +
                ",[WebPrice]" +
                ",[CartPrice]" +
                ",[Quantity]" +
                ",[Tax])" +
                "VALUES(" +
                "@OrderID" +
                ",@ProductSKU" +
                ",@WebPrice" +
                ",@CartPrice" +
                ",@Quantity" +
                ",@Tax" +
                ")";
            return sqlOrderDetails;
        }
        public static string CreateOrderDetailsNav()
        {
            string sqlOrderDetails =
                "INSERT INTO [harriswebdb].[OrderDetailNav]" +
                "([OrderID]" +
                ",[ProductSKU]" +
                ",[WebPrice]" +
                ",[CartPrice]" +
                ",[Quantity]" +
                ",[Tax])" +
                "VALUES(" +
                "@OrderID" +
                ",@ProductSKU" +
                ",@WebPrice" +
                ",@CartPrice" +
                ",@Quantity" +
                ",@Tax" +
                ")";
            return sqlOrderDetails;
        }
        public static string CreateOrderHead()
        {
            string sqlOrderHead =
                "INSERT INTO[harriswebdb].[OrderHead]" +
               "([CustNo]" +
               ",[BillingFirstName]" +
               ",[BillingLastName]" +
               ",[BillingFullName]" +
               ",[BillingAddress]" +
               ",[BillingApt]" +
               ",[BillingCity]" +
               ",[BillingState]" +
               ",[BillingZip]" +
               ",[BillingCountry]" +
               ",[BillingPhone]" +
               ",[BillingEmail]" +
               ",[BillingFax]" +
               ",[BillingCompany]" +
               ",[ShippingFirstName]" +
               ",[ShippingLastName]" +
               ",[ShippingFullName]" +
               ",[ShippingAddress]" +
               ",[ShippingApt]" +
               ",[ShippingCity]" +
               ",[ShippingState]" +
               ",[ShippingZip]" +
               ",[ShippingCountry]" +
               ",[ShippingPhone]" +
               ",[ShippingEmail]" +
               ",[ShippingFax]" +
               ",[ShippingCompany]" +
               ",[ShipProvider]" +
               ",[ShipProviderCode]" +
               ",[ShipType]" +
               ",[ShipFee]" +
               ",[ShipDesc]" +
               ",[CreateDateTime]" +
               ",[ShipDateTime]" +
               ",[PaymentType]" +
               ",[PromoCode]" +
               ",[Tax]" +
               ",[Total]" +
               ",[Status]" +
               ",[Isexported])" +
               "VALUES" +
               "(@CustNo" +
               ", @BillingFirstName" +
               ", @BillingLastName" +
               ", @BillingFullName" +
               ", @BillingAddress" +
               ", @BillingApt" +
               ", @BillingCity" +
               ", @BillingState" +
               ", @BillingZip" +
               ", @BillingCountry" +
               ", @BillingPhone" +
               ", @BillingEmail" +
               ", @BillingFax" +
               ", @BillingCompany" +
               ", @ShippingFirstName" +
               ", @ShippingLastName" +
               ", @ShippingFullName" +
               ", @ShippingAddress" +
               ", @ShippingApt" +
               ", @ShippingCity" +
               ", @ShippingState" +
               ", @ShippingZip" +
               ", @ShippingCountry" +
               ", @ShippingPhone" +
               ", @ShippingEmail" +
               ", @ShippingFax" +
               ", @ShippingCompany" +
               ", @ShipProvider" +
               ", @ShipProviderCode" +
               ", @ShipType" +
               ", @ShipFee" +
               ", @ShipDesc" +
               ", @CreateDateTime" +
               ", @ShipDateTime" +
               ", @PaymentType" +
               ", @PromoCode" +
               ", @Tax" +
               ", @Total" +
               ", @Status" +
               ", @Isexported)" +
               "SELECT CAST(SCOPE_IDENTITY() as int)";
            return sqlOrderHead;
        }
        // public static string CreateOrderHeadNav()
        // {
        //     string sqlOrderHead =
        //         "INSERT INTO[harriswebdb].[OrderHeadNav]" +
        //        "([CustNo]" +
        //        ",[BillingFirstName]" +
        //        ",[BillingLastName]" +
        //        ",[BillingFullName]" +
        //        ",[BillingAddress]" +
        //        ",[BillingApt]" +
        //        ",[BillingCity]" +
        //        ",[BillingState]" +
        //        ",[BillingZip]" +
        //        ",[BillingCountry]" +
        //        ",[BillingPhone]" +
        //        ",[BillingEmail]" +
        //        ",[BillingFax]" +
        //        ",[BillingCompany]" +
        //        ",[ShippingFirstName]" +
        //        ",[ShippingLastName]" +
        //        ",[ShippingFullName]" +
        //        ",[ShippingAddress]" +
        //        ",[ShippingApt]" +
        //        ",[ShippingCity]" +
        //        ",[ShippingState]" +
        //        ",[ShippingZip]" +
        //        ",[ShippingCountry]" +
        //        ",[ShippingPhone]" +
        //        ",[ShippingEmail]" +
        //        ",[ShippingFax]" +
        //        ",[ShippingCompany]" +
        //        ",[ShipProvider]" +
        //        ",[ShipProviderCode]" +
        //        ",[ShipType]" +
        //        ",[ShipFee]" +
        //        ",[ShipDesc]" +
        //        ",[CreateDateTime]" +
        //        ",[ShipDateTime]" +
        //        ",[PaymentType]" +
        //        ",[PromoCode]" +
        //        ",[Tax]" +
        //        ",[Total]" +
        //        ",[Status]" +
        //        ",[Isexported])" +
        //        "VALUES" +
        //        "(@CustNo" +
        //        ", @BillingFirstName" +
        //        ", @BillingLastName" +
        //        ", @BillingFullName" +
        //        ", @BillingAddress" +
        //        ", @BillingApt" +
        //        ", @BillingCity" +
        //        ", @BillingState" +
        //        ", @BillingZip" +
        //        ", @BillingCountry" +
        //        ", @BillingPhone" +
        //        ", @BillingEmail" +
        //        ", @BillingFax" +
        //        ", @BillingCompany" +
        //        ", @ShippingFirstName" +
        //        ", @ShippingLastName" +
        //        ", @ShippingFullName" +
        //        ", @ShippingAddress" +
        //        ", @ShippingApt" +
        //        ", @ShippingCity" +
        //        ", @ShippingState" +
        //        ", @ShippingZip" +
        //        ", @ShippingCountry" +
        //        ", @ShippingPhone" +
        //        ", @ShippingEmail" +
        //        ", @ShippingFax" +
        //        ", @ShippingCompany" +
        //        ", @ShipProvider" +
        //        ", @ShipProviderCode" +
        //        ", @ShipType" +
        //        ", @ShipFee" +
        //        ", @ShipDesc" +
        //        ", @CreateDateTime" +
        //        ", @ShipDateTime" +
        //        ", @PaymentType" +
        //        ", @PromoCode" +
        //        ", @Tax" +
        //        ", @Total" +
        //        ", @Status" +
        //        ", @Isexported)" +
        //        "SELECT CAST(SCOPE_IDENTITY() as int)";
        //     return sqlOrderHead;
        // }
        public static string CreateReviewOrderHead()
        {
            string sqlOrderHead =
                "INSERT INTO[harriswebdb].[OrderHead]" +
               "([CustNo]" +
               ",[BillingFirstName]" +
               ",[BillingLastName]" +
               ",[BillingFullName]" +
               ",[BillingCountry]" +
               ",[BillingEmail]" +
               ",[ShippingFullName]" +
               ",[ShippingAddress]" +
               ",[ShippingApt]" +
               ",[ShippingCity]" +
               ",[ShippingState]" +
               ",[ShippingZip]" +
               ",[ShippingCountry]" +
               ",[ShipProvider]" +
               ",[ShipType]" +
               ",[ShipFee]" +
               ",[ShipDesc]" +
               ",[CreateDateTime]" +
               ",[ShipDateTime]" +
               ",[PaymentType]" +
               ",[PromoCode]" +
               ",[Total])" +
               "VALUES" +
               "(@CustNo" +
               ", @BillingFirstName" +
               ", @BillingLastName" +
               ", @BillingFullName" +
               ", @BillingCountry" +
               ", @BillingEmail" +
               ", @ShippingFullName" +
               ", @ShippingAddress" +
               ", @ShippingApt" +
               ", @ShippingCity" +
               ", @ShippingState" +
               ", @ShippingZip" +
               ", @ShippingCountry" +
               ", @ShipProvider" +
               ", @ShipType" +
               ", @ShipFee" +
               ", @ShipDesc" +
               ", @CreateDateTime" +
               ", @ShipDateTime" +
               ", @PaymentType" +
               ", @PromoCode" +
               ", @Total) " +
               "SELECT CAST(SCOPE_IDENTITY() as int)";
            return sqlOrderHead;
        }
        public static string CreateShippingCharge()
        {
            string sql =
                "SELECT ChargeFee " +
                "FROM harriswebdb.ShippingCharge " +
                "WHERE CountryID = @CountryID AND " +
                "StateID= @StateID and @Total " +
                "BETWEEN TotalMin and TotalMax ";
            return sql;
        }
    }
}
