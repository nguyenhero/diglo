﻿using Dapper;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public static class CustomerTransalator
    {
        public static DynamicParameters CreateGetParameters(string email, string password)
        {
            DynamicParameters parameters = new DynamicParameters();          
            parameters.Add("@Password", password);
            parameters.Add("@Email", email.Trim().ToLower());
            parameters.Add("@CustLastLogin", DateTime.Now);
            parameters.Add("@ErrorNumber", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        public static DynamicParameters CreateCreateUserParameters(CreateUserRequest request)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@FirstName", request.FirstName.Trim());
            parameters.Add("@LastName", request.LastName.Trim());
            parameters.Add("@Password", request.Password.Trim());
            parameters.Add("@Email", request.Email.Trim().ToLower());
            parameters.Add("@Type", request.Type);
            parameters.Add("@CustCatStatus", request.CatStatus);
            parameters.Add("@Status", request.Status);
            parameters.Add("@Created", request.CreatedDate);
            parameters.Add("@ErrorNumber", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        public static DynamicParameters CreateCreateCreditParameters(CreateCreditRequest request)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@CustNo", request.CustNo);            
            parameters.Add("@custfirstName", request.firstName.Trim());
            parameters.Add("@custlastName", request.lastName.Trim());
            parameters.Add("@custEmail", request.email.Trim().ToLower());
            parameters.Add("@expirationDateM", request.dateM);
            parameters.Add("@expirationDateY", request.dateY);
            parameters.Add("@cardVerifyNumber", request.cardVerifyNumber);
            parameters.Add("@creditCardNumber", request.creditCardNumber);
            parameters.Add("@ErrorNumber", dbType: DbType.Int32, direction: ParameterDirection.Output);
            return parameters;
        }
        public static DynamicParameters CreateCreateCartParameters(CreateCartDetailRequest request)
        {
            DynamicParameters parameters = new DynamicParameters();
            foreach (var p in request.ProductDetails)
                {
                    parameters.Add("@CustCartNo", request.CustNo);
                    parameters.Add("@custfirstName", request.firstName);
                    parameters.Add("@CustlastName", request.lastName);
                    parameters.Add("@CustEmail", request.email);
                    parameters.Add("@productSKU", p.productSKU);
                    parameters.Add("@availability", p.availability);
                    parameters.Add("@cartPrice", p.cartPrice);
                    parameters.Add("@classDesc", p.classDesc);
                    parameters.Add("@classID", p.classID);
                    parameters.Add("@clearanceInd", p.clearanceInd);
                    parameters.Add("@color", p.color);
                    parameters.Add("@crossoffInd", p.crossoffInd);
                    parameters.Add("@departmentDesc", p.departmentDesc);   
                    parameters.Add("@departmentID", p.departmentID);
                    parameters.Add("@filter",p.filter);
                    parameters.Add("@image1", p.image1);
                    parameters.Add("@image2", p.image2);
                    parameters.Add("@image3", p.image3);
                    parameters.Add("@image4", p.image4);
                    parameters.Add("@imageMain", p.imageMain);
                    parameters.Add("@isFreeShip", p.isFreeShip);
                    parameters.Add("@media", p.media);
                    parameters.Add("@metaDescription", p.metaDescription); 
                    parameters.Add("@metaKeyword",  p.metaKeyword);
                    parameters.Add("@metaTitle", p.metaTitle);
                    parameters.Add("@mSRPPrice", p.mSRPPrice);
                    parameters.Add("@newArrivalInd", p.newArrivalInd);
                    parameters.Add("@overallCount", p.overallCount);
                    parameters.Add("@pageID", p.pageID);
                    parameters.Add("@productDocument1", p.productDocument1);
                    parameters.Add("@productDocument2",  p.productDocument2);
                    parameters.Add("@productID",  p.productID);
                    parameters.Add("@productLongDesc", p.productLongDesc); 
                    parameters.Add("@productSKU", p.productSKU);
                    parameters.Add("@productDesc", p.productDesc);
                    parameters.Add("@productSpec",  p.productSpec);
                    parameters.Add("@qty", p.qty);
                    parameters.Add("@required", p.required);
                    parameters.Add("@reviewCount",  p.reviewCount);
                    parameters.Add("@reviewRate",  p.reviewRate);
                    parameters.Add("@shipToCa",  p.shipToCa);
                    parameters.Add("@specialShipFee",  p.specialShipFee);
                    parameters.Add("@subclassDesc", p.subclassDesc);      
                    parameters.Add("@subclassID", p.subclassID);
                    parameters.Add("@validQty", p.validQty);
                    parameters.Add("@webPrice", p.webPrice);
                    parameters.Add("@weight", p.weight);
                    parameters.Add("@reOderqty", p.reOderqty);
                    parameters.Add("@sub_total", p.sub_total);
                    parameters.Add("@subTotal", p.subTotal);
                    parameters.Add("@ErrorNumber", dbType: DbType.Int32, direction: ParameterDirection.Output);
                }
            return parameters;
        }
    } 
}
