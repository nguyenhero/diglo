﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class ShippingChargeQueries : ConnectionBase, IShippingChargeQueries
    {
        public ShippingChargeQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public decimal GetFee(ShippingCharge request)
        {
            string sqlShippingCharge = OrderTransalator.CreateShippingCharge();
            var result = Execute(connection =>
                connection.Query<ShippingCharge>(sqlShippingCharge, request)
            );
            return result != null && result.Count() > 0 ? result.Single().ChargeFee : 0;
        }
        public IEnumerable<Customer> GetByEmail(string email)
        {
            var getResult = Execute(connection =>
               connection.Query<Customer>(Procedures.CustomerGetByEmail, param: new { Email = email }, commandType: CommandType.StoredProcedure));
            return getResult;
        }
        public int CreateUser(CreateUserRequest request)
        {
            DynamicParameters parameters = CustomerTransalator.CreateCreateUserParameters(request);
            Execute(connection =>
            {
                return connection.Query<int>(Procedures.CustomerCreate,
                   parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();
            });
            var errorNumber = parameters.Get<int>("@ErrorNumber");
            return errorNumber;
        }
        public int UpdateUserByCustomerNo(ResetPasswordRequest request)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@CustomerNo", request.CustomerNo.Trim());
            parameters.Add("@Password", request.Password.Trim());
            parameters.Add("@ErrorNumber", dbType: DbType.Int32, direction: ParameterDirection.Output);
            Execute(connection =>
            {
                return connection.Query<int>(Procedures.CustomerUpdateByCustNo, parameters,
                   commandType: CommandType.StoredProcedure).SingleOrDefault();
            });
            var errorNumber = parameters.Get<int>("@ErrorNumber");
            return errorNumber;
        }
    }
}
