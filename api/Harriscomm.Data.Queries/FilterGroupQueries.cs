﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class FilterGroupQueries : ConnectionBase, IFilterGroupQueries
    {
        public FilterGroupQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public IList<FilterGroup> Get(string departmentId, string classId, string subclassId, string kind, string filterId, string q)
        {
            Dictionary<int, FilterGroup> filterGroupDic = new Dictionary<int, FilterGroup>();
            string sqlOrderBy = "ORDER BY f.gOrderNo, f.FilterDetailName";
            string sqlWhere = string.Empty;
            string sqlField = string.Empty;
            string sql = string.Empty;
            Console.WriteLine("departmentId");
            Console.WriteLine(departmentId);
            Console.WriteLine("classId");
            Console.WriteLine(classId);
            Console.WriteLine("subclassId");
            Console.WriteLine(subclassId);
            
            if (!(string.IsNullOrEmpty(departmentId) && string.IsNullOrEmpty(classId) && string.IsNullOrEmpty(subclassId)) || (string.IsNullOrEmpty(q)))
            {
                sqlWhere = CreateSqlWhere(departmentId, classId, subclassId, filterId);
                sqlField = CreateSqlField(departmentId, classId, subclassId, kind);
                sql = "SELECT  distinct f.dOrderNo, f.FilterGroupID, f.FilterGroupName, f.gOrderNo, f.dFilterGroupID, f.FilterDetailID, f.FilterDetailName, " +
                        "f.TableName, f.Searchcolumn, f.Operation, f.FilterDetailValue " +
                        sqlField +
                        "FROM [harriswebdb].FilterDetailCountTotal f " +
                        sqlWhere +
                        sqlOrderBy;
            }
            else
            {
                sql = "SELECT  DISTINCT d.OrderNo, g.FilterGroupID, g.FilterGroupName, g.OrderNo, d.FilterGroupID, d.FilterDetailID, d.FilterDetailName, " +
                    "d.TableName, d.Searchcolumn, d.Operation, d.FilterDetailValue, " +
                    "jps.DetailCount FilterCount " +
                    "FROM [harriswebdb].[FilterGroup] g " +
                    "INNER JOIN ( SELECT COUNT(ProductID) DetailCount, FilterGroupID, FilterDetailID FROM [harriswebdb].[ProductFilterSearch] " +
                    "WHERE ProductID IN ( SELECT ProductID FROM [harriswebdb].[ProductGenSearch] WHERE DataSearch LIKE " + string.Format("'%{0}%'", q) + " ) GROUP BY FilterGroupID, FilterDetailID ) jps " +
                    "ON g.FilterGroupID = jps.FilterGroupID " +
                    "INNER JOIN [harriswebdb].[FilterDetail] d ON g.FilterGroupID = d.FilterGroupID " +
                    "WHERE d.FilterDetailID = jps.FilterDetailID  ORDER BY g.OrderNo, d.FilterDetailName ";
            }
            Console.WriteLine("Get Menu Item");
            Console.WriteLine("");
            Console.WriteLine(sql);
            return Execute(connection =>
            {
                var result = connection.Query<FilterGroup, FilterDetail, FilterGroup>(sql,
                   (filterGroup, filterDetail) => CreateFilterGroup(filterGroupDic, filterGroup, filterDetail),
                   param: new { DepartmentId = departmentId, ClassId = classId, SubclassId = subclassId },
                   splitOn: "FilterGroupID,FilterDetailID");
                return CreateFilterGroups(filterGroupDic);
            });
        }
        private IList<FilterGroup> CreateFilterGroups(Dictionary<int, FilterGroup> filterGroupDic)
        {
            List<FilterGroup> filterGroups = new List<FilterGroup>();
            if (filterGroupDic != null && filterGroupDic.Count() > 0)
            {
                foreach (var dic in filterGroupDic)
                {
                    FilterGroup filterGroup = new FilterGroup()
                    {
                        FilterGroupId = dic.Value.FilterGroupId,
                        FilterGroupName = dic.Value.FilterGroupName,
                        FilterDetails = dic.Value.FilterDetails
                         .Where(x => x.FilterCount > 0)
                         .GroupBy(x => x.FilterDetailId)
                         .Select(y => y.FirstOrDefault()).ToList()
                    };
                    filterGroups.Add(filterGroup);
                }
                  filterGroups[0].Active = true;  //filter price expanse out by default
            }
            return filterGroups;
        }
        private FilterGroup CreateFilterGroup(Dictionary<int, FilterGroup> filterGroupDic, FilterGroup filterGroup, FilterDetail filterDetail)
        {
            FilterGroup filterGroupEntry;
            if (!filterGroupDic.TryGetValue(filterGroup.FilterGroupId, out filterGroupEntry))
            {
                filterGroupEntry = filterGroup;
                filterGroupEntry.FilterDetails = new List<FilterDetail>();
                filterGroupDic.Add(filterGroupEntry.FilterGroupId, filterGroupEntry);
            }
            filterDetail.FilterGroupId = filterGroupEntry.FilterGroupId;
            filterGroupEntry.FilterDetails.Add(filterDetail);
            return filterGroup;
        }
        private string CreateSqlWhere(string departmentId, string classId, string subclassId, string filterId)
        {
            string sqlWhere = "WHERE 1 = 1 ";
            if (string.IsNullOrEmpty(departmentId) && string.IsNullOrEmpty(classId) && string.IsNullOrEmpty(subclassId))
            {
                sqlWhere += "AND f.DepartmentID = 99999";
            }
            else
            {
                sqlWhere += !string.IsNullOrEmpty(departmentId) ? "AND f.DepartmentID =@DepartmentId " : string.Empty;
                sqlWhere += !string.IsNullOrEmpty(classId) ? "AND f.ClassID =@ClassID " : string.Empty;
                sqlWhere += !string.IsNullOrEmpty(subclassId) ? "AND f.SubclassID =@SubclassId " : string.Empty;
            }
            if (!string.IsNullOrEmpty(classId) && (classId.Equals("14") || classId.Equals("12")))
            {
                sqlWhere += "AND f.ExtendID IS NOT NULL ";
                if (!string.IsNullOrEmpty(filterId))
                {
                    sqlWhere += "AND f.ExtendID = CAST(" + filterId + " AS int) ";
                }
            }
            else
            {
                sqlWhere += "AND f.ExtendID IS NULL ";
            }
            return sqlWhere;
        }
        private string CreateSqlField(string departmentId, string classId, string subclassId, string kind)
        {
            string sqlField = string.Empty;
            if (string.IsNullOrEmpty(kind))
            {
                // if (!string.IsNullOrEmpty(classId) && (classId.Equals("14") || classId.Equals("12")))
                // {
                //     sqlField = !string.IsNullOrEmpty(subclassId)
                //         ? ", f.SubURLCnt FilterCount "
                //         : (!string.IsNullOrEmpty(classId)
                //             ? ", f.ClassURLCnt FilterCount, f.ClassCLRCnt UrlFilterCount "
                //             : ", f.DeptURLCnt FilterCount, f.DeptCLRCnt UrlFilterCount ");
                // }
                // else
                // {
                //     sqlField = !string.IsNullOrEmpty(subclassId)
                //         ? ", f.SubCnt FilterCount, f.SubURLCnt UrlFilterCount "
                //         : (!string.IsNullOrEmpty(classId)
                //             ? ", f.ClassCnt FilterCount, f.ClassCLRCnt UrlFilterCount "
                //             : ", f.DeptCnt FilterCount, f.DeptCLRCnt UrlFilterCount ");
                // }

                sqlField = !string.IsNullOrEmpty(subclassId)
                        ? ", f.SubCnt FilterCount, f.SubURLCnt UrlFilterCount "
                        : (!string.IsNullOrEmpty(classId)
                            ? ", f.ClassCnt FilterCount, f.ClassCLRCnt UrlFilterCount "
                            : ", f.DeptCnt FilterCount, f.DeptCLRCnt UrlFilterCount ");






            }
            else
            {
                sqlField = kind.Equals("new") ? ", f.DeptNewCnt FilterCount " : ", f.DeptCLRCnt FilterCount ";
            }
            return sqlField;
        }
    }
}
