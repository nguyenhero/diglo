﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Data.Queries
{
    public class DepartmentQueries : ConnectionBase, IDepartmentQueries
    {
        public DepartmentQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public IList<Department> Get()
        {
            Console.WriteLine("Get Menu");
            Console.WriteLine("");

            // string sql = "SELECT d.DepartmentID, DepartmentDesc, c.DepartmentID, c.[ClassID], [ClassDesc], s.[ClassID], [SubclassID], [SubclassDesc], [Url] " +
            //              "FROM[harriswebdb].[Department] d " +
            //              "INNER JOIN[harriswebdb].[Class] c ON d.[DepartmentID] = c.[DepartmentID] " +
            //              "INNER JOIN [harriswebdb].[Subclass] s ON s.[ClassID] = c.[ClassID]" +
            //              "Order By d.OrderNo , c.OrderNo, s.OrderNo";

            string sql = "SELECT d.DepartmentID, DepartmentDesc, c.DepartmentID, c.[ClassID], [ClassDesc], s.[ClassID], [SubclassID], [SubclassDesc], [Url] " +
                         "FROM[harriswebdb].[Department] d " +
                         "LEFT JOIN [harriswebdb].[Class] c ON d.[DepartmentID] = c.[DepartmentID] " +
                         "LEFT JOIN [harriswebdb].[Subclass] s ON s.[ClassID] = c.[ClassID]" +
                         "Order By d.OrderNo , c.OrderNo, s.OrderNo";

            Console.WriteLine(sql);
            var departmentDictionary = new Dictionary<int, Department>();
            var classDictionary = new Dictionary<int, Class>();
            return Execute(connection =>
            {
                var result = connection.Query<Department, Class, Subclass, Department>(sql,
                    (department, classItem, subclass) =>
                    {
                        Department departmentEntry;
                        Class classEntry;
                        if (!departmentDictionary.TryGetValue(department.DepartmentID, out departmentEntry))
                        {
                            departmentEntry = department;
                            departmentDictionary.Add(departmentEntry.DepartmentID, departmentEntry);
                        }
                        if (!classDictionary.TryGetValue(classItem.ClassID, out classEntry))
                        {
                            classEntry = classItem;
                            classEntry.Subclasses.Add(subclass);
                            classDictionary.Add(classEntry.ClassID, classEntry);
                            departmentEntry.Classes.Add(classEntry);
                        }
                        else
                        {
                            foreach (var item in departmentEntry.Classes)
                            {
                                if (item.ClassID == classEntry.ClassID)
                                {
                                    item.Subclasses.Add(subclass);
                                }
                            }
                        }
                        departmentEntry.ClassHtml = "glyphicon glyphicon-plus";
                        return departmentEntry;
                    }, splitOn: "DepartmentID,ClassID");
                if (result != null && result.Count() > 0)
                {
                    var departments = result.Distinct().ToList();
                    // Console.WriteLine("Get departments");
                    // Console.WriteLine("");
                    // Console.WriteLine("DepartmentID");
                    // Console.WriteLine(departments[0].DepartmentID);
                    // Console.WriteLine("ClassesID");
                    // Console.WriteLine(departments[0].Classes[0].ClassID);
                    
                    // Console.WriteLine("DepartmentID");
                    // foreach (var d in departments)
                    // {
                    //     Console.WriteLine(d.DepartmentID);
                    //     var classes = d.Classes;
                    //     Console.WriteLine("ClassID");
                    //     foreach (var c in classes)
                    //     {
                    //         Console.WriteLine(c.ClassID);
                    //         var subclasses = c.Subclasses;
                    //         Console.WriteLine("SubclassID");
                    //         foreach (var s in subclasses)
                    //         {
                                
                    //             if (!(s.SubclassID == null || s.SubclassID == 0))
                    //             {
                    //               Console.WriteLine(s.SubclassID);
                    //             }


                    //         }
                    //     }
                    // }

                    departments[1].Left = -115;
                    departments[2].Left = -224;
                    departments[3].Left = -374;

                    return departments;
                }
                return null;
            });
        }
    }
}
