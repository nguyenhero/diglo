﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;


namespace Harriscomm.Data.Queries
{
    public class OrderQueries : ConnectionBase, IOrderQueries
    {
        public OrderQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public int CheckPromoCode(string promoCode)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ReturnValue", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
            parameters.Add("@promotionCode", promoCode);
            return Execute(connection =>
            {
                var returnCode = connection.Execute("[harriswebdb].[sp_CheckPromoCode]",
                      param: parameters,
                      commandType: CommandType.StoredProcedure);
                var returnValue = parameters.Get<int>("@ReturnValue");
                return returnValue;
            });
        }
        public IList<OrderHead> Get(string custNo, string monthCount, string orderId)
        {
            string sql = string.Empty;
            if (!string.IsNullOrEmpty(monthCount))
            {
                if (monthCount.Equals("-1"))
                {
                    sql = "SELECT * FROM harriswebdb.OrderHead WHERE CustNo = @CustNo ";
                    if (!string.IsNullOrEmpty(orderId))
                    {
                        sql += "AND OrderID = @OrderId";
                    }
                }
                else
                {
                    sql = "SELECT * FROM harriswebdb.OrderHead WHERE CustNo = @CustNo " +
                         "AND[CreateDateTime] > dateadd(m, (-1) * CAST(@MonthCount AS INT), getdate() - datepart(d, getdate()) + 1)";
                }
            }
            else
            {
                sql = "SELECT TOP 3 * FROM  [harriswebdb].[OrderHead] WHERE CustNo =@CustNo ORDER BY [CreateDateTime] DESC";
            }
            return Execute(connection =>
            {
                var result = connection.Query<OrderHead>(sql, new { CustNo = custNo, MonthCount = monthCount, OrderId = orderId});
                if (result != null && result.Count() > 0)
                {
                    return result.ToList();
                }
                return null;
            });
        }
        public IList<BillingAddress> GetBillingAddressByCustNo(string custNo)
        {
            string sql = "SELECT BillAddrID AddrID " +
                ",[PrimaryAddress]" +
                ",[BillingFirstName] FirstName" +
                ",[BillingLastName] LastName" +
                ",[BillingfullName] FullName" +
                ",[BillingCompany] Company" +
                ",[BillingAddress] Address" +
                ",[BillingApt] Apt" +
                ",[BillingCity] City" +
                ",[BillingState] State" +
                ",[BillingZip] Zip" +
                ",[BillingCountry] Country" +
                ",[BillingPhone] Phone" +
                ",[BillingEmail] Email" +
                ",[CreateDatetime]" +
                ",[UpdateDatetime]" +
                ",[UpdateUser]" +
                " FROM  " +
                " harriswebdb.BillingAddress Where CustNo = @CustNo";
            return Execute(connection =>
            {
                var result = connection.Query<BillingAddress>(sql, new { CustNo = custNo }, commandType: CommandType.Text);
                if (result != null && result.Count() > 0)
                {
                    return result.ToList();
                }
                return null;
            });
        }
        public IList<ShippingAddress> GetShippingAddressByCustNo(string custNo)
        {
            string sql = "SELECT ShipAddrID AddrID " +
                ",[PrimaryAddress] PrimaryAddress" +
                ",[ShippingFirstName] FirstName" +
                ",[ShippingLastName] LastName" +
                ",[ShippingfullName] FullName" +
                ",[ShippingCompany] Company" +
                ",[ShippingAddress] Address" +
                ",[ShippingApt] Apt" +
                ",[ShippingCity] City" +
                ",[ShippingState] State" +
                ",[ShippingZip] Zip" +
                ",[ShippingCountry] Country" +
                ",[ShippingPhone] Phone" +
                ",[ShippingEmail] Email" +
                ",[CreateDatetime]" +
                ",[UpdateDatetime]" +
                ",[UpdateUser]" +
                " FROM " +
                " harriswebdb.ShippingAddress Where CustNo = @CustNo";
            return Execute(connection =>
            {
                var result = connection.Query<ShippingAddress>(sql, new { CustNo = custNo }, commandType: CommandType.Text);
                if (result != null && result.Count() > 0)
                {
                    return result.ToList();
                }
                return null;
            });
        }
        public BillingAddress GetBillingAddressByEmail(string email)
        {
            string sql = "SELECT BillAddrID AddrID " +
                ",[PrimaryAddress]" +
                ",[BillingFirstName] FirstName" +
                ",[BillingLastName] LastName" +
                ",[BillingfullName] FullName" +
                ",[BillingCompany] Company" +
                ",[BillingAddress] Address" +
                ",[BillingApt] Apt" +
                ",[BillingCity] City" +
                ",[BillingState] State" +
                ",[BillingZip] Zip" +
                ",[BillingCountry] Country" +
                ",[BillingPhone] Phone" +
                ",[BillingEmail] Email" +
                ",[CreateDatetime]" +
                ",[UpdateDatetime]" +
                ",[UpdateUser]" +
                " FROM  " +
                " harriswebdb.BillingAddress Where BillingEmail = @Email";
            return Execute(connection =>
            {
                var result = connection.Query<BillingAddress>(sql, new { Email = email }, commandType: CommandType.Text);
                if (result != null && result.Count() > 0)
                {
                    return result.SingleOrDefault();
                }
                return null;
            });
        }
        public int Add(AddOrderRequest request)
        {
            return Execute(connection =>
            {
                using (var tran = connection.BeginTransaction())
                {
                    try
                    {
                        if (request.CreateUserRequest != null)
                        {
                            DynamicParameters parameters = new DynamicParameters();
                            parameters.Add("@CustNo", dbType: DbType.String, direction: ParameterDirection.Output, size: 20);
                            connection.Execute("harriswebdb.GetNextCusNo", param: parameters, commandType: CommandType.StoredProcedure, transaction: tran);
                            var custNo = parameters.Get<string>("@CustNo");
                            request.OrderHead.CustNo = custNo;
                            request.CreateUserRequest.CustNo = custNo;
                            string sqlUser = OrderTransalator.CreateUser();
                            connection.Query<int>(sqlUser, request.CreateUserRequest, tran);
                        }
                        string sqlOrderHead = OrderTransalator.CreateOrderHead();
                        //string sqlOrderHeadNav = OrderTransalator.CreateOrderHeadNav();
                        request.OrderHead.CreateDateTime = DateTime.Now;
                        request.OrderHead.ShipDateTime = DateTime.Now;
                        decimal total = request.OrderHead.Tax + request.OrderHead.ShipFee;
                        foreach (var order in request.OrderDetails)
                        {
                            total = total + order.Quantity * (order.CartPrice > 0 ? order.CartPrice : order.WebPrice);
                        }
                        request.OrderHead.Total = total;
                        var id = connection.Query<int>(sqlOrderHead, request.OrderHead, tran).Single();
                        //var id1 = connection.Query<int>(sqlOrderHeadNav, request.OrderHead, tran).Single();
                        string sqlOrderDetails = OrderTransalator.CreateOrderDetails();
                        string sqlOrderDetailsNav = OrderTransalator.CreateOrderDetailsNav();
                        foreach (var order in request.OrderDetails)
                        {
                            order.OrderID = id;
                        }
                        
                        connection.Execute(sqlOrderDetails, request.OrderDetails, tran);
                        connection.Execute(sqlOrderDetailsNav, request.OrderDetails, tran);
                        tran.Commit();
                        
                        return id;
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        return 0;
                        throw;
                    }
                }
            });
        }
        public int UpdateStatusByOrderId(int orderId, string status)
        {
            return Execute(connection =>
            {
                using (var tran = connection.BeginTransaction())
                {
                    try
                    {
                        string sql = "UPDATE harriswebdb.OrderHead " +
                                     "SET Status = @Status, OrderStatus = @OrderStatus " +
                                     "WHERE OrderID = @OrderID ";
                        var result = connection.Execute(sql,
                            new { Status = status, OrderID = orderId, OrderStatus = "Processing" },
                            tran);
                        tran.Commit();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        return 0;
                        throw;
                    }
                }
            });
        }
        public int AddReviewOrder(AddReviewOrdertRequest request)
        {
            return Execute(connection =>
            {
                using (var tran = connection.BeginTransaction())
                {
                    try
                    {
                        string sqlOrderHead = OrderTransalator.CreateOrderHead();
                        request.OrderHead.CreateDateTime = DateTime.Now;
                        request.OrderHead.ShipDateTime = DateTime.Now;
                        var id = connection.Query<int>(sqlOrderHead, request.OrderHead, tran).Single();
                        string sqlOrderDetails = OrderTransalator.CreateOrderDetails();
                        foreach (var order in request.OrderDetails)
                        {
                            order.OrderID = id;
                        }
                        connection.Execute(sqlOrderDetails, request.OrderDetails, tran);
                        tran.Commit();
                        return id;
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        return 0;
                        throw;
                    }
                }
            });
        }
        public int AddBillingAddress(BillingAddress request)
        {
            request.CreateDatetime = DateTime.Now;
            string sql = OrderTransalator.CreateBillingAdress();
            return Execute(connection =>
            {
                int result = 0;
                string sqlCount = "SELECT count(1) FROM [harriswebdb].[BillingAddress] WHERE " +
                                    "[CustNo] = @CustNo AND " +
                                    "[BillingFirstName] = @FirstName AND " +
                                    "[BillingLastName] = @LastName AND " +
                                    "[BillingCompany] = @Company AND " +
                                    "[BillingAddress] = @Address AND " +
                                    "[BillingApt] = @Apt AND " +
                                    "[BillingCity] = @City AND " +
                                    "[BillingState] = @State AND " +
                                    "[BillingZip] = @Zip AND " +
                                    "[BillingCountry] = @Country AND " +
                                    "[BillingPhone] = @Phone AND " +
                                    "[BillingEmail] = @Email ";
                var count = connection.ExecuteScalar<int>(sqlCount, request);
                if (count == 0)
                {
                    sql = OrderTransalator.CreateBillingAdress();
                    result = connection.Execute(sql, request);
                }
                return result;
            });
        }
        public int AddShippingAddress(ShippingAddress request)
        {
            request.CreateDatetime = DateTime.Now;
            return Execute(connection =>
            {
                int result = 0;
                string sql = string.Empty;
                string sqlCount = "SELECT count(1) FROM [harriswebdb].[ShippingAddress] WHERE " +
                                     "[CustNo] = @CustNo AND " +
                                     "[ShippingFirstName] = @FirstName AND " +
                                     "[ShippingLastName] = @LastName AND " +
                                     "[ShippingCompany] = @Company AND " +
                                     "[ShippingAddress] = @Address AND " +
                                     "[ShippingApt] = @Apt AND " +
                                     "[ShippingCity] = @City AND " +
                                     "[ShippingState] = @State AND " +
                                     "[ShippingZip] = @Zip AND " +
                                     "[ShippingCountry] = @Country AND " +
                                     "[ShippingPhone] = @Phone  AND " +
                                     "[ShippingEmail] = @Email ";
                var count = connection.ExecuteScalar<int>(sqlCount, request);
                if (count == 0)
                {
                    sql = OrderTransalator.CreateShippingAdress();
                    result = connection.Execute(sql, request);
                }
                return result;
            });
        }
    }
}

