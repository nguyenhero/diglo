﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class SuggestionSearchQueries : ConnectionBase, ISuggestionSearchQueries
    {
        public SuggestionSearchQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public List<string> Get(string querySearch)
        {
            List<string> itemListSuggestions = new List<string>();
            List<string> itemListSuggestionsTmp = new List<string>();
            string querySearchTmp = "%";
            string[] arrQS = querySearch.Split(' ');
            int iLen = arrQS.Length;
           for (int i = 0; i < iLen; i++)
            {
                querySearchTmp = querySearchTmp + arrQS[i] + "%";
            }
            querySearchTmp = string.Format("'{0}'", querySearchTmp);
            /*string sql = "SELECT DISTINCT TOP 20 SUBSTRING(ps.DataSearch, PATINDEX(" + string.Format("'%[ ,.\"\\]{0}%'", querySearch) + ", ps.DataSearch) + 1, DATALENGTH(" + string.Format("'{0}'", querySearch) + ") + 30) suggSearch " + 
            "FROM [harriswebdb].[ProductGenSearch] ps " +
            "WHERE PATINDEX(" + string.Format("'%[ ,.\"\\]{0}%'", querySearch) + ", ps.DataSearch) > 0 ";*/
            string sql = "SELECT DISTINCT TOP 20 SUBSTRING(ps.DataSearch, PATINDEX(" + string.Format("'%{0}%'", querySearch) + ", ps.DataSearch) - 30, DATALENGTH(" + string.Format("'{0}'", querySearch) + ") + 60) suggSearch " +
            "FROM [harriswebdb].[ProductGenSearch] ps " +
            "WHERE PATINDEX(" + string.Format("'%{0}%'", querySearch) + ", ps.DataSearch) > 0 ";
            /*string sql = "SELECT DISTINCT TOP 20 SUBSTRING(ps.DataSearch, PATINDEX(" + querySearchTmp + ", ps.DataSearch) - 30, DATALENGTH(" + string.Format("'{0}'", querySearch) + ") + 60) suggSearch " +
            "FROM [harriswebdb].[ProductGenSearch] ps " +
            "WHERE PATINDEX(" + querySearchTmp + ", ps.DataSearch) > 0 ";*/
            Execute(connection =>
            {
                var result = connection.Query<string>(sql,
                    commandType: CommandType.Text);
                itemListSuggestions = result.ToList();
            });
            return itemListSuggestions;
        }    
        private List<string> converIEnumToList(IEnumerable<string> items)
        {
            List<string> itemListSuggestions = new List<string>();
            foreach (string item in items) {
                itemListSuggestions.Add(item);
            }
            return itemListSuggestions;
        }
    }
}
