﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class ProductQueries : ConnectionBase, IProductQueries
    {
        public ProductQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public IEnumerable<Product> Get(GetProductRequest request)
        {
            string sql = ProductTransalator.CreateTempaleSortBy(request);
            Console.WriteLine("Get Product");
            Console.WriteLine("");
            Console.WriteLine(sql);
            return Execute(connection =>
             {
                 var result = connection.Query<Product>(sql,
                     commandType: CommandType.Text);
                 return result;
             });
        }
        public int GetCount(GetProductRequest request)
        {
            string sql = ProductTransalator.CreateTempaleCountSortBy(request);
            return Execute(connection =>
            {
                var result = connection.ExecuteScalar<int>(sql,
                    commandType: CommandType.Text);
                return result;
            });
        }
        public IEnumerable<Product> GetByProductSKU(string productSKU, bool parent)
        {
            string sql =
                "SELECT p.*, d.DepartmentDesc, c.ClassDesc, s.SubclassDesc, " +
                "p.DepartmentID, p.ClassID, p.SubclassID " +
                "FROM [harriswebdb].[Products] p " +
                "JOIN [harriswebdb].[Department] d ON d.DepartmentID = p.DepartmentID " +
                "JOIN [harriswebdb].[Class] c ON c.ClassID = p.ClassID " +
                "JOIN [harriswebdb].[Subclass] s ON s.SubclassID = p.SubclassID ";
            sql += parent ? "WHERE [ProductParent] = @ProductSKU AND Status = 1" :
                     "WHERE [ProductSKU] = @ProductSKU AND Status = 1";
            Console.WriteLine("Get Product Details");
            Console.WriteLine("");
            Console.WriteLine(sql);
            return Execute(connection =>
            {
                var result = connection.Query<Product>(sql, new { ProductSKU = productSKU });
                return result;
            });
        }
        public IEnumerable<ProductAdditionInfo> GetProductAdditionInfo(string productSKU,int class_subclassid)
        {
            // string sql =
            //     "SELECT * FROM [harriswebdb].[ProductAdditionInfo] WHERE [ProductSKU] = @ProductSKU ";
            
            string sql =
			"select i.ID , m.ProductSKU,s.Class_SubclassName,i.AdditionName,i.AdditionValue,s.OrderNo " +
            "from [harriswebdb].[MenuProducts] m " +
            "inner join [harriswebdb].[ProductAdditionInfoOrder] s on (m.SubclassID = s.Class_SubclassID or m.ClassID = s.Class_SubclassID) " +
            "inner join [harriswebdb].[ProductAdditionInfo] i on m.ProductSKU = i.ProductSKU " +
            "where (m.SubclassID = @Class_SubclassID or m.ClassID = @Class_SubclassID) " +
            "and i.ProductSKU = @ProductSKU " +
            "and i.AdditionName = s.Name " +
            "order by s.OrderNo " ;
                
            return Execute(connection =>
            {
                var result = connection.Query<ProductAdditionInfo>(sql, new { ProductSKU = productSKU, Class_SubclassID = class_subclassid });
                return result;
            });
        }
        public IEnumerable<Product> GetProductRelated(string productSKU)
        {
            string sql =
                "SELECT * FROM [harriswebdb].[Products] p WHERE Status = 1 AND p.ProductSKU  IN " +
                "(SELECT  pr.RelatedSKU FROM [harriswebdb].[ProductRelated] pr WHERE pr.ProductSKU = @ProductSKU)";
            return Execute(connection =>
            {
                var result = connection.Query<Product>(sql, new { ProductSKU = productSKU });
                return result;
            });
        }
        public IEnumerable<ProductCount> GetProductCount()
        {
            string sql = "SELECT * FROM [harriswebdb].[ProductCount]";
            return Execute(connection =>
            {
                var result = connection.Query<ProductCount>(sql);
                return result;
            });
        }
    }
}
