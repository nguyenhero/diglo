﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class ShippingAddressQueries : ConnectionBase, IShippingAddressQueries
    {
        public ShippingAddressQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public int Update(ShippingAddress request)
        {
            request.UpdateDatetime = DateTime.Now;
            string sql = OrderTransalator.UpdateShippingAdress();
            return Execute(connection =>
            {
                return connection.Execute(sql, request);
            });
        }
        public int Add(ShippingAddress request)
        {
            request.CreateDatetime = DateTime.Now;
            return Execute(connection =>
            {
                int result = 0;
                string sql = string.Empty;
                string sqlCount = "SELECT count(1) FROM [harriswebdb].[ShippingAddress] WHERE " +
                                     "[CustNo] = @CustNo AND " +
                                     "[ShippingFirstName] = @FirstName AND " +
                                     "[ShippingLastName] = @LastName AND " +
                                     "[ShippingCompany] = @Company AND " +
                                     "[ShippingAddress] = @Address AND " +
                                     "[ShippingApt] = @Apt AND " +
                                     "[ShippingCity] = @City AND " +
                                     "[ShippingState] = @State AND " +
                                     "[ShippingZip] = @Zip AND " +
                                     "[ShippingCountry] = @Country AND " +
                                     "[ShippingPhone] = @Phone  AND " +
                                     "[ShippingEmail] = @Email ";
                var count = connection.ExecuteScalar<int>(sqlCount, request);
                if (count == 0)
                {
                    sql = OrderTransalator.CreateShippingAdress();
                    result = connection.Execute(sql, request);
                }
                return result;
            });
        }
    }
}

