﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public static class Procedures
    {
        public const string CustomerGet = "[harriswebdb].[CustomerGet]";
        public const string CustomerCreate = "[harriswebdb].[CustomerCreate]";
        public const string CustomerCartCreate = "[harriswebdb].[CustomerCartCreate]";
        public const string CustomerCreditCreate = "[harriswebdb].[CustomerCreditCreate]";
        public const string ProductRecentCreate = "[harriswebdb].[ProductRecentCreate]";
        public const string CustomerGetByEmail = "[harriswebdb].[CustomerGetByEmail]";
        public const string CustomerUpdateByCustNo = "[harriswebdb].[CustomerUpdateByCustNo]";
        public const string CustomerCatCreate = "[harriswebdb].[CustomerCatCreate]";
        public const string StateGet = "[harriswebdb].[StateGet]";
        public const string ProductsGet = "[harriswebdb].[ProductsGet]";
        public const string ProductsGetByProductSKU = "[harriswebdb].[ProductsGetByProductSKU]";

    }
}

