﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class StateQueries : ConnectionBase, IStateQueries
    {
        public StateQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public IList<State> Get(string countryId)
        {
            return Execute(connection =>
             {
                 var result = connection.Query<State>(Procedures.StateGet,
                     param: new { CountryId = countryId },
                     commandType: CommandType.StoredProcedure);
                 if (result != null && result.Count() > 0)
                 {
                     return result.ToList();
                 }
                 return null;
             });
        }
    }
}
