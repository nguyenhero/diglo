﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class CustomerCatQueries : ConnectionBase, ICustomerCatQueries
    {
        public CustomerCatQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }       
        public int CreateFreeCatalog(CreateFreeCatalogRequest request)
        {
            DynamicParameters parameters = CustomerCatTransalator.CreateCreateFreeCatalogParameters(request);
            Execute(connection =>
            {
                return connection.Query<int>(Procedures.CustomerCatCreate,
                   parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();
            });
            var errorNumber = parameters.Get<int>("@ErrorNumber");
            return errorNumber;
        }
    }
}
