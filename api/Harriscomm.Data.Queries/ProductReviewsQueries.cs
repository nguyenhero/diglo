﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Data.Queries
{
    public class ProductReviewsQueries : ConnectionBase, IProductReviewsQueries
    {
        public ProductReviewsQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public IEnumerable<ProductReviews> Get(string productSku, int start, int end)
        {
            string sql = "SELECT  *, COUNT(*) OVER () as overallCount" +
                         " FROM [harriswebdb].[ProductReview] " +
                         "WHERE ProductSKU = '" + productSku + "' AND RevApproved = 1 " +
                         "ORDER BY   RevID " +
                         "OFFSET " + start + " ROWS " +
                          "FETCH NEXT " + end + " ROWS ONLY";
            return Execute(connection =>
             {
                 var result = connection.Query<ProductReviews>(sql, null, commandType: CommandType.Text);
                 return result;
             });
        }
        public int Create(CreateProductReviews request)
        {
            string sql =
                "INSERT INTO [harriswebdb].[ProductReview] " +
                "([CustNo],[ProductSKU], [RevComments], [RevName], [RevRate], [CreateDatetime]) " +
                "VALUES('" + request.CustNo + "','" +
                request.productSKU + "','" + System.Net.WebUtility.HtmlEncode(request.Review) + "','"+ request.NickName +"',"+
                request.Star +  ",'" + DateTime.Now +"')"; 
            return Execute(connection =>
            {
               return connection.Execute(sql, null, commandType: CommandType.Text);
            });
        }
        public IEnumerable<Product> GetByProductSKU(string productSKU)
        {
            return Execute(connection =>
            {
                var result = connection.Query<Product>(Procedures.ProductsGetByProductSKU,
                    param: new { ProductSKU = productSKU },
                    commandType: CommandType.StoredProcedure);
                return result;
            });
        }
    }
}
