﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Queries
{
    public class OrderDetailsQueries : ConnectionBase, IOrderDetailsQueries
    {
        public OrderDetailsQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public IList<OrderDetail> Get(int orderId)
        {
            string sql = "SELECT * FROM harriswebdb.OrderDetail o " +
                         "JOIN[harriswebdb].[Products] p On o.[ProductSKU] = p.[ProductSKU] " +
                         "WHERE OrderID = @orderId ";
            return Execute(connection =>
            {
                var result = connection.Query<OrderDetail, Product, OrderDetail>
               (sql, (wishList, product) => { wishList.Product = product; return wishList; },
               new { OrderID = orderId }, splitOn: "ProductSKU");
                if (result != null && result.Count() > 0)
                {
                    return result.ToList();
                }
                return null;
            });
        }
    }
}

