﻿using Dapper;
using Harriscomm.Data.Entities;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.Repository;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Data.Queries
{
    public class CustomerQueries : ConnectionBase, ICustomerQueries
    {
        public CustomerQueries(IConnectionFactory connectionFactory)
           : base(connectionFactory)
        {
        }
        public IEnumerable<Customer> Get(string email, string password, ref int errorNumber)
        {
            DynamicParameters parameters = CustomerTransalator.CreateGetParameters(email, password);
            var getResult = Execute(connection =>
               connection.Query<Customer>(Procedures.CustomerGet, parameters, commandType: CommandType.StoredProcedure));
            errorNumber = parameters.Get<int>("@ErrorNumber");
            return getResult;
        }
        public IEnumerable<Customer> GetByEmail(string email)
        {
            var getResult = Execute(connection =>
               connection.Query<Customer>(Procedures.CustomerGetByEmail, param: new { Email = email }, commandType: CommandType.StoredProcedure));
            return getResult;
        }
        public IEnumerable<ProductCart> GetCart(int custNo)
        {
            string sql = "SELECT availability,cartPrice,classDesc,classID,clearanceInd,color,crossoffInd,departmentDesc,departmentID,filter,image1,image2,image3,image4,imageMain,isFreeShip,media,metaDescription,metaKeyword,metaTitle,mSRPPrice,newArrivalInd,overallCount,pageID,productDocument1,productDocument2,productID,productLongDesc,productSKU,productDesc,productSpec,qty,required,reviewCount,reviewRate,shipToCa,specialShipFee,subclassDesc,subclassID,validQty,webPrice,weight,reOderqty,sub_total,subTotal FROM [harriswebdb].[CustomerCart] WHERE CustCartNo = " + custNo ;
            return Execute(connection =>
             {
                 var result = connection.Query<ProductCart>(sql,
                     commandType: CommandType.Text);
                 return result;
             });
        }
        public IEnumerable<Product> GetProdRecent(int custNo)
        {
            string sql = "SELECT availability,cartPrice,classDesc,classID,clearanceInd,color,crossoffInd,departmentDesc,departmentID,filter,image1,image2,image3,image4,imageMain,isFreeShip,media,metaDescription,metaKeyword,metaTitle,mSRPPrice,newArrivalInd,overallCount,pageID,productDocument1,productDocument2,productID,productLongDesc,productSKU,productDesc,productSpec,qty,required,reviewCount,reviewRate,shipToCa,specialShipFee,subclassDesc,subclassID,validQty,webPrice,weight FROM [harriswebdb].[ProductRecent] WHERE CustNo = " + custNo ;
            return Execute(connection =>
             {
                 var result = connection.Query<Product>(sql,
                     commandType: CommandType.Text);
                 return result;
             });
        }
        public int CreateUser(CreateUserRequest request)
        {
            DynamicParameters parameters = CustomerTransalator.CreateCreateUserParameters(request);
            Execute(connection =>
            {
                return connection.Query<int>(Procedures.CustomerCreate,
                   parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();
            });
            var errorNumber = parameters.Get<int>("@ErrorNumber");
            return errorNumber;
        }
        public IEnumerable<CreditCards> GetCredit(int custNo)
        {
            string sql = "SELECT expirationDateM , expirationDateY, cardVerifyNumber, creditCardNumber FROM [harriswebdb].[CustomerCreditCard] WHERE CustNo = " + custNo ;
            return Execute(connection =>
             {
                 var result = connection.Query<CreditCards>(sql,
                     commandType: CommandType.Text);
                 return result;
             });
        }
        public int CreateCredit(CreateCreditRequest request)
        {
            DynamicParameters parameters = CustomerTransalator.CreateCreateCreditParameters(request);
            Execute(connection =>
            {
                return connection.Query<int>(Procedures.CustomerCreditCreate, parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();
            });
            var errorNumber = parameters.Get<int>("@ErrorNumber");
            return errorNumber;
        }
        public int CheckCredit(CreateCreditRequest request)
        {
            string sql;
            sql = "Select count(1) FROM [harriswebdb].[CustomerCreditCard] " +
            "WHERE CustNo = " + request.CustNo +  
            " and creditCardNumber = '" + request.creditCardNumber + "'" +
            " and custEmail = '" + request.email + "'" ;
            return Execute(connection =>
            {
                var result = connection.ExecuteScalar<int>(sql, request);
                return result;
            });
        }
        public int CreateCart(CreateCartDetailRequest request)
        {
            DynamicParameters parameters = new DynamicParameters();
            int i = 1 ;
            foreach (var p in request.ProductDetails)
            {
                if (p.flag_in == -1)
                {
                        parameters.Add("@flag_in", -1);
                }
                else
                {
                        parameters.Add("@flag_in", i);
                }
                    parameters.Add("@CustCartNo", request.CustNo);
                    parameters.Add("@custfirstName", request.firstName);
                    parameters.Add("@CustlastName", request.lastName);
                    parameters.Add("@CustEmail", request.email);
                    parameters.Add("@productSKU", p.productSKU);
                    parameters.Add("@availability", p.availability);
                    parameters.Add("@cartPrice", p.cartPrice);
                    parameters.Add("@classDesc", p.classDesc);
                    parameters.Add("@classID", p.classID);
                    parameters.Add("@clearanceInd", p.clearanceInd);
                    parameters.Add("@color", p.color);
                    parameters.Add("@crossoffInd", p.crossoffInd);
                    parameters.Add("@departmentDesc", p.departmentDesc);   
                    parameters.Add("@departmentID", p.departmentID);
                    parameters.Add("@filter",p.filter);
                    parameters.Add("@image1", p.image1);
                    parameters.Add("@image2", p.image2);
                    parameters.Add("@image3", p.image3);
                    parameters.Add("@image4", p.image4);
                    parameters.Add("@imageMain", p.imageMain);
                    parameters.Add("@isFreeShip", p.isFreeShip);
                    parameters.Add("@media", p.media);
                    parameters.Add("@metaDescription", p.metaDescription); 
                    parameters.Add("@metaKeyword",  p.metaKeyword);
                    parameters.Add("@metaTitle", p.metaTitle);
                    parameters.Add("@mSRPPrice", p.mSRPPrice);
                    parameters.Add("@newArrivalInd", p.newArrivalInd);
                    parameters.Add("@overallCount", p.overallCount);
                    parameters.Add("@pageID", p.pageID);
                    parameters.Add("@productDocument1", p.productDocument1);
                    parameters.Add("@productDocument2",  p.productDocument2);
                    parameters.Add("@productID",  p.productID);
                    parameters.Add("@productLongDesc", p.productLongDesc); 
                    parameters.Add("@productSKU", p.productSKU);
                    parameters.Add("@productDesc", p.productDesc);
                    parameters.Add("@productSpec",  p.productSpec);
                    parameters.Add("@qty", p.qty);
                    parameters.Add("@required", p.required);
                    parameters.Add("@reviewCount",  p.reviewCount);
                    parameters.Add("@reviewRate",  p.reviewRate);
                    parameters.Add("@shipToCa",  p.shipToCa);
                    parameters.Add("@specialShipFee",  p.specialShipFee);
                    parameters.Add("@subclassDesc", p.subclassDesc);      
                    parameters.Add("@subclassID", p.subclassID);
                    parameters.Add("@validQty", p.validQty);
                    parameters.Add("@webPrice", p.webPrice);
                    parameters.Add("@weight", p.weight);
                    parameters.Add("@reOderqty", p.reOderqty);
                    parameters.Add("@sub_total", p.sub_total);
                    parameters.Add("@subTotal", p.subTotal);
                    parameters.Add("@ErrorNumber", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    i = i + 1;
                    Execute(connection =>
                    {
                        return connection.Query<int>(Procedures.CustomerCartCreate,
                        parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    });
            }
            var errorNumber = parameters.Get<int>("@ErrorNumber");
            return errorNumber;
        }
        public int CreateProdRecent(CreateProdRecentRequest request)
        {
            DynamicParameters parameters = new DynamicParameters();
            int i = 1 ;
            foreach (var p in request.ProductRecent)
            {
                    parameters.Add("@CustNo", request.CustNo);
                    parameters.Add("@custfirstName", request.firstName);
                    parameters.Add("@custlastName", request.lastName);
                    parameters.Add("@custEmail", request.email);
                    parameters.Add("@productSKU", p.ProductSKU);
                    parameters.Add("@availability", p.Availability);
                    parameters.Add("@cartPrice", p.CartPrice);
                    parameters.Add("@classDesc", p.ClassDesc);
                    parameters.Add("@classID", p.ClassID);
                    parameters.Add("@clearanceInd", p.ClearanceInd);
                    parameters.Add("@color", p.Color);
                    parameters.Add("@crossoffInd", p.CrossoffInd);
                    parameters.Add("@departmentDesc", p.DepartmentDesc);   
                    parameters.Add("@departmentID", p.DepartmentID);
                    parameters.Add("@filter",p.Filter);
                    parameters.Add("@image1", p.Image1);
                    parameters.Add("@image2", p.Image2);
                    parameters.Add("@image3", p.Image3);
                    parameters.Add("@image4", p.Image4);
                    parameters.Add("@imageMain", p.ImageMain);
                    parameters.Add("@isFreeShip", p.IsFreeShip);
                    parameters.Add("@media", p.Media);
                    parameters.Add("@metaDescription", p.MetaDescription); 
                    parameters.Add("@metaKeyword",  p.MetaKeyword);
                    parameters.Add("@metaTitle", p.MetaTitle);
                    parameters.Add("@mSRPPrice", p.MSRPPrice);
                    parameters.Add("@newArrivalInd", p.NewArrivalInd);
                    parameters.Add("@overallCount", p.OverallCount);
                    parameters.Add("@pageID", p.PageID);
                    parameters.Add("@productDocument1", p.ProductDocument1);
                    parameters.Add("@productDocument2",  p.ProductDocument2);
                    parameters.Add("@productID",  p.ProductID);
                    parameters.Add("@productLongDesc", p.ProductLongDesc); 
                    parameters.Add("@productSKU", p.ProductSKU);
                    parameters.Add("@productDesc", p.ProductDesc);
                    parameters.Add("@productSpec",  p.ProductSpec);
                    parameters.Add("@qty", p.Qty);
                    parameters.Add("@required", p.Required);
                    parameters.Add("@reviewCount",  p.ReviewCount);
                    parameters.Add("@reviewRate",  p.ReviewRate);
                    parameters.Add("@shipToCa",  p.ShipToCa);
                    parameters.Add("@specialShipFee",  p.SpecialShipFee);
                    parameters.Add("@subclassDesc", p.SubclassDesc);      
                    parameters.Add("@subclassID", p.SubclassID);
                    parameters.Add("@validQty", p.ValidQty);
                    parameters.Add("@webPrice", p.WebPrice);
                    parameters.Add("@weight", p.Weight);
                    parameters.Add("@flag", i);
                    parameters.Add("@ErrorNumber", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    i = i + 1;
                    Execute(connection =>
                    {
                        return connection.Query<int>(Procedures.ProductRecentCreate,
                        parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    });
            }
            var errorNumber = parameters.Get<int>("@ErrorNumber");
            return errorNumber;
        }
        public int DelCredit(CreateCreditRequest request)
        {
            string sql;
            sql = "DELETE FROM [harriswebdb].[CustomerCreditCard] " +
            "WHERE CustNo = " + request.CustNo +  
            " and creditCardNumber = '" + request.creditCardNumber +
            "' and cardVerifyNumber =  '" + request.cardVerifyNumber +
            "' and expirationDateM =  '" + request.dateM + 
            "' and expirationDateY =  '" + request.dateY + "'" ;
            return Execute(connection =>
            {
                return connection.Execute(sql, request);
            });
        }
        public int DelCart(DelCartRequest request)
        {
            string sql;
            if ( request.productID == -1 && request.productID < 0 ) {
                sql = "DELETE FROM [harriswebdb].[CustomerCart] WHERE CustCartNo = " + request.custNo ;
            }
            else
            {
                sql = "DELETE FROM [harriswebdb].[CustomerCart] WHERE CustCartNo = " + request.custNo + " and productID = " + request.productID ;
            }
            return Execute(connection =>
            {
                return connection.Execute(sql, request);
            });
        }
        public int UpdateUser(UpdateUserRequest request)
        {
            request.CustModified = DateTime.Now;
            string sql = "UPDATE[harriswebdb].[Customer] " +
                "SET " +
                "CustFirstName = @FirstName " +
                ",CustLastName = @LastName " +
                ",CustLFullName = rtrim(@FirstName)+' '+rtrim(@LastName)" +
                ",CustEmail = @Email " +
                ",CustModified = @CustModified " +
                "WHERE CustNo =  @CustNo";
            return Execute(connection =>
            {
                return connection.Execute(sql, request);
            });
        }
        public int UpdateUserPassword(UpdateUserPasswordRequest request)
        {
            request.CustModified = DateTime.Now;
            string sql = "UPDATE[harriswebdb].[Customer] " +
                "SET " +
                "[CustPassword] =@Password " +
                ",CustModified = @CustModified " +
                "WHERE CustNo =  @CustNo";
            return Execute(connection =>
            {
                return connection.Execute(sql, request);
            });
        }
        public int UpdateUserByCustomerNo(ResetPasswordRequest request)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@CustomerNo", request.CustomerNo.Trim());
            parameters.Add("@Password", request.Password.Trim());
            parameters.Add("@ErrorNumber", dbType: DbType.Int32, direction: ParameterDirection.Output);
            Execute(connection =>
            {
                return connection.Query<int>(Procedures.CustomerUpdateByCustNo, parameters,
                   commandType: CommandType.StoredProcedure).SingleOrDefault();
            });
            var errorNumber = parameters.Get<int>("@ErrorNumber");
            return errorNumber;
        }
    }
}
