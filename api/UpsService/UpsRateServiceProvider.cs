﻿﻿using Harriscomm.Common;
using Harriscomm.Data.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
namespace UpsService
{
    public class UpsRateServiceProvider
    {
        private static readonly HttpClient client = new HttpClient();
        private static string _harrisAddress;
        private static string _harrisCity;
        private static string _harrisState;
        private static string _harrisZip;
        private static string _harrisCountry;
        private static ShippingAddress _shippingAddress;
        private static string _weight;
        private static string _weightMeasurement;
        private static string _weightMeasurementDescription;
        private static string _dimensionsWidth;
        private static string _dimensionsHeight;
        private static string _dimensionsLength;
        private static string _dimensionsMeasurement;
        private static string _dimensionsMeasurementDescription;
        private Dictionary<string, string> _packageType;
        private Dictionary<string, string> _serviceCodes;
        private readonly string _upsAccessLicenseNumber;
        private readonly string _upsPassword;
        private readonly string _upsUsername;
        private readonly string _maximumListSize = "35";
        private readonly string _timeInTransitRequestOption = "TNT";
        private readonly string _timeInTransitCustomerContext = "";
        private readonly string _timeInTransitTransactionIdentifie = "Harris_1";
        private readonly string _rateRequestRequestOption = "Rate";
        private readonly string _rateRequestCustomerContext = "";
        private readonly string _timeInTransitUri = "https://wwwcie.ups.com/rest/TimeInTransit";
        private readonly string _rateUri = "https://wwwcie.ups.com/rest/Rate";
        public UpsRateServiceProvider(AppSettings appSettings,ShippingAddress shippingAddress,string weight,string weightMeasurement,string weightMeasurementDescription)
        {
            _upsUsername = appSettings.UpsUsername;
            _upsPassword = appSettings.UpsPassword;
            _upsAccessLicenseNumber = appSettings.UpsAccessLicenseNumber;
            _shippingAddress = shippingAddress;
            _weight = weight;
            _weightMeasurement = weightMeasurement;
            _weightMeasurementDescription = weightMeasurementDescription;
            _harrisAddress = appSettings.HarrisAddress;
            _harrisCity = appSettings.HarrisCity;
            _harrisState = appSettings.HarrisState;
            _harrisZip = appSettings.HarrisZip;
            _harrisCountry = appSettings.HarrisCountry;
        }
        public async Task<RateResponseObj> GetRate(string serviceType, string packageTpe, bool isSaturdayDelivery)
        {
            try
            {
                var request = new
                {
                    UPSSecurity = new
                    {
                        UsernameToken = new
                        {
                            Username = _upsUsername,
                            Password = _upsPassword
                        },
                        ServiceAccessToken = new
                        {
                            AccessLicenseNumber = _upsAccessLicenseNumber
                        }
                    },
                    RateRequest = new
                    {
                        Request = new
                        {
                            RequestAction = isSaturdayDelivery ? "Rate" : null,
                            RequestOption = isSaturdayDelivery ? "ratetimeintransit" : "Rate",
                            TransactionReference = new
                            {
                                CustomerContext = _rateRequestCustomerContext
                            }
                        },
                        Shipment = new
                        {
                            Shipper = new
                            {
                                Name = "Harriscomm",
                                ShipperNumber = "Shipper Number",
                                Address = new
                                {
                                    AddressLine = new List<string>(new[] { _harrisAddress }),
                                    City = _harrisCity,
                                    CountryCode = _harrisCountry,
                                    PostalCode = _harrisZip,
                                    StateProvinceCode = _harrisState
                                }
                            },
                            ShipTo = new
                            {
                                Name = "CUSTOMER",
                                Address = new
                                {
                                    AddressLine = new List<string>(new[] { _shippingAddress.Address }),
                                    City = _shippingAddress.City,
                                    CountryCode = _shippingAddress.Country,
                                    PostalCode = _shippingAddress.Zip,
                                    StateProvinceCode = _shippingAddress.State
                                }
                            },
                            ShipFrom = new
                            {
                                Name = "Harriscomm",
                                Address = new
                                {
                                    AddressLine = new List<string>(new[] { _harrisAddress }),
                                    City = _harrisCity,
                                    CountryCode = _harrisCountry,
                                    PostalCode = _harrisZip,
                                    StateProvinceCode = _harrisState
                                }
                            },
                            Service = new
                            {
                                Code = serviceType,
                                Description = ""
                            },
                            Package = new
                            {
                                PackagingType = new
                                {
                                    Code = packageTpe,
                                    Description = ""
                                },
                                PackageWeight = new
                                {
                                    UnitOfMeasurement = new
                                    {
                                        Code = _weightMeasurement,
                                        Description = _weightMeasurement
                                    },
                                    Weight = _weight
                                }
                            },
                            // isSaturdayDelivery
                            ShipmentServiceOptions = isSaturdayDelivery ? new { SaturdayDeliveryIndicator = "" } : null,
                            DeliveryTimeInformation = isSaturdayDelivery ? new { PackageBillType = "03" } : null,
                            ShipmentRatingOptions = new { NegotiatedRatesIndicator = "" }
                        }
                    }
                };
                var stringContent = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8,"application/json");
                var response = await client.PostAsync(_rateUri, stringContent);
                var responseString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<RateResponseObj>(responseString);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        private ResponseRating caculateCustomDate13(DateTime startDate, int dt)
        {
            var result = new ResponseRating();
            #region Caculate date
            var startGroundDate = startDate;
            var endGroundDate = startDate;

            if (startGroundDate.DayOfWeek == DayOfWeek.Monday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(1);
                result.groundFreeEndDate = endGroundDate.AddDays(3);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Monday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
                result.groundFreeEndDate = endGroundDate.AddDays(4);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(1);
                result.groundFreeEndDate = endGroundDate.AddDays(3);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(1);
                result.groundFreeEndDate = endGroundDate.AddDays(5);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(1);
                result.groundFreeEndDate = endGroundDate.AddDays(5);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(5);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Saturday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(5);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Sunday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
                result.groundFreeEndDate = endGroundDate.AddDays(4);
            }
            #endregion
            return result;
        }
        private ResponseRating caculateCustomDate23(DateTime startDate, int dt)
        {
            var result = new ResponseRating();
            #region Caculate date
            var startGroundDate = startDate;
            var endGroundDate = startDate;

            if (startGroundDate.DayOfWeek == DayOfWeek.Monday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
                result.groundFreeEndDate = endGroundDate.AddDays(3);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Monday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(4);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
                result.groundFreeEndDate = endGroundDate.AddDays(3);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
                result.groundFreeEndDate = endGroundDate.AddDays(5);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(5);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(5);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Saturday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(5);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Sunday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(4);
            }

            #endregion
            return result;
        }
        private ResponseRating caculateCustomDate34(DateTime startDate, int dt)
        {
            var result = new ResponseRating();
            #region Caculate date
            var startGroundDate = startDate;
            var endGroundDate = startDate;

            if (startGroundDate.DayOfWeek == DayOfWeek.Monday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(4);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Monday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(7);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(7);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(7);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(7);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(7);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Saturday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(6);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Sunday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(5);
            }
            #endregion
            return result;
        }
        private ResponseRating caculateCustomDate35(DateTime startDate, int dt)
        {
            var result = new ResponseRating();
            #region Caculate date
            var startGroundDate = startDate;
            var endGroundDate = startDate;

            if (startGroundDate.DayOfWeek == DayOfWeek.Monday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(7);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Monday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(8);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(7);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(8);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(7);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(8);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(7);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(8);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(7);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(10);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Saturday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(9);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Sunday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(8);
            }

            #endregion
            return result;
        }
        private ResponseRating caculateCustomDate36(DateTime startDate, int dt)
        {
            var result = new ResponseRating();
            #region Caculate date
            var startGroundDate = startDate;
            var endGroundDate = startDate;

            if (startGroundDate.DayOfWeek == DayOfWeek.Monday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(8);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Monday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(9);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
                result.groundFreeEndDate = endGroundDate.AddDays(8);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(9);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(8);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(9);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(8);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(11);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(10);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(6);
                result.groundFreeEndDate = endGroundDate.AddDays(11);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Saturday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(5);
                result.groundFreeEndDate = endGroundDate.AddDays(10);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Sunday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
                result.groundFreeEndDate = endGroundDate.AddDays(9);
            }

            #endregion
            return result;
        }
        private ResponseRating caculateDays(DateTime startDate, int dt)
        {
            var result = new ResponseRating();
            #region Caculate date
            var startGroundDate = startDate;
            var endGroundDate = startDate;

            if (startGroundDate.DayOfWeek == DayOfWeek.Monday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(1);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Monday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(1);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Tuesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(1);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Wednesday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(1);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Thursday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && dt < 12)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Friday && (dt == 12 || dt > 12))
            {
                result.groundFreeStartDate = startGroundDate.AddDays(4);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Saturday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(3);
            }
            else if (startGroundDate.DayOfWeek == DayOfWeek.Sunday)
            {
                result.groundFreeStartDate = startGroundDate.AddDays(2);
            }
            #endregion
            return result;
        }
        public async Task<ResponseRating> GetGroundRate(DateTime startDate, string packageTpe)
        {
            int dt = DateTime.Now.Hour;
            var result = new ResponseRating();
            var caculatedDate = new ResponseRating();
            // free ground ship rule for any states
            var isFreeGroundShippingForAllStates = true;
            // states not apply free ground ship rule and must be charged
            var chargedFeeStateList = new List<string>() { "AK", "HI", "PR" };
            var yellow_state = new List<string>() { "IA", "MN", "WI" };
            var pink_state = new List<string>() { "IL","IN","KS","KY","MI","MO","ND","NE","OH","SD" };
            var green_state = new List<string>() { "AL", "AR", "CO", "DE", "FL", "GA", "LA", "MD", "MS", "MT", "NC", "NJ", "NM", "OK", "PA", "SC", "TN", "VA", "WV" };
            var blue_state = new List<string>() { "AK", "AZ", "CA", "CT", "MA", "ME", "NH", "NV", "NY", "RI", "TX", "UT", "VT", "WA", "WY", "PR" };
            var purple_state = new List<string>() { "HI", "ID", "OR" };
            if (chargedFeeStateList.Contains(_shippingAddress.State))
            {
                result.totalNetCharge = "15.00";
            }
            else{
                result.totalNetCharge = "0.00";
            }
            result.isGroundFree = true;
            result.serviceTypeCode = "03";
            result.serviceType = "Ground Shipping";
            if (yellow_state.Contains(_shippingAddress.State))
            {
               caculatedDate = caculateCustomDate13(startDate,dt);
            }
            else if (pink_state.Contains(_shippingAddress.State)) {
               caculatedDate = caculateCustomDate23(startDate,dt);
            }
            else if (green_state.Contains(_shippingAddress.State)) {
               caculatedDate = caculateCustomDate34(startDate,dt);
            }
            else if (blue_state.Contains(_shippingAddress.State)) {
               caculatedDate = caculateCustomDate35(startDate,dt); 
            }
            else if (purple_state.Contains(_shippingAddress.State)) {
               caculatedDate = caculateCustomDate36(startDate,dt);  
            }
            result.groundFreeStartDate = caculatedDate.groundFreeStartDate;
            result.groundFreeEndDate = caculatedDate.groundFreeEndDate;
            return result;
        }
        public async Task<TimeInTransitResponseObj> GetTimeInTransit(DateTime startDate)
        {
            try
            {
                var request = new
                {
                    Security = new
                    {
                        UsernameToken = new
                        {
                            Username = _upsUsername,
                            Password = _upsPassword
                        },
                        UPSServiceAccessToken = new
                        {
                            AccessLicenseNumber = _upsAccessLicenseNumber
                        }
                    },
                    TimeInTransitRequest = new
                    {
                        Request = new
                        {
                            RequestOption = _timeInTransitRequestOption,
                            TransactionReference = new
                            {
                                CustomerContext = _timeInTransitCustomerContext,
                                TransactionIdentifie = _timeInTransitTransactionIdentifie
                            }
                        },
                        ShipFrom = new
                        {
                            Address = new
                            {
                                StateProvinceCode = _harrisState,
                                CountryCode = _harrisCountry,
                                PostalCode = _harrisZip,
                            }
                        },
                        ShipTo = new
                        {
                            Address = new
                            {
                                StateProvinceCode = _shippingAddress.State,
                                CountryCode = _shippingAddress.Country,
                                PostalCode = _shippingAddress.Zip
                            }
                        },
                        Pickup = new
                        {
                            Date = String.Format("{0:yyyyMMdd}", startDate),
                            // "20180629" 
                        },
                        ShipmentWeight = new
                        {
                            UnitOfMeasurement = new
                            {
                                Code = _weightMeasurement,
                                Description = _weightMeasurement
                            },
                            Weight = _weight
                        },
                        MaximumListSize = _maximumListSize
                    }
                };
                var stringContent = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8,"application/json");
                var response = await client.PostAsync(_timeInTransitUri, stringContent);
                var responseString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TimeInTransitResponseObj>(responseString);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public async Task<List<ResponseRating>> GetResponseRating(DateTime startDate,string packageType)
        {
            var result = new List<ResponseRating>();
            int dt = DateTime.Now.Hour;
            var caculatedDate = new ResponseRating();
            var chargedFeeStateList = new List<string>() { "AK", "HI", "PR" };
            var yellow_state = new List<string>() { "IA", "MN", "WI" };
            var pink_state = new List<string>() { "IL","IN","KS","KY","MI","MO","ND","NE","OH","SD" };
            caculatedDate = caculateDays(startDate,dt);  
            var getTimeInTransit = await GetTimeInTransit(startDate);
            var ListServiceSummary = getTimeInTransit.TimeInTransitResponse.TransitResponse.ServiceSummary;
            foreach (var serviceSummary in ListServiceSummary)
            {
                var transferCode = TransferCode(serviceSummary.Service.Code, ListServiceSummary);
                if (transferCode != null)
                {
                    var model = new ResponseRating();
                    var year = serviceSummary.EstimatedArrival.Arrival.Date.Substring(0, 4);
                    var mounth = serviceSummary.EstimatedArrival.Arrival.Date.Substring(4, 2);
                    var date = serviceSummary.EstimatedArrival.Arrival.Date.Substring(6, 2);
                    var hour = serviceSummary.EstimatedArrival.Arrival.Time.Substring(0, 2);
                    var min = serviceSummary.EstimatedArrival.Arrival.Time.Substring(2, 2);
                    var sec = serviceSummary.EstimatedArrival.Arrival.Time.Substring(4, 2);
                    switch (serviceSummary.Service.Code)
                    {
                        case "1DA":
                            model.deliveryTimestamp = caculatedDate.groundFreeStartDate;
                            if (chargedFeeStateList.Contains(_shippingAddress.State))
                            {
                                model.totalNetCharge = "45.00";
                            }
                            else{
                                model.totalNetCharge = "32.00";
                            }
                            break;
                        case "1DAS":
                            model.deliveryTimestamp = caculatedDate.groundFreeStartDate;
                            if (chargedFeeStateList.Contains(_shippingAddress.State))
                            {
                                model.totalNetCharge = "45.00";
                            }
                            else{
                                model.totalNetCharge = "32.00";
                            }
                            break;
                        case "2DA":
                            model.deliveryTimestamp = caculatedDate.groundFreeStartDate.AddDays(1);
                            if (chargedFeeStateList.Contains(_shippingAddress.State))
                            {
                                model.totalNetCharge = "34.00";
                            }
                            else{
                                model.totalNetCharge = "17.00";
                            }
                            break;
                        case "2DAS":
                            model.deliveryTimestamp = caculatedDate.groundFreeStartDate.AddDays(1);
                            if (chargedFeeStateList.Contains(_shippingAddress.State))
                            {
                                model.totalNetCharge = "34.00";
                            }
                            else{
                                model.totalNetCharge = "17.00";
                            }
                            break;
                        case "3DS":
                            model.deliveryTimestamp = caculatedDate.groundFreeStartDate.AddDays(2);
                            if (chargedFeeStateList.Contains(_shippingAddress.State))
                            {
                                model.totalNetCharge = "28.00";
                            }
                            else{
                                model.totalNetCharge = "12.00";
                            }
                            break;
                    }
                    model.serviceTypeCode = transferCode.Code;
                    model.serviceType = transferCode.Description;
                    if ( (yellow_state.Contains(_shippingAddress.State) || pink_state.Contains(_shippingAddress.State)) 
                     &&  ((serviceSummary.Service.Code == "1DA") || (serviceSummary.Service.Code == "1DAS") || (serviceSummary.Service.Code == "2DA") || (serviceSummary.Service.Code == "2DAS")) )
                    {
                       result.Add(model);
                    }
                    else  if (!yellow_state.Contains(_shippingAddress.State) && !pink_state.Contains(_shippingAddress.State)) {
                       result.Add(model);
                    }
                }
            }
            return result;
        }
        public ServiceCode TransferCode(string code, List<ServiceSummaryItem> listServiceSummary)
        {
            switch (code)
            {
                case "1DA":
                    if (!listServiceSummary.Any(s => s.Service.Code == "1DAS"))//if not Saturday Delivery
                        return new ServiceCode("01", "Next Day Air");
                    break;
                case "1DAS":
                    return new ServiceCode("01", "Next Day Air (Saturday Delivery)", true);
                case "2DA":
                    if (!listServiceSummary.Any(s => s.Service.Code == "2DAS"))
                        return new ServiceCode("02", "2nd Day Air");
                    break;
                case "2DAS":
                    return new ServiceCode("02", "2nd Day Air (Saturday Delivery)", true);
                case "3DS":
                    return new ServiceCode("12", "3 Day Select");
            }
            return null;
        }
        public class ServiceCode
        {
            public string Code { get; set; }
            public string Description { get; set; }
            public bool IsSaturdayDelivery { get; set; }
            public ServiceCode(string code, string description, bool isSaturdayDelivery = false)
            {
                Code = code;
                Description = description;
                IsSaturdayDelivery = isSaturdayDelivery;
            }
        }
    }
}
