﻿﻿using Harriscomm.Common;
using Harriscomm.Data.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
namespace UpsService
{
    public class Details
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
    public class DetailsDate
    {
        public string Date { get; set; }
        public string Time { get; set; }
    }
    #region  RateResponse
    public class RateResponseObj
    {
        public RateResponse RateResponse { get; set; }
    }

    public class RateResponse
    {
        public object Response { get; set; }
        public RatedShipment RatedShipment { get; set; }
    }
    public class RatedShipment
    {
        public object Service { get; set; }
        public object RatedShipmentAlert { get; set; }
        public object BillingWeight { get; set; }
        public object TransportationCharges { get; set; }
        public object ServiceOptionsCharges { get; set; }
        public Charges TotalCharges { get; set; }
        public object RatedPackage { get; set; }

        public object NegotiatedRateCharges { get; set; }
        public object GuaranteedDelivery { get; set; }
        public TimeInTransitGND  TimeInTransit { get; set; }
    }  
    public class TimeInTransitGND
    { 
        public ServiceSummaryItem ServiceSummary { get; set; } 
    }  
    public class Charges
    {
        public string CurrencyCode { get; set; }
        public string MonetaryValue { get; set; }
    }
    #endregion
    #region  TimeInTransitResponse
    public class TimeInTransitResponseObj
    {
        public TimeInTransitResponse TimeInTransitResponse { get; set; }
    }
    public class TimeInTransitResponse
    {
        public Object Response { get; set; }

        public TransitResponse TransitResponse { get; set; }
    }
    public class TransitResponse
    {
        public Object ShipFrom { get; set; }
        public Object ShipTo { get; set; }
        public string PickupDate { get; set; }
        public Object ShipmentWeight { get; set; }
        public string MaximumListSize { get; set; }
        public List<ServiceSummaryItem> ServiceSummary { get; set; }
    }
    public class ServiceSummaryItem
    {
        public Details Service { get; set; }
        public string GuaranteedIndicator { get; set; }
        public EstimatedArrival EstimatedArrival { get; set; }

    }
    public class EstimatedArrival
    {
        public string BusinessDaysInTransit { get; set; }
        public string DayOfWeek { get; set; }
        public string CustomerCenterCutoff { get; set; }
        public DetailsDate Pickup { get; set; }
        public DetailsDate Arrival { get; set; }
    }
    #endregion
    #region  ResponseRating
    public class ResponseRating
    {
        public string serviceType { get; set; }
        public string serviceTypeCode { get; set; }
        public string totalNetCharge { get; set; }
        public DateTime deliveryTimestamp { get; set; }
        public bool isGroundFree { get; set; }
        public DateTime groundFreeStartDate { get; set; }
        public DateTime groundFreeEndDate { get; set; }
    }
    #endregion
}
