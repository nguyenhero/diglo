﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business.Interfaces
{
    public interface IFilterGroupManager
    {
        IList<FilterGroup> Get(string departmentId, string classId, string subclassId, string kind, string filterId, string q);
    }
}
