﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business.Interfaces
{
    public interface IShippingAddressManager
    {
        int Update(ShippingAddress request);
        int Add(ShippingAddress request);
    }
}
