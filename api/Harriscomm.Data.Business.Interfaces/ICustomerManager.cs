﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business.Interfaces
{
    public interface ICustomerManager
    {
        Customer Get(string email, string password);
        IEnumerable<Customer> GetByEmail(string email);
        IEnumerable<ProductCart> GetCart(int custNo);
        IEnumerable<Product> GetProdRecent(int custNo);  
        IEnumerable<CreditCards> GetCredit(int custNo);
        int CreateUser(CreateUserRequest request);
        int CreateCredit(CreateCreditRequest request);
        int DelCredit(CreateCreditRequest request); 
        int CheckCredit(CreateCreditRequest request); 
        int CreateCart(CreateCartDetailRequest request); 
        int CreateProdRecent(CreateProdRecentRequest request); 
        int DelCart(DelCartRequest request); 
        int UpdateUser(UpdateUserRequest request);
        int UpdateUserPassword(UpdateUserPasswordRequest request);
        int UpdateUserByCustomerNo(ResetPasswordRequest request);
    }
}
