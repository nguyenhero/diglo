﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business.Interfaces
{
    public interface IBillingAddressManager
    {
        int Update(BillingAddress request);
        int Add(BillingAddress request);
    }
}
