﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business.Interfaces
{
    public interface IWishListManager
    {
        int Add(AddWishListRequest request);
        IList<WishList> Get(string custNo);
        int DeleteWishListByCustNo(string custNo);
        int DeleteById(int id);
    }
}
