﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business.Interfaces
{
    public interface IUserManager
    {
        IEnumerable<User> Get();
    }
}
