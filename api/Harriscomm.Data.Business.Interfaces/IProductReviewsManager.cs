﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Business.Interfaces
{
    public interface IProductReviewsManager
    {
        IEnumerable<ProductReviews> Get(string productSKU, int start, int end);
        int Create(CreateProductReviews request);
    }
}
