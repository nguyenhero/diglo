﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer.Interfaces
{
    public interface IShippingAddressDataLayer
    {
        int Update(ShippingAddress request);
        int Add(ShippingAddress request);
    }
}
