﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer.Interfaces
{
    public interface IProductDataLayer
    {
        IEnumerable<Product> Get(GetProductRequest request);
        int GetCount(GetProductRequest request);
        IEnumerable<Product> GetByProductSKU(string productSKU, bool parent);
        IEnumerable<ProductAdditionInfo> GetProductAdditionInfo(string productSKU, int class_subclassid);
        IEnumerable<ProductCount> GetProductCount();
        IEnumerable<Product> GetProductRelated(string productSKU);
    }
}
