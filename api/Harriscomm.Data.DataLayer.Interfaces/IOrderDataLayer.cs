﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer.Interfaces
{
    public interface IOrderDataLayer
    {
        int CheckPromoCode(string promoCode);
        int Add(AddOrderRequest request);
        int AddReviewOrder(AddReviewOrdertRequest request);
        int AddShippingAddress(ShippingAddress request);
        int AddBillingAddress(BillingAddress request);
        BillingAddress GetBillingAddressByEmail(string email);
        IList<ShippingAddress> GetShippingAddressByCustNo(string custNo);
        IList<BillingAddress> GetBillingAddressByCustNo(string custNo);
        IList<OrderHead> Get(string custNo, string monthCount, string orderId);
        int UpdateStatusByOrderId(int orderId, string status);
    }
}
