﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer.Interfaces
{
    public interface IUserDataLayer
    {
        IEnumerable<User> Get();
    }
}
