﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer.Interfaces
{
    public interface IFilterGroupDataLayer
    {
        IList<FilterGroup> Get(string departmentId, string classId, string subclassId, string kind, string filterId, string q);
    }
}
