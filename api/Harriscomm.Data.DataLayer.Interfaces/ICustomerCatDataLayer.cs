﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer.Interfaces
{
    public interface ICustomerCatDataLayer
    {
        int CreateFreeCatalog(CreateFreeCatalogRequest request);
    }
}
