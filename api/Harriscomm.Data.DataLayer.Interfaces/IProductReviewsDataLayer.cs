﻿using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.DataLayer.Interfaces
{
    public interface IProductReviewsDataLayer
    {
        IEnumerable<ProductReviews> Get(string productSku, int start, int end);
        int Create(CreateProductReviews request);
    }
}
