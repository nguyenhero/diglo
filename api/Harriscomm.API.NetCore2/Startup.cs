﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Harriscomm.Common;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Data.Queries.Interface;
using Harriscomm.Data.DataLayer.Interfaces;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Queries;
using Harriscomm.Data.DataLayer;
using Harriscomm.Data.Business;
using Harriscomm.Data.Repository;
namespace Harriscomm.API.NetCore2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddCors();
            services.AddOptions();  // Setup options with DI
            services.Configure<AppSettings>(options => Configuration.GetSection("AppSettings").Bind(options));
            // Add application services.
            // Add application services.
            services.AddTransient<Harriscomm.Common.Interfaces.IConfiguration, Configuration>();
            services.AddTransient<IConnectionFactory, ConnectionFactory>();
            services.AddTransient<IDepartmentQueries, DepartmentQueries>();
            services.AddTransient<IDepartmentDataLayer, DepartmentDataLayer>();
            services.AddTransient<IDepartmentManager, DepartmentManager>();
            services.AddTransient<IUserQueries, UserQueries>();
            services.AddTransient<IUserDataLayer, UserDataLayer>();
            services.AddTransient<IUserManager, UserManager>();
            services.AddTransient<ICustomerQueries, CustomerQueries>();
            services.AddTransient<ICustomerDataLayer, CustomerDataLayer>();
            services.AddTransient<ICustomerManager, CustomerManager>();
            services.AddTransient<ICustomerCatQueries, CustomerCatQueries>();
            services.AddTransient<ICustomerCatDataLayer, CustomerCatDataLayer>();
            services.AddTransient<ICustomerCatManager, CustomerCatManager>();
            services.AddTransient<IStateQueries, StateQueries>();
            services.AddTransient<IStateDataLayer, StateDataLayer>();
            services.AddTransient<IStateManager, StateManager>();
            services.AddTransient<IProductQueries, ProductQueries>();
            services.AddTransient<IProductDataLayer, ProductDataLayer>();
            services.AddTransient<IProductManager, ProductManager>();
            services.AddTransient<IProductReviewsQueries, ProductReviewsQueries>();
            services.AddTransient<IProductReviewsDataLayer, ProductReviewsDataLayer>();
            services.AddTransient<IProductReviewsManager, ProductReviewsManager>();
            services.AddTransient<IOrderQueries, OrderQueries>();
            services.AddTransient<IOrderDataLayer, OrderDataLayer>();
            services.AddTransient<IOrderManager, OrderManager>();
            services.AddTransient<IShippingAddressQueries, ShippingAddressQueries>();
            services.AddTransient<IShippingAddressDataLayer, ShippingAddressDataLayer>();
            services.AddTransient<IShippingAddressManager, ShippingAddressManager>();
            services.AddTransient<IBillingAddressQueries, BillingAddressQueries>();
            services.AddTransient<IBillingAddressDataLayer, BillingAddressDataLayer>();
            services.AddTransient<IBillingAddressManager, BillingAddressManager>();
            services.AddTransient<IShippingChargeQueries, ShippingChargeQueries>();
            services.AddTransient<IShippingChargeDataLayer, ShippingChargeDataLayer>();
            services.AddTransient<IShippingChargeManager, ShippingChargeManager>();
            services.AddTransient<IWishListQueries, WishListQueries>();
            services.AddTransient<IWishListDataLayer, WishListDataLayer>();
            services.AddTransient<IWishListManager, WishListManager>();
            services.AddTransient<IOrderDetailsQueries, OrderDetailsQueries>();
            services.AddTransient<IOrderDetailsDataLayer, OrderDetailsDataLayer>();
            services.AddTransient<IOrderDetailsManager, OrderDetailsManager>();
            services.AddTransient<IFilterGroupQueries, FilterGroupQueries>();
            services.AddTransient<IFilterGroupDataLayer, FilterGroupDataLayer>();
            services.AddTransient<IFilterGroupManager, FilterGroupManager>();
            services.AddTransient<ISuggestionSearchQueries, SuggestionSearchQueries>();
            services.AddTransient<ISuggestionSearchDataLayer, SuggestionSearchDataLayer>();
            services.AddTransient<ISuggestionSearchManager, SuggestionSearchManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // Allow the CORS
            app.UseCors(builder =>
            builder.AllowAnyOrigin().AllowAnyHeader());

            // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
