﻿using Harriscomm.Common;
using Harriscomm.Model;
using Harriscomm.Web.Provider;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace Harriscomm.Controllers
{
    [Route("api/[controller]")]
    public class OrderCheckoutController : Controller
    {
        private readonly AppSettings m_AppSettings;
        private IHostingEnvironment m_HostingEnvironment;
        public OrderCheckoutController(IOptions<AppSettings> appSettings, IHostingEnvironment environment)
        {
            m_AppSettings = appSettings.Value;
            m_HostingEnvironment = environment;
        }
        [HttpPost]
        public int SendEmail([FromBody]SendOrderCheckout request)
        {
            int errorNumber = 0;
            string body = CreateEmailBody(request);
            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = body;
            bool sendEmail = SendEmailProvider.SendEmailOrdeCheckout(m_AppSettings, request.ToEmail, request.ToName, bodyBuilder);

            return errorNumber = sendEmail ? 1 : 2;
        }
        private string CreateEmailBody(SendOrderCheckout request)
        {
            var path = System.IO.Path.Combine(m_HostingEnvironment.WebRootPath, "send-message-order-checkout.html");

            using (StreamReader reader = System.IO.File.OpenText(path))
            {
                string body = reader.ReadToEnd();
                List<string> productList = new List<string>();
                var productListString = "";
                for (int i = 0; i < request.OtherCheckoutDetail.Count; i++)
                {
                    string product = "<tr><td style ='padding:5px; text-align:left;'>" + request.OtherCheckoutDetail[i].ProductName + "</td>" +
                                    "<td style = 'padding:5px; text-align:left;' width = '27%'>" + request.OtherCheckoutDetail[i].ProductSKU + "</td>" +
                                    "<td style = 'padding:5px; text-align:right;' width = '13%'>$" + request.OtherCheckoutDetail[i].Price + "</td>" +
                                    "<td style = 'padding:5px; text-align:right;' width = '13%'>" + request.OtherCheckoutDetail[i].Qty + "</td>" +
                                    "<td style = 'padding:5px; text-align:right;' width = '7%'>$" + request.OtherCheckoutDetail[i].SubTotal + "</td></tr>";
                    productList.Add(product);
                }
                for (int i = 0; i < productList.Count; i++)
                {
                    productListString += productList[i];
                }
                if (body != null && body != "")
                {
                    body = body.Replace("{{OrderNumber}}", request.OrderNumber);
                    body = body.Replace("{{shippingFullName}}", request.ShippingFullName);
                    body = body.Replace("{{shippingAddress}}", request.ShippingAddress);
                    body = body.Replace("{{shippingApt}}", request.ShippingApt);
                    body = body.Replace("{{shippingCity}}", request.ShippingCity);
                    body = body.Replace("{{shippingState}}", request.ShippingState);
                    body = body.Replace("{{shippingZip}}", request.ShippingZip);
                    body = body.Replace("{{shippingCountry}}", request.ShippingCountry);
                    body = body.Replace("{{shippingPhone}}", request.ShippingPhone);
                    body = body.Replace("{{billingFullName}}", request.BillingFullName);
                    body = body.Replace("{{billingAddress}}", request.BillingAddress);
                    body = body.Replace("{{billingApt}}", request.BillingApt);
                    body = body.Replace("{{billingCity}}", request.BillingCity);
                    body = body.Replace("{{billingState}}", request.BillingState);
                    body = body.Replace("{{billingZip}}", request.BillingZip);
                    body = body.Replace("{{billingCountry}}", request.BillingCountry);
                    body = body.Replace("{{billingPhone}}", request.BillingPhone);
                    body = body.Replace("{{shipProvider}}", request.ShipProvider);
                    body = body.Replace("{{productList}}", productListString);
                    body = body.Replace("{{total}}", request.total.ToString());
                    body = body.Replace("{{tax}}", request.tax.ToString());
                    body = body.Replace("{{checkoutDate}}", DateTime.Now.ToString());
                    body = body.Replace("{{shipFee}}", request.ShipFee.ToString());
                    body = body.Replace("{{shippingType}}", request.ShippingType.ToString());
                    body = body.Replace("{{paymentType}}", request.PaymentType.ToString());
                    return body;
                }
            }
            return string.Empty;
        }
    }
}
