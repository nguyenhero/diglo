﻿using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Linq;
using Harriscomm.Web.Model;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class ShareWishListController : Controller
    {
        private readonly IWishListManager m_WishListManager;
        private IHostingEnvironment m_HostingEnvironment;
        private readonly ICustomerManager m_CustomerManager;
        private readonly AppSettings m_AppSettings;

        public ShareWishListController(ICustomerManager customerManager, IOptions<AppSettings> appSettings,
            IWishListManager customerCatManager, IHostingEnvironment environment)
        {
            m_WishListManager = customerCatManager;
            m_HostingEnvironment = environment;
            m_CustomerManager = customerManager;
            m_AppSettings = appSettings.Value;
        }

        [HttpPost]
        public int SendEmail([FromBody]ShareWishRequest request)
        {
            int errorNumber = 0;
            Customer customer = GetByEmail(request.FromEmails);

            if (customer == null) return errorNumber;
            string body = CreateEmailBody(customer, request);
            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = body;
            bool sendEmail = SendEmailProvider.SendEmailShareWishList(m_AppSettings, request, customer, bodyBuilder);

            return errorNumber = sendEmail ? 1 : 2;
        }

        private string CreateEmailBody(Customer customer, ShareWishRequest request)
        {
            var path = System.IO.Path.Combine(m_HostingEnvironment.WebRootPath, "share-wish-list-template.html");
            string resetPasswordUrl = Request.HttpContext.Request.Scheme + "://" +
             Request.HttpContext.Request.Host.Value +
             "/reset-password/" + customer.CustNo +
             "/" + DateTime.Now.AddHours(4).ToString("yyyyMMddHHmmss");
            string homeUrl = Request.HttpContext.Request.Scheme + "://" +
             Request.HttpContext.Request.Host.Value + "/home";

            string logoUrl = Request.HttpContext.Request.Scheme + "://" +
            Request.HttpContext.Request.Host.Value + "/images/logo.png";
            string productList = CreateProductList(request);
            using (StreamReader reader = System.IO.File.OpenText(path))
            {
                string body = reader.ReadToEnd();
                if (body != null && body != "")
                {
                    body = body.Replace("{{LogoUrl}}", logoUrl);
                    body = body.Replace("{{CustFullName}}", customer.CustFirstName + " " + customer.CustLastName);
                    body = body.Replace("{{CustFirstName}}", customer.CustFirstName);
                    body = body.Replace("{{CustEmail}}", customer.CustEmail);
                    body = body.Replace("{{HomeUrl}}", homeUrl);
                    body = body.Replace("{{ProductList}}", productList);
                    body = body.Replace("{{Comments}}", request.Messages);
                    return body;
                }
            }
            return string.Empty;
        }

        private string CreateProductList(ShareWishRequest rquest)
        {
            string body = "";

            foreach (var item in rquest.WishList)
            {
                string imageUrl = Request.HttpContext.Request.Scheme + "://" +
           Request.HttpContext.Request.Host.Value + "/images/" + item.Product.ImageMain;
                string productsUrl = Request.HttpContext.Request.Scheme + "://" +
         Request.HttpContext.Request.Host.Value + "/all-products";

                body += "<div class='item-content'>";
                body += "<div class='col-item'>";
                body += "<div class='photo'>";
                body += "<img class='img-responsive' src='" + imageUrl + "'/>";
                body += "</div>";
                body += "<div class='info'>";
                body += "<a class='productDesc'>" + item.Product.ProductDesc + "</a><br/>";
                body += "</div>";
                body += "<div class='btn-view-details'>";
                body += "<a class='productDesc' href='" + productsUrl + "'>View Products</a><br/>";
                body += "</div>";
                body += "</div>";
                body += "</div>";
            }

            return body;
        }
        private Customer GetByEmail(string email)
        {
            IEnumerable<Customer> customers = m_CustomerManager.GetByEmail(email);

            if (customers != null && customers.Count() > 0)
            {
                return customers.FirstOrDefault();
            }

            return null;
        }
    }
}
