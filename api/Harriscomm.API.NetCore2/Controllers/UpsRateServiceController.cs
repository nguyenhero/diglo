﻿﻿using Harriscomm.Common;
using Harriscomm.Web.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Harriscomm.Data.Entities;
using UpsService;
namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class UpsRateServiceController : Controller
    {
        private readonly AppSettings m_AppSettings;
        public UpsRateServiceController(IOptions<AppSettings> appSettings)
        {
            m_AppSettings = appSettings.Value;
        }
        [HttpPost]
        public async Task<List<ResponseRating>> GetUpsRates([FromBody] GetUpsRatesRequest request)
        {
            UpsRateServiceProvider UpsProvider = new UpsRateServiceProvider(m_AppSettings, request.ShippingAddress,
                request.WeightTotal,
                request.WeightMeasurement,
                request.WeightMeasurementDescription
                );
            // Step2 : getRate
            var packageType = "02";
            var listReponse = await UpsProvider.GetResponseRating(DateTime.Today,
             packageType
            //  , listServiceType
             );
            return listReponse;
        }
        [Route("gndrate")]
        [HttpPost]
        public async Task<ResponseRating> GetUpsGNDRates([FromBody] GetUpsRatesRequest request)
        {
            UpsRateServiceProvider UpsProvider = new UpsRateServiceProvider(m_AppSettings, request.ShippingAddress,
                request.WeightTotal,
                request.WeightMeasurement,
                request.WeightMeasurementDescription
                );
            // Step2 : getRate
            var packageType = "02";
            var response = await UpsProvider.GetGroundRate(DateTime.Today,packageType);
            return response;
        }
        public class GetUpsRatesRequest
        {
            public ShippingAddress ShippingAddress { get; set; }
            public string WeightTotal { get; set; }
            public string WeightMeasurement { get; set; } = "Lbs";
            public string WeightMeasurementDescription { get; set; } = "pounds";
        }
    }
}
