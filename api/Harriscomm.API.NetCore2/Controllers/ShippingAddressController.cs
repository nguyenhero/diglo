﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class ShippingAddressController : Controller
    {
        private readonly IOrderManager m_OrderManager;
        private readonly IShippingAddressManager m_ShippingAddressManager;
        public ShippingAddressController(IOrderManager orderManager,
            IShippingAddressManager shippingAddressManager)
        {
            m_OrderManager = orderManager;
            m_ShippingAddressManager = shippingAddressManager;
        }

        [HttpGet]
        public IList<ShippingAddress> Get(string custNo)
        {
            IList<ShippingAddress> shippingAddresses = new List<ShippingAddress>();
            var result = m_OrderManager.GetShippingAddressByCustNo(custNo);
            if (result != null) { shippingAddresses = result; }
            return shippingAddresses;
        }

        [HttpPut("{id}")]
        public int Put(int id, [FromBody]ShippingAddress request)
        {
            request.AddrID = id;
            return m_ShippingAddressManager.Update(request);
        }
        [HttpPost]
        public int Post([FromBody]ShippingAddress request)
        {
            return m_ShippingAddressManager.Add(request);
        }
    }
}
