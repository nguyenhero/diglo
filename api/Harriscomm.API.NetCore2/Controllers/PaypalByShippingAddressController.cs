﻿using EDWrapper;
using EDWrapper.Internals;
using EDWrapper.Models;
using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class PaypalByShippingAddressController : Controller
    {
        private readonly AppSettings m_AppSettings;
        private readonly IOrderManager m_OrderManager;
        private readonly ICustomerManager m_CustomerManager;
        public PaypalByShippingAddressController(IOrderManager orderManager,
            ICustomerManager customerManager,
            IOptions<AppSettings> appSettings)
        {
            m_OrderManager = orderManager;
            m_AppSettings = appSettings.Value;
        }

        [HttpPost]
        public async Task<PayPalResponse> GetToken([FromBody]CheckoutPayPalRequest request)
        {
            PayPalResponse payPalResponse = new PayPalResponse();
            var dict = await PayPalProvider.SetExpressCheckout(m_AppSettings, request);

            if (dict["ACK"] == "Success")
            {
                string token = dict["TOKEN"];
                var url = string.Format("{0}?cmd=_express-checkout&token={1}",
                m_AppSettings.ExpressCheckoutSetting.UrlEndpoint, token);
                payPalResponse.SecureToken = token;
                payPalResponse.Result = 0;
                payPalResponse.Url = m_AppSettings.ExpressCheckoutSetting.UrlEndpoint;

            }
            else
            {
                payPalResponse.Result = -1;
                payPalResponse.ErrorMsg = dict["L_SHORTMESSAGE0"];
            }

            return payPalResponse;

        }
    }
}
