﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class PromoCodeController : Controller
    {
        private readonly IOrderManager m_OrderManager;

        public PromoCodeController(IOrderManager orderManager)
        {
            m_OrderManager = orderManager;
        }

        [HttpPost]
        public int CheckPromoCode(string promoCode)
        {
            return m_OrderManager.CheckPromoCode(promoCode);
        }
    }
}
