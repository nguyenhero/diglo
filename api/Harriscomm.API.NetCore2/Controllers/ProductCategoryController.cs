﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Linq;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class ProductCategoryController : Controller
    {
        private readonly IProductManager m_ProductManager;

        public ProductCategoryController(IProductManager productManager)
        {
            m_ProductManager = productManager;
        }

        [HttpGet]
        public IList<Department> Get()
        {
            List<Department> departments = new List<Department>();
            IEnumerable<ProductCount> productCounts = m_ProductManager.GetProductCount();

            var departmentDictionary = new Dictionary<int, Department>();
            var classDictionary = new Dictionary<int, Class>();

            foreach (ProductCount productCount in productCounts)
            {
                Department departmentEntry;
                Class classEntry;
                Subclass subclass = CreateSubclass(productCount);

                if (!departmentDictionary.TryGetValue(productCount.DepartmentID, out departmentEntry))
                {
                    Department department = CreateDepartment(productCount);
                    departmentEntry = department;
                    departmentDictionary.Add(departmentEntry.DepartmentID, departmentEntry);
                }

                if (!classDictionary.TryGetValue(productCount.ClassID, out classEntry))
                {
                    Class classItem = CreateClass(productCount);
                    classEntry = classItem;
                    classEntry.Subclasses.Add(subclass);
                    classDictionary.Add(classEntry.ClassID, classEntry);
                    departmentEntry.Classes.Add(classEntry);
                }
                else
                {
                    foreach (var item in departmentEntry.Classes)
                    {
                        if (item.ClassID == classEntry.ClassID)
                        {
                            item.Subclasses.Add(subclass);
                        }
                    }
                }
            }
            foreach (var dic in departmentDictionary)
            {
                departments.Add(dic.Value);
            }
            return departments;
        }
        private Subclass CreateSubclass(ProductCount productCount)
        {
            return new Subclass()
            {
                ClassID = productCount.ClassID,
                SubclassID = productCount.SubclassID,
                SubclassDesc = productCount.SubclassDesc,
                Count = productCount.SubProdCnt,
                ClearanceCount = productCount.SubCLCnt,
                NewArrivalsCount = productCount.SubNewCnt
            };
        }
        private Department CreateDepartment(ProductCount productCount)
        {
            return new Department()
            {
                DepartmentID = productCount.DepartmentID,
                DepartmentDesc = productCount.DepartmentDesc,
                Count = productCount.DeptProdCnt
            };
        }

        private Class CreateClass(ProductCount productCount)
        {
            return new Class()
            {
                DepartmentID = productCount.DepartmentID,
                ClassID = productCount.ClassID,
                ClassDesc = productCount.ClassDesc,
                Count = productCount.ClassProdCnt
            };
        }
    }
}
