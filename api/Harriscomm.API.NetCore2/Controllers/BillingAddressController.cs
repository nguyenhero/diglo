﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class BillingAddressController : Controller
    {
        private readonly IOrderManager m_OrderManager;
        private readonly IBillingAddressManager m_BillingAddressManager;
        public BillingAddressController(IOrderManager orderManager, IBillingAddressManager billingAddressManager)
        {
            m_OrderManager = orderManager;
            m_BillingAddressManager = billingAddressManager;
        }

        [HttpGet]
        public IList<BillingAddress> Get(string custNo)
        {
            IList<BillingAddress> billingAddresses = new List<BillingAddress>();
            var result = m_OrderManager.GetBillingAddressByCustNo(custNo);
            if (result != null) { billingAddresses = result; }
            return billingAddresses;
        }

        [HttpPut("{id}")]
        public int Put(int id, [FromBody]BillingAddress request)
        {
            request.AddrID = id;
            return m_BillingAddressManager.Update(request);
        }
        [HttpPost]
        public int Post([FromBody]BillingAddress request)
        {
            return m_BillingAddressManager.Add(request);
        }
    }
}
