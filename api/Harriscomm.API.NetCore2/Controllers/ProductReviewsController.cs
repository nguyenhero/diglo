﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.Encodings.Web;
namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class ProductReviewsController : Controller
    {
        private readonly IProductReviewsManager m_ProductReviewsManager;

        public ProductReviewsController(IProductReviewsManager productReviewsManager)
        {
            m_ProductReviewsManager = productReviewsManager;
        }
        [HttpGet]
        public IEnumerable<ProductReviews> Get(string productSku, int start, int end)
        {
            var result = m_ProductReviewsManager.Get(productSku, start, end);
            if (result != null)
            {
                foreach (var productReview in result)
                {
                    if (!string.IsNullOrEmpty(productReview.RevComments))
                    {
                        var str = System.Net.WebUtility.HtmlDecode(productReview.RevComments);
                        productReview.RevComments = str;
                    }
                }
            }
            return result;
        }
        [HttpPost]
        public int Create([FromBody]CreateProductReviews request)
        {
            return m_ProductReviewsManager.Create(request);
        }
    }
}
