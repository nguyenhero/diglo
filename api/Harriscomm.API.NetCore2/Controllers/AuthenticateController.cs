﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Utilities;
using Harriscomm.Web.Auth;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticateController : Controller
    {
        private readonly ICustomerManager m_CustomerManager;

        public AuthenticateController(ICustomerManager customerManager)
        {
            m_CustomerManager = customerManager;
        }
        [HttpPost]
        public Customer Login([FromBody]CreateUserRequest request)
        {
            request.Password = Cryptography.Encrypt(request.Password);
            Customer customer = m_CustomerManager.Get(request.Email, request.Password);
            return customer;
        }
        private string GenerateToken(Customer customer, DateTime expires)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(customer.CustEmail, "TokenAuth"),
                new[] {
                    new Claim("ID", customer.CustID.ToString())
                }
            );
            var securityToken = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = TokenAuthOption.Issuer,
                Audience = TokenAuthOption.Audience,
                SigningCredentials = TokenAuthOption.SigningCredentials,
                Subject = identity,
                Expires = expires
            });
            return tokenHandler.WriteToken(securityToken);
        }
    }
}
