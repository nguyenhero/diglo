﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Linq;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class ProductRelated : Controller
    {
        private readonly IProductManager m_ProductManager;

        public ProductRelated(IProductManager productManager)
        {
            m_ProductManager = productManager;
        }

        [HttpGet]
        public IList<Product> Get(string productSku)
        {
            List<Product> products = new List<Product>();

            var result = m_ProductManager.GetProductRelated(productSku);
            if (result != null && result.Count() > 0)
            {
                products = result.ToList();
            }
            return products;
        }

    }
}
