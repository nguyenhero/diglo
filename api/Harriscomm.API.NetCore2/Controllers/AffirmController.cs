﻿using EDWrapper;
using EDWrapper.Internals;
using EDWrapper.Models;
using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class AffirmController : Controller
    {
        const string AFFIRM_COMPLETED = "AffirmCompleted";

        private readonly AppSettings m_AppSettings;
        private readonly IOrderManager m_OrderManager;
        private readonly ICustomerManager m_CustomerManager;
        public AffirmController(IOrderManager orderManager,
            ICustomerManager customerManager,
            IOptions<AppSettings> appSettings)
        {
            m_OrderManager = orderManager;
            m_AppSettings = appSettings.Value;
        }

        [HttpPost]
        public async Task<RedirectResult> Post()
        {
            if (HttpContext.Request.Form != null && HttpContext.Request.Form.Count() > 0
                && !string.IsNullOrEmpty(HttpContext.Request.Form["checkout_token"]))
            {
                string token = HttpContext.Request.Form["checkout_token"];
                var response = await AffirmProvider.Checkout(m_AppSettings, token);
                int orderId = 0;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    string responseEncode = System.Net.WebUtility.UrlDecode(responseString);
                    var deserializedObject = JsonConvert.DeserializeObject<Dictionary<string, object>>(responseEncode);
                    orderId = Convert.ToInt32(deserializedObject["order_id"].ToString());
                    var updateResult = m_OrderManager.UpdateStatusByOrderId(orderId, AFFIRM_COMPLETED);
                    orderId = updateResult > 0 ? orderId : 0;
                }

                string url = string.Format("{0}?orderNumber={1}",
                     m_AppSettings.ExpressCheckoutSetting.ReturnUrl2, orderId);
                return RedirectPermanent(url);
            }

            return null;
        }
    }
}
