﻿using EDWrapper;
using EDWrapper.Internals;
using EDWrapper.Models;
using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly AppSettings m_AppSettings;
        private readonly IOrderManager m_OrderManager;
        private readonly ICustomerManager m_CustomerManager;
        const string CREDIT_CARD_COMPLETED = "CreditCardCompleted";
        const string CREDIT_CARD_IN_PROGRESS = "CreditCardInProgress";
        const string PAY_PAL_COMPLETED = "PayPalCompleted";
        const string PAY_PAL_FAILD = "PayPalFaild";
        const string PAY_PAL_IN_PROGRESS = "PayPalInProgress";

        public OrderController(IOrderManager orderManager,
            ICustomerManager customerManager,
            IOptions<AppSettings> appSettings)
        {
            m_OrderManager = orderManager;
            m_AppSettings = appSettings.Value;

            RequestOptions.ApiKey = m_AppSettings.ApiKeyCampaigner;
            RequestOptions.Endpoint = m_AppSettings.EndPointCampaigner;
        }
        [HttpGet]
        public IList<OrderHead> Get(string custNo, string monthCount, string orderId)
        {
            return m_OrderManager.Get(custNo, monthCount, orderId);
        }

        [HttpPost]
        public async Task<AddOrderResponse> AddOrder([FromBody] AddOrderRequest request)
        {
            AddOrderResponse addOrderResponse = new AddOrderResponse();
            if (request == null)
            {
                addOrderResponse.Result = -1;
                addOrderResponse.ErrorMsg = "Invalid Request";
                return addOrderResponse;
            }

            try
            {
                request.OrderHead.Status = GetStatus(Convert.ToInt32(request.OrderHead.PaymentType));
                int result = AddOrderRequest(request);
                if (result > 0)
                {
                        // Trigger NAV
                        Process proc = null;
                        var procs = Process.Start("C:\\test_2_vs2017\\Console_Web_SaleOrderNav_Soap.exe");
                        if (procs == null){
                           //InternalServerError();
                        }
                        else{
                            procs.WaitForExit();
                            procs.Close();
                        }

                    if (request.OrderHead.PaymentType == ((int)PaymentType.CreditCard).ToString())
                    {
                        addOrderResponse = await PayFlowProvider.SendPayFlow(m_AppSettings, request);
                        if (addOrderResponse.Result != -1)
                        {
                            var updateResult = m_OrderManager.UpdateStatusByOrderId(result, CREDIT_CARD_COMPLETED);
                            addOrderResponse.Result = updateResult > 0 ? result : 0;
                        }
                    }
                    else if (request.OrderHead.PaymentType == ((int)PaymentType.Paypal).ToString())
                    {
                        request.OrderHead.OrderID = result;
                        addOrderResponse = await DoExpressCheckout(request);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(request.Token))
                        {
                            var affirmResponse = await AffirmProvider.Checkout(m_AppSettings, request.Token);
                            addOrderResponse.Result = affirmResponse.StatusCode == HttpStatusCode.OK ? 1 : 0;
                        }
                    }
                }
                else
                {
                    addOrderResponse.Result = result;

                }
            }
            catch (Exception ex)
            {
                addOrderResponse.Result = -1;
                addOrderResponse.ErrorMsg = ex.Message;
            }

            return addOrderResponse;
        }
        public int AddOrderRequest(AddOrderRequest request)
        {
            if (string.IsNullOrEmpty(request.User.CustEmail))
            {
                OrderTransalator.CreateUser(request);
            }
            OrderTransalator.CreateShippingCharge(request);

            var result = m_OrderManager.Add(request);

            if (result > 0 && !request.OrderHead.CustNo.Equals("-1"))
            {
                CreateTasks(request);
            }

            return result;
        }
        public async Task<string> Subscriber(string email, string flagg)
        {
            string body = string.Empty;

            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(flagg))
            {
                string path = flagg.Equals("Add") ? "/Subscribers" : "/Subscribers/Remove";
                SubscriberDetails subscriber = new SubscriberDetails()
                {
                    EmailAddress = email
                };
                var subscribers = new SubscriberAdd(subscriber);
                body = serializeObject(subscribers);
                RequestHelper requestHelper = new RequestHelper();
                string content = await requestHelper.Post(path, body);
                return content;
            }
            return null;
        }
        private string GetStatus(int paymentType)
        {
            switch (paymentType)
            {
                case (int)PaymentType.CreditCard:
                    return CREDIT_CARD_IN_PROGRESS;
                case (int)PaymentType.Paypal:
                    return PAY_PAL_IN_PROGRESS;
            }
            return null;
        }
        private static string serializeObject(object payload)
        {
            if (payload == null)
                return string.Empty;

            return JsonConvert.SerializeObject(payload, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            });
        }
        private void CreateTasks(AddOrderRequest request)
        {
            request.BillingAddress.CustNo = request.OrderHead.CustNo;
            request.BillingAddress.Email = request.OrderHead.BillingEmail;
            request.ShippingAddress.CustNo = request.OrderHead.CustNo;
            request.ShippingAddress.Email = request.OrderHead.BillingEmail;
            if (request.BillingAddress != null && request.BillingAddress.AddrID == 0)
            {
                Task.Run(() => m_OrderManager.AddBillingAddress(request.BillingAddress));
            }
            if (request.ShippingAddress != null && request.ShippingAddress.AddrID == 0)
            {
                Task.Run(() => m_OrderManager.AddShippingAddress(request.ShippingAddress));
            }
            if (request.SubcribeNewletter == 1)
            {
                Task.Run(() => Subscriber(request.User.CustEmail, "Add"));
            }
        }
        private async Task<AddOrderResponse> DoExpressCheckout([FromBody]AddOrderRequest request)
        {
            CheckoutPayPalRequest checkoutPayPalRequest = new CheckoutPayPalRequest()
            {
                ShippingAddress = request.ShippingAddress,
                Amount = request.OrderHead.Total,
                OrderId = request.OrderHead.OrderID.ToString()
            };
            AddOrderResponse response = new AddOrderResponse();
            var dict = await PayPalProvider.SetExpressCheckout(m_AppSettings, checkoutPayPalRequest);
            if (dict["ACK"] == "Success")
            {
                string token = dict["TOKEN"];
                var url = string.Format("{0}?cmd=_express-checkout&token={1}",
                m_AppSettings.ExpressCheckoutSetting.UrlEndpoint, token);
                response.Result = 0;
                response.SecuretokenId = url;
                response.Securetoken = token;
            }
            else
            {
                response.Result = -1;
                response.ErrorMsg = dict["L_SHORTMESSAGE0"];
            }

            return response;
        }
    }
}

