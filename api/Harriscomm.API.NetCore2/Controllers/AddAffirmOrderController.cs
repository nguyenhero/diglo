﻿using EDWrapper;
using EDWrapper.Internals;
using EDWrapper.Models;
using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class AddAffirmOrderController : Controller
    {
        const string AFFIRM_PROGRESS = "AffirmInProgress";

        private readonly AppSettings m_AppSettings;
        private readonly IOrderManager m_OrderManager;
        private readonly ICustomerManager m_CustomerManager;
        public AddAffirmOrderController(IOrderManager orderManager,
            ICustomerManager customerManager,
            IOptions<AppSettings> appSettings)
        {
            m_OrderManager = orderManager;
            m_AppSettings = appSettings.Value;
        }
        [HttpPost]
        public CheckoutAffimObject AddOrder([FromBody] AddOrderRequest request)
        {
            CheckoutAffimObject checkoutAffimObject = new CheckoutAffimObject()
            {
                FinancialProductKey = m_AppSettings.AffirmSetting.FinancialProductKey,
                ConfirmationUrl = m_AppSettings.AffirmSetting.ReturnUrl,
                CancelUrl = m_AppSettings.AffirmSetting.CancelUrl,
                BillingAddress = request.BillingAddress,
                ShippingAddress = request.ShippingAddress,
                OrderDetails = request.OrderDetails,
                OrderHead = request.OrderHead
            };
            request.OrderHead.Status = AFFIRM_PROGRESS;
            int result = AddOrderRequest(request);
            if (result > 0)
            {
                checkoutAffimObject.OrderId = result;
            }

            return checkoutAffimObject;
        }
        public int AddOrderRequest(AddOrderRequest request)
        {
            if (string.IsNullOrEmpty(request.User.CustEmail))
            {
                OrderTransalator.CreateUser(request);
            }
            OrderTransalator.CreateShippingCharge(request);

            var result = m_OrderManager.Add(request);

            if (result > 0 && !request.OrderHead.CustNo.Equals("-1"))
            {
                CreateTasks(request);
            }

            return result;
        }
        private void CreateTasks(AddOrderRequest request)
        {
            request.BillingAddress.CustNo = request.OrderHead.CustNo;
            request.BillingAddress.Email = request.OrderHead.BillingEmail;

            request.ShippingAddress.CustNo = request.OrderHead.CustNo;
            request.ShippingAddress.Email = request.OrderHead.BillingEmail;

            if (request.BillingAddress != null && request.BillingAddress.AddrID == 0)
            {
                Task.Run(() => m_OrderManager.AddBillingAddress(request.BillingAddress));
            }
            if (request.ShippingAddress != null && request.ShippingAddress.AddrID == 0)
            {
                Task.Run(() => m_OrderManager.AddShippingAddress(request.ShippingAddress));
            }
            if (request.SubcribeNewletter == 1)
            {
                Task.Run(() => Subscriber(request.User.CustEmail, "Add"));
            }
        }
        public async Task<string> Subscriber(string email, string flagg)
        {
            string body = string.Empty;
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(flagg))
            {
                string path = flagg.Equals("Add") ? "/Subscribers" : "/Subscribers/Remove";
                SubscriberDetails subscriber = new SubscriberDetails()
                {
                    EmailAddress = email
                };
                var subscribers = new SubscriberAdd(subscriber);
                body = serializeObject(subscribers);
                RequestHelper requestHelper = new RequestHelper();
                string content = await requestHelper.Post(path, body);
                return content;
            }
            return null;
        }
        private static string serializeObject(object payload)
        {
            if (payload == null)
                return string.Empty;
            return JsonConvert.SerializeObject(payload, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}
