﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class WishListController : Controller
    {
        private readonly IWishListManager m_WishListManager;

        public WishListController(IWishListManager customerCatManager)
        {
            m_WishListManager = customerCatManager;
        }

        [HttpGet]
        public IList<WishList> Get(string custNo)
        {
            return m_WishListManager.Get(custNo);
        }
        [HttpPost]
        public int Add([FromBody]AddWishListRequest request)
        {
            if (request != null)
            {
                request.CreateDatetime = DateTime.Now;
                request.UpdateDatetime = DateTime.Now;
                return m_WishListManager.Add(request);
            }
            else
            {
                return 0;
            }

        }
        [HttpDelete("{id}")]
        public int DeleteById(int id)
        {
            return m_WishListManager.DeleteById(id);
        }
        [HttpDelete]
        public int DeleteWishListByCustNo(string custNo)
        {
            return m_WishListManager.DeleteWishListByCustNo(custNo);
        }
        
    }
}
