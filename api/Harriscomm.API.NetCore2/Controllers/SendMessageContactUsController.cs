﻿using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Linq;
using Harriscomm.Web.Model;

namespace Harriscomm.Controllers
{
    [Route("api/[controller]")]
    public class SendMessageContactUsController : Controller
    {
        private readonly AppSettings m_AppSettings;
        private IHostingEnvironment m_HostingEnvironment;
        public SendMessageContactUsController(ICustomerManager customerManager, IOptions<AppSettings> appSettings,
            IWishListManager customerCatManager, IHostingEnvironment environment)
        {
            m_AppSettings = appSettings.Value;
            m_HostingEnvironment = environment;
        }
        [HttpPost]
        public int SendEmail([FromBody]SendContactUs request)
        {
            int errorNumber = 0;
            string body = CreateEmailBody(request);
            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = body;
            bool sendEmail = SendEmailProvider.SendEmailContactUs(m_AppSettings, request.ToEmail, request.ToName, bodyBuilder);

            return errorNumber = sendEmail ? 1 : 2;
        }
        private string CreateEmailBody(SendContactUs request)
        {
            var path = System.IO.Path.Combine("send-message-contract-us.html");
            if (!string.IsNullOrEmpty(m_HostingEnvironment.WebRootPath))
                path = System.IO.Path.Combine(m_HostingEnvironment.WebRootPath, "send-message-contract-us.html");

            using (StreamReader reader = System.IO.File.OpenText(path))
            {
                string body = reader.ReadToEnd();
                if (body != null && body != "")
                {
                    body = body.Replace("{{ToName}}", request.ToName);
                    body = body.Replace("{{ClientName}}", request.ClientName);
                    body = body.Replace("{{ClientEmail}}", request.ClientEmail);
                    body = body.Replace("{{Telephone}}", request.Telephone); 
                    body = body.Replace("{{Comments}}", request.Comment);
                    return body;
                }
            }
            return string.Empty;
        }
    }
}
