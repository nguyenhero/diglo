﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using Harriscomm.Common;
using Harriscomm.Utilities;
using Harriscomm.Web.Helpers;
using Harriscomm.Web.Provider;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using System.IO;
using System.Linq;
using System.Text;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class ProductDetailsController : Controller
    {
        private readonly IProductManager m_ProductManager;
        private readonly ICustomerManager m_CustomerManager;
        public ProductDetailsController(IProductManager productManager , ICustomerManager customerManager)
        {
            m_ProductManager = productManager;
            m_CustomerManager = customerManager;
        }
        [HttpGet]        
        public IEnumerable<Product> GetproductSKU(string productSKU, bool parent)
        {
            return m_ProductManager.GetByProductSKU(productSKU, parent);
        }
        [HttpPut("{custNo}")]
        public IList<Product> getProdRecent(int custNo,string request)
        {
            List<Product> products = new List<Product>();
            var result = m_CustomerManager.GetProdRecent(custNo);
            if (result != null && result.Count() > 0)
            {
                products = result.ToList() ;
            }
            return products;
        }
        [HttpPost]
        public int createProdRecent([FromBody]CreateProdRecentRequest request)
        {
            int prodRecent = m_CustomerManager.CreateProdRecent(request);
            return prodRecent;
        }
    }
}
