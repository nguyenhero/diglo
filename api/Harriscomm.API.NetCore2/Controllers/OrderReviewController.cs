﻿using EDWrapper;
using EDWrapper.Internals;
using EDWrapper.Models;
using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class OrderReviewController : Controller
    {
        private readonly AppSettings m_AppSettings;
        private readonly IOrderManager m_OrderManager;
        private readonly ICustomerManager m_CustomerManager;
        public OrderReviewController(IOrderManager orderManager,
            ICustomerManager customerManager,
            IOptions<AppSettings> appSettings)
        {
            m_OrderManager = orderManager;
            m_AppSettings = appSettings.Value;
        }
        [HttpGet]
        public async Task<ExpressCheckoutDetails> GetExpressDetails(string token)
        {
            ExpressCheckoutDetails expressCheckoutDetails = new ExpressCheckoutDetails();
            var dict = await PayPalProvider.GetExpressCheckoutDetails(m_AppSettings, token);
            if (dict["ACK"] == "Success")
            {
                expressCheckoutDetails = new ExpressCheckoutDetails()
                {
                    FirstName = dict["FIRSTNAME"],
                    LastName = dict["LASTNAME"],
                    Countrycode = dict["COUNTRYCODE"],
                    Email = dict["EMAIL"],
                    ShipToName = dict["SHIPTONAME"],
                    ShipToStreet = dict["SHIPTOSTREET"],
                    ShipToCity = dict["SHIPTOCITY"],
                    ShipToState = dict["SHIPTOSTATE"],
                    ShipToZip = dict["SHIPTOZIP"],
                    ShipToCountryCode = dict["SHIPTOCOUNTRYCODE"],
                    Token = dict["TOKEN"]
                };
            }
            else
            {
                expressCheckoutDetails.MsgError = dict["L_SHORTMESSAGE0"];
            }
            return expressCheckoutDetails;
        }
        [HttpPost]
        public async Task<AddOrderResponse> Save([FromBody]AddReviewOrdertRequest request)
        {
            AddOrderResponse response = new AddOrderResponse();
            var dict = await PayPalProvider.DoExpressCheckoutPayment(m_AppSettings, request);
            if (dict["ACK"] == "Success")
            {
                var result = m_OrderManager.AddReviewOrder(request);
                response.Result = result;
            }
            else
            {
                response.Result = -1;
                response.ErrorMsg = dict["L_SHORTMESSAGE0"];
            }
            return response;
        }
    }
}
