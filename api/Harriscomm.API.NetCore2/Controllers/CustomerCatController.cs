﻿using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class CustomerCatController : Controller
    {
        private readonly ICustomerCatManager m_CustomerCatManager;

        public CustomerCatController(ICustomerCatManager customerCatManager)
        {
            m_CustomerCatManager = customerCatManager;
        }

        [HttpPost]
        public int CreateFreeCatalog([FromBody]CreateFreeCatalogRequest request)
        {
            request.CreatedDate = DateTime.Now;
            return m_CustomerCatManager.CreateFreeCatalog(request);
        }
    }
}
