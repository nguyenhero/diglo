﻿using Harriscomm.Common;
using Harriscomm.Data.Business.Interfaces;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Request;
using Harriscomm.Utilities;
using Harriscomm.Web.Helpers;
using Harriscomm.Web.Provider;
using Harriscomm.Web.Transalator;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Harriscomm.Web.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly ICustomerManager m_CustomerManager;
        private readonly AppSettings m_AppSettings;
        public CustomerController(ICustomerManager customerManager, IOptions<AppSettings> appSettings)
        {
            m_CustomerManager = customerManager;
            m_AppSettings = appSettings.Value;
        }     
        [HttpGet]
        public int GetByEmail(string email)
        {
            IEnumerable<Customer> customers = m_CustomerManager.GetByEmail(email);
            if (customers != null && customers.Count() > 0)
            {
                return 1;
            }

            return 0;
        }
        [HttpPost]
        public int addCredit([FromBody]CreateCreditRequest request)
        {
            CustomerTransalator.UpdateCreateCreditRequest(request);
            int createCredit  = m_CustomerManager.CreateCredit(request);         
            return createCredit;
        }
        [HttpPut("{custNo}")]
        public IList<CreditCards> getCredit(int custNo,string request)
        {
            List<CreditCards> products = new List<CreditCards>();
            var result = m_CustomerManager.GetCredit(custNo);
            if (result != null && result.Count() > 0)
            {   
                products = result.ToList() ;
            }
            foreach (var p in products)
            {
                p.cardVerifyNumber = Cryptography.Decrypt(p.cardVerifyNumber);
                p.creditCardNumber = Cryptography.Decrypt(p.creditCardNumber);
            }
            return products;
        }
        [HttpPut]
        public int delCredit([FromBody]CreateCreditRequest request)
        {
            request.creditCardNumber = Cryptography.Encrypt(request.creditCardNumber);
            request.cardVerifyNumber = Cryptography.Encrypt(request.cardVerifyNumber);
            return m_CustomerManager.DelCredit(request);           
        }
    }
}
