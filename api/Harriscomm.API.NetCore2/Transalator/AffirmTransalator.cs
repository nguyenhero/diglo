﻿using Harriscomm.Common;
using Harriscomm.Data.Entities;
using Harriscomm.Entities.Enum;
using Harriscomm.Entities.Request;
using Harriscomm.Utilities;
using Harriscomm.Web.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Web.Transalator
{
    public static class AffirmTransalator
    {
        private static List<Item> CreateItems(AddOrderRequest request)
        {
            List<Item> items = new List<Model.Item>();
            if (request.OrderDetails != null)
            {
                request.OrderDetails.ForEach(x =>
                {
                    Item item = new Item()
                    {
                        display_name = x.ProductSKU,
                        sku = x.ProductSKU,
                        qty = x.Quantity.ToString(),
                        unit_price = x.WebPrice > 0 ? x.WebPrice.ToString() : x.CartPrice.ToString(),
                        item_image_url = string.Empty,
                        item_url = string.Empty
                    };
                    items.Add(item);
                });
            }
            return items;
        }
        private static string serializeObject(object payload)
        {
            if (payload == null)
                return string.Empty;
            return JsonConvert.SerializeObject(payload, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}
