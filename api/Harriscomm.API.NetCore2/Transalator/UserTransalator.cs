﻿using Harriscomm.Entities.Enum;
using Harriscomm.Entities.Request;
using Harriscomm.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Transalator
{
    public static class UserTransalator
    {
        public static void UpdateCreateUserRequest(CreateUserRequest request)
        {
            request.Password = Cryptography.Encrypt(request.Password);
            request.Type = CustType.C.ToString();            
            request.CreatedDate = DateTime.Now;
            request.CatStatus = 0;
            request.Status = (char)CustStatus.Active;
            request.IsExported = 0;

        }
    }
}
