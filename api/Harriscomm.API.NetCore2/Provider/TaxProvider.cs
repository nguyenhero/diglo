﻿using Harriscomm.Common;
using Harriscomm.Entities.Request;
using Harriscomm.Web.Model;
using Harriscomm.Web.Transalator;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
namespace Harriscomm.Web.Provider
{
    public static class TaxProvider
    {
        public static async Task<TaxResponse> GetTax(AppSettings appSettings, TaxOrdertRequest request)
        {
            TaxResponse taxResponse = new TaxResponse();
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + appSettings.TaxSetting.AccessToken);
            string json = OrderTransalator.CreateTaxRequest(request);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(appSettings.TaxSetting.UrlEndpoint, content);
            var responseString = await response.Content.ReadAsStringAsync();
            if (responseString.Contains("error"))
            {
                taxResponse.Errors = responseString;
            }
            else
            {
                var deserializedObject = JsonConvert.DeserializeObject<Dictionary<string, object>>(responseString);

                taxResponse = JsonConvert.DeserializeObject<TaxResponse>(deserializedObject["tax"].ToString());
            }
            return taxResponse;
        }
    }
}

