﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Model
{
    public class TaxOrdertRequest
    {
        public BillingAddress BillingAddress { get; set; }
        public ShippingAddress ShippingAddress { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public decimal ExcludingShippingTotal { get; set; }
        public decimal ShipFeeTotal { get; set; }
    }
}
