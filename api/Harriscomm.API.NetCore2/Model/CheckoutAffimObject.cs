﻿using Harriscomm.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Harriscomm.Web.Model
{
    public class CheckoutAffimObject
    {
        public string FinancialProductKey { get; set; }
        public string ConfirmationUrl { get; set; }
        public string CancelUrl { get; set; }
        public int OrderId { get; set; }
        public ShippingAddress ShippingAddress { get; set; }
        public BillingAddress BillingAddress { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public OrderHead OrderHead { get; set; }
    }
    public class Config
    {
        public string financial_product_key { get; set; }
    }
    public class Merchant
    {
        public string user_confirmation_url { get; set; }
        public string user_cancel_url { get; set; }
        public string user_confirmation_url_action { get; set; }
        public string name { get; set; }
    }
    public class Shipping
    {
        public Name name { get; set; }
        public Address address { get; set; }
        public string phone_number { get; set; }
        public string email { get; set; }
    }
    public class Billing
    {
        public Name name { get; set; }
        public Address address { get; set; }
        public string phone_number { get; set; }
        public string email { get; set; }
    }
    public class Name
    {
        public string first { get; set; }
        public string last { get; set; }
    }
    public class Address
    {
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string country { get; set; }
    }
    public class Metadata
    {
        public string mode { get; set; }
    }
    public class Item
    {
        public string display_name { get; set; }
        public string sku { get; set; }
        public string unit_price { get; set; }
        public string qty { get; set; }
        public string item_image_url { get; set; }
        public string item_url { get; set; }
    }
}
