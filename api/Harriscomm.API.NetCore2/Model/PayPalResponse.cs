﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Model
{
    public class PayPalResponse
    {
        public int Result { get; set; }
        public string ErrorMsg { get; set; }
        public string Url { get; set; }
        public string SecureToken { get; set; }
    }
}
