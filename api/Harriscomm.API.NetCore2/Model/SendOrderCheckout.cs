﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Model
{
    public class SendOrderCheckout
    {
        public string ToEmail { get; set; }
        public string ToName { get; set; }
        public string Content { get; set; }
        public string OrderNumber { get; set; }
        public string ShippingFullName { get; set; }
        public string ShippingAddress { get; set; }
        public string ShippingApt { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingZip { get; set; }
        public string ShippingCountry { get; set; }
        public string ShippingPhone { get; set; }
        public string BillingFullName { get; set; }
        public string BillingAddress { get; set; }
        public string BillingApt { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
        public string BillingCountry { get; set; }
        public string BillingPhone { get; set; }
        public string ShipProvider { get; set; }
        public float total { get; set; }
        public float tax { get; set; }
        public float ShipFee { get; set; }
        public string ShippingType { get; set; }
        public string PaymentType { get; set; }
        public List<OtherCheckoutDetailItem> OtherCheckoutDetail { get; set; }
    }
    public class OtherCheckoutDetailItem
    {
        public string ProductName { get; set; }
        public string ProductSKU { get; set; }
        public string Price { get; set; }
        public string Qty { get; set; }
        public string SubTotal { get; set; }
    }
}
