﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Web.Model
{
    public class TaxResponse
    {
        public string order_total_amount { get; set; }
        public string shipping { get; set; }
        public string taxable_amount { get; set; }
        public string amount_to_collect { get; set; }
        public string rate { get; set; }
        public Breakdown breakdown { get; set; }

        public string Errors { get; set; }
}

   public class Breakdown
    {
        public string taxable_amount { get; set; }
        public string combined_tax_rate { get; set; }
        public List<Line_Items> line_items { get; set; }


    }

    public class Line_Items
    {
        public string id { get; set; }
        public string taxable_amount { get; set; }
        public string tax_collectable { get; set; }
        public string state_taxable_amount { get; set; }
        public string state_sales_tax_rate { get; set; }
        public string state_amount { get; set; }
        public string county_taxable_amount { get; set; }
        public string county_tax_rate { get; set; }
        public string county_amount { get; set; }
        public string city_taxable_amount { get; set; }
        public string city_tax_rate { get; set; }
        public string city_amount { get; set; }
        public string special_district_taxable_amount { get; set; }
        public string special_tax_rate { get; set; }
        public string special_district_amount { get; set; }

    }
}
