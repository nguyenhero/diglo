﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
namespace EDWrapper
{
    public class RequestHelper
    {
        public async Task<string> Post(string path, string body)
        {
            string content = await makeRequest(HttpMethod.Post, path, body);
            return content;
        }
        public async Task<string> Put(string path, string body)
        {
            string content = await makeRequest(HttpMethod.Put, path, body);
            return content;
        }
        public async Task<string> Delete(string path, string body)
        {
            string content = await makeRequest(HttpMethod.Delete, path, body);
            return content;
        }
        private async Task<string> makeRequest(HttpMethod method, string path, string body)
        {
            string request = RequestOptions.Endpoint + path;
            var authInfo = Convert.ToBase64String(Encoding.ASCII.GetBytes("x:" + RequestOptions.ApiKey));
            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            using (HttpClient Client = new HttpClient(handler))
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
                using (HttpRequestMessage RequestMessage = new HttpRequestMessage(method, request))
                {
                    if (!string.IsNullOrEmpty(body))
                    {
                        RequestMessage.Content = new StringContent(body, Encoding.UTF8, "application/json");
                    }
                    using (HttpResponseMessage ResponseMessage = await Client.SendAsync(RequestMessage))
                    {
                        string content = await ResponseMessage.Content.ReadAsStringAsync();
                        return content;
                    }
                }

            }
        }
    }
}
