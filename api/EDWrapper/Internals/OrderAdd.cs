﻿using EDWrapper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDWrapper.Internals
{
    public class OrderAdd
    {

        public OrderAdd(Order order)
        {

            this.EmailAddress = order.EmailAddress;
            this.OrderNumber = order.OrderNumber;

            if (order.PurchaseDate < new DateTime(1900, 1, 1))
                this.PurchaseDate = string.Empty;
            else if (order.PurchaseDate > new DateTime(2079, 6, 1))
                this.PurchaseDate = string.Empty;
            else
                this.PurchaseDate = Utils.ToUTC(order.PurchaseDate);

            this.Items = order.Items;

        }

        public string EmailAddress { get; set; }
        public string OrderNumber { get; set; }
        public string PurchaseDate { get; set; }
        public List<OrderItem> Items { get; set; }

    }

    public class SubscriberAdd
    {

        public SubscriberAdd(SubscriberDetails details)
        {

            this.EmailAddress = details.EmailAddress;
            this.CustomFields = details.CustomFields;
            this.SourceID = details.SourceID;
            this.Publications = details.Publications;
            this.Lists = details.Lists;
            this.AutoResponderID = details.AutoResponderID;
            this.Force = details.Force;
            this.ForcePublications = details.ForcePublications;

            if (details.Orders != null && details.Orders.Count > 0)
            {

                this.Orders = new List<OrderAdd>();
                foreach (var order in details.Orders)
                    this.Orders.Add(new OrderAdd(order));

            }

        }

        public string EmailAddress { get; set; }
        public List<CustomField> CustomFields { get; set; }
        public int SourceID { get; set; }
        public List<int> Publications { get; set; }
        public List<int> Lists { get; set; }
        public int AutoResponderID { get; set; }
        public bool Force { get; set; }
        public bool ForcePublications { get; set; }

        public List<OrderAdd> Orders { get; set; }

    }

    internal class EmailList
    {

        public List<string> Emails { get; set; }

    }
}
