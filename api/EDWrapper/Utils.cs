﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDWrapper
{
    public class Utils
    {

        public static string ToUTC(DateTime someDate)
        {

            if (someDate.Kind != DateTimeKind.Utc)
                someDate = someDate.ToUniversalTime();

            return someDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssK");

        }

        public static bool IsNullOrWhiteSpace(string val)
        {

            if (val == null)
                return true;

            return string.IsNullOrEmpty(val.Trim());

        }
    }
}
