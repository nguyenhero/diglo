﻿using Harriscomm.Common;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDWrapper
{
    public static class RequestOptions
    {

        static string apiKey;
        static string endpoint;
        static bool gzip = true;
        static int defaulttimeout = 30000;
        static RequestOptions()
        {
        }

        public static string ApiKey
        {
            get { return apiKey; }
            set { apiKey = value; }
        }

        public static string Endpoint
        {
            get { return endpoint; }
            set { endpoint = value; }
        }

        public static bool Gzip
        {
            get { return gzip; }
            set { gzip = value; }
        }

        public static string Version
        {
            get
            {
                return "1.10";
            }
        }

        public static int DefaultTimeout
        {
            get { return defaulttimeout; }
            set { defaulttimeout = value; }
        }
    }

}
