﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDWrapper.Models
{
    public class HyperMediaLink
    {

        public string rel { get; set; }
        public string href { get; set; }

        public override string ToString()
        {
            return string.Format("{0} : {1}", this.rel, this.href);
        }

    }
}
