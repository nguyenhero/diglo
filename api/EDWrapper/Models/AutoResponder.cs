﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDWrapper.Models
{
    public class AutoResponder
    {

        public int AutoResponderID { get; set; }
        public string Name { get; set; }
        public string ResponderType { get; set; }

        public override string ToString()
        {
            return string.Format("{0} : {1}", this.Name, this.ResponderType);
        }

    }
}
