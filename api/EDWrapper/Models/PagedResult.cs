﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class PagedResult<T>
    {

        public PagedResult() { }

        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalRecords { get; set; }
        public int TotalPages { get; set; }

        public List<T> Items { get; set; }

        public List<HyperMediaLink> Links { get; set; }

    }

}
