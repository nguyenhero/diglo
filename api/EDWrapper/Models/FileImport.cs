﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class FileImport
    {

        public int FileImportID { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public int Records { get; set; }

        public string ImportType { get; set; }
        public string Status { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class FileImportDetails
    {

        public int FileImportID { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public int Records { get; set; }
        public string ImportType { get; set; }
        public string Status { get; set; }
        public Source Source { get; set; }
        public List<Publication> Publications { get; set; }
        public List<List> Lists { get; set; }
        public AutoResponder AutoResponder { get; set; }
        public List<FieldMapping> FieldMappings { get; set; }
        public FileImportResult Results { get; set; }

    }

    public class FieldMapping
    {

        public int FileColumnIndex { get; set; }
        public string ColumnName { get; set; }

        public override string ToString()
        {
            return string.Format("{0} : {1}", this.FileColumnIndex, this.ColumnName);
        }

    }

    public class FileImportResult
    {

        public int Duplicates { get; set; }
        public int FoundButBounced { get; set; }
        public int FoundButRemoved { get; set; }
        public int MalformedEmails { get; set; }
        public int MalformedRecords { get; set; }
        public int RecordsAdded { get; set; }
        public int RecordsDeleted { get; set; }
        public int RecordsRemoved { get; set; }
        public int RecordsUpdated { get; set; }

    }

    public class NewFileImport
    {

        public string LocalFTPFilePath { get; set; }
        public string ImportType { get; set; }
        public string Name { get; set; }
        public string Delimiter { get; set; }
        public List<FieldMapping> FieldMappings { get; set; }
        public int SourceID { get; set; }
        public List<int> Lists { get; set; }
        public List<int> Publications { get; set; }
        public bool SkipFirstLine { get; set; }
        public int AutoResponderID { get; set; }

    }

    public class CopyFileImport
    {

        public string LocalFTPFilePath { get; set; }
        public int CopyFileImportID { get; set; }
        public string Name { get; set; }

    }

}
