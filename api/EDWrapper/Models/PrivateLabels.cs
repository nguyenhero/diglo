﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class ChildAccount
    {

        public string AccountName { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastAction { get; set; }
        public int EmailLimit { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.AccountName;
        }

    }

    public class PrivateLabelSpecs
    {

        public string AccountName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public int EmailLimit { get; set; }

    }

}
