﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDWrapper.Models
{

    public class BulkImportResults
    {

        public int ContactsSubmitted { get; set; }
        public int Successes { get; set; }
        public List<BulkImportFailure> Failures { get; set; }

    }

    public class BulkImportFailure
    {
        public string EmailAddress { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return string.Format("{0} : {1}", this.EmailAddress, this.Message);
        }

    }

    public class BulkOrderResults
    {

        public int OrdersSubmitted { get; set; }
        public int Successes { get; set; }
        public List<OrderFailure> Failures { get; set; }

    }

    public class OrderFailure
    {

        public string EmailAddress { get; set; }
        public string OrderNumber { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }

    }

}
