﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace EDWrapper
{

    public class EDWrapperException : Exception
    {

        private ErrorResult _edError = null;
        private string _serverResponseBody = string.Empty;

        public EDWrapperException(string errorMessage, Exception wex) : base(errorMessage, wex) { }

        public EDWrapperException(string errorMessage, Exception wex, string responseBody)
            : base(errorMessage, wex)
        {

            this._serverResponseBody = responseBody;
            this.Data.Add("EDWrapper.ServerResponse", this._serverResponseBody);

        }

        public EDWrapperException(string errorMessage, Exception wex, string responseBody, ErrorResult edResult)
            : base(errorMessage, wex)
        {

            this._serverResponseBody = responseBody;
            this.Data.Add("EDWrapper.ServerResponse", this._serverResponseBody);
            this.Data.Add("EDWrapper.EDError", edResult);
            this._edError = edResult;
        }


        public ErrorResult EDError
        {
            get
            {
                return this._edError;
            }
        }

        public string ServerResponseBody
        {
            get
            {
                return this._serverResponseBody;
            }
        }

    }
    
    public class ErrorResult
    {

        public int ErrorCode { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return this.Message;
        }

        public enum ErrorEnum
        {

            InternalServerError = 1,
            NotFound = 2,
            Unauthorized = 10,
            SeeResponseBodyForInfo = 11,
            InvalidContentType = 12,

            EmailLimitWillBeReached = 20,

            InvalidPayload = 100,
            InvalidEmail = 101,
            TooManyItems = 102,
            InvalidPageNumber = 103,
            InvalidPageSize = 104,
            InvalidDomainName = 105,

            InvalidSourceName = 130,
            DuplicateSource = 131,
            SourceNotFound = 132,
            LastActiveSource = 133,
            SourceIsNotActive = 134,

            BounceNotFound = 135,

            InvalidListName = 140,
            DuplicateList = 141,
            ListNotFound = 142,
            ListIsNotActive = 143,

            InvalidFilterName = 145,
            DuplicateFilter = 146,
            FilterNotFound = 147,

            InvalidPublicationName = 150,
            DuplicatePublication = 151,
            PublicationNotFound = 152,
            LastActivePublication = 153,
            PublicationIsNotActive = 154,
            InvalidSuppressionName = 160,
            DuplicateSuppression = 161,
            SuppressionNotFound = 162,
            SuppressionIsNotActive = 163,

            InvalidRelaySendCategoryName = 170,
            DuplicateRelaySendCategoryName = 171,
            RelaySendCategoryNotFound = 172,
            RelaySendCategoryIsNotActive = 173,
            ReceiptNotFound = 175,

            InvalidCampaignName = 180,
            DuplicateCampaignName = 181,
            CampaignNotFound = 182,
            CampaignFolderNotFound = 183,
            InvalidCampaignFolderName = 184,
            CampaignFolderAlreadyExists = 185,
            CampaignIsNotActive = 186,

            InvalidWorkflowName = 190,
            DuplicateWorkflowName = 191,
            WorkflowNotFound = 192,
            WorkflowNotActive = 193,

            WorkflowSendNodeNotFound = 195,

            EmailNotFound = 200,
            EmailRemoved = 201,
            EmailBounced = 202,
            EmailAlreadyExists = 204,
            ContactLimitReached = 205,
            EmailNotInPublication = 206,
            EmailRemovedFromPublication = 207,

            SMTPNotFound = 210,
            SMTPInvalidUserName = 211,
            SMTPInvalidPassword = 212,
            SMTPUserNameExists = 213,

            ShortUrlNotFound = 220,
            ShortUrlMalformed = 221,
            ShortUrlVanityExists = 222,

            InvalidDatabaseColumnName = 230,
            InvalidDatabaseColumnType = 231,
            InvalidDatabaseColumnSize = 232,
            DatabaseColumnAlreadyExists = 233,
            DatabaseColumnNotFound = 234,

            ImageFolderNotFound = 240,
            ImageFolderExists = 241,
            InvalidImageFolderName = 242,

            ImageFileNotFound = 243,
            ImageUploadURLMalformed = 244,
            ImageUploadURLNotFound = 245,
            ImageUploadInvalidName = 246,
            ImageUploadInvalidImageType = 247,
            ImageUploadFileExists = 248,
            ImageUploadErrorDownloading = 249,

            ImageStreamEmpty = 250,

            CreativeFolderNotFound = 260,
            CreativeFolderExists = 261,
            InvalidCreativeFolderName = 262,
            CreativeNotFound = 263,

            InvalidCreativeName = 264,
            CreativeAlreadyExistsInFolder = 265,
            CreativeNoMessage = 266,

            ProductCategoryNotFound = 270,
            InvalidCategoryName = 271,
            ProductCategoryExists = 272,

            InvalidProductName = 273,
            InvalidProductSKU = 274,
            NoProductNameOrSKU = 275,
            ProductAlreadyExists = 276,
            ProductNotFound = 277,
            ProductSKUAlreadyExists = 278,

            InvalidOrderNumber = 280,
            NoProductsInOrder = 281,
            OrderNotFound = 282,
            OrderAlreadyExists = 283,

            OrderItemNotFound = 284,

            RelaySend_NoRecipients = 300,
            RelaySend_NoSubject = 301,
            RelaySend_NoMessage = 302,
            RelaySend_TooManyRecipients = 303,

            PrivateLabelAccountNotFound = 320,
            InvalidPrivateLabelAccountName = 321,
            PrivateLabelAccountNameAlreadyExists = 322,
            InvalidPrivateLabelPassword = 323,
            InvalidPrivateLabelEmailLimit = 324,
            PrivateLabelAccountLimitReached = 325,

            FTPFolderNotFound = 330,
            InvalidFTPFolderName = 331,
            FTPFileNotFound = 332,
            FTPFolderExists = 333,
            FTPFolderNotDeletable = 334,
            FTPInvalidUploadURL = 335,
            FTPInvalidFileType = 336,
            FTPFileExists = 337,

            FileImportNotFound = 340,
            FileImportInvalidType = 341,
            FileImportNoRecords = 342,
            FileImportNoEmailColumnSpecified = 343,

            // Campaign Specific
            InvalidCampaignSubject = 350,
            InvalidFromName = 351,
            InvalidFromEmail = 352,
            NoHtmlOrTextSpecified = 353,
            NoPublicationSpecified = 354,
            CampaignCreatedNotScheduled = 355,
            CampaignNotDraft = 356,
            CampaignTargetEmpty = 357,
            InvalidScheduleDate = 358,
            CampaignNotScheduled = 359,
            CampaignCanNotBeCanceled = 360,
            CampaignNotDraftOrSent = 361

        }

    }

}
