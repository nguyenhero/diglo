﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class FTPFolder
    {

        public string FolderName { get; set; }
        public string FolderPath { get; set; }
        public int FileCount { get; set; }
        public List<FTPFolder> Folders { get; set; }
        public string Uri { get; set; }

        public override string ToString()
        {
            return this.FolderName;
        }

    }

    public class FTPFile
    {

        public string FileName { get; set; }
        public string Uri { get; set; }
        public string LocalPath { get; set; }
        public long Size { get; set; }
        public DateTime Created { get; set; }

        public override string ToString()
        {
            return this.FileName;
        }

    }

    public class FileUpload
    {

        public string URL { get; set; }
        public string FileName { get; set; }
        public string FolderPath { get; set; }

        public override string ToString()
        {
            return this.URL;
        }

    }

}
