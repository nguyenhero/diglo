﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class WorkflowDetails
    {

        public int WorkflowID { get; set; }
        public int Members { get; set; }
        public DateTime Created { get; set; }
        public bool IsActive { get; set; }
        public string State { get; set; }
        public string Name { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class WorkflowMember
    {

        public DateTime ActionDate { get; set; }
        public long EmailID { get; set; } //ks port from int to long for campaigner
        public string EmailAddress { get; set; }
        public DateTime Created { get; set; }
        public string Status { get; set; }

        public List<CustomField> CustomFields { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.EmailAddress;
        }

    }

    public class WorkflowStats
    {

        public int WorkflowID { get; set; }
        public string Name { get; set; }

        public int EmailsSent { get; set; }
        public int Opens { get; set; }
        public int UniqueClicks { get; set; }
        public int TotalClicks { get; set; }
        public int Bounces { get; set; }
        public int Removes { get; set; }
        public int Complaints { get; set; }

        public int Delivered { get; set; }
        public double DeliveryRate { get; set; }
        public double OpenRate { get; set; }
        public double CTR { get; set; }
        public double RemoveRate { get; set; }
        public double BounceRate { get; set; }
        public double ComplaintRate { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class WorkflowSendNode
    {

        public int WorkflowID { get; set; }
        public int SendNodeID { get; set; }
        public string CampaignName { get; set; }
        public string Subject { get; set; }
        public DateTime Created { get; set; }
        public bool IsActive { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.CampaignName;
        }

    }

    public class WorkflowSendNodeStats
    {

        public int WorkflowID { get; set; }
        public int SendNodeID { get; set; }

        public string CampaignName { get; set; }
        public string Subject { get; set; }

        public int EmailsSent { get; set; }
        public int Opens { get; set; }
        public int UniqueClicks { get; set; }
        public int TotalClicks { get; set; }
        public int Bounces { get; set; }
        public int Removes { get; set; }
        public int Complaints { get; set; }

        public int Delivered { get; set; }
        public double DeliveryRate { get; set; }
        public double OpenRate { get; set; }
        public double CTR { get; set; }
        public double RemoveRate { get; set; }
        public double BounceRate { get; set; }
        public double ComplaintRate { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.CampaignName;
        }

    }

    public class WorkflowRecipient : Subscriber
    {

        public DateTime SentDate { get; set; }
        public bool Opened { get; set; }
        public bool Clicked { get; set; }
        public bool Removed { get; set; }
        public bool Bounced { get; set; }
        public bool Complained { get; set; }

    }

}
