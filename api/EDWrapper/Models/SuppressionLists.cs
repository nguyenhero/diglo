﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class SuppressionListDetails
    {

        public int SuppressionListID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int SuppressedEmails { get; set; }
        public int SuppressedDomains { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class SuppressedEmail
    {

        public string EmailAddress { get; set; }
        public DateTime Added { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.EmailAddress;
        }

    }


    public class SuppressedDomain
    {

        public string DomainName { get; set; }
        public DateTime Added { get; set; }

        public override string ToString()
        {
            return this.DomainName;
        }

    }

}
