﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class SMTPDetails
    {

        public int RelaySendCategoryID { get; set; }
        public string UserName { get; set; }

        public string Password { get; set; }
        public string Server { get; set; }

        public bool TrackLinks { get; set; }
        public bool Force { get; set; }
        public bool ReceiveReplies { get; set; }
        public bool AddToDatabase { get; set; }

        public List<Publication> Publications { get; set; }
        public List<List> Lists { get; set; }

        public Source Source { get; set; }

        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.UserName;
        }

    }

    public class SMTPLogin
    {

        public string UserName { get; set; }
        public string Password { get; set; }

        public bool TrackLinks { get; set; }
        public bool Force { get; set; }
        public bool ReceiveReplies { get; set; }
        public bool AddToDatabase { get; set; }

        public int SourceID { get; set; }
        public List<int> Publications { get; set; }
        public List<int> Lists { get; set; }

        public override string ToString()
        {
            return this.UserName;
        }

    }

}
