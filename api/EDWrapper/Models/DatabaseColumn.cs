﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class DatabaseColumn
    {

        public string ColumnName { get; set; }
        public string ColumnType { get; set; }
        public int ColumnSize { get; set; }
        public bool IsCustom { get; set; }
        public string Variable { get; set; }

        public override string ToString()
        {
            return this.ColumnName;
        }

    }

    public class DatabaseColumnSpec
    {

        public string ColumnName { get; set; }
        public string ColumnType { get; set; }
        public int ColumnSize { get; set; }

    }

}
