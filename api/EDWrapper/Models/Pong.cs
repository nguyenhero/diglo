﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class Pong
    {

        public string AccountName { get; set; }

        public bool Campaigns_Add { get; set; }
        public bool Campaigns_GetStats { get; set; }

        public bool Creatives_Add { get; set; }
        public bool Creatives_Get { get; set; }

        public bool Database_CreateFields { get; set; }
        public bool Database_ReadFields { get; set; }

        public bool Elements_Create { get; set; }
        public bool Elements_Get { get; set; }

        public bool Emails_AddAndUpdate { get; set; }
        public bool Emails_Get { get; set; }
        public bool Emails_RemoveAndDelete { get; set; }

        public bool ImageLibrary_Access { get; set; }
        public bool PrivateLabel_Access { get; set; }
        public bool PurchaseBehavior_Access { get; set; }
        public bool RelaySends_Access { get; set; }
        public bool ShortUrl_Access { get; set; }
        public bool SMTP_Access { get; set; }
        public bool Workflow_Access { get; set; }

    }

}
