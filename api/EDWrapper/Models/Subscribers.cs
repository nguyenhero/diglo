﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDWrapper.Models
{

    public class SubscriberDetails
    {

        public string EmailAddress { get; set; }
        public List<CustomField> CustomFields { get; set; }
        public int SourceID { get; set; }
        public List<int> Publications { get; set; }
        public List<int> Lists { get; set; }
        public int AutoResponderID { get; set; }
        public bool Force { get; set; }
        public bool ForcePublications { get; set; }

        public List<Order> Orders { get; set; }

    }

    public class Subscriber
    {

        public long EmailID { get; set; } //ks ported from int for campaigner
        public string EmailAddress { get; set; }
        public DateTime Created { get; set; }
        public string Status { get; set; }

        public List<CustomField> CustomFields { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.EmailAddress;
        }

    }

    public class SubscriberAction
    {

        public DateTime ActionDate { get; set; }
        public long EmailID { get; set; } //ks ported from int for campaigner
        public string EmailAddress { get; set; }
        public DateTime Created { get; set; }
        public string Status { get; set; }

        public List<CustomField> CustomFields { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.EmailAddress;
        }

    }

    public class SubscriberProperties
    {

        public long EmailID { get; set; } //ks ported from int for campaigner
        public string EmailAddress { get; set; }
        public DateTime Created { get; set; }
        public string Status { get; set; }

        public List<CustomField> CustomFields { get; set; }
        public List<Publication> Publications { get; set; }
        public List<EDWrapper.Models.List> Lists { get; set; }
        public Source Source { get; set; }

        public List<HyperMediaLink> Links { get; set; }
    }

    public class CustomField
    {

        public string FieldName { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return string.Format("{0} : {1}", this.FieldName, this.Value);
        }

    }

    public class SubscriberHistory
    {

        public string EmailAddress { get; set; }
        public List<HistoryAction> Actions { get; set; }

        public List<HyperMediaLink> Links { get; set; }

    }

    public class HistoryAction
    {

        public string Action { get; set; }
        public DateTime Date { get; set; }
        public string Details { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public ActionType ElementType
        {
            get
            {

                if (this.Links == null || this.Links.Count != 1)
                    return ActionType.Unknown;

                switch (this.Links[0].rel)
                {
                    case "campaign":
                        return ActionType.Campaign;
                    case "publication":
                        return ActionType.Publication;
                    case "relaysend":
                        return ActionType.RelaySend;
                    case "workflowcampaign":
                        return ActionType.WorkflowCampaign;
                    case "workflow":
                        return ActionType.Workflow;
                };

                return ActionType.Unknown;

            }
        }

        public int ElementID
        {
            get
            {

                if (this.Links == null || this.Links.Count == 0)
                    return 0;

                string href = this.Links[0].href;
                int lastIndex = href.LastIndexOf('/');

                if (lastIndex < 0 || lastIndex >= href.Length)
                    return 0;

                int elementID = 0;
                if (int.TryParse(href.Substring(lastIndex + 1), out elementID))
                    return elementID;

                return 0;

            }
        }

        public enum ActionType
        {
            Unknown,
            Campaign,
            Publication,
            RelaySend,
            Workflow,
            WorkflowCampaign
        }

    }

}
