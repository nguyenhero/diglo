﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class ShortUrlDetails
    {

        public DateTime Created { get; set; }
        public string Url { get; set; }
        public string LongUrl { get; set; }
        public int UniqueClicks { get; set; }
        public int TotalClicks { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.LongUrl;
        }

    }

    public class ShortUrlClick
    {

        public DateTime Clicked { get; set; }
        public string IPAddress { get; set; }
        public bool FirstClick { get; set; }
        public string Referrer { get; set; }
        public string UserAgent { get; set; }

        public override string ToString()
        {
            return this.IPAddress;
        }

    }

    public class ShortUrlSpecs
    {

        public string Url { get; set; }
        public string VanityName { get; set; }

    }

}
