﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class TrackedLink
    {

        public int LinkID { get; set; }
        public string URL { get; set; }
        public string FriendlyName { get; set; }
        public int TotalClicks { get; set; }
        public int Uniqueclicks { get; set; }

        public List<HyperMediaLink> Links { get; set; }
    }

    public class Campaign
    {

        public int CampaignID { get; set; }
        public DateTime Created { get; set; }
        public DateTime? ScheduledDate { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class CampaignSpecs
    {

        public string Name { get; set; }

        public int CreativeID { get; set; }

        public string ToName { get; set; }
        public string Subject { get; set; }
        public string FromName { get; set; }
        public string FromEmail { get; set; }

        public int PublicationID { get; set; }
        public int FilterID { get; set; }
        public int ListID { get; set; }
        public int SourceID { get; set; }
        public int GroupID { get; set; }

        public List<int> SuppressionListIDs { get; set; }
        public bool UseGoogleAnalytics { get; set; }

        public List<DefaultValue> DefaultValues { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class DefaultValue
    {

        public string FieldName { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return string.Format("{0} : {1}", this.FieldName, this.Value);
        }

    }

    public class CampaignDetails
    {

        public int CampaignID { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? ScheduleDate { get; set; }

        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string ToName { get; set; }

        public string Subject { get; set; }
        public string ArchiveURL { get; set; }

        public Creative Creative { get; set; }
        public string Target { get; set; }

        public Publication Publication { get; set; }

        public int EmailsSent { get; set; }
        public int Opens { get; set; }
        public int UniqueClicks { get; set; }
        public int TotalClicks { get; set; }
        public int Removes { get; set; }
        public int Forwards { get; set; }
        public int ForwardsFrom { get; set; }
        public int HardBounces { get; set; }
        public int SoftBounces { get; set; }
        public int Complaints { get; set; }

        public int Delivered { get; set; }
        public double DeliveryRate { get; set; }
        public double OpenRate { get; set; }
        public double UniqueRate { get; set; }
        public double CTR { get; set; }
        public double RemoveRate { get; set; }
        public double BounceRate { get; set; }
        public double SoftBounceRate { get; set; }
        public double ComplaintRate { get; set; }

        public List<HyperMediaLink> Links { get; set; }

    }

    public class CampaignEmail
    {

        public int CampaignID { get; set; }

        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string ToName { get; set; }
        public string Subject { get; set; }

        public string HTML { get; set; }
        public string Text { get; set; }

    }



}
