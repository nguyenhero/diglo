﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDWrapper.Models
{
    public class Bounce
    {

        public long EmailID { get; set; } //ks ported from int to long for campaigner
        public string EmailAddress { get; set; }
        public string Status { get; set; }

        public string SendType { get; set; }
        public long BounceID { get; set; } //ks ported from int to long for campaigner
        public DateTime BouncedAt { get; set; }

        public int NDRCode { get; set; }
        public string BounceType { get; set; }

        public string Reason { get; set; }

        public List<HyperMediaLink> Links { get; set; }

    }
}
