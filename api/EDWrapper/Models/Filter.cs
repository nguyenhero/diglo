﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class Filter
    {

        public int FilterID { get; set; }
        public DateTime Created { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime LastUpdated { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class FilterDetails
    {

        public int FilterID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ActiveMembers { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

}
