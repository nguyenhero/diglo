﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class OrderDetails
    {

        public string OrderNumber { get; set; }
        public string EmailAddress { get; set; }

        public DateTime PurchaseDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }

        public int TotalItems { get; set; }
        public double TotalAmount { get; set; }
        public double TotalWeight { get; set; }
        public bool IsActive { get; set; }

        public List<OrderItemDetails> OrderItems { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.OrderNumber;
        }

    }

    public class OrderItemDetails
    {

        public int OrderItemID { get; set; }
        public int ProductID { get; set; }
        public string OrderNumber { get; set; }
        public string EmailAddress { get; set; }

        public string ProductName { get; set; }
        public string SKU { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double Weight { get; set; }
        public string Status { get; set; }
        public double TotalAmount { get; set; }

        public DateTime PurchaseDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return string.Format("SKU: {0}; ProductName: {1}", this.SKU, this.ProductName);
        }

    }

    public class Order
    {

        public string EmailAddress { get; set; }
        public string OrderNumber { get; set; }
        public DateTime PurchaseDate { get; set; }
        public List<OrderItem> Items { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.OrderNumber;
        }

    }

    public class OrderItem
    {

        public string ProductName { get; set; }
        public string SKU { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double Weight { get; set; }
        public string Status { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return string.Format("SKU: {0}; ProductName: {1}", this.SKU, this.ProductName);
        }

    }

    public class ProductDetails
    {

        public int ProductID { get; set; }

        public string SKU { get; set; }
        public string ProductName { get; set; }

        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }

        public string ProductURL { get; set; }
        public string ProductImage { get; set; }

        public double Weight { get; set; }
        public double Cost { get; set; }
        public double Price { get; set; }
        public bool Active { get; set; }

        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return string.Format("SKU: {0}; ProductName: {1}", this.SKU, this.ProductName);
        }

    }

    public class Product
    {

        public string SKU { get; set; }
        public string ProductName { get; set; }

        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }

        public string ProductURL { get; set; }
        public string ProductImage { get; set; }

        public double Weight { get; set; }
        public double Cost { get; set; }
        public double Price { get; set; }

        public List<int> Categories { get; set; }

        public override string ToString()
        {
            return string.Format("SKU: {0}; ProductName: {1}", this.SKU, this.ProductName);
        }

    }

    public class CategoryDetails
    {

        public int CategoryID { get; set; }
        public string Name { get; set; }
        public int ProductCount { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string URL { get; set; }

        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }

        public List<CategoryDetails> Categories { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class Category
    {

        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string URL { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

}
