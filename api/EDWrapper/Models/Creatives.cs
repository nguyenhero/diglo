﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class CreativeSpecs
    {

        public string Name { get; set; }
        public string HTML { get; set; }
        public string Text { get; set; }
        public int CreativeFolderID { get; set; }
        public bool TrackLinks { get; set; }
        public List<DefaultValue> DefaultValues { get; set; }

        public List<HyperMediaLink> Links { get; set; }

    }

    public class Creative
    {

        public int CreativeID { get; set; }
        public string CreativeName { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.CreativeName;
        }

    }

    public class CreativeDetail
    {

        public int CreativeID { get; set; }
        public string CreativeName { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdated { get; set; }

        public string HTML { get; set; }
        public string Text { get; set; }
        public bool TrackingLinks { get; set; }
        public bool IsActive { get; set; }
        public int FolderID { get; set; }

        public List<DefaultValue> DefaultValues { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.CreativeName;
        }

    }

    public class CreativeFolder
    {

        public int FolderID { get; set; }
        public string FolderName { get; set; }
        public int Creatives { get; set; }
        public List<CreativeFolder> Folders { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.FolderName;
        }

    }

}
