﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class ImageFolder
    {

        public string FolderName { get; set; }
        public string FolderPath { get; set; }
        public int FileCount { get; set; }
        public List<ImageFolder> Folders { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.FolderName;
        }

    }

    public class ImageFile
    {

        public string FileName { get; set; }
        public string Uri { get; set; }
        public string LocalPath { get; set; }
        public long Size { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public DateTime Created { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.FileName;
        }

    }

    public class ImageUpload
    {

        public string URL { get; set; }
        public string FileName { get; set; }
        public string FolderPath { get; set; }

    }

}
