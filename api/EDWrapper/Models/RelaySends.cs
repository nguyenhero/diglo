﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class RelayLink
    {
        public string URL { get; set; }
        public int UniqueClicks { get; set; }
        public int TotalClicks { get; set; }
    }

    public class RelayClick
    {
        public DateTime ClickDate { get; set; }
        public string URL { get; set; }
    }

    public class RelaySendCategory
    {

        public int RelaySendCategoryID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class RelaySendCategoryStats
    {

        public int RelaySendCategoryID { get; set; }
        public string Name { get; set; }

        public int EmailsSent { get; set; }
        public int Opens { get; set; }
        public int UniqueClicks { get; set; }
        public int TotalClicks { get; set; }
        public int Bounces { get; set; }
        public int Removes { get; set; }
        public int Complaints { get; set; }

        public int Delivered { get; set; }
        public double DeliveryRate { get; set; }
        public double OpenRate { get; set; }
        public double CTR { get; set; }
        public double RemoveRate { get; set; }
        public double BounceRate { get; set; }
        public double ComplaintRate { get; set; }

        public List<HyperMediaLink> Links { get; set; }

    }

    public class Header
    {

        public string Name { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return string.Format("{0}: {1}", this.Name, this.Value);
        }

    }

    public class Attachment
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public int Size { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class RelaySendDetails
    {

        public int RelaySendReceiptID { get; set; }
        public string EmailAddress { get; set; }
        public DateTime SentDate { get; set; }

        public string ToName { get; set; }
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string ReplyTo { get; set; }
        public string Subject { get; set; }

        public string CustomID { get; set; }
        public List<Header> Headers { get; set; }

        public bool Opened { get; set; }
        public bool Clicked { get; set; }
        public bool Removed { get; set; }
        public bool Bounced { get; set; }
        public bool Complained { get; set; }

        public bool Forced { get; set; }
        public RelaySendCategory RelaySendCategory { get; set; }

        public List<HyperMediaLink> Links { get; set; }

    }

    public class RelaySendMessage
    {

        public int RelaySendReceiptID { get; set; }
        public string EmailAddress { get; set; }
        public DateTime SentDate { get; set; }

        public string ToName { get; set; }
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string ReplyTo { get; set; }
        public string Subject { get; set; }

        public string CustomID { get; set; }
        public List<Header> Headers { get; set; }
        public List<Attachment> Attachments { get; set; }

        public int CreativeID { get; set; }
        public string HTML { get; set; }
        public string Text { get; set; }
        public bool TrackedLinks { get; set; }
        public bool AddedToDatabase { get; set; }

        public bool Opened { get; set; }
        public bool Clicked { get; set; }
        public bool Removed { get; set; }
        public bool Bounced { get; set; }
        public bool Complained { get; set; }

        public bool Forced { get; set; }
        public List<string> Tags { get; set; }
        public RelaySendCategory RelaySendCategory { get; set; }

    }

    public class RelaySendReceipt
    {

        public int RelaySendReceiptID { get; set; }
        public string EmailAddress { get; set; }
        public string Result { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return string.Format("{0} : {1}", this.EmailAddress, this.Result);
        }

    }

    public class RelaySendStat
    {

        public int RelaySendReceiptID { get; set; }
        public string EmailAddress { get; set; }
        public DateTime SentDate { get; set; }

        public bool Opened { get; set; }
        public bool Clicked { get; set; }
        public bool Removed { get; set; }
        public bool Bounced { get; set; }
        public bool Complained { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.EmailAddress;
        }

    }

    public class RelaySendCustomIDStats
    {

        public string CustomID { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int EmailsSent { get; set; }
        public int Opens { get; set; }
        public int UniqueClicks { get; set; }
        public int TotalClicks { get; set; }
        public int Bounces { get; set; }
        public int Removes { get; set; }
        public int Complaints { get; set; }

        public int Delivered { get; set; }
        public double DeliveryRate { get; set; }
        public double OpenRate { get; set; }
        public double CTR { get; set; }
        public double RemoveRate { get; set; }
        public double BounceRate { get; set; }
        public double ComplaintRate { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.CustomID;
        }

    }

}
