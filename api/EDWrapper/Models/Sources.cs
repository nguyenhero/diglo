﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDWrapper.Models
{

    public class Source
    {

        public int SourceID { get; set; }
        public string Name { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

    public class SourceDetails
    {

        public int SourceID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ActiveMembers { get; set; }
        public bool IsActive { get; set; }

        public List<HyperMediaLink> Links { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

    }

}
