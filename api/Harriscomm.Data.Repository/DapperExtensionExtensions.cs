﻿using DapperExtensions;
using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Harriscomm.Data.Repository
{
    public static class DapperExtensionExtensions
    {
        public static object GetPrimaryKey<T>(this IDapperExtensionsConfiguration configuration, T entity) where T : class
        {
            return configuration.GetPrimaryKeyPropertyInfo<T>().GetValue(entity);
        }

        public static PropertyInfo GetPrimaryKeyPropertyInfo<T>(this IDapperExtensionsConfiguration configuration) where T : class
        {
            IClassMapper map = configuration.GetMap<T>();
            IPropertyMap propertyMap = map.Properties.Single(p => p.KeyType != KeyType.NotAKey);
            return propertyMap.PropertyInfo;
        }
    }
}
