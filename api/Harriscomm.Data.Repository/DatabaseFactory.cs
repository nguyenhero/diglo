﻿using DapperExtensions;
using DapperExtensions.Sql;
using Harriscomm.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Repository
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private ISqlGenerator m_SqlGenerator;

        public DatabaseFactory(ISqlGenerator sqlGenerator)
        {
            m_SqlGenerator = sqlGenerator;
        }

        public IDatabase CreateDatabase(DbConnection connection)
        {
            return new Database(connection, m_SqlGenerator);
        }
    }
}
