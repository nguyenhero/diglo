﻿using DapperExtensions;
using Harriscomm.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Repository
{
    public class Repository<T> : ConnectionBase, IRepository<T> where T : class
    {
        private readonly IDatabaseFactory m_DatabaseFactory;

        public Repository(IConnectionFactory connectionFactory, IDatabaseFactory databaseFactory)
            : base(connectionFactory)
        {
            m_DatabaseFactory = databaseFactory;
        }

        public T GetById(object id)
        {
            return Execute(database => database.Get<T>(id));
        }

        public dynamic Insert(T item)
        {
            return Execute(database => database.Insert(item));
        }

        public void Update(T item)
        {
            Execute(database => database.Update(item));
        }

        public void Delete(T entity)
        {
            Execute(database => database.Delete(entity));
        }

        public int GetCount()
        {
            return Execute(database => database.Count<T>(null));
        }

        public IEnumerable<T> GetAll()
        {
            return Execute(database => database.GetList<T>());
        }

        protected TResult Execute<TResult>(Func<IDatabase, TResult> action)
        {
            return Execute(connection =>
            {
                IDatabase database = m_DatabaseFactory.CreateDatabase(connection.Connection); // TODO - if we have Idatabase as Idisposable we have to do using here
                return action(database);
            });
        }

        protected void Execute(Action<IDatabase> action)
        {
            Execute(connection =>
            {
                IDatabase database = m_DatabaseFactory.CreateDatabase(connection.Connection); // TODO - if we have Idatabase as Idisposable we have to do using here
                action(database);
            });
        }
    }
}
