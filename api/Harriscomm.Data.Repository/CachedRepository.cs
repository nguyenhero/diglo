﻿using DapperExtensions;
using Harriscomm.Data.Repository.Interface;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Harriscomm.Data.Repository
{ 
    // This class is not 100% thread safe as it is design with ConcurrentDictionary implies that more than one request can go to the underlying source.
    // This means that a value in the dictionary may be populated more than once (if two simultaneous reads occur on the empty cache)
    // It is designed like that as it should work faster than using locks plus the only problem with this design is double population of cache which is at most is a little performance issue.
    public class CachedRepository<T> : IRepository<T> where T : class // TODO - we need some cache clearing strategy as well + maybe some prepopulation option??
    {
        private readonly IRepository<T> m_Repository;
        private readonly IDapperExtensionsConfiguration m_DapperExtensionsConfiguration;

        private readonly ConcurrentDictionary<object, T> m_Cache = new ConcurrentDictionary<object, T>();
        private bool m_AllLoaded = false;

        public CachedRepository(IRepository<T> repository, IDapperExtensionsConfiguration dapperExtensionsConfiguration)
        {
            m_Repository = repository;
            m_DapperExtensionsConfiguration = dapperExtensionsConfiguration;
        }

        public T GetById(object id)
        {
            T result;
            if (!m_Cache.TryGetValue(id, out result))
            {
                result = m_Repository.GetById(id);

                if (result != null)
                {
                    m_Cache[id] = result;
                }
            }

            return result;
        }

        public dynamic Insert(T entity)
        {
            dynamic result = m_Repository.Insert(entity);

            UpdateCache(entity);

            return result;
        }

        public void Update(T entity)
        {
            m_Repository.Update(entity);

            UpdateCache(entity);
        }

        public void Delete(T entity)
        {
            m_Repository.Delete(entity);

            RemoveFromCache(entity);
        }

        private void UpdateCache(T entity)
        {
            object primaryKey = m_DapperExtensionsConfiguration.GetPrimaryKey(entity);

            m_Cache[primaryKey] = entity;
        }

        private void RemoveFromCache(T entity)
        {
            object primaryKey = m_DapperExtensionsConfiguration.GetPrimaryKey(entity);

            T deletedEntity;
            m_Cache.TryRemove(primaryKey, out deletedEntity);
        }

        public IEnumerable<T> GetAll()
        {
            PopulateCacheWithAllEntitiesIfNeeded();

            return m_Cache.Values;
        }

        private void PopulateCacheWithAllEntitiesIfNeeded()
        {
            if (!m_AllLoaded)
            {
                IEnumerable<T> allEntities = m_Repository.GetAll();

                PropertyInfo propertyInfo = m_DapperExtensionsConfiguration.GetPrimaryKeyPropertyInfo<T>();

                foreach (T entity in allEntities)
                {
                    object primaryKey = propertyInfo.GetValue(entity);
                    m_Cache[primaryKey] = entity;
                }

                m_AllLoaded = true;
            }
        }

        public int GetCount()
        {
            PopulateCacheWithAllEntitiesIfNeeded();

            return m_Cache.Count;
        }
    }
}
