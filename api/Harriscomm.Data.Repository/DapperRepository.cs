﻿using DapperExtensions;
using Harriscomm.Data.Repository.Interface;
using Harriscomm.Utilities;
using Harriscomm.Utilities.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Repository
{
    public class DapperRepository<T> : Repository<T>, IDapperRepository<T> where T : class
    {
        private readonly IPager m_Pager;

        public DapperRepository(IDatabaseFactory databaseFactory, IConnectionFactory connectionFactory, IPager pager)
            : base(connectionFactory, databaseFactory)
        {
            m_Pager = pager;
        }

        public IEnumerable<T> GetAll(IList<ISort> orderBy = null)
        {
            return Get(null, orderBy);
        }

        public IEnumerable<T> Get(IPredicate filter, IList<ISort> orderBy = null)
        {
            return Execute(database => database.GetList<T>(filter, orderBy));
        }

        public IEnumerablePage<T> GetPage(int pageNumber, int pageSize, IPredicate filter = null, IList<ISort> orderBy = null)
        {
            return Execute(database =>
            {
                int count = database.Count<T>(filter);

                IEnumerable<T> result = database.GetPage<T>(filter, orderBy, pageNumber, pageSize);

                return m_Pager.CreatePage(result, pageNumber, pageSize, count);
            });
        }

        public int GetCount(IPredicate filter = null)
        {
            return Execute(database => database.Count<T>(filter));
        }

        public void Delete(IFieldPredicate filter)
        {
            Execute(database => database.Delete<T>(filter));
        }
    }
}
