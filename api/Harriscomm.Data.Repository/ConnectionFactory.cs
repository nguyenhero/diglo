﻿using Harriscomm.Common;
using Harriscomm.Common.Interfaces;
using Harriscomm.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Repository
{
    public class ConnectionFactory : IConnectionFactory
    {
        private string m_ConnectionString;
        private IConfiguration m_configuration;

        public ConnectionFactory(IConfiguration configuration)
        {
            m_configuration = configuration;
        }

        public IDbConnection CreateConnection()
        {
            var result = new SqlConnection(m_configuration.ConnectionString);
            result.Open();

            return result;
        }
    }
}
