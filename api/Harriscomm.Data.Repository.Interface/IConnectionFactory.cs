﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Repository.Interface
{
    public interface IConnectionFactory
    {
        IDbConnection CreateConnection();
    }
}
