﻿using Harriscomm.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Repository.Interface
{
    public abstract class ConnectionBase
    {
        private readonly IConnectionFactory m_ConnectionFactory;

        protected ConnectionBase(IConnectionFactory factory)
        {
            m_ConnectionFactory = factory;
        }

        protected T Execute<T>(Func<IDbConnection, T> action)
        {
            using (var connection = m_ConnectionFactory.CreateConnection())
            {
                return action(connection);
            }
        }

        protected void Execute(Action<IDbConnection> action)
        {
            using (var connection = m_ConnectionFactory.CreateConnection())
            {
                action(connection);
            }
        }
    }
}
