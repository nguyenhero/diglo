﻿using DapperExtensions;
using Harriscomm.Utilities.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Repository
{
    public interface IDapperRepository<T> where T : class
    {
        T GetById(object id);
        IEnumerable<T> Get(IPredicate filter, IList<ISort> orderBy = null);
        IEnumerable<T> GetAll(IList<ISort> orderBy = null);
        IEnumerablePage<T> GetPage(int pageNumber, int pageSize, IPredicate filter = null, IList<ISort> orderBy = null);
        int GetCount(IPredicate filter = null);
        dynamic Insert(T item);
        void Update(T item);
        void Delete(IFieldPredicate filter);
        void Delete(T entity);
    }
}
