﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harriscomm.Data.Repository.Interface
{
    public interface IRepository<T> where T : class
    {
        T GetById(object id);
        IEnumerable<T> GetAll();
        dynamic Insert(T item);
        void Update(T item);
        void Delete(T entity);
        int GetCount();
    }
}
