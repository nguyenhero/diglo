﻿CREATE SCHEMA [harriswebdb] AUTHORIZATION [dbo]
GO
CREATE TABLE [harriswebdb].Customer(
[CustID] [int] IDENTITY(1,1) NOT NULL,
	[CustNo] [nvarchar](20) NOT NULL,
	[CustFirstName] [nvarchar](25) NULL,
	[CustLastName] [nvarchar](25) NULL,
	[CustFullName] [nvarchar](50) NULL,
	[CustAddress] [nvarchar](100) NULL,
	[CustCity] [nvarchar](100) NULL,
	[CustState] [nvarchar](3) NULL,
	[CustZip] [nchar](20) NULL,
	[CustCountry] [nvarchar](3) NULL,
	[CustPhone] [nvarchar](25) NULL,
	[CustEmail] [nvarchar](128) NULL,
	[CustPassword] [nvarchar](128) NULL,
	[CustCreated] [datetime] NULL,
	[Company] [nvarchar](50) NULL,
	[CustType] [nvarchar](3) NULL,
	[CustLastLogin] [datetime] NULL,
	[CustModified] [datetime] NULL,
	[CustCreator] [nvarchar](25) NULL,
	[CustGroup] [nvarchar](3) NULL,
	[CustStatus] [char](1) NULL,
	[CustCatStatus] [tinyint] NULL,
	[IsExported] [tinyint] NULL,
	[IsDeleted] [tinyint] NULL,

 CONSTRAINT [pk_Customer] PRIMARY KEY NONCLUSTERED 
(
	[CustID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] 