﻿GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [harriswebdb].[Class](
	[ClassID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[ClassDesc] [nvarchar](120) NULL,
	[OrderNo] [int] NULL,
	[VatInd] [nchar](1) NULL,
	[CreateDatetime] [datetime] NULL,
	[UpdateDatetime] [datetime] NULL,
	[UpdateUser] [nvarchar](25) NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[ClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [harriswebdb].[Department](
	[DepartmentID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentDesc] [nvarchar](120) NULL,
	[OrderNo] [int] NULL,
	[VatInd] [nchar](1) NULL,
	[CreateDatetime] [datetime] NULL,
	[UpdateDatetime] [datetime] NULL,
	[UpdateUser] [nvarchar](25) NULL,
 CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED 
(
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [harriswebdb].[Subclass](
	[SubclassID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentID] [int] NULL,
	[ClassID] [int] NOT NULL,
	[SubclassDesc] [nvarchar](120) NOT NULL,
	[OrderNo] [int] NULL,
	[VatInd] [nchar](1) NULL,
	[CreateDatetime] [datetime] NULL,
	[UpdateDatetime] [datetime] NULL,
	[UpdateUser] [nvarchar](25) NULL,
 CONSTRAINT [PK_Subclass] PRIMARY KEY CLUSTERED 
(
	[SubclassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [harriswebdb].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_Department] FOREIGN KEY([DepartmentID])
REFERENCES [harriswebdb].[Department] ([DepartmentID])
GO
ALTER TABLE [harriswebdb].[Class] CHECK CONSTRAINT [FK_Class_Department]
GO
ALTER TABLE [harriswebdb].[Subclass]  WITH CHECK ADD  CONSTRAINT [FK_Subclass_Class] FOREIGN KEY([ClassID])
REFERENCES [harriswebdb].[Class] ([ClassID])
GO
ALTER TABLE [harriswebdb].[Subclass] CHECK CONSTRAINT [FK_Subclass_Class]
GO
ALTER TABLE [harriswebdb].[Subclass]  WITH CHECK ADD  CONSTRAINT [FK_Subclass_Department] FOREIGN KEY([DepartmentID])
REFERENCES [harriswebdb].[Department] ([DepartmentID])
GO
ALTER TABLE [harriswebdb].[Subclass] CHECK CONSTRAINT [FK_Subclass_Department]
GO