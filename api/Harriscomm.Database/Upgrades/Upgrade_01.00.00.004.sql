﻿SET IDENTITY_INSERT[harriswebdb].[Department] ON 

GO
INSERT[harriswebdb].[Department] ([DepartmentID], [DepartmentDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (1, N'Alarm', 1, NULL, CAST(0x0000A79800000000 AS DateTime), CAST(0x0000A79800000000 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Department] ([DepartmentID], [DepartmentDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (2, N'Phone', 2, NULL, CAST(0x0000A79800000000 AS DateTime), CAST(0x0000A79800000000 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Department] ([DepartmentID], [DepartmentDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (3, N'Education', 3, NULL, CAST(0x0000A79800000000 AS DateTime), CAST(0x0000A79800000000 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Department] ([DepartmentID], [DepartmentDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (4, N'Listening', 4, NULL, CAST(0x0000A79800000000 AS DateTime), CAST(0x0000A79800000000 AS DateTime), NULL)
GO
SET IDENTITY_INSERT[harriswebdb].[Department] OFF
GO
SET IDENTITY_INSERT[harriswebdb].[Class] ON 

GO
INSERT[harriswebdb].[Class] ([ClassID], [DepartmentID], [ClassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (1, 1, N'Alarms', 1, NULL, CAST(0x0000A7BC0166EE77 AS DateTime), CAST(0x0000A7BE01670EE8 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Class] ([ClassID], [DepartmentID], [ClassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (2, 1, N'Signalers', 2, NULL, CAST(0x0000A7BC0166EE77 AS DateTime), CAST(0x0000A7BE01670EE8 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Class] ([ClassID], [DepartmentID], [ClassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (3, 1, N'Others', 3, NULL, CAST(0x0000A7BC0166EE77 AS DateTime), CAST(0x0000A7BE01670EE8 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Class] ([ClassID], [DepartmentID], [ClassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (4, 2, N'Phones', 1, NULL, CAST(0x0000A7BC0166EE77 AS DateTime), CAST(0x0000A7BE01670EE8 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Class] ([ClassID], [DepartmentID], [ClassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (5, 2, N'Others', 2, NULL, CAST(0x0000A7BC0166EE77 AS DateTime), CAST(0x0000A7BE01670EE8 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Class] ([ClassID], [DepartmentID], [ClassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (6, 3, N'Book & Media', 1, NULL, CAST(0x0000A7BC0166EE77 AS DateTime), CAST(0x0000A7BE01670EE8 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Class] ([ClassID], [DepartmentID], [ClassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (7, 3, N'Novelties & Games', 2, NULL, CAST(0x0000A7BC0166EE77 AS DateTime), CAST(0x0000A7BE01670EE8 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Class] ([ClassID], [DepartmentID], [ClassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (8, 3, N'Others', 3, NULL, CAST(0x0000A7BC0166EE77 AS DateTime), CAST(0x0000A7BE01670EE8 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Class] ([ClassID], [DepartmentID], [ClassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (9, 4, N'Systems', 1, NULL, CAST(0x0000A7BC0166EE77 AS DateTime), CAST(0x0000A7BE01670EE8 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Class] ([ClassID], [DepartmentID], [ClassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (10, 4, N'Others', 1, NULL, CAST(0x0000A7BC0166EE77 AS DateTime), CAST(0x0000A7BE01670EE8 AS DateTime), N'TinhN')
GO
SET IDENTITY_INSERT[harriswebdb].[Class] OFF
GO
SET IDENTITY_INSERT[harriswebdb].[Subclass] ON 

GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (1, 1, 1, N'Clock & Watch', 1, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (2, 1, 1, N'Smokes & Fire', 2, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (3, 1, 1, N'Weather', 3, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (4, 1, 1, N'Carbon Monoxide', 4, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (7, 1, 2, N'Baby Cry', 1, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (8, 1, 2, N'Door & Door bell', 2, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (9, 1, 2, N'Phone & VP', 3, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (10, 1, 2, N'Motion', 4, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (11, 1, 2, N'Recievers', 5, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (12, 1, 2, N'ADA Compliance', 6, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (13, 1, 3, N'Accessories', 1, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (14, 2, 4, N'All Phones', 1, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (15, 2, 5, N'Accessories', 1, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (16, 3, 6, N'Books', 1, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (18, 3, 6, N'Flash Card & Games', 2, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (19, 3, 6, N'Teacher Resources', 3, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (20, 3, 7, N'Novelties', 1, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (21, 3, 7, N'Games and Puzzles', 2, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (22, 3, 7, N'School & Office supplies', 3, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (24, 3, 8, N'Accessories', 1, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (25, 4, 9, N'FM', 1, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (26, 4, 9, N'Headset', 2, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (27, 4, 9, N'Hearing Protection', 3, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (28, 4, 9, N'Looping', 4, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (29, 4, 9, N'Personal Amplifiers', 5, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (30, 4, 9, N'Stethoscopes', 6, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (31, 4, 9, N'TV', 7, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (32, 4, 9, N'Tinitus Makers', 8, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
INSERT[harriswebdb].[Subclass] ([SubclassID], [DepartmentID], [ClassID], [SubclassDesc], [OrderNo], [VatInd], [CreateDatetime], [UpdateDatetime], [UpdateUser]) VALUES (33, 4, 10, N'Accessories', 1, NULL, CAST(0x0000A7BC017362DA AS DateTime), CAST(0x0000A7BE017362E4 AS DateTime), N'TinhN')
GO
SET IDENTITY_INSERT[harriswebdb].[Subclass] OFF
GO
update Department set DepartmentDesc='Alarms' where DepartmentID=1;

INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           1
           ,3
		   ,'Clearance'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           1
           ,3
		   ,'New Arrivals'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           1
           ,3
		   ,'On Sale'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           1
           ,3
		   ,'All Alarms'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           2
           ,5
		   ,'Clearance'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
	INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           2
           ,5
		   ,'On Sale'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
		   INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           2
           ,5
		   ,'New Arrivals'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           3
           ,8
		   ,'Clearance'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
	INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           3
           ,8
		   ,'On Sale'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
		   INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           3
           ,8
		   ,'New Arrivals'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
		   		   INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           3
           ,8
		   ,'All Products'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           4
           ,10
		   ,'Clearance'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
	INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           4
           ,10
		   ,'On Sale'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
		   INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           4
           ,10
		   ,'New Arrivals'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');
		   		   INSERT INTO [harriswebdb].[Subclass]
           ([DepartmentID]
           ,[ClassID]
           ,[SubclassDesc]
           ,[OrderNo]
           ,[VatInd]
           ,[CreateDatetime]
           ,[UpdateDatetime]
           ,[UpdateUser])
     VALUES
           (
           4
           ,10
		   ,'All Devices'
           ,0
           ,Null
           ,GETDATE()
           ,getdate()
           ,'TinhN');		   