﻿update harriswebdb.Products set subclassid=14
GO
CREATE PROCEDURE [harriswebdb].[ProductsGet]
	@Start INT,
	@End INT
AS
BEGIN	

SELECT * ,COUNT(*) OVER () as overallCount
FROM [harriswebdb].[Products]
ORDER BY ProductID
OFFSET @Start ROWS
FETCH NEXT @End ROWS ONLY

END
