﻿
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [harriswebdb].[ProductReview](
	[RevID] [int] IDENTITY(1,1) NOT NULL,
	[CustNo] [nvarchar](20) NULL,
	[ProductSKU] [nvarchar](20) NULL,
	[RevComments] [nvarchar](500) NULL,
	[RevName] [nvarchar](50) NULL,
	[RevStatus] [nchar](1) NOT NULL,
	[RevRate] [int] NULL,
	[ApproveDate] [datetime] NULL,
	[CreateDatetime] [datetime] NULL,
	[UpdateDatetime] [datetime] NULL,
	[UpdateUser] [nvarchar](25) NULL,
	[IsDelete] [tinyint] NULL,
 CONSTRAINT [PK_ProductReview] PRIMARY KEY CLUSTERED 
(
	[RevID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [harriswebdb].[Products](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductSKU] [nvarchar](20) NOT NULL,
	[ProductUPC] [nvarchar](20) NULL,
	[ProductDesc] [nvarchar](250) NULL,
	[ProductShortDesc] [nvarchar](100) NULL,
	[ProductDescUp] [nvarchar](250) NULL,
	[MetaDescription] [nvarchar](4000) NULL,
	[MetaKeyword] [nvarchar](1000) NULL,
	[MetaTitle] [nvarchar](250) NULL,
	[ProductLongDesc] [nvarchar](max) NULL,
	[ProductSpec] [nvarchar](max) NULL,
	[DepartmentID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[SubclassID] [int] NOT NULL,
	[ProductParent] [nvarchar](20) NULL,
	[ProductGrandParent] [nvarchar](20) NULL,
	[Availability] [nvarchar](20) NULL,
	[InTheBoxInd] [nchar](1) NULL,
	[InTheBoxDesc] [nvarchar](max) NULL,
	[ISBN] [nvarchar](50) NULL,
	[Color] [nvarchar](10) NULL,
	[DealerDesc] [nvarchar](max) NULL,
	[GiftMessageAvailable] [nchar](1) NULL,
	[ProductDocument1] [nvarchar](250) NULL,
	[ProductDocument2] [nvarchar](250) NULL,
	[ImageMain] [nvarchar](250) NULL,
	[Image1] [nvarchar](250) NULL,
	[Image2] [nvarchar](250) NULL,
	[Image3] [nvarchar](250) NULL,
	[Image4] [nvarchar](250) NULL,
	[Media] [nvarchar](250) NULL,
	[ProductLevel] [int] NOT NULL,
	[Manufacturer] [nvarchar](500) NULL,
	[manufacturerPart] [nvarchar](500) NULL,
	[MediaType] [nvarchar](10) NULL,
	[Status] [nchar](1) NULL,
	[ReInStockFee] [decimal](20, 2) NULL,
	[MapInd] [nchar](1) NULL,
	[MapPrice] [decimal](20, 2) NULL,
	[RegPrice] [decimal](20, 2) NULL,
	[PromoPrice] [decimal](20, 2) NULL,
	[PromoInd] [nchar](1) NULL,
	[ClearancePrice] [decimal](20, 2) NULL,
	[ClearanceInd] [nchar](1) NULL,
	[ProductQty] [int] NULL,
	[StandardOUM] [nvarchar](4) NULL,
	[UOMConvertFact] [int] NULL,
	[ShipAloneInd] [nchar](1) NULL,
	[IsFreeShip] [nchar](1) NULL,
	[SpecialShipFee] [decimal](20, 2) NULL,
	[EndNewArrivalDate] [datetime] NULL,
	[NewArrivealInd] [nchar](1) NULL,
	[CreateDatetime] [datetime] NULL,
	[UpdateDatetime] [datetime] NULL,
	[UpdateUser] [nvarchar](25) NULL,
	[IsDelete] [tinyint] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [Unique_ProductSKU] UNIQUE NONCLUSTERED 
(
	[ProductSKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [harriswebdb].[ProductReview] ADD  CONSTRAINT [DF_ProductReview_RevStatus]  DEFAULT (N'W') FOR [RevStatus]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_ProductLevel]  DEFAULT ((2)) FOR [ProductLevel]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_Status]  DEFAULT (N'W') FOR [Status]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_PromoInd]  DEFAULT (N'N') FOR [PromoInd]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_ClearanceInd]  DEFAULT (N'N') FOR [ClearanceInd]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_IsFreeShip]  DEFAULT (N'Y') FOR [IsFreeShip]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_NewArrivealInd]  DEFAULT (N'N') FOR [NewArrivealInd]
GO
ALTER TABLE [harriswebdb].[ProductReview]  WITH CHECK ADD  CONSTRAINT [FK_ProductReview_ProductReview] FOREIGN KEY([ProductSKU])
REFERENCES [harriswebdb].[Products] ([ProductSKU])
GO
ALTER TABLE [harriswebdb].[ProductReview] CHECK CONSTRAINT [FK_ProductReview_ProductReview]
GO
ALTER TABLE [harriswebdb].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Class] FOREIGN KEY([ClassID])
REFERENCES [harriswebdb].[Class] ([ClassID])
GO
ALTER TABLE [harriswebdb].[Products] CHECK CONSTRAINT [FK_Products_Class]
GO
ALTER TABLE [harriswebdb].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Department] FOREIGN KEY([DepartmentID])
REFERENCES [harriswebdb].[Department] ([DepartmentID])
GO
ALTER TABLE [harriswebdb].[Products] CHECK CONSTRAINT [FK_Products_Department]
GO
ALTER TABLE [harriswebdb].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Subclass] FOREIGN KEY([SubclassID])
REFERENCES [harriswebdb].[Subclass] ([SubclassID])
GO
ALTER TABLE [harriswebdb].[Products] CHECK CONSTRAINT [FK_Products_Subclass]
GO
