﻿
CREATE TABLE [harriswebdb].[Products](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductSKU] [nvarchar](20) NOT NULL,
	[ProductUPC] [nvarchar](20) NULL,
	[ProductDesc] [nvarchar](250) NULL,
	[ProductShortDesc] [nvarchar](100) NULL,
	[ProductDescUp] [nvarchar](250) NULL,
	[MetaDescription] [nvarchar](4000) NULL,
	[MetaKeyword] [nvarchar](1000) NULL,
	[MetaTitle] [nvarchar](250) NULL,
	[ProductLongDesc] [nvarchar](max) NULL,
	[ProductSpec] [nvarchar](max) NULL,
	[DepartmentID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[SubclassID] [int] NOT NULL,
	[ProductParent] [nvarchar](20) NULL,
	[ProductGrandParent] [nvarchar](20) NULL,
	[Availability] [nvarchar](20) NULL,
	[InTheBoxInd] [nchar](1) NULL,
	[InTheBoxDesc] [nvarchar](max) NULL,
	[ISBN] [nvarchar](50) NULL,
	[Color] [nvarchar](10) NULL,
	[DealerDesc] [nvarchar](max) NULL,
	[GiftMessageAvailable] [nchar](1) NULL,
	[ProductDocument1] [nvarchar](250) NULL,
	[ProductDocument2] [nvarchar](250) NULL,
	[ImageMain] [nvarchar](250) NULL,
	[Image1] [nvarchar](250) NULL,
	[Image2] [nvarchar](250) NULL,
	[Image3] [nvarchar](250) NULL,
	[Image4] [nvarchar](250) NULL,
	[Media] [nvarchar](250) NULL,
	[ProductLevel] [int] NOT NULL,
	[Manufacturer] [nvarchar](500) NULL,
	[manufacturerPart] [nvarchar](500) NULL,
	[MediaType] [nvarchar](10) NULL,
	[Status] [tinyint] NULL,
	[ReInStockFee] [decimal](20, 2) NULL,
	[VendorInd] [tinyint] NULL,
	[VendCartInd] [tinyint] NULL,
	[MapInd] [nchar](1) NULL,
	[MapPrice] [decimal](20, 2) NULL,
	[RegPrice] [decimal](20, 2) NULL,
	[WebPrice] [decimal](20, 2) NULL,
	[CartPrice] [decimal](20, 2) NULL,
	[HarrisPrice] [decimal](20, 2) NULL,
	[PromoPrice] [decimal](20, 2) NULL,
	[PromoInd] [nchar](1) NULL,
	[ClearancePrice] [decimal](20, 2) NULL,
	[ClearanceInd] [nchar](1) NULL,
	[ProductQty] [int] NULL,
	[StandardOUM] [nvarchar](4) NULL,
	[UOMConvertFact] [int] NULL,
	[ShipAloneInd] [nchar](1) NULL,
	[IsFreeShip] [nchar](1) NULL,
	[SpecialShipFee] [decimal](20, 2) NULL,
	[EndNewArrivalDate] [datetime] NULL,
	[NewArrivalInd] [nchar](1) NULL,
	[CreateDatetime] [datetime] NULL,
	[UpdateDatetime] [datetime] NULL,
	[UpdateUser] [nvarchar](25) NULL,
	[IsDelete] [tinyint] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [harriswebdb].[Products] ON 

GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (32, N'AMER-P300', NULL, N'Photo Phone P300 Amplified Phone by Clarity', N'Clarity', N'CLARITY', N'The P300 is an amplified telephone that features Clarity Power technology to make telephone voices louder, clearer and easier to understand for seniors and people with hearing loss. Manufacturer: Clarity', N'photo phone p300, amplified, clarity, hearing loss, big buttons telephones, hearing impaired telephones, picture telephones, phones for seniors, low vision aids', N'Photo Phone P300 Corded Picture Telephones by Clarity', N'The P300&trade; by Clarity&reg;, makes sounds louder, clearer and easier to understand for people with mild-to-moderate hearing loss, with 18 decibel sound amplification and bright visual ring flasher. Programmable picture buttons and high contrast buttons make dialing easier for those with low vision.', N'The P300&trade; by Clarity&reg;, makes sounds louder, clearer and easier to understand for people with mild-to-moderate hearing loss, with 18 decibel sound amplification and bright visual ring flasher. Programmable picture buttons and high contrast buttons make dialing easier for those with low vision.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The P300&trade; amplified corded phone, by Clarity&reg;, features Clarity&reg; Power&trade; technology to make sounds not only louder, but also clearer and easier to understand.<br><br>

With up to 18 decibels of amplification and programmable photo memory buttons, the P300 is an ideal solution for those with mild-to-moderate hearing loss or low vision.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>ClarityÂ® Power&trade; technology
<li>Amplifies incoming sounds up to 18dB
<li>Nine programmable one-touch photo memory buttons
<li>Bright visual ring flasher
<li>75dB adjustable ringer
<li>Illuminated emergency button
<li>Extra large, high contrast buttons
<li>3.5mm neckloop jack
<li>Hearing aid compatible</ul>

<h2>Is this phone right for you?</h2>
This phone is recommended for mild hearing loss. If you miss words or strain to hear the conversation but still manage to get through it, this is the phone for you.<br><br>

<a href="/newsletter/support/AMER-P300_manual.pdf" target="_blank" title="Photo Phone P300 user manual">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (33, N'HC-SPAMP', NULL, N'Speech Amplified Phone', NULL, NULL, N'Speech amplified telephones allows people with a low volume voice to control the loudness of their voice that callers hear through a control switch on the telephone handset.', N'speech amplified telephones, low voice telephones, outgoing voice phone amplifiers', N'Outgoing Speech Amplified Telephone', N'People with a low volume voice can control the loudness their callers hear through a control switch on the handset (26dB gain).', N'People with a low volume voice can control the loudness their callers hear through a control switch on the handset (26dB gain).', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'People with a low volume voice can control the loudness that their caller hears through a control switch on the handset (26dB gain). Available in beige.<br><br>

<h2>Features</h2>
<ul class="disc"><li>26dB amplification 
<li>Hearing aid compatible 
<li>Tone or pulse dialing 
<li>No "redial" button</ul>

<strong>Is this phone right for you?</strong><br>
This phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, NULL, NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (34, N'CL-C4230HS', NULL, N'Clarity Professional C4220/C4230 Expansion Handset', N'Clarity', N'CLARITY', N'Expansion handset for the Clarity Professional C4230 Amplified Phone and the Clarity Professional C4220 Amplified Phone. Manufacturer: Clarity', N'clarity professional, c4220, c4230, expansion handset', N'Clarity Professional C4220 C4230 Expansion Handset', N'Expansion handset for the Clarity Professional C4230 Amplified Phone and the Clarity Professional C4220 Amplified Phone.', N'Expansion handset for the Clarity Professional C4230 Amplified Phone and the Clarity Professional C4220 Amplified Phone.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'Expansion handset for the Clarity Professional C4230 Amplified Phone and the Clarity Professional C4220 Amplified Phone. Up to 3 expansion handsets can be used with each base system. Base system sold separately.<br><br>

<a href="/newsletter/support/CL-C4230HS_manual.pdf" target="_blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (35, N'CL-P400', NULL, N'Clarity P400 Amplified Photo-Phone', N'Clarity', N'CLARITY', N'The Clarity P400 amplified corded phone, featuring Clarity Power technology, makes sounds not only louder up to 26dB, but also clearer and easier to understand. Manufacturer: Clarity', N'clarity, p400, amplified, photo buttons, picture telephones, hearing loss phones, amplification', N'Clarity P400 26dB Amplified Corded Picture Telephone', N'The ClarityÂ® P400&trade; amplified corded phone, featuring ClarityÂ® Power&trade; technology, makes sounds not only louder, but also clearer and easier to understand.', N'The ClarityÂ® P400&trade; amplified corded phone, featuring ClarityÂ® Power&trade; technology, makes sounds not only louder, but also clearer and easier to understand.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The ClarityÂ® P400&trade; amplified corded phone, featuring ClarityÂ® Power&trade; technology, makes sounds not only louder, but also clearer and easier to understand. With up to 26dB of amplification and programmable photo memory buttons, the P400 is an ideal solution for those with mild-to-moderate hearing loss or low vision.<br><br>

<strong>Features:</strong>
<ul class="disc"><li>Clarity Power technology
<li>Amplifies incoming sounds up to 26dB
<li>Nine programmable one-touch photo memory buttons
<li>Bright visual ring indicator
<li>Large, high contrast keypad
<li>Use a 9V battery for brighter visual ringer
<li>3.5mm neckloop jack
<li>Hearing aid compatible</ul>
<strong>Is this phone right for you?</strong> This phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.<br><br>

<a href="/newsletter/support/CL-P400_manual.pdf" target="blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (36, N'FC-PICPHONE', NULL, N'Future Call FC-1007 Amplified Picture Phone', N'Future Call', N'FUTURE CALL', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button. Manufacturer: Future Call', N'future call, picture buttons, dial by picture, amplified corded phone, emergency telephones, hc-ampliphoto50', N'Future Call Amplified Photo Telephone', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button.', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, N'ALLCOLOR', N'Always know who you are calling with this amplified corded picture phone. Ten large memory picture buttons let you quickly dial the person you want to call in just 2 key presses.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Up to 40dB amplification
<li>Loud ringer with visual ring indicator
<li>10 memory picture button keys (switch for one-touch or two-touch dialing)
<li>Emergency button
<li>Stored numbers retain their memory even when unplugged from the telephone line
<li>Store key prevents memory button numbers from being accidentally changed or erased
<li>Redial key
<li>Flash key for call waiting
<li>Line powered
<li>Desk/wall mount
<li>Hearing aid compatible</ul>

<a href="/newsletter/support/FC-PICPHONE_manual.pdf" target="Future Call FC-1007 picture phone user manual">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main_image_red.jpg', N'/image/image_red_1.jpg', N'/image/image_red_2.jpg', N'/image/image_red_3.jpg', N'/image/image_red_1.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 1, N'Future Call', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'Y', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (37, N'CL-C420', NULL, N'Clarity C420 Amplified Phone', N'Clarity', N'CLARITY', N'The Clarity C420 amplified 900MHz cordless phone features Clarity Power technology that makes sounds not only louder, but also clearer and easier to understand. Manufacturer: Clarity', N'clarity, c420, corded amplified phone, hearing impaired, cordless phones, hearing loss, clarity power', N'Clarity C420 Cordless 30dB 900MHz Telephone for Hearing Loss', N'The ClarityÂ® C420â„¢ is an amplified 900MHz cordless phone, featuring ClarityÂ® Powerâ„¢ technology that makes sounds not only louder, but also clearer and easier to understand for people with a hearing loss.', N'The ClarityÂ® C420â„¢ is an amplified 900MHz cordless phone, featuring ClarityÂ® Powerâ„¢ technology that makes sounds not only louder, but also clearer and easier to understand for people with a hearing loss.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The ClarityÂ® C420â„¢ is an amplified telephone with ClarityÂ® Powerâ„¢ technology that makes sounds not only louder, but also clearer and easier to understand. With up to 30 decibels of amplification, the C420 is an ideal solution for those with a mild-to-moderate hearing loss.<br><br>

<strong>Features:</strong>
<ul class="disc"><li>ClarityÂ® Powerâ„¢ technology
<li>Up to 30dB amplification
<li>Call Waiting Caller ID with 50 name and number memory*
<li>Ergonomic phone design
<li>Bright visual ring indicator in base and handset
<li>Customizable handset ringer
<li>Large backlit keypad
<li>10 memory keys
<li>3 one-touch emergency keys
<li>Boost reset switch
<li>Adjustable ringer volume
<li>Last number redial
<li>Tone/Pulse
<li>Flash, Mute, Page buttons
<li>2.5mm audio jack
<li>40-channel operation
<li>Hearing aid compatible</ul>
* May require subscription from local phone company.<br><br>

<a href="/newsletter/support/CL-C420_manual.pdf" target="blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (38, N'CL-C510', NULL, N'Clarity C510 Amplified Telephone', N'Clarity', N'CLARITY', N'The Clarity C510 amplified corded phone makes sounds not only louder, but also clearer and easier to understand. Manufacturer: Clarity', N'clarity, c510, amplified corded phone, moderate hearing loss, impaired', N'Clarity C510 Amplified Telephones for Mild Hearing Loss Impaired', N'The ClarityÂ® C510&trade; amplified corded phone, featuring ClarityÂ® Power&trade; technology, makes sounds not only louder, but also clearer and easier to understand. With up to 30 decibels of amplification, the C510 is an ideal solution for those with a mild-to-moderate hearing loss.', N'The ClarityÂ® C510&trade; amplified corded phone, featuring ClarityÂ® Power&trade; technology, makes sounds not only louder, but also clearer and easier to understand. With up to 30 decibels of amplification, the C510 is an ideal solution for those with a mild-to-moderate hearing loss.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The ClarityÂ® C510&trade; is an amplified corded phone, featuring ClarityÂ® Power&trade; technology, that makes sounds not only louder, but also clearer and easier to understand for people with mild-to-moderate hearing loss. With up to 30 decibels of amplification, the C510 is an ideal solution for hearing better on the telephone.<br><br>

<strong>Features:</strong>
<ul class="disc"><li>Clarity Power technology
<li>Amplifies incoming sounds up to 30dB
<li>Call Waiting Caller ID* with 50 number memory
<li>Loud ringer volume with adjustable ring tone
<li>Large, backlit keypad flashes to indicate incoming calls
<li>Phone can be reset to nominal sound level after each use for multi-user homes
<li>Message waiting indicator* illuminates when messages are received or a call is missed
<li>One-step voicemail* access for quick message retrieval
<li>LCD with large text for easy viewing of names and numbers
<li>Nitelite illuminates the keypad while the phone is in standby mode
<li>Choice of eight ringer volumes and six ring styles
<li>3.5mm neckloop jack
<li>2.5mm headset jack
<li>Hearing aid compatible
<li>Uses phone jack and a standard 120-volt electrical outlet</ul>

*Note: some phone features are available with subscription from local phone company.<br><br>

<strong>Is this phone right for you?</strong> This phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.<br><br>

<a href="javascript:void open(''/newsletter/support/CL-C510_manual.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (39, N'CL-C2210', NULL, N'Clarity Professional C2210 Amplified Phone', N'Clarity', N'CLARITY', N'The Clarity Professional C2210 amplified corded phone, featuring Digital Clarity Power (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable. Manufacturer: Clarity', N'clarity professional, c2210, amplified corded phone, digital clarity power', N'Clarity Professional C2210 Amplified Corded Telephone', N'The Clarity ProfessionalÂ® C2210â„¢ amplified corded phone, featuring Digital Clarity PowerÂ® (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable.', N'The Clarity ProfessionalÂ® C2210â„¢ amplified corded phone, featuring Digital Clarity PowerÂ® (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'<strong>MSRP: $189.95. Our Price: $99.95.</strong>
The Clarity ProfessionalÂ® C2210â„¢ amplified corded phone, featuring Digital Clarity PowerÂ® (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable. Now with up to 50 decibels of amplification, the C2210 is an ideal solution for those with a moderate-to-severe hearing loss. <strong>One year warranty.</strong>

<strong>Features:</strong>
Digital Clarity PowerÂ® technology
Amplifies incoming sounds up to 50dB
Multiband Compression amplifies only the human voice while suppressing other unimportant sounds
Acoustic Echo Cancellation eliminates squealing
Noise Reduction suppresses incoming line and background noises, making conversations more comfortable
Adjustable volume and tone for a customized listening experience
Two customizable tone settings
Call Waiting Caller ID* with 50 name and number memory
Built-in alarm clock
Large lighted dial pad flashes when phone rings
Two one-touch emergency speed dial keys
Adjustable loud ringer (95+ dB)
Lamp button operates the light source connected to the optional Lamp Flasher accessory
Tone/Pulse setting
Redial and Flash buttons
3.5mm bed shaker jack and lamp flash jack (sold separately)
3.5mm neckloop jack (neckloop not included)
2.5mm headset jack (headset not included)
Hearing aid compatible

*Requires subscription from local phone company.

<strong>Is this phone right for you?</strong> This phone is recommended for moderate to severe hearing loss. If you constantly ask the other person to repeat what they are saying and it is still hard to get through the conversation, this is the phone for you.

<a href="javascript:void open(''http://shop.clarityproducts.com/pdf/userguides/51408.001_Manual.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (40, N'CL-C4105', NULL, N'Clarity Professional C4105 Amplified Phone', N'Clarity', N'CLARITY', N'The Clarity Professional C4105 amplified 2.4GHz cordless phone, featuring Clarity Power technology, makes sounds not only louder, but also clearer and easier to understand.', N'clarity professional, c4105, amplified phone, cordless phone, white', N'Clarity Professional C4105 Amplified Phone', N'With up to 40 decibels of amplification, the Clarity ProfessionalÂ® C4105&trade; is an ideal solution for those with a moderate hearing loss or low vision.', N'With up to 40 decibels of amplification, the Clarity ProfessionalÂ® C4105&trade; is an ideal solution for those with a moderate hearing loss or low vision.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The Clarity ProfessionalÂ® C4105&trade; amplified 2.4GHz cordless phone, featuring ClarityÂ® Power&trade; technology, makes sounds not only louder, but also clearer and easier to understand. With up to 40 decibels of amplification, the C4105 is an ideal solution for those with a moderate hearing loss or low vision. <strong>One year warranty.</strong>

<strong>Features:</strong>
ClarityÂ® Powerâ„¢ technology
Up to 40dB amplification
2.5mm headset jack
3.5mm neckloop jack
Adjustable volume and tone for a customized listening experience
Super loud vibrating handset ringer and bright visual ring indicators
Battery backup (4 AA batteries not included)
Large backlit keypad
Hearing aid compatible

<a href="javascript:void open(''/newsletter/support/CL-C4105_manual.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (41, N'CL-C4205', NULL, N'Clarity Professional C4205 Amplified Phone', N'Clarity', N'CLARITY', N'The Clarity Professional C4205 amplified 2.4GHz cordless phone, featuring Digital Clarity Power (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable. Manufacturer: Clarity', N'clarity professional, c4205, amplified phone, cordless phone, white, caller id', N'Clarity Professional C4205 Amplified Cordless Telephone', N'The Clarity ProfessionalÂ® C4205â„¢ amplified 2.4GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable.', N'The Clarity ProfessionalÂ® C4205â„¢ amplified 2.4GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The Clarity ProfessionalÂ® C4205â„¢ amplified 2.4GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable. Now with up to 50 decibels of amplification, the C4205â„¢ is an ideal solution for those with a moderate-to-severe hearing loss or low vision. <strong>One year warranty.</strong>

<iframe width="420" height="315" src="http://www.youtube.com/embed/2tvl5Flu4ms?rel=0" frameborder="0" allowfullscreen></iframe>

<strong>Features:</strong>
<ul class="disc"><li>Digital Clarity PowerÂ® technology
<li>2.4GHz technology
<li>Amplifies incoming sounds up to 50dB
<li>Multiband Compression amplifies the human voice while suppressing other unimportant sounds
<li>Acoustic Echo Cancellation eliminates squealing
<li>Noise Reduction suppresses incoming line and background noises for more comfortable conversations
<li>Adjustable volume and tone for a customized listening experience
<li>Three tone settings
<li>Super loud vibrating handset ringer and bright visual ring indicators
<li>Battery backup in base (four AA alkaline batteries not included)
<li>Three one-touch emergency speed dial keys
<li>Last number redial
<li>Tone/Pulse setting
<li>Flash, Mute, Page
<li>3.5mm neckloop jack
<li>2.5mm headset jack
<li>40 channel operation
<li>Hearing aid t-coil compatible</ul>
<strong>Is this phone right for you?</strong> This phone is recommended for moderate to severe hearing loss. If you constantly ask the other person to repeat what they are saying and it is still hard to get through the conversation, this is the phone for you.

<a href="/newsletter/support/CL-C4205_manual.pdf" target="_blank">[Click for user manual]</a>
<a href="/newsletter/support/CL-C4205_QSG.pdf" target="_blank">[Click for quick start guide]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (42, N'HC-AMPLI600', NULL, N'Geemarc AMPLI600 Amplified Emergency Phone', N'Geemarc', N'GEEMARC', N'The AMPLI600 is an emergency phone with Caller ID, speakerphone and amplification for hearing loss. Dial the phone for help with one key press. Good for seniors living alone and does not require a monthly subscription. Manufacturer: Geemarc', N'geemarc ampli600 emergency telephones, emergency amplified telephones, telephones for hearing loss, telephones for emergencies', N'Geemarc AMPLI600 Cordless Emergency Response Telephones', N'Attain peace of mind for yourself or a loved one who''s living alone. Many accidents happen at home, and with the AMPLI600''s wireless transmitter, loved ones will be able to call family and friends if they can''t reach the telephone. There''s no monthly service or monitoring fees, a 50dB (decibel) incoming amplification, Caller ID and a speakerphone.', N'Attain peace of mind for yourself or a loved one who''s living alone. Many accidents happen at home, and with the AMPLI600''s wireless transmitter, loved ones will be able to call family and friends if they can''t reach the telephone. There''s no monthly service or monitoring fees, a 50dB (decibel) incoming amplification, Caller ID and a speakerphone.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The Emergency Response Amplified Phone has a 50dB (decibel) incoming amplification, Caller ID, speakerphone and emergency connect features all in one. The emergency connect feature gives you the peace of mind of a security monitoring service, without the monthly fees.<br><br>

<h2>How does the emergency feature work?</h2>
In the event of an emergency, the wearer presses the emergency button on the small wrist transmitter or the phone. Once the emergency button is pressed, the six preprogrammed emergency contact numbers are dialed, such as family members, friends or neighbors who would be able to respond to the emergency call. If there is no response after 30 seconds or the line is busy, the phone automatically dials the next preprogrammed phone number. It will cycle through the emergency numbers twice.<br><br>

When the phone reaches a live person, it will play your pre-recorded emergency message. The other person will dial a number (0-9) to confirm that the emergency message has been received and to deactivate the phone from dialing the next emergency contact number. The AEC phone then turns on its speakerphone so that the emergency contact is able to speak and listen to the other user, known as Remote Audio Monitoring.<br><br>

<h2>Benefits:</h2>
<ul class="disc"><li>50+ dB amplification makes it easy to hear callers
<li>Outgoing amplification up to 12dB
<li>Two waterproof battery-operated transmitters with wristband and lanyard included
<li>Easy to install and use
<li>Allows you to remain independent with peace of mind
<li>Reduces the anxiety of those living alone
<li>Provides added security without monthly security monitoring fees</ul>

<h2>Features:</h2>
<ul class="disc"><li>Emergency contact numbers: 6
<li>Number of attempts to dial emergency numbers: 2 cycles
<li>Waterproof wrist-watch style (with included neck lanyard) remote control transmitter
<li>Emergency number displayed on Caller ID screen during dialing
<li>10 second custom message recording time
<li>Dials pre-programmed emergency phone number every 30 seconds
<li>Emergency dial button
<li>Wireless remote, (wrist-watch style with included neck lanyard), up to 100 feet distance
<li>Call cancel on transmitter and base
<li>Transmitter requires one alkaline "23A" 12V size cigarette lighter battery (replaceable)
<li>Remote audio monitoring
<li>Adjustable amplification up to 50dB with volume control slide bar, amplification boost button and situational boost button
<li>Tone control with full amplification in high and low tones
<li>Strobe light ring flasher
<li>Super loud 95dB adjustable ringer volume
<li>Amplified 3.5mm neckloop and 2.5mm headset jacks
<li>Hearing aid t-coil compatible
<li>Boost override
<li>Six memory buttons and three priority memory buttons
<li>Screen contrast adjustment
<li>Caller ID* with 99 name and number storage
<li>Phonebook with 99 name and number storage
<li>Speakerphone
<li>Up to 12dB outgoing speech amplification
<li>Auto on-hook after 8 minutes
<li>Bed shaker (optional - sold separately)
<li>Desk or wall mount</ul>

<h2>Is this phone right for you?</h2>
This phone is recommended for moderate to severe hearing loss. If you constantly ask the other person to repeat what they are saying and it is still hard to get through the conversation, this is the phone for you.<br><br>

* May require subscription with local phone company.<br><br>

<a href="http://dl.dropbox.com/u/58830387/Ampli600_User_Guide.pdf" target="blank">[Click for user manual]</a> (2.7MB)', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Geemarc', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (43, N'HC-AMPLI1100B', NULL, N'Geemarc AMPLI1100 Amplified Phone', N'Geemarc', N'GEEMARC', N'The AMPLI1100 is an affordable amplified desk phone with volume boost and tone control for customized hearing on the phone for people with hearing impairments. Manufacturer: Geemarc', N'geemarc, ampli1100, black, amplified, corded phone, amplified telephones, hearing impaired telephones, telephones for hearing loss', N'Geemarc AMPLI1100 Corded Telephones for Hearing Impaired', N'The Geemarc AMPLI1100 is an affordable 40 decibel amplified desk phone with additional volume boost that''s good for mild to moderate hearing loss. Tone can be adjusted +/- 10dB for individual listening needs.', N'The Geemarc AMPLI1100 is an affordable 40 decibel amplified desk phone with additional volume boost that''s good for mild to moderate hearing loss. Tone can be adjusted +/- 10dB for individual listening needs.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The AMPLI1100 is an affordable amplified desk phone with extra volume boost and tone control for customized listening on the phone. Making outgoing telephone calls is easy with 12 one-touch direct-dial memory buttons.<br><br>

<h2>Features:</h2>
<ul class="disc">
<li>Up to 40dB amplification
<li>12 one-touch direct dial memory buttons
<li>Receiving tone control +/- 10dB
<li>Ringer volume can be increased to 10dB if 4 AAA alkaline batteries are inserted (not included)
<li>Extra bright visual ring strobe
<li>Adjustable ringer level, tone and melody
<li>Last number redial
<li>Hearing aid compatible</ul>

<h2>Is this phone right for you?</h2>
This phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.<br><br.

Click below to view video of features.<br>
<object id="scPlayer" width="317" height="272"> <param name="movie" value="http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/a5eaffc4-d965-4016-812f-2edaa83c235b/flvplayer.swf"></param> <param name="quality" value="high"></param> <param name="bgcolor" value="#FFFFFF"></param> <param name="flashVars" value="thumb=http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/a5eaffc4-d965-4016-812f-2edaa83c235b/FirstFrame.jpg&containerwidth=317&containerheight=272&content=http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/a5eaffc4-d965-4016-812f-2edaa83c235b/Ampli1100.flv"></param> <param name="allowFullScreen" value="true"></param> <param name="scale" value="showall"></param> <param name="allowScriptAccess" value="always"></param> <param name="base" value="http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/a5eaffc4-d965-4016-812f-2edaa83c235b/"></param>  <embed src="http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/a5eaffc4-d965-4016-812f-2edaa83c235b/flvplayer.swf" quality="high" bgcolor="#FFFFFF" width="317" height="272" type="application/x-shockwave-flash" allowScriptAccess="always" flashVars="thumb=http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/a5eaffc4-d965-4016-812f-2edaa83c235b/FirstFrame.jpg&containerwidth=317&containerheight=272&content=http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/a5eaffc4-d965-4016-812f-2edaa83c235b/Ampli1100.flv" allowFullScreen="true" base="http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/a5eaffc4-d965-4016-812f-2edaa83c235b/" scale="showall"></embed> </object><br>

<a href="javascript:void open(''/newsletter/support/HC-AMPLI1100_manual.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Geemarc', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (44, N'HC-AMPLIVOICE50', NULL, N'Geemarc AMPLIVOICE50 Amplified Phone', N'Geemarc', N'GEEMARC', N'The AMPLIVOICE50 is an amplified telephone with 12 decibel amplification, talking features, big buttons, and a speakerphone designed for easy setup and meeting the hearing loss needs of a hard of hearing user. Manufacturer: Geemarc', N'geemarc, amplivoice50, talking telephone, caller id, big number pad, speakerphone, big buttons, amplification', N'Geemarc AMPLIVOICE50 Talking Telephone products for hearing loss', N'The Geemarc AMPLIVOICE50 is a flexible phone that can be easily setup to meet the specific requirements of someone with hearing and/or vision loss, for under $100. The phone is designed to meet the needs of people with hearing loss with up to 40dB of amplification. The AMPLIVOICE50 is also designed for aiding low vision with talking Caller ID, talking keypad and large buttons.', N'The Geemarc AMPLIVOICE50 is a flexible phone that can be easily setup to meet the specific requirements of someone with hearing and/or vision loss, for under $100. The phone is designed to meet the needs of people with hearing loss with up to 40dB of amplification. The AMPLIVOICE50 is also designed for aiding low vision with talking Caller ID, talking keypad and large buttons.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'Announcement of names and numbers is just one of the key features of this Caller ID corded telephone. The AMPLIVOICE50 is a flexible phone that can be easily setup to meet the user''s specific requirements. The phone is designed to meet the needs of people with either vision or hearing loss.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Caller ID stores up to 64 incoming names and numbers
<li>30 name/number phone book
<li>Hear the name of the person in your phone book announced when they call
<li>Announces the number as you dial
<li>3 direct memory buttons can be used as emergency numbers
<li>Handset volume can be adjusted to one of 10 different volume levels
<li>+40dB button can be activated for extra amplification for the handset during a call
<li>Receiving volume amplification up to +12dB
<li>Receiving tone +10dB
<li>Large backlit display
<li>Hands-free speakerphone with visual indicator when in use
<li>Adjustable ringer volume
<li>Extra bright visual ring indicator
<li>Battery backup using 4 AAA batteries (not included)
<li>Power pack included
<li>Hearing aid compatible</ul>
<h2>Is this phone right for you?</h2>
This phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.<br><br>

<a href="http://dl.dropbox.com/u/58830387/HC-AMPLIVOICE50_manual.pdf" target="blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Geemarc', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (45, N'CL-C4210', NULL, N'Clarity Professional C4210 Amplified Phone', N'Clarity', N'CLARITY', N'The Clarity Professional C4210 amplified 2.4GHz cordless phone, featuring Digital Clarity Power (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable.', N'clarity professional, c4210, cordless phone, amplified phone, caller id', N'Clarity Professional C4210 Amplified Cordless Telephone', N'The Clarity ProfessionalÂ® C4210â„¢ amplified 2.4GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable.', N'The Clarity ProfessionalÂ® C4210â„¢ amplified 2.4GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The Clarity ProfessionalÂ® C4210â„¢ amplified 2.4GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, provides intelligent amplification to make soft sounds audible while keeping loud, sudden sounds bearable. Now with up to 50 decibels of amplification, the C4210â„¢ is an ideal solution for those with a moderate-to-severe hearing loss. <strong>One year warranty.</strong>

<strong>Features:</strong>
Digital Clarity PowerÂ® technology
2.4GHz technology
Amplifies incoming sounds up to 50dB
Multiband Compression amplifies the human voice while suppressing other unimportant sounds
Acoustic Echo Cancellation eliminates squealing
Three customizable tone settings
Adjustable volume and tone for a customized listening experience
Call Waiting Caller ID with 50 name and number memory
Noise Reduction suppresses incoming line and background noises for more comfortable conversations
Super loud vibrating handset ringer and bright visual ring indicators
Adjustable ringer volume
Battery backup uses four AA alkaline batteries (not included)
Large, lighted dial pad
3.5mm neckloop jack
2.5mm headset jack
Hearing aid compatible

<a href="javascript:void open(''http://shop.clarityproducts.com/pdf/userguides/74210.000_Manual.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (46, N'CL-C4220', NULL, N'Clarity Professional C4220 Amplified Phone', N'Clarity', N'CLARITY', N'The Clarity Professional C4220 amplified 5.8GHz cordless phone, featuring Digital Clarity Power (DCP) technology, has up to 50 decibels of intelligent amplification for people with hearing loss. Also suitable for low vision. Manufacturer: Clarity', N'clarity professional, c4220, cordless telephones, amplified, loud, 5.8ghz, tia 1083 compliant, compliance, hearing aids compatible, hearing loss, low vision', N'Clarity Professional C4220 Amplified Telephone', N'Featuring a speakerphone in the handset, backlit keypad buttons and a bright LED screen, the Clarity ProfessionalÂ® C4220â„¢ is a feature-rich amplified telephone solution for people with a moderate-to-severe hearing loss or low vision.', N'Featuring a speakerphone in the handset, backlit keypad buttons and a bright LED screen, the Clarity ProfessionalÂ® C4220â„¢ is a feature-rich amplified telephone solution for people with a moderate-to-severe hearing loss or low vision.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The Clarity ProfessionalÂ® C4220â„¢ amplified 5.8GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, has up to 50 decibels of intelligent amplification. The C4220 is a feature-rich solution for those with a moderate-to-severe hearing loss or have low vision.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Digital Clarity PowerÂ® technology with Multiband Compression, Noise Reduction and Acoustic Echo Cancellation
<li>TIA-1083 compliant: hearing aid wearers will experience significantly reduced problems with audio interference during conversations
<li>Amplifies incoming sounds up to 50dB at 118 dBSPL
<li>Adjustable volume and four (4) tone settings for a customized listening experience
<li>Digital 5.8GHz technology provides interference-free sound
<li>Full-duplex speakerphone in handset
<li>Boost Button
<li>Call Waiting Caller ID with 40 name and number memory
<li>Visual ringers in handset and base for incoming calls
<li>Large LED screen is easy to read
<li>Extra loud ringer amplifies up to 88dB
<li>Voicemail indicator on base
<li>Add up to three (3) expandable handsets (see C4220/C4230 Expansion Handset)
<li>3.5mm neckloop jack
<li>2.5mm headset jack
<li>Hearing aid compatible</ul>

<h2>Is this phone right for you?</h2>
This phone is recommended for moderate to severe hearing loss. If you constantly ask the other person to repeat what they are saying and it is still hard to get through the conversation, this is the phone for you.<br><br>

<a href="/newsletter/support/CL-C4220_manual.pdf" target="_blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (47, N'CL-C4230', NULL, N'Clarity Professional C4230 Amplified Phone', N'Clarity', N'CLARITY', N'The Clarity Professional C4230 amplified 5.8GHz cordless phone, featuring Digital Clarity Power (DCP) technology, has up to 50 decibels of intelligent amplification and a digital answering machine. Manufacturer: Clarity', N'clarity professional, c4230, amplified phone, cordless phone, answering machine, tia 1083 compliant, compliance, hearing aids compatible', N'Clarity Professional C4230 Amplified Telephone with Answering Machine', N'Featuring dual speakerphones and a digital answering machine, the Clarity ProfessionalÂ® C4230â„¢ is a feature-rich solution for those with a moderate-to-severe hearing loss or low vision who want to hear clearly on the phone.', N'Featuring dual speakerphones and a digital answering machine, the Clarity ProfessionalÂ® C4230â„¢ is a feature-rich solution for those with a moderate-to-severe hearing loss or low vision who want to hear clearly on the phone.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The Clarity ProfessionalÂ® C4230â„¢ amplified 5.8GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, has up to 50 decibels of intelligent amplification. Featuring dual speakerphones and a digital answering machine, the C4230 is a feature-rich solution for those with a moderate-to-severe hearing loss or low vision.<br><br>

<iframe width="420" height="315" src="http://www.youtube.com/embed/FfenE7IL07k?rel=0" frameborder="0" allowfullscreen></iframe>

<strong>Features:</strong>
<ul class="disc"><Li>Digital Clarity PowerÂ® technology with Multiband Compression, <li>Noise Reduction and Acoustic Echo Cancellation
<li>TIA-1083 compliant: hearing aid wearers will experience significantly reduced problems with audio interference during conversations
Amplifies incoming sounds up to 50dB at 118 dBSPL
<li>Adjustable volume and four (4) tone settings for a customized listening experience
<li>Boost button
<li>Digital 5.8GHz technology provides interference-free sound
<li>Full-duplex speakerphone in handset and base
<li>Speaker volume control
<li>Extra loud ringer amplifies up to 88dB
<li>Digital answering machine with remote access and adjustable-speed playback
<li>Add up to three (3) expandable handsets (see C4230/C4220 Expansion Handset
<li>Lamp flasher and bed shaker ports; optional accessory for use with Clarity Lamp Flasher/Bed Shaker Combo
<li>Visual ringers in handset and base for incoming calls
<li>Call Waiting Caller ID with 40 name and number memory
<li>Large LED screen is easy to read
<li>Battery backup uses four AA alkaline batteries (not included)
<li>Large, lighted dial pad
<li>3.5mm neckloop jack
<li>2.5mm headset jack
<li>Hearing aid compatible</ul>

<strong>Is this phone right for you?</strong> This phone is recommended for moderate to severe hearing loss. If you constantly ask the other person to repeat what they are saying and it is still hard to get through the conversation, this is the phone for you.<br><br>

<a href="/newsletter/support/CL-C4230_manual.pdf" target="_blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (48, N'AMER-XL40A', NULL, N'Clarity XL40A Amplified Phone', N'Clarity', N'CLARITY', N'The XL40A with Clarity Power technology makes words not only louder, but also clearer and easier to understand for individuals who are hearing impaired. Manufacturer: Clarity', N'clarity amplified telephones, ameriphone, xl40d, corded big button telephones, hearing impaired', N'Clarity Ameriphone XL40A Hearing Loss Amplified Telephone', N'The Clarity XL40A&trade; features ClarityÂ® Power&trade; technology that amplifies incoming call volumes up to 43 decibels, with battery backup and an extra large high contrast number dial pad and visual ring indicators.', N'The Clarity XL40A&trade; features ClarityÂ® Power&trade; technology that amplifies incoming call volumes up to 43 decibels, with battery backup and an extra large high contrast number dial pad and visual ring indicators.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The XL40A&trade; amplified phone not only makes calls louder, but conversations clearer and easier to understand for those with moderate hearing loss. The XL40A features a large, high contrast keypad for easy dialing, ideal for low vision.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>ClarityÂ® Power&trade; technology makes calls louder and clearer
<li>Battery backup (batteries not included)
<li>Amplifies incoming sounds up to 43dB
<li>Adjustable tone control
<li>12 programmable one-touch speed dial buttons
<li>Large high contrast dial pad makes numbers easier to see
<li>Hold, Redial, Flash, Program and Mute functions
<li>Super bright visual ring indicator
<li>Extra loud incoming call ringer
<li>Ringer pitch and volume controls
<li>Desk or wall mount
<li>3.5mm audio ouput
<li>Hearing aid compatible</ul><br>

<h2>Is this phone right for you?</h2>
This phone is recommended for moderate to severe hearing loss. If you constantly ask the other person to repeat what they are saying and it is still hard to get through the conversation, this is the phone for you.<br><br>

<a href="/newsletter/support/AMER-XL40A_manual.pdf" target="Clarity Ameriphone XL40A phone user manual">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (49, N'AMER-XL50', NULL, N'Clarity Professional XL50 Amplified Phone', N'Clarity', N'CLARITY', N'The Clarity Professional XL50 amplified telephone has Digital Clarity Power technology that makes soft sounds louder and loud sounds more bearable, up to 60dB of amplification for people with moderate to severe hearing loss. Manufacturer: Clarity', N'clarity professional, xl50, amplified corded phone, digital clarity power, clarity logic, hearing impaired, telephones for hearing loss', N'Clarity Professional XL50 Amplified Corded Telephone', N'DCPÂ® (Digital Clarity PowerÂ®) technology makes soft sounds louder and loud sounds more bearable on the Clarity Professional&reg; XL50&trade;, with up to 60dB of amplification with ports for a lamp and bed shaker and built-in ClarityLogic&trade; customer service! <em>Save 15% with the Clarity Professional XL50 Amplified Phone - Damaged Box option! Full warranty terms; only the box was damaged during shipping. Quantities limited.</em>', N'DCPÂ® (Digital Clarity PowerÂ®) technology makes soft sounds louder and loud sounds more bearable on the Clarity Professional&reg; XL50&trade;, with up to 60dB of amplification with ports for a lamp and bed shaker and built-in ClarityLogic&trade; customer service! <em>Save 15% with the Clarity Professional XL50 Amplified Phone - Damaged Box option! Full warranty terms; only the box was damaged during shipping. Quantities limited.</em>', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'DCPÂ® (Digital Clarity PowerÂ®) technology makes soft sounds louder and loud sounds more bearable, up to 60dB of amplification with ports for a lamp and bed shaker! The XL50â„¢ is ideal for those with a severe hearing loss or low vision.<br><br>

The XL50 now comes with ClarityLogic&trade; customer support service built right in! Just press the blue ClarityLogic button, and you are instantly connected with a customer service representative who can see inside your phone and remotely make adjustments for you. Watch an informational video on <a href="http://youtu.be/k8ad2RM6gdU" target="blank">ClarityLogic&trade;.</a><br><br>

<iframe width="420" height="315" src="http://www.youtube.com/embed/UYkc21IZnqk?rel=0" frameborder="0" allowfullscreen></iframe><br>

<strong>Features:</strong>
<ul class="disc"><li>Digital Clarity PowerÂ®
<li>ClarityLogic&trade;
<li>Up to 60dB amplification
<li>Tone control
<li>Lamp flasher and bed shaker jacks
<li>3.5mm neckloop jack
<li>Bright ring flasher
<li>95+dB extra loud ringer with adjustable volume and tone
<li>Smart Plexing technology allows both people to talk at the same time
<li>11 memory buttons
<li>DCPâ„¢ activated when Boost is turned on (automatically turns off when the handset placed back in cradle)
<li>Battery backup (4AA not included)
<li>Hearing aid compatible</ul>
<strong>Is this phone right for you?</strong> This phone is recommended for severe hearing loss. If you have decided to stop using the phone because it is too hard to hear, or are only able to hear certain voices, this is the phone for you.<br><br>

<a href="https://dl.dropbox.com/u/58830387/AMER-XL50_manual.pdf" target="_blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (50, N'CL-C200', NULL, N'Clarity C200 Amplified Phone', N'Clarity', N'CLARITY', N'The Clarity C200 Trimline amplified phone with 26dB amplification makes sounds louder, clearer and easier to understand for people with a hearing loss. Manufacturer: Clarity', N'clarity, c200, trimline, amplified corded phone for hearing loss, hearing impaired telephones', N'Clarity C200 Amplified Telephone', N'With up to 26dB of amplification, the easy-to-use ClarityÂ® C200&trade; Trimline phone is an ideal solution for those with a mild-to-moderate hearing loss.', N'With up to 26dB of amplification, the easy-to-use ClarityÂ® C200&trade; Trimline phone is an ideal solution for those with a mild-to-moderate hearing loss.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The ClarityÂ® C200&trade; Trimline phone, featuring ClarityÂ® Power&trade; technology, makes phone calls louder, clearer and easier to understand. With up to 26dB of amplification, the user-friendly C200 is an ideal solution amplified phone solution for people with mild-to-moderate hearing loss.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Clarity&reg; Power&trade; technology
<li>Amplifies incoming sounds up to 26dB
<li>Bright visual ring indicator
<li>Loud ringer volume
<li>Large backlit keypad
<li>Hearing aid compatible</ul>
<h2>Is this phone right for you?</h2>
This amplified phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.<br><br>

<a href="/newsletter/support/CL-C200_manual.pdf" target="_blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (51, N'AMER-VCOP', NULL, N'VCO Phone with Port by Clarity', N'Clarity', N'CLARITY', N'For people with severe hearing loss or who are deaf, the VCO allows you to speak to the other party and read their incoming message on the VCO phone''s screen. Manufacturer: Clarity', N'clarity, vco, voice carry over telephone, read and speak, amplified, severe hearing loss, phones for deaf people', N'VCO Voice Carry Over Telephone by Clarity', N'For people with severe hearing loss or who are deaf, the VCO Phone by Clarity&reg; allows you to speak to your caller and read their incoming message on the VCO phone''s screen.', N'For people with severe hearing loss or who are deaf, the VCO Phone by Clarity&reg; allows you to speak to your caller and read their incoming message on the VCO phone''s screen.', 1, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'A read and talk telephone with port for connecting to an LVD or printer. For people with severe hearing loss or who are deaf, the VCO allows you to speak to the other party and read their incoming message on the VCO phone''s screen. (VCO calls must be placed through toll-free relay service.)<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Programmable Relay Service memory dialing
<li>Up to 30dB amplification
<li>Tone control
<li>Adjustable 85dB extra loud ringer
<li>Bright ring flasher
<li>Six programmable memory dial buttons
<li>Lighted, 2-line LCD screen display for text
<li>Powerful amplifier increases incoming voice by 30dB
<li>Booster button with automatic tone for extra amplification of incoming calls
<li>Built-in TTY answering machine (use with regular answering machine* for voice messages)
<li>No fees involved
<li>Hold and last number redial buttons</ul>
* Answering machine should not have auto-disconnect.<br><br>

<strong>Is this phone right for you?</strong> This phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.<br><br>

<a href="javascript:void open(''/newsletter/support/AMER-VCOP_manual.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (52, N'HC-AMPLI500W', NULL, N'Geemarc AMPLI500 White Amplified Phone', N'Geemarc', N'GEEMARC', N'The corded AMPLI500 Caller ID phone provides 40 decibels of amplification that''s adjustable for all levels of hearing loss on the telephone. Outgoing speech amplification is ideal for people with a soft voice. Manufacturer: Geemarc', N'geemarc ampli500 telephones, hearing impaired telephones, corded phones, phones with outgoing speech amplifiers', N'Geemarc AMPLI500 Amplified Telephones with Caller ID', N'The corded AMPLI500 amplified phone boasts up to 50dB amplification for moderate to severe hearing loss, Caller ID with large LCD display to see who''s calling, and a flashing ring strobe to visually notify you when there''s an incoming call. Outgoing speech amplification is ideal for people with soft voices.', N'The corded AMPLI500 amplified phone boasts up to 50dB amplification for moderate to severe hearing loss, Caller ID with large LCD display to see who''s calling, and a flashing ring strobe to visually notify you when there''s an incoming call. Outgoing speech amplification is ideal for people with soft voices.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The AMPLI500 is the perfect blend of features for a remarkable price. It boasts up to 50dB incoming amplification, 12dB outgoing speech amplification, Caller ID, and a flashing ring strobe, plus a full speakerphone with up to +15dB (decibel) amplification.<br><br>

Large numbers and 11 memory dial buttons make dialing a breeze. Simply program all your important numbers into the phone for one-touch dialing. The AMPLI500 phone provides maximum amplification that''s customizable to fit your needs. Standard volume control offers a 30dB gain. When the "AMPLI" button is pressed, the phone provides up to 40dB amplification gain. For additional amplification, the handset boost button provides another 10dB, for a total of up to 50dB gain.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Call Waiting/Caller ID display stores up to 99 incoming names and numbers with date/time stamp for each call
<li>Large 1" buttons
<li>Amplification up to 50dB
<li>Tone control
<li>Amplified speakerphone up to 15dB
<li>95dB loud ringer
<li>3-level Speech Output Control adjusts outgoing voice volume up to 12dB
<li>11 one-touch memory dial buttons: 3 emergency, 8 standard
<li>2.5mm headset and 3.5mm neckloop jack
<li>Built-in strobe ring signaler
<li>3.5mm accessories jack
<li>Redial, Flash, Mute buttons
<li>Uses four (4) AAA batteries for LCD display (not included)
<li>Uses line power and AC power (included)
<li>Desk or wall-mount
<li>Hearing aid compatible</ul>
<strong>Is this phone right for you?</strong> This phone is recommended for moderate to severe hearing loss. If you constantly ask the other person to repeat what they are saying and it is still hard to get through the conversation, this is the phone for you.<br><br>

Click below to view video of features.<br>
<object id="scPlayer" width="317" height="272"> <param name="movie" value="http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/99c8ab8b-78f2-4468-950d-891bce548ea3/flvplayer.swf"></param> <param name="quality" value="high"></param> <param name="bgcolor" value="#FFFFFF"></param> <param name="flashVars" value="thumb=http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/99c8ab8b-78f2-4468-950d-891bce548ea3/FirstFrame.jpg&containerwidth=317&containerheight=272&content=http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/99c8ab8b-78f2-4468-950d-891bce548ea3/Ampli500.flv"></param> <param name="allowFullScreen" value="true"></param> <param name="scale" value="showall"></param> <param name="allowScriptAccess" value="always"></param> <param name="base" value="http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/99c8ab8b-78f2-4468-950d-891bce548ea3/"></param>  <embed src="http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/99c8ab8b-78f2-4468-950d-891bce548ea3/flvplayer.swf" quality="high" bgcolor="#FFFFFF" width="317" height="272" type="application/x-shockwave-flash" allowScriptAccess="always" flashVars="thumb=http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/99c8ab8b-78f2-4468-950d-891bce548ea3/FirstFrame.jpg&containerwidth=317&containerheight=272&content=http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/99c8ab8b-78f2-4468-950d-891bce548ea3/Ampli500.flv" allowFullScreen="true" base="http://content.screencast.com/users/Sonic-Alert/folders/English%20with%20Captions/media/99c8ab8b-78f2-4468-950d-891bce548ea3/" scale="showall"></embed> </object><br>

<a href="/newsletter/support/HC-AMPLI500_manual.pdf" target="_blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Geemarc', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (53, N'HC-AMPLI200', NULL, N'Geemarc AMPLI200 Amplified Phone', N'Geemarc', N'GEEMARC', N'The Geemarc AMPLI200 amplified telephone for hearing loss is a multifunction corded telephone with extra amplification up to 45 decibels and tone control. Manufacturer: Geemarc', N'geemarc ampli200 amplified telephone, amplified phones for hearing loss, hearing impaired telephones', N'Geemarc AMPLI200 Amplified Corded Telephone', N'The Geemarc AMPLI200 is a multifunction corded telephone with extra amplification and tone control for people with mild hearing loss.', N'The Geemarc AMPLI200 is a multifunction corded telephone with extra amplification and tone control for people with mild hearing loss.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The Geemarc AMPLI200 is a multifunction corded telephone with extra amplification and tone control. Receive amplified volume control up to 45 decibels (dB). The speaker offers clearer reception and conversations.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Volume amplification control up to 45dB
<li>Boost override
<li>Tone control
<li>Hands-free dialing
<li>Speakerphone volume control
<li>Extra bright strobe visual ringer indicator
<li>Message waiting indicator
<li>2.5mm headset jack and 3.5mm neckloop jack
<li>Bed shaker output (not included)
<li>Big button keypad
<li>Three emergency dial memory buttons
<li>Nine one-touch memory buttons
<li>Adjustable ringer level and tone
<li>Last number redial
<li>Battery backup for ring (4 AAA alkaline batteries not included)
<li>Wall mountable
<li>Hearing aid compatible</ul>

<strong>Is this phone right for you?</strong> This phone is recommended for moderate to severe hearing loss. If you constantly ask the other person to repeat what they are saying and it is still hard to get through the conversation, this is the phone for you.<br><br>

<a href="/newsletter/support/HC-AMPLI200_manual.pdf" target="blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Geemarc', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (54, N'AMER-JV35', NULL, N'JV-35 Amplified Phone by Clarity', N'Clarity', N'CLARITY', N'The JV35 amplified phone, by Clarity, features Clarity Power technology to make words not only louder, but also clearer and easier to understand. Manufacturer: Clarity', N'clarity, jv35, amplified phone, big buttons, hearing loss telephones, impaired, amplification', N'JV35 Big Button Amplified Corded Telephone by Clarity', N'The JV35&trade; amplified phone, by ClarityÂ®, features ClarityÂ® Power&trade; technology to make words not only louder up to 37dB, but also clearer and easier to understand.', N'The JV35&trade; amplified phone, by ClarityÂ®, features ClarityÂ® Power&trade; technology to make words not only louder up to 37dB, but also clearer and easier to understand.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The JV35&trade; amplified phone, by ClarityÂ®, features ClarityÂ® Power&trade; technology to make words not only louder, but also clearer and easier to understand. With large high-contrast buttons and up to 37 decibels of amplification, the JV35&trade; is an ideal solution for those with a moderate hearing loss or low vision.<br><br>

<iframe width="420" height="315" src="//www.youtube.com/embed/L3B4ND1iacw?rel=0" frameborder="0" allowfullscreen></iframe><br>

<h2>Features:</h2>
<ul class="disc"><li>Clarity&reg; Power&trade; technology
<li>Amplifies incoming sounds up to 37dB
<li>Adjustable volume and tone control
<li>High contrast, jumbo-size buttons with Braille characters
<li>Electronic voice repeats each number as it is dialed
<li>10 programmable memory buttons announce the name you are calling - prerecorded in your own voice!
<li>3 one-touch dedicated programmable emergency memory buttons with Braille characters
<li>Adjustable Tone Selector
<li>Extra loud ringer with a choice of ringing tones
<li>Bright visual ring flasher
<li>Audio output jack
<li>Hearing aid compatible
<li>Neckloop compatible
<li>Redial, Hold and Flash buttons
<li>AC adapter with battery back-up (battery not included)</ul>
<strong>Is this phone right for you?</strong> This phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.<br><br>

<a href="/newsletter/support/AMER-JV35_manual.pdf" target="blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (55, N'CL-C4230COMBO', NULL, N'Clarity Professional C4230 Amplified Phone with Expansion Handset', N'Clarity', N'CLARITY', N'The Clarity Professional C4230 amplified 5.8GHz cordless phone, featuring Digital Clarity Power (DCP) technology, has up to 50 decibels of intelligent amplification.', N'clarity professional, c4230, amplified telephones, cordless, answering machine, tia1083 compliant, compliance, hearing aids compatible', N'Clarity Professional C4230 Amplified Phone w/Add''l Handset Combo', N'The Clarity ProfessionalÂ® C4230â„¢ amplified 5.8GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, has up to 50 decibels of intelligent amplification.', N'The Clarity ProfessionalÂ® C4230â„¢ amplified 5.8GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, has up to 50 decibels of intelligent amplification.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The Clarity ProfessionalÂ® C4230â„¢ amplified 5.8GHz cordless phone, featuring Digital Clarity PowerÂ® (DCP) technology, has up to 50 decibels of intelligent amplification. Featuring dual speakerphones and a digital answering machine, the C4230 is a feature-rich solution for those with a moderate-to-severe hearing loss or low vision. <strong>One year warranty.</strong>

<iframe width="420" height="315" src="http://www.youtube.com/embed/FfenE7IL07k?rel=0" frameborder="0" allowfullscreen></iframe>

<strong>Features:</strong>
<ul class="disc"><li>Digital Clarity PowerÂ® technology with Multiband Compression, Noise Reduction and Acoustic Echo Cancellation
<li>TIA-1083 compliant: hearing aid wearers will experience significantly reduced problems with audio interference during conversations
<li>Ease-of-Use Commendation by Arthritis Foundation
<li>Amplifies incoming sounds up to 50dB at 118 dBSPL
<li>Adjustable volume and four (4) tone settings for a customized listening experience
<li>Boost button
<li>Digital 5.8GHz technology provides interference-free sound
<li>Full-duplex speakerphone in handset and base
<li>Speaker volume control
<li>Extra loud ringer amplifies up to 88dB
<li>Digital answering machine with remote access and adjustable-speed playback
<li>Add up to three (3) expandable handsets (see C4230/C4220 Expansion Handset
<li>Lamp flasher and bed shaker ports; optional accessory for use with Clarity Lamp Flasher/Bed Shaker Combo
<li>Visual ringers in handset and base for incoming calls
<li>Call Waiting Caller ID with 40 name and number memory
<li>Large LED screen is easy to read
<li>Battery backup uses four AA alkaline batteries (not included)
<li>Large, lighted dial pad
<li>3.5mm neckloop jack
<li>2.5mm headset jack
<li>Hearing aid compatible</ul>

<strong>Is this phone right for you?</strong> This phone is recommended for moderate to severe hearing loss. If you constantly ask the other person to repeat what they are saying and it is still hard to get through the conversation, this is the phone for you.

<a href="/newsletter/support/CL-C4230_manual.pdf" target="_blank">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Clarity', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (56, N'FC-SOSPHONE', NULL, N'Future Call SOS Amplified Phone', N'Future Call', N'FUTURE CALL', N'For individuals living alone, the SOS Emergency Phone features a pendant remote control that dials up to 30 of your programmed emergency phone numbers in case of an emergency.', N'future call, sos emergency phone, amplified corded phone, remote button', N'Future Call SOS Emergency Amplified Telephone with Pendant', N'For individuals living alone, the SOS Emergency Phone features a pendant remote control that dials up to 30 of your programmed emergency phone numbers in case of an emergency.', N'For individuals living alone, the SOS Emergency Phone features a pendant remote control that dials up to 30 of your programmed emergency phone numbers in case of an emergency.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'For individuals living alone, the SOS Emergency Phone features a pendant remote control that dials up to 30 of your programmed emergency phone numbers in case of an emergency. The SOS key can be pushed while in the on-hook position; the phone will do the dialing for you. It does not require a monthly monitoring service fee. <strong>90 day limited warranty.</strong>

<strong>Features:</strong>
Up to 30 emergency numbers dialed by remote control pendant
2 one-touch memory dial buttons
Talking Caller ID (name and number, user recordable)
60 name/number Caller ID
Dial from Caller ID
30 name/number phone book
14 digits number, 16 character name
Remote emergency dialing up to 280 feet away
Remote speakerphone answer on and off function
SOS key can be pushed in the on-hook position
Large backlit 3-line display
Adjustable display contrast
Receiver volume Normal/High/Very High 40dB
Receiving volume gain +40dB
Receiving tone gain +/-10dB
Speech volume gain +/-3dB
Extra loud 90dB ringer
Speakerphone volume slide control
Large keypad buttons with backlight
Visual ring indicator
Red LED for low battery/no battery indicator
Language voice prompts (English, French, Dutch, Italian)
High quality two-way speakerphone
Clock
New call indicator
Key lock for unauthorized programming
Redial/Pause key
2 RJ11 phone line in/out jacks
2 RJ9 handset jacks
9ft handset coil cord
7ft RJ11 cord
Desk/wall mount
Hearing aid compatible', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Future Call', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (57, N'HC-TALK500-ER', NULL, N'ClearSounds TALK500-ER Amplified SOS Telephone', N'ClearSounds', N'CLEARSOUNDS', N'The TALK500-ER features a talking Caller ID that speaks the name/number of the caller. It also has a talking keypad with big buttons, speakerphone in the base, 40dB amplification and more!', N'clearsounds, talk500-er, amplified phone, emergency calling', N'ClearSounds TALK500 Amplified SOS Phone', N'The TALK500-ER features a talking Caller ID that speaks the name/number of the caller. It also has a talking keypad with big buttons, speakerphone in the base, 40dB amplification and more!', N'The TALK500-ER features a talking Caller ID that speaks the name/number of the caller. It also has a talking keypad with big buttons, speakerphone in the base, 40dB amplification and more!', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The TALK500-ER features a talking Caller ID that speaks the name/number of the caller. It also has a talking keypad with big buttons, speakerphone in the base, 40dB amplification and more!

The Emergency SOS features gives you the security of being able to call for help without the monthly monitoring subscription fees. The TALK500-ER will automatically dial up to 30 pre-programmed phone numbers when the red SOS button is pressed on the base or pendant. The waterproof pendant has a 100 foot range from the base and can perform remote call answering and disconnecting. <strong>One year warranty.</strong>

<strong>Features:</strong>
Adjustable handset amplification up to 40dB with tone control
Talking Caller ID*
4 LCD display languages
Number verification for pre-dialing (speaks back through speakerphone)
30 phone book entries
2 programmable memory keys
Speakerphone with volume control in base
64 number Caller ID memory
Large backlit LCD display
90dB adjustable loud ringer with flashing alert light indicator
Hi/Lo/Off ringer volume control
Recordable outgoing emergency message
Auto dials up to 30 pre-programmed emergency phone numbers
Pendant works up to 100 feet from phone
Remote call answering and disconnect through waterproof pendant
Phone dimensions: 6-3/4" x 7" x 2-1/2"
Wall or desk mount
Hearing aid compatible

* Requires subscription from local phone company.

<a href="javascript:void open(''http://www.clearsounds.com/Documents/Ctrl_Hyperlink/Talk500ER-Trilingual_uid8192009858131.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'ClearSounds', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (58, N'HC-CSC48', NULL, N'ClearSounds CSC48 HearingEasy Amplified Phone', N'ClearSounds', N'CLEARSOUNDS', N'The CSC48 HearingEasy Amplified phone features a large keypad, 16-digit large LCD display and 3 one-touch speed dial buttons for easy use, with an adjustable 40dB handset amplification. Manufacturer: ClearSounds', N'clearsounds, csc48, hearingeasy amplified phone, big buttons, hearing impaired, hearing loss', N'ClearSounds CSC48 HearingEasy Amplified Corded Telephone', N'The CSC48 HearingEasyâ„¢ Amplified phone features a large keypad, 16-digit large LCD display and 3 one-touch speed dial buttons for easy use, with an adjustable 40dB (decibel) handset amplification.', N'The CSC48 HearingEasyâ„¢ Amplified phone features a large keypad, 16-digit large LCD display and 3 one-touch speed dial buttons for easy use, with an adjustable 40dB (decibel) handset amplification.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The CSC48 HearingEasyâ„¢ Amplified phone features a large keypad, 16-digit large LCD display and 3 one-touch speed dial buttons for easy use, with an adjustable 40dB (decibel) handset amplification.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Up to 40dB amplification
<li>Tone control
<li>UltraClearâ„¢ Sound-Clarifying Technology controls background noise
<li>Big button keypad (.875" x .75")
<li>Big LCD display with 16 digits: displays time, timer or number dialed
<li>Adjustable receiver volume control
<li>Adjustable tone control when Boost is on
Ringer LED indicator
<li>Three one-touch speed dial buttons
<li>One emergency button
<li>Boost key with LED indicator
<li>Hi/Lo/Off ringer volume control
<li>Touch-tone dialing
<li>1 AAA battery needed for clock function (not included)
<li>Line powered
<li>Dimensions: 6-1/2" x 6-3/4" x 2-1/4"
<li>Hearing aid compatible</ul>
<h2>Is this phone right for you?</h2>
This phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.<br><br>

<a href="javascript:void open(''/newsletter/support/HC-CSC48_manual.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'ClearSounds', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (59, N'HC-PHOTO50', NULL, N'ClearSounds PHOTO50 Photo Memory Amplified Phone', N'ClearSounds', N'CLEARSOUNDS', N'The PHOTO50 amplifies your incoming caller''s voice up to 40dB. Three "photo frame" buttons on the base let you customize the phone with pictures for one-touch access to favorite phone numbers or emergency numbers.', N'clearsounds, photo50, picture buttons keypad, seniors telephone, amplified', N'ClearSounds PHOTO50 Photo Memory Amplified Phone', N'The PHOTO50 amplifies your incoming caller''s voice up to 40dB. Three "photo frame" buttons on the base let you customize the phone with pictures for one-touch access to favorite phone numbers or emergency numbers.', N'The PHOTO50 amplifies your incoming caller''s voice up to 40dB. Three "photo frame" buttons on the base let you customize the phone with pictures for one-touch access to favorite phone numbers or emergency numbers.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The PHOTO50 amplifies your incoming caller''s voice up to 40dB. Three "photo frame" buttons on the base let you customize the phone with pictures for one-touch access to favorite phone numbers or emergency numbers. There are also 10 additional two-touch speed dials. Have hands-free conversations with the speakerphone in the base. <strong>One year warranty.</strong>

<strong>Key Features</strong>
40dB adjustable handset amplification with tone control
3 pre-programmed "photo frame" buttons for easy recollection of faces and easy outgoing dialing
Speakerphone in base
Digital answering machine
UltraClearâ„¢ sound-clarifying technology

<strong>Digital Answering System Features</strong>
Playback through handset or base speakerphone
Amplified up to 40dB through handset
14 minutes digital recording time
Call Screening/Intercept: listen to incoming callers leaving a message without answering; answer the call by picking up the phone
Remote access to messages
Time/Day Stamp

<strong>Additional Features:</strong>
Tone control
Visual ringer
Big button easy-to-read keypad
Volume control speakerphone
Remote room monitoring: microphone feature in digital answering machine for remote room monitoring of sounds where the answering machine is located
3 one-touch memory dial buttons
10 two-touch memory dial buttons
Hi/Lo/Off ringer volume control
2.5mm headset jack
3.5mm jack for optional vibrating pad (sold separately)
Last number redial
Table/wall convertible
AC power adapter
Hearing aid compatible

<a href="javascript:void open(''http://www.clearsounds.com/Documents/Ctrl_Hyperlink/Photo_50_Trilingual-7-08%5B1%5D_uid12232008332232.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'ClearSounds', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (60, N'CS-TALK500', NULL, N'ClearSounds TALK500 Amplified Phone', N'ClearSounds', N'CLEARSOUNDS', N'Great for every home or office looking for a corded telephone, the ClearSounds Talk500 is designed for everyone, including people with hearing and/or vision loss. Know who is calling before you answer with Talking Caller ID. Manufacturer: ClearSounds', N'clearsounds, talk500, talking telephones, amplified telephones, talking caller id, talking products, products for low vision', N'ClearSounds TALK500 Talking Telephone', N'Great for every home or office looking for a corded telephone, the ClearSounds Talk500 is designed for everyone, including people with hearing and/or vision loss.', N'Great for every home or office looking for a corded telephone, the ClearSounds Talk500 is designed for everyone, including people with hearing and/or vision loss.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'Great for every home or office looking for a corded telephone, the ClearSounds Talk500 is designed for everyone, including people with hearing and/or vision loss. Know who is calling before you answer with Talking Caller ID; the Talk500 telephone announces the name or number of incoming calls in a clear voice, and the information also appears on the large screen. In addition, this phone features an adjustable amplified handset keypad, large display, speakerphone, super loud ringer with visual indicator and more!<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Adjustable amplification up to 40dB with tone control
<li>Boost override
<li>Tone control
<li>Talking Caller ID*
<li>Quad-lingual talking Caller ID: English, French, Spanish, Italian
<li>Number verification for pre-dialing (speaks back through speakerphone)
<li>30 phone book entries (user-recordable for talk-back)
<li>3 one-touch memory buttons
<li>4 LCD display languages and voice prompt: English, French, Spanish, Italian
<li>Frequency control
<li>Speakerphone in base
<li>64 number Caller ID memory
<li>Large backlit LCD display with contrast control
<li>Adjustable loud ringer with light indicator
<li>Wall or desk mount
<li>Hearing aid compatible</ul>

* Requires subscription from local phone company.<br><br>

<h2>Is this phone right for you?</h2>
This phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.<br><br>

<a href="http://dl.dropbox.com/u/58830387/CS-TALK500_manual.pdf" target="blank">[Click for user manual]</a> 4.6MB PDF', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'ClearSounds', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (61, N'HC-A55', NULL, N'ClearSounds A55 UltraClear Amplified Phone', N'ClearSounds', N'CLEARSOUNDS', N'The A55 UltraClear cordless phone combines style and versatility with up to 50dB of amplification, along with many other features. Manufacturer: ClearSounds.', N'clearsounds, a55, ultraclear, amplified cordless phone, hearing impaired', N'ClearSounds A55 UltraClear Amplified Big Button Phone for hearing loss', N'The A55 UltraClear cordless phone combines style and versatility with up to 50dB of amplification, along with many other features.', N'The A55 UltraClear cordless phone combines style and versatility with up to 50dB of amplification, along with many other features.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The A55 UltraClear cordless phone combines style and versatility with up to 50dB amplification, along with many other features. Includes one base cordless handset and base charger cradle.<br><br>

<h2>Key Features:</h2>
<ul class="disc"><li>Adjustable handset amplification: 50dB
<li>UltraClearâ„¢ Sound-Clarifying Technology
<li>900MHz
<li>Caller ID/Call Waiting
<li>Amplified 2.5mm headset jack; 3.5mm bed shaker jack
<li>90dB adjustable extra loud ringer on base; 80dB on handset</ul>
<strong>Caller ID Features:</strong>
<ul class="disc"><li>99 name/number Caller ID history
<li>Caller ID/Call Waiting capable
<li>Caller ID display dial
<li>New call and visual message waiting indicators</ul>
<strong>Additional Features:</strong>
<ul class="disc"><li>Amplification override reset
<li>Separate volume and tone control
<li>60 name/number phonebook directory
<li>Programmable pause
<li>Handset amplification control with separate volume and tone control
<li>Visual ringer on handset
<li>Vibrating ringer on handset
<li>2.5mm headset jack; 3.5mm bed shaker jack (bed shaker sold separately)
<li>Backlit keypad
<li>Uses 3 NiMH batteries (included)
<li>Hearing aid compatible</ul>
<strong>Is this phone right for you?</strong> This phone is recommended for moderate to severe hearing loss. If you constantly ask the other person to repeat what they are saying and it is still hard to get through the conversation, this is the phone for you.<br><br>

<a href="javascript:void open(''/newsletter/support/CS-A55_manual.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'ClearSounds', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (62, N'HC-A300', NULL, N'ClearSounds A300 DECT 6.0 Amplified Cordless Phone', N'ClearSounds', N'CLEARSOUNDS', N'The ClearSounds A300 DECT 6.0 phone provides interference-free cordless phone conversations with up to 25dB amplification! DECT. Manufacturer: ClearSounds', N'clearsounds, dect 6.0 technology, a300, amplified cordless telephone, hearing impaired', N'ClearSounds DECT 6.0 A300 Amplified Telephone with Sound Boost', N'The ClearSounds A300 DECT 6.0 phone provides interference-free cordless phone conversations with up to 25dB amplification!', N'The ClearSounds A300 DECT 6.0 phone provides interference-free cordless phone conversations with up to 25dB amplification!', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, NULL, N'The ClearSounds A300 DECT 6.0 phone provides interference-free cordless phone conversations with up to 25dB amplification! DECT (Digital Enhanced Cordless Telecommunications) phones use a dedicated channel reserved for voice communication applications, so they are less likely to experience interference from other wireless devices such as baby monitors and wireless networks. The A300 also has a large LCD display that lets you see who is calling, and the flashing ring signaler in the handset has a vibrate feature if you do not wish to be disturbed by incoming calls.<br><br>

<iframe width="420" height="315" src="//www.youtube.com/embed/0lnm6x5-PtM?rel=0" frameborder="0" allowfullscreen></iframe><br>

<h2>Features:</h2>
<ul class="disc"><li>DECT 6.0 technology
<li>Caller ID & Call Waiting*
<li>Up to 25dB amplification
<li>Adjustable extra loud ringer up to 85dB with 15 ring tones
<li>Flashing, vibrating ring signaler in cordless handset
<li>Volume control speakerphone in handset
<li>2.5mm headset jack
<li>4 one-touch speed dial buttons
<li>1 large one-touch emergency button on handset
<li>50 name/number Caller ID memory
<li>250 phone book entries
<li>Large 1-1/2" backlit LCD display with adjustable contrast
<li>7 LCD display languages
<li>Digital answering machine in handset
<li>Remote answering machine message retrieval
<li>AC adapter and 3 AAA rechargeable handset batteries (included)
<li>Backlit keypad
<li>Dimensions: 5" x 3-3/4" x 2"
<li>Wall or desk mount
<li>Hearing aid compatible**</ul>
*Requires subscription with local phone company.<br>
**T-coil is present but DECT 6.0 technology can cause buzzing in some hearing aids.<br><br>

<strong>Is this phone right for you?</strong> This phone is recommended for mild to moderate hearing loss. If you have a hard time hearing the conversation and need some words repeated, but can still manage to get through it, this is the phone for you.

<!--<a href="javascript:void open(''http://www.clearsounds.com/Documents/Ctrl_Hyperlink/CS-A300_uid12302008429242.pdf'',''_blank'',''menubar=no,scrollbars=yes,resizable=yes'');" onMouseOver="return showStatus(''user manual'');" onMouseOut="return showStatus('''');">[Click for user manual]</a>-->', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main.jpg', N'/image/image1.jpg', N'/image/image2.jpg', N'/image/image3.jpg', N'/image/image4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'ClearSounds', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'N', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (79, N'FC-PICPHONER', NULL, N'Future Call FC-1007 Amplified Picture Phone', N'Future Call', N'FUTURE CALL', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button. Manufacturer: Future Call', N'future call, picture buttons, dial by picture, amplified corded phone, emergency telephones, hc-ampliphoto50', N'Future Call Amplified Photo Telephone', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button.', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, N'Red', N'Always know who you are calling with this amplified corded picture phone. Ten large memory picture buttons let you quickly dial the person you want to call in just 2 key presses.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Up to 40dB amplification
<li>Loud ringer with visual ring indicator
<li>10 memory picture button keys (switch for one-touch or two-touch dialing)
<li>Emergency button
<li>Stored numbers retain their memory even when unplugged from the telephone line
<li>Store key prevents memory button numbers from being accidentally changed or erased
<li>Redial key
<li>Flash key for call waiting
<li>Line powered
<li>Desk/wall mount
<li>Hearing aid compatible</ul>

<a href="/newsletter/support/FC-PICPHONE_manual.pdf" target="Future Call FC-1007 picture phone user manual">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main_image_red.jpg', N'/image/image_red_1.jpg', N'/image/image_red_2.jpg', N'/image/image_red_3.jpg', N'/image/image_red_4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Future Call', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'Y', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (80, N'FC-PICPHONEG', NULL, N'Future Call FC-1007 Amplified Picture Phone', N'Future Call', N'FUTURE CALL', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button. Manufacturer: Future Call', N'future call, picture buttons, dial by picture, amplified corded phone, emergency telephones, hc-ampliphoto50', N'Future Call Amplified Photo Telephone', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button.', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, N'Green', N'Always know who you are calling with this amplified corded picture phone. Ten large memory picture buttons let you quickly dial the person you want to call in just 2 key presses.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Up to 40dB amplification
<li>Loud ringer with visual ring indicator
<li>10 memory picture button keys (switch for one-touch or two-touch dialing)
<li>Emergency button
<li>Stored numbers retain their memory even when unplugged from the telephone line
<li>Store key prevents memory button numbers from being accidentally changed or erased
<li>Redial key
<li>Flash key for call waiting
<li>Line powered
<li>Desk/wall mount
<li>Hearing aid compatible</ul>

<a href="/newsletter/support/FC-PICPHONE_manual.pdf" target="Future Call FC-1007 picture phone user manual">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main_image_red.jpg', N'/image/image_green_1.jpg', N'/image/image_green_2.jpg', N'/image/image_green_3.jpg', N'/image/image_green_4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Future Call', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'Y', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
INSERT [harriswebdb].[Products] ([ProductID], [ProductSKU], [ProductUPC], [ProductDesc], [ProductShortDesc], [ProductDescUp], [MetaDescription], [MetaKeyword], [MetaTitle], [ProductLongDesc], [ProductSpec], [DepartmentID], [ClassID], [SubclassID], [ProductParent], [ProductGrandParent], [Availability], [InTheBoxInd], [InTheBoxDesc], [ISBN], [Color], [DealerDesc], [GiftMessageAvailable], [ProductDocument1], [ProductDocument2], [ImageMain], [Image1], [Image2], [Image3], [Image4], [Media], [ProductLevel], [Manufacturer], [manufacturerPart], [MediaType], [Status], [ReInStockFee], [VendorInd], [VendCartInd], [MapInd], [MapPrice], [RegPrice], [WebPrice], [CartPrice], [HarrisPrice], [PromoPrice], [PromoInd], [ClearancePrice], [ClearanceInd], [ProductQty], [StandardOUM], [UOMConvertFact], [ShipAloneInd], [IsFreeShip], [SpecialShipFee], [EndNewArrivalDate], [NewArrivalInd], [CreateDatetime], [UpdateDatetime], [UpdateUser], [IsDelete]) VALUES (81, N'FC-PICPHONEP', NULL, N'Future Call FC-1007 Amplified Picture Phone', N'Future Call', N'FUTURE CALL', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button. Manufacturer: Future Call', N'future call, picture buttons, dial by picture, amplified corded phone, emergency telephones, hc-ampliphoto50', N'Future Call Amplified Photo Telephone', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button.', N'This 40dB amplified corded phone from Future Call has 10 memory picture buttons to dial the person you want to call along with an emergency button.', 2, 4, 14, NULL, NULL, N'InStock', N'N', NULL, NULL, N'Pink', N'Always know who you are calling with this amplified corded picture phone. Ten large memory picture buttons let you quickly dial the person you want to call in just 2 key presses.<br><br>

<h2>Features:</h2>
<ul class="disc"><li>Up to 40dB amplification
<li>Loud ringer with visual ring indicator
<li>10 memory picture button keys (switch for one-touch or two-touch dialing)
<li>Emergency button
<li>Stored numbers retain their memory even when unplugged from the telephone line
<li>Store key prevents memory button numbers from being accidentally changed or erased
<li>Redial key
<li>Flash key for call waiting
<li>Line powered
<li>Desk/wall mount
<li>Hearing aid compatible</ul>

<a href="/newsletter/support/FC-PICPHONE_manual.pdf" target="Future Call FC-1007 picture phone user manual">[Click for user manual]</a>', N'N', N'/documents/quickstart.pdf', N'/documents/manual.pdf', N'/image/main_image_red.jpg', N'/image/image_pink_1.jpg', N'/image/image_pink_2.jpg', N'/image/image_pink_3.jpg', N'/image/image_pink_4.jpg', N'https://www.youtube.com/watch?v=77umP7IRxD4l', 2, N'Future Call', NULL, NULL, 1, CAST(76.66 AS Decimal(20, 2)), 0, NULL, N'Y', CAST(299.99 AS Decimal(20, 2)), CAST(299.99 AS Decimal(20, 2)), CAST(297.99 AS Decimal(20, 2)), CAST(296.99 AS Decimal(20, 2)), NULL, CAST(295.99 AS Decimal(20, 2)), N'Y', NULL, N'Y', 200, N'EA', NULL, N'N', N'Y', CAST(0.00 AS Decimal(20, 2)), CAST(0x0000A7BD01491104 AS DateTime), N'Y', CAST(0x0000A7C6017912A4 AS DateTime), CAST(0x0000A7C6017912A4 AS DateTime), N'TinhN', 0)
GO
SET IDENTITY_INSERT [harriswebdb].[Products] OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Unique_ProductSKU]    Script Date: 8/21/2017 9:01:29 AM ******/
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [Unique_ProductSKU] UNIQUE NONCLUSTERED 
(
	[ProductSKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_ProductLevel]  DEFAULT ((2)) FOR [ProductLevel]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_VendorInd]  DEFAULT ((0)) FOR [VendorInd]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_MapInd]  DEFAULT ((0)) FOR [MapInd]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_PromoInd]  DEFAULT (N'N') FOR [PromoInd]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_ClearanceInd]  DEFAULT (N'N') FOR [ClearanceInd]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_IsFreeShip]  DEFAULT (N'Y') FOR [IsFreeShip]
GO
ALTER TABLE [harriswebdb].[Products] ADD  CONSTRAINT [DF_Products_NewArrivealInd]  DEFAULT (N'N') FOR [NewArrivalInd]
GO
ALTER TABLE [harriswebdb].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Class] FOREIGN KEY([ClassID])
REFERENCES [harriswebdb].[Class] ([ClassID])
GO
ALTER TABLE [harriswebdb].[Products] CHECK CONSTRAINT [FK_Products_Class]
GO
ALTER TABLE [harriswebdb].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Department] FOREIGN KEY([DepartmentID])
REFERENCES [harriswebdb].[Department] ([DepartmentID])
GO
ALTER TABLE [harriswebdb].[Products] CHECK CONSTRAINT [FK_Products_Department]
GO
ALTER TABLE [harriswebdb].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Subclass] FOREIGN KEY([SubclassID])
REFERENCES [harriswebdb].[Subclass] ([SubclassID])
GO
ALTER TABLE [harriswebdb].[Products] CHECK CONSTRAINT [FK_Products_Subclass]
GO
UPDATE [harriswebdb].[Products]
SET Media = 'https://www.youtube.com/embed/77umP7IRxD4'