﻿IF OBJECT_ID('[harriswebdb].[CustomerCreate]', 'P') IS NOT NULL
DROP PROCEDURE [harriswebdb].[CustomerCreate];
GO
CREATE PROCEDURE [harriswebdb].[CustomerCreate]
	@FirstName NVARCHAR(25),
	@LastName NVARCHAR(25),
	@Password NVARCHAR(128),
	@Email NVARCHAR(128),
	@Type NVARCHAR(3),	
	@CustCatStatus TINYINT,
	@Status CHAR(1),
	@Export TINYINT,
	@Created DATETIME,	
	@ErrorNumber INT OUT
AS
BEGIN	
	SET @ErrorNumber = -1
	IF EXISTS(SELECT 1 FROM [harriswebdb].[Customer]  WHERE  CustEmail = @Email AND CustPassword = @Password)
	BEGIN		
		SET @ErrorNumber = 100
	END
	ELSE
	BEGIN
		BEGIN TRANSACTION
			SET IDENTITY_INSERT [harriswebdb].[Customer] ON

			DECLARE @Id INT	
			SET @Id = IDENT_CURRENT('[harriswebdb].[Customer]') + 1;  

			INSERT INTO [harriswebdb].[Customer] (CustID, CustNo, CustFirstName, CustLastName, CustFullName, CustEmail, CustPassword, CustCreated, CustType,  CustStatus, CustCatStatus, IsExported)
			VALUES (@Id, @Id, @FirstName, @LastName, rtrim(@FirstName)+' '+rtrim(@LastName), @Email, @Password,  @Created, @Type,  @Status, @CustCatStatus, @Export)

			SET IDENTITY_INSERT [harriswebdb].[Customer] OFF 	

		COMMIT TRANSACTION
		SET @ErrorNumber = 0
   END
END
GO
IF OBJECT_ID('[harriswebdb].[CustomerGet]', 'P') IS NOT NULL
DROP PROCEDURE [harriswebdb].[CustomerGet];
GO
CREATE  PROCEDURE [harriswebdb].[CustomerGet]	
	@Password VARCHAR(25),
	@Email CHAR(128),
	@CustLastLogin DATETIME,
	@ErrorNumber INT OUT
AS
BEGIN
	SET @ErrorNumber = 0
	IF EXISTS(SELECT 1 FROM [harriswebdb].[Customer]  WHERE  CustEmail = @Email AND CustPassword = @Password)
	BEGIN		
		SELECT * FROM [harriswebdb].[Customer] WHERE CustEmail = @Email AND CustPassword = @Password
		UPDATE [harriswebdb].[Customer]
		SET CustLastLogin = @CustLastLogin
		WHERE CustEmail = @Email AND CustPassword = @Password
	END
	ELSE
	BEGIN
		SET @ErrorNumber = 100
		IF EXISTS(SELECT 1 FROM [harriswebdb].[Customer]  WHERE  CustEmail = @Email)			
		SET @ErrorNumber = 101
	END
END

