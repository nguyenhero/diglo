﻿ALTER PROCEDURE [harriswebdb].[ProductsGet]
	@Start INT,
	@End INT
AS
BEGIN	

SELECT * ,COUNT(*) OVER () as overallCount
FROM [harriswebdb].[Products]
WHERE [ProductParent] IS NULL
ORDER BY ProductID
OFFSET @Start ROWS
FETCH NEXT @End ROWS ONLY

END

GO 

CREATE PROCEDURE [harriswebdb].[ProductsGetByProductSKU]
	@ProductSKU NVARCHAR(20)
AS
BEGIN	

SELECT * 
FROM [harriswebdb].[Products]
WHERE [ProductParent]   =  @ProductSKU
END

GO
UPDATE [harriswebdb].[Products]
SET Media = 'https://www.youtube.com/embed/4gdBgTZSEdI'
