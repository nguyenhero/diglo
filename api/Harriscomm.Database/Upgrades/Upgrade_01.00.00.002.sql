﻿CREATE PROCEDURE [harriswebdb].[CustomerGetByEmail]		
	@Email NVARCHAR(128)
AS
BEGIN
	SELECT * FROM [harriswebdb].[Customer]  WHERE  CustEmail = @Email
END
GO
CREATE PROCEDURE [harriswebdb].[CustomerUpdateByCustNo]
	@CustomerNo NVARCHAR(20),
	@Password NVARCHAR(128),
	@ErrorNumber INT OUT
AS
BEGIN	
	SET @ErrorNumber = -1
	IF EXISTS(SELECT 1 FROM [harriswebdb].[Customer]  WHERE  CustNo = @CustomerNo)
	BEGIN				
	    UPDATE  [harriswebdb].[Customer]
		SET [CustPassword] =@Password
		WHERE  CustNo = @CustomerNo
		SET @ErrorNumber = 0
	END
	ELSE
	BEGIN
	SET @ErrorNumber = 100
   END
END
GO
ALTER  PROCEDURE [harriswebdb].[CustomerGet]	
	@Password VARCHAR(25),
	@Email NVARCHAR(128),
	@CustLastLogin DATETIME,
	@ErrorNumber INT OUT
AS
BEGIN
	SET @ErrorNumber = 0
	IF EXISTS(SELECT 1 FROM [harriswebdb].[Customer]  WHERE  CustEmail = @Email AND CustPassword = @Password)
	BEGIN		
		SELECT * FROM [harriswebdb].[Customer] WHERE CustEmail = @Email AND CustPassword = @Password
		UPDATE [harriswebdb].[Customer]
		SET CustLastLogin = @CustLastLogin
		WHERE CustEmail = @Email AND CustPassword = @Password
	END
	ELSE
	BEGIN
		SET @ErrorNumber = 100
		IF EXISTS(SELECT 1 FROM [harriswebdb].[Customer]  WHERE  CustEmail = @Email)			
		SET @ErrorNumber = 101
	END
END
GO
ALTER PROCEDURE [harriswebdb].[CustomerCatCreate]
	@CustNo NVARCHAR(20),
	@Type NVARCHAR(3),	
	@CatFirstName NVARCHAR(50),
	@CatLastName NVARCHAR(50),
	@CatEmail NVARCHAR(128),
	@CatAddress NVARCHAR(100),
	@CatCompany NVARCHAR(50),
	@CatCity NVARCHAR(100),
	@CatState NVARCHAR(3),
	@CatZip NVARCHAR(20),
	@CatCountry NVARCHAR(3),
	@CatCreated DATETIME,
	@ErrorNumber INT OUT
AS
BEGIN	
	SET @ErrorNumber = -1
	BEGIN TRANSACTION					
	INSERT INTO [harriswebdb].[CustomerCat]
           ([CustNo]
           ,[CatFirstName]
           ,[CatLastName]
           ,[CustFullName]
		   ,[CatEmail]
           ,[CatAddress]
           ,[CatCompany]
           ,[CatCity]
           ,[CatState]
           ,[CatZip]
           ,[CatCountry]
           ,[CatCreated])
     VALUES
           (@CustNo
           ,@CatFirstName
           ,@CatLastName
           ,rtrim(@CatFirstName)+' '+rtrim(@CatLastName)
		   ,@CatEmail
           ,@CatAddress
           ,@CatCompany
           ,@CatCity
           ,@CatState
           ,@CatZip
           ,@CatCountry
           ,@CatCreated)

		   UPDATE [harriswebdb].[Customer]
		   SET [CustCatStatus] = 1
		   WHERE [CustNo] = @CustNo

	COMMIT TRANSACTION
	SET @ErrorNumber = 0
END
