import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { tap } from 'rxjs/operators';
import { StorageService } from './storage/storage.service';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

    constructor(private storageService: StorageService) { }
    private nextAuthHeader: string;
    private nextAuthValue: string;

    /** Set to basic authentication */
    basic(user: string, password: string): void {
        this.nextAuthHeader = 'Authorization';
        this.nextAuthValue = 'Basic ' + btoa(user + ':' + password);
    }

    /** Set to bearer authentication */
    bearer(token: string): void {
        this.nextAuthHeader = 'Authorization';
        this.nextAuthValue = 'Bearer ' + token;
    }


    /** Set to session key */
    nextAsSession(sessionKey: string): void {
        this.nextAuthHeader = 'Session';
        this.nextAuthValue = sessionKey;
    }

    /** Clear any authentication headers (to be called after logout) */
    clear(): void {
        this.nextAuthHeader = null;
        this.nextAuthValue = null;
    }

    /** Apply the current authorization headers to the given request */
    apply(req: HttpRequest<any>): HttpRequest<any> {
        const headers = {};
        if (this.nextAuthHeader) {
            headers[this.nextAuthHeader] = this.nextAuthValue;
        }
        // Apply the headers to the request
        return req.clone({
            setHeaders: headers
        });
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Apply the headers
        req =this.apply(req);
        // Also handle errors globally
        return next.handle(req).pipe(
            tap(x => x, err => {
                // Handle this err
                console.error(`Error performing request, status code = ${err.status}`);
            })
        );
    }
}