﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import {Newsletter} from '../models/newsletter';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable()
export class NewsletterService {
    private newsletterUrl = environment.apiUrl +'newsletter';
    constructor(private http: Http) { }
    subscribers(email: string, flagg: string): Observable<Newsletter> {
        var body = 'email=' + email + '&flagg=' + flagg;
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.newsletterUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}