﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Department}  from '../models/department';
import { environment } from 'src/environments/environment';
@Injectable()
export class MenuService {
    private menuUrl = environment.apiUrl + 'menu';
    constructor(private http: Http) { }
    get(): Observable<Department[]> {
    return this.http.get(this.menuUrl)
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}