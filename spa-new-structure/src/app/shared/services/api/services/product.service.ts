﻿import {Injectable, EventEmitter} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import {Product} from '../models/product';
@Injectable()
export class ProductService {
    private productRelatedUrl = environment.apiUrl +'productRelated';
    private productUrl = environment.apiUrl +'product';
    private productCountUrl = environment.apiUrl +'productCount';
    private productCategoryUrl = environment.apiUrl +'productCategory';
    constructor(private http: Http) { }
    get(request, filterDetails: any): Observable<Product[]> {
    let params: URLSearchParams = new URLSearchParams();
        params.set("sortBy", request.sortBy);
        params.set("start", request.start);
        params.set("end", request.end);
        params.set("departmentId", request.departmentId);
        params.set("classId", request.classId);
        params.set("subClassId", request.subClassId);
        if (filterDetails != null && filterDetails.length > 0) {
            filterDetails.forEach(filterDetail => {
                if (filterDetail.searchcolumn) {
                    params.append('searchColumns', filterDetail.searchcolumn);
                }
                if (filterDetail.operation) {
                    params.append('operations', filterDetail.operation);
                }
                if (filterDetail.filterDetailValue) {
                    params.append('filterDetailValues', filterDetail.filterDetailValue);
                }
            });         
        }
        if (request.newArrivalInd) {
            params.set("newArrivalInd", request.newArrivalInd);
        }
        return this.http.get(this.productUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    getCount(request, filterDetails: any): Observable<number> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("sortBy", request.sortBy);
        params.set("start", request.start);
        params.set("end", request.end);
        params.set("departmentId", request.departmentId);
        params.set("classId", request.classId);
        params.set("subClassId", request.subClassId);
        if (filterDetails != null && filterDetails.length > 0) {
            filterDetails.forEach(filterDetail => {
                if (filterDetail.searchcolumn) {
                    params.append('searchColumns', filterDetail.searchcolumn);
                }
                if (filterDetail.operation) {
                    params.append('operations', filterDetail.operation);
                }
                if (filterDetail.filterDetailValue) {
                    params.append('filterDetailValues', filterDetail.filterDetailValue);
                }
            });
        }
        if (request.newArrivalInd) {
            params.set("newArrivalInd", request.newArrivalInd);
        }
        return this.http.get(this.productCountUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    getCategory(): Observable<any> {
        return this.http.get(this.productCategoryUrl)
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    getProductRelated(productSku: string): Observable<Product[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("productSku", productSku);
        return this.http.get(this.productRelatedUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}