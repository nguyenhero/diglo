﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {WishList} from '../models/wishList';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'; 
@Injectable()
export class WishListService {
    private wishListUrl = environment.apiUrl +'wishList';
    private shareWishListUrl = environment.apiUrl +'shareWishList';
    constructor(private http: Http) { }
    add(body: Object): Observable<number> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.wishListUrl, bodyString, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    get(custNo: string): Observable<WishList[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("custNo", custNo);
        return this.http.get(this.wishListUrl, { search: params })
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    deleteByCustNo(custNo: string): Observable<number> {       
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({           
            headers: headers,
            search: new URLSearchParams('custNo=' + custNo)
        }); // Create a request option
        return this.http.delete(this.wishListUrl, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    deleteByWishListId(wishListId: number): Observable<number> {
        const url = `${this.wishListUrl}/${wishListId}`;
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.delete(url, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
    sendshareWishList(body: Object): Observable<number> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.shareWishListUrl, bodyString, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}