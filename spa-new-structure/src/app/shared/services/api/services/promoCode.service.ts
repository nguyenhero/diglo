﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {User} from '../models/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable()
export class PromoCodeService {
    user: User;
    private promoCodeUrl = environment.apiUrl +'promoCode';
    constructor(private http: Http) { }
    checkPromoCode(code: string): Observable<number> {
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('promoCode', code);
        let body = urlSearchParams.toString()
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.promoCodeUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }
}