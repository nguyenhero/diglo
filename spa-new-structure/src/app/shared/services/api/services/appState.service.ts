﻿import {Injectable} from '@angular/core';
import { Subject } from 'rxjs';
@Injectable()
export class AppState {
    public _subject = new Subject<Object>();
    public event = this._subject.asObservable();
    public publish(data: any) {
        this._subject.next(data);
    }
}