﻿import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable()
export class CustomerCatService {     
    private customerCatUrl = environment.apiUrl +'customerCat';
    constructor(private http: Http) { }
    save(body: Object): Observable<number> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.customerCatUrl, body, options) // ...using post request
            .pipe(map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            );
    }    
}