﻿import {Product} from './product';
export class OrderDetail {
    constructor(
    ) {}
    public orderDetailID: number;
    public orderID: number;
    public productSKU: string;
    public webPrice: number;
    public quantity: number;
    public cartPrice: number;
    public specialFee: number;
    public tax: number;
    public productDesc: string;
    public product: Product;
}