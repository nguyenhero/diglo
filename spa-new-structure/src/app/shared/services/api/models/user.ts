﻿export class User {
    constructor() {}
    public custNo: number;
    public custFirstName: string;
    public custLastName: string;
    public custFullName: string;
    public custLFullName: string;
    public custEmail: string;
    public custAddress: string;    
    public custCity: string;    
    public custPassword: string;
    public errorNumber: number;
    public custCatStatus: number;
    public custPhone: string;
    public company: string;
    public custState: string;
}

