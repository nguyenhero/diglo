﻿import {las} from './las';
export class Department {
    constructor(
    ) {}
    public departmentID: number;
    public departmentDesc: string;
    public classHtml: string;
    public left: number;    
    public classes: las[];
    public show: boolean;
}
