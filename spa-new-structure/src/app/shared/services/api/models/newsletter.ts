﻿export class Newsletter {
    constructor(
    ) {}
    public Created: Date;
    public EmailID: number;
    public EmailAddress: string;
    public Status: string;
    public Links: string;
    public Message: string; 
    public ErrorCode: string;  
}

