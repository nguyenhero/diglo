﻿import {Product} from './product';
import {PromoCode} from './promoCode';
import {User} from './user';
export class OrderStorage {
    constructor() {}
    public custNo: string;
    public promoCode: string;
    public expires_at: string;
    public feeAll: number;
    public tax: number;
    public subtotal: number;
    public products: Product[];
}
