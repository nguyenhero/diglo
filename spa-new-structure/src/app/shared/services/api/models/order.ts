﻿import {BillingAddress} from './billingAddress';
import {ShippingAddress} from './shippingAddress';
import {OrderDetail} from './orderDetail';
import {OrderHead} from './orderHead';
import {CreditCard} from './creditCard';
import {User} from './user';
export class Order {
    constructor(
        public subcribeNewletter: number,
        public user: User,
        public billingAddress: BillingAddress,
        public shippingAddress: ShippingAddress,
        public orderDetails: OrderDetail[],
        public orderHead: OrderHead,
        public creditCard: CreditCard
    ) {}
}
