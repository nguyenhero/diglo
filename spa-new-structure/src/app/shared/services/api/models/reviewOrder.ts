﻿import {BillingAddress} from './billingAddress';
import {ShippingAddress} from './shippingAddress';
import {OrderDetail} from './orderDetail';
import {OrderHead} from './orderHead';
import {CreditCard} from './creditCard';
import {User} from './user';
export class ReviewOrder {
    constructor() {}
    public payerId: string;
    public token: string;
    public billingAddress: BillingAddress;
    public shippingAddress: ShippingAddress;
    public orderDetails: OrderDetail[];
    public orderHead: OrderHead;
}
