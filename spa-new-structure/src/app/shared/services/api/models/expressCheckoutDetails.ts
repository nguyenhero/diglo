﻿export class ExpressCheckoutDetails {
    constructor(
    ) {}
    public token: string;
    public firstName: string;
    public lastName: string;
    public countrycode: string;
    public email: string;
    public shipToName: string;
    public shipToStreet: string;
    public shipToCity: string;
    public shipToState: string;
    public shipToZip: string;
    public shipToCountryCode: string;
    public shipToPhoneNum: string;
    public payerId: string;
    public amt: string;
}
