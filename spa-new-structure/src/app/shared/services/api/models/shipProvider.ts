﻿export class ShipProvider {
    constructor() {}
    public shipProvider: string;
    public shipType: string;
    public shipFee: number;
    public shipDesc: string;
}
