﻿export class Product {
    constructor(
    ) {}
    public pageID: number;
    public productID: number;
    public productSKU: string;
    public productDesc: string;
    public productLongDesc: string;
    public productShortDesc: string;
    public productSpec: string;
    public qty: number;
    public imageMain: string;
    public image1: string;
    public image2: string;
    public image3: string;
    public image4: string;
    public mSRPPrice: number;
    public webPrice: number;
    public cartPrice: number;
    public crossoffInd: string;
    public isFreeShip: string;
    public specialShipFee: number;;
    public availability: string;
    public media: string;
    public productDocument1: string;
    public productDocument2: string;
    public overallCount: number;
    public color: string;
    public reviewRate: string;
    public newArrivalInd: string;
    public clearanceInd: string;
    public reviewCount: number;
    public sub_total: number;
    public weight: number;
    public departmentDesc: string;
    public classDesc: string;
    public subclassDesc: string;
    public departmentID: number;
    public classID: number;
    public subclassID: number;
    public shipToCa: string;
    public required: boolean;
    public validQty: boolean;

    set subTotal(value) {
        this.sub_total = value;
    }

    get subTotal() {
        return this.sub_total;
    }
}

