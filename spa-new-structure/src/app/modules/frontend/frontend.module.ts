import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontendRoutingModule } from './frontend-routing.module';
import { FrontendComponent } from './frontend.component';
import { HeaderMenuComponent } from './components/header/header.component';
import { MainMenuComponent } from './components/mainmenu/mainmenu.component';
import { NewsletterMenuComponent } from './components/newsletter/newsletter.component';
import { FooterMenuComponent } from './components/footer/footer.component';
import { ClassItemComponent } from './components/mainmenu/menu-item.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalModule } from 'angular2-modal';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
import { UserService } from 'src/app/shared/services/api/services/user.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { CustomerCatService } from 'src/app/shared/services/api/services/customer.cat.service';
import { StateService } from 'src/app/shared/services/api/services/state.service';
import { NewsletterService } from 'src/app/shared/services/api/services/newsletter.service';
import { ProductService } from 'src/app/shared/services/api/services/product.service';
import { ProductDetailsService } from 'src/app/shared/services/api/services/productDetails.service';
import { ProductReviewService } from 'src/app/shared/services/api/services/productReview.service';
import { CardProductService } from 'src/app/shared/services/api/services/cardProduct.service';
import { OrderService } from 'src/app/shared/services/api/services/order.service';
import { PromoCodeService } from 'src/app/shared/services/api/services/promoCode.service';
import { ShippingChargeService } from 'src/app/shared/services/api/services/shippingCharge.service';
import { MenuService } from 'src/app/shared/services/api/services/menu.service';
import { CardWishProductsService } from 'src/app/shared/services/api/services/cardWishProducts.service';
import { ShippingAddressService } from 'src/app/shared/services/api/services/shippingAddress.service';
import { BillingAddressService } from 'src/app/shared/services/api/services/billingAddress.service';
import { WishListService } from 'src/app/shared/services/api/services/wishList.service';
import { ProductAdditionInfoService } from 'src/app/shared/services/api/services/productAdditionInfo.service';
import { OrderStorageService } from 'src/app/shared/services/api/services/orderStorage.service';
import { OrderLogicService } from 'src/app/shared/services/api/services/orderLogic.service';
import { FilterGroupService } from 'src/app/shared/services/api/services/filterGroup.service';
import { HttpModule } from '@angular/http';
import { AddOrderModule } from './components/add-order/addOrder.module';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap/src/bootstrap';
import { LoginModal } from './components/login/login.component';
import { RegisterModal } from './components/register/register.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FrontendRoutingModule,
    HttpModule,
    AddOrderModule,
    // MyOrderModule,
    BootstrapModalModule,
    ModalModule.forRoot(),
  ],
  providers: [
    ValidationService,
    UserService,
    AuthenticationService,
    AppState,
    CustomerCatService,
    StateService,
    NewsletterService,
    ProductService,
    ProductDetailsService,
    ProductReviewService,
    CardProductService,
    OrderService,
    PromoCodeService,
    ShippingChargeService,
    MenuService,
    CardWishProductsService,
    ShippingAddressService,
    BillingAddressService,
    WishListService,
    ProductAdditionInfoService,
    OrderStorageService,
    OrderLogicService,
    FilterGroupService,
  ],

  declarations: [FrontendComponent, LoginModal, RegisterModal, HeaderMenuComponent, MainMenuComponent, NewsletterMenuComponent, FooterMenuComponent, ClassItemComponent],
  entryComponents: [LoginModal, RegisterModal]
})
export class FrontendModule { }
