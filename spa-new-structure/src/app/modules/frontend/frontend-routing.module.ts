 

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 
import { FrontendComponent } from './frontend.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
    {
        path: '',
        component: FrontendComponent,
        children: [
            {
                path: '',
                redirectTo: 'home'
            },
            { path: 'home', loadChildren: './pages/home/home.module#HomeModule' },
            { path: 'allproducts', loadChildren: './pages/products/products.module#ProductsModule' },
            { path: 'productDetails', loadChildren: './pages/products-details/products-details.module#ProductDetailsModule' },
            { path: 'place-order-successfully', loadChildren: './pages/place-order-successfully/place-order-successfully.module#PlaceOrderCuccessfullyModule' },
            { path: 'checkout-order', loadChildren: './pages/checkout-order/checkout-order.module#CheckoutOrderModule' },
            { path: 'helpful-resources', loadChildren: './pages/helpful-resources/helpful-resources.module#HelpfulResourcesModule' },
            { path: 'my-account', loadChildren: './pages/my-account/my-account.module#MyAccountModule' },
            { path: 'search', loadChildren: './pages/search/search.module#SearchModule' },
            { path: 'about-us', loadChildren: './pages/about-us/about-us.module#AboutUsModule' },

            { path: 'contactus', loadChildren: './pages/contact-us/contact-us.module#ContactUsModule' },

            { path: 'condition', loadChildren: './pages/condition/condition.module#ConditionModule' },

            { path: 'privacy', loadChildren: './pages/privacy/privacy.module#PrivacyModule' },
        ]
   }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FrontendRoutingModule {}
