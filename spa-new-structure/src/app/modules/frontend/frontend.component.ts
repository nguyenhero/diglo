import { DOCUMENT } from '@angular/platform-browser';
import { Component, Inject, HostListener, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-frontend',
  templateUrl: './frontend.component.html',
  styleUrls: ['./frontend.component.scss']
})
export class FrontendComponent implements OnInit {
  headerMenu: string = '';
  mainMenu: string = '';
  lastScrollTop: number = 0;
  constructor(@Inject(DOCUMENT) private document, private router: Router) {
  }
  onNotify(message) {
    alert(message);
  }
  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
  @HostListener("window:scroll", ['$event'])
  onWindowScroll($event: Event) {
    if (window.pageYOffset > 0) {
      if (this.document.getElementById('contactId').className.indexOf('in') > 0) {
        this.document.getElementById('contactId').classList.remove('in');
      };
      if (this.document.getElementById('headerMenu').className.indexOf('main-menu-scroll') < 0) {
        this.document.getElementById('headerMenu').classList.add("main-menu-scroll");
      };
      if (this.document.getElementById('mainMenu').className.indexOf('main-menu-scroll') < 0) {
        this.document.getElementById('mainMenu').classList.add("main-menu-scroll");
      };
    } else {
      if (this.document.getElementById('headerMenu').className.indexOf('main-menu-scroll') > 0) {
        this.document.getElementById('headerMenu').classList.remove("main-menu-scroll");
      };
      if (this.document.getElementById('mainMenu').className.indexOf('main-menu-scroll') > 0) {
        this.document.getElementById('mainMenu').classList.remove("main-menu-scroll");
      };
    }
  }
}