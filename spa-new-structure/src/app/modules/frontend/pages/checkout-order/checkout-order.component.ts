﻿import {Component, Inject, HostListener, Input, Output, EventEmitter, ChangeDetectorRef} from '@angular/core';
import {DialogRef, ModalComponent} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {DOCUMENT} from '@angular/platform-browser';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { Product } from 'src/app/shared/services/api/models/product';
import { BillingAddress } from 'src/app/shared/services/api/models/billingAddress';
import { ShippingAddress } from 'src/app/shared/services/api/models/shippingAddress';
import { CreditCard } from 'src/app/shared/services/api/models/creditCard';
import { ShipProvider } from 'src/app/shared/services/api/models/shipProvider';
import { Payflow } from 'src/app/shared/services/api/models/payflow';
import { OrderDetail } from 'src/app/shared/services/api/models/orderDetail';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { CardProductService } from 'src/app/shared/services/api/services/cardProduct.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { OrderService } from 'src/app/shared/services/api/services/order.service';
import { ShippingChargeService } from 'src/app/shared/services/api/services/shippingCharge.service';
import { OrderLogicService } from 'src/app/shared/services/api/services/orderLogic.service';
 
@Component({
    selector: 'checkout-order',
    templateUrl: './checkout-order.component.html',
    styleUrls: ['./checkout-order.component.scss']
})
export class CheckoutOrderComponent {
    products: Product[];
    totals: number = 0;
    sub_totals: number = 0;
    cartPrice: number = 0;
    promoCode: string = '';
    promoCodeValid: number = 0;
    billingAddress: BillingAddress;
    shippingAddress: ShippingAddress;
    creditCard: CreditCard;
    showPass: number = 0;
    paymentType: number = 0;
    shipLocFee: number = 0;
    shipProvider: ShipProvider;
    payflow: Payflow;
    shipFee: number = 0;
    taxTotal: number = 0;
    tax: number = 0;
    moneyOver = 5000;
    taxResponse: any;
    orderDetails: OrderDetail[] = [];
    billing: string = 'panel-collapse collapse in';
    shipping: string = 'panel-collapse collapse out';
    placeOrder: string = 'panel-collapse collapse out';
    billingProgress: string = 'bar done first';
    billingCircleProgress: string = 'circle done';
    shippingProgress: string = 'bar half';
    shippingCircleProgress: string = 'circle active';
    placeOrderProgress: string = 'bar half';
    placeOrderCircleProgress: string = 'circle active';
    billingOrderPanel: string = '';
    shippingOrderPanel: string = 'disable';
    placeOrderPanel: string = 'disable';
    editBilling: boolean = false;
    editShipping: boolean = false;
    private ADD_ORDER_CARD: string = 'ADD_ORDER_CARD';
    isExistEmail: boolean = false;
    sameAsBillingAddress: number = 0;
    email: string;
    comments: string;
    shipCountry: string;
    orderId: string;
    order: any;
    constructor(
        private cdr: ChangeDetectorRef,
        private formBuilder: FormBuilder,
        public appState: AppState,
        @Inject(DOCUMENT) private document,
        private router: Router,
        private _routeParams: ActivatedRoute,
        private modal: Modal,
        private cardProductService: CardProductService,
        private authenticationService: AuthenticationService,
        private orderService: OrderService,
        private shippingChargeService: ShippingChargeService,
        private orderLogicService: OrderLogicService
    ) {
        appState.publish(this.router.url);
        this._routeParams.params.subscribe(params => {
            this.orderId = params['orderId'] ? params['orderId'] : this.getParameterByName('orderId');
            if (this.orderId) {
                this.loadDetailsOrder();
                this.getOrder();
            } else {
                this.init();
            }
        });
        appState.event.subscribe((data) => {
            if (data && data == this.ADD_ORDER_CARD) {
                this.products = this.orderLogicService.getProducts();
            }
        });
    }
    loadOrder() {
        this.getOrder();
    }
    loadDetailsOrder() {
        let orderOperation = this.orderService.getOrderDetails(this.orderId);
        orderOperation.subscribe(result => {
            this.orderDetails = result;
            this.orderDetails.forEach(orderDetail => {
                orderDetail.product.qty = orderDetail.quantity;
                this.orderLogicService.addToCart(orderDetail.product);
            });
            this.init();
        });
    }
    getOrder() {
        let currentUser = this.authenticationService.getCurrentUser();
        let operation = this.orderService.getOrder(currentUser.custNo.toString(), "-1", this.orderId);
        operation.subscribe(result => {
            if (result && result.length > 0) {
                this.order = result[0];
                if (this.order) {
                    this.billingAddress = this.createBillingAddress(this.order);
                }
            }
        });
    }
    init() {
        let order = this.orderLogicService.getOrder();
        if (order) {
            this.products = order.products;
            this.sub_totals = this.orderLogicService.getSubTotal(order);
            this.promoCode = order.promoCode;
            this.createShipProvider();
            this.shipFee = 0;
            this.getTotals();
        }
    }
    createShipProvider() {
        this.shipProvider = new ShipProvider();
        this.shipProvider.shipProvider = 'Fedex';
        this.shipProvider.shipFee = 0;
    }
    changeQtyMobi(product, value) {
        if (value) {
            product.required = false;
            product.qty = parseInt(value);
            if (product.qty > 0) {
                product.validQty = false;
            } else {
                product.validQty = true;
            }
            product.qty = parseInt(value);
            this.orderLogicService.updateToCart(product);
            let order = this.orderLogicService.getOrder();
            if (order) {
                this.sub_totals = this.shipCountry == 'CA'
                    ? this.orderLogicService.getSubTotalShipToCa() :
                    this.orderLogicService.getSubTotal();
                if (this.sub_totals > this.moneyOver) {
                    this.showShippingChargesOver();
                }
                if (this.shippingAddress) {
                    this.getTax();
                } else {
                    this.getTotals();
                }
                this.showMsgShipToCa();
            }
        } else {
            product.required = true;
        }
    }
    changeQty(product, value) {
        if (value) {
            product.required = false;
            product.qty = parseInt(value);
            if (product.qty > 0) {
                product.validQty = false;
            } else {
                product.validQty = true;
            }
            product.qty = parseInt(value);
            this.orderLogicService.updateToCart(product);
            let order = this.orderLogicService.getOrder();
            if (order) {
                this.sub_totals = this.shipCountry == 'CA'
                    ? this.orderLogicService.getSubTotalShipToCa() :
                    this.orderLogicService.getSubTotal();
                if (this.sub_totals > this.moneyOver) {
                    if (order.products.length == 1) {
                        this.diableAllPanel();
                    }
                    this.showShippingChargesOver();
                } else {
                    if (order.products.length == 1) { this.editBillingOrderPanel(); }
                }
                if (this.shippingAddress) {
                    this.getTax();
                } else {
                    this.getTotals();
                }
                this.showMsgShipToCa();
            }
        } else {
            product.required = true;
        }
    }
    showShippingChargesOver() {
        let msg = `Please call for orders outside the U.S. or Canada.
                   Shipping charges for Internet orders over USD $5, 000 will be Actual Shipping`;
        this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .body(msg)
            .open().then(x=>{
                this.document.body.classList.remove('modal-open');
            });
    }
    getTotals() {
        this.totals = Math.round((this.sub_totals + this.shipFee + this.tax * this.sub_totals) * 100) / 100;
    }
    removeProduct(product) {
        this.orderLogicService.remove(product);
        let order = this.orderLogicService.getOrder();
        if (order) {
            this.products = order.products;
            this.sub_totals = this.shipCountry == 'CA'
                ? this.orderLogicService.getSubTotalShipToCa() :
                this.orderLogicService.getSubTotal();
            this.getTotals();
            this.getTax();
        } else {
            this.reset();
        }
    }
    reset() {
        this.products = [];
        this.sub_totals = 0;
        this.totals = 0;
        this.shipFee = 0;
        this.tax = 0;
    }
    diableAllPanel() {
        this.shipping = 'panel-collapse collapse out';
        this.billing = 'panel-collapse collapse out';
        this.placeOrder = 'panel-collapse collapse out';
        this.placeOrder = 'disable';
        this.shippingOrderPanel = 'disable';
        this.placeOrderPanel = 'disable';
        this.billingOrderPanel = 'disable';
    }
    billingOrderFinished(value) {
        this.billingAddress = value.billingOrder;
        this.email = value.email;
        this.isExistEmail = value.isExistEmail;
        if (value.paymentType == 24) {
            this.creditCard = new CreditCard();
            this.creditCard.number = value.billingOrder['creditCardNumer'];
            this.creditCard.expireMonth = value.billingOrder['expirationDateM'];
            this.creditCard.expireYear = value.billingOrder['expirationDateY'];
            this.creditCard.cvv2 = value.billingOrder['cardVerifyNumber'];
        }
        this.paymentType = value.paymentType;
        this.billing = 'panel-collapse collapse out';
        this.shipping = 'panel-collapse collapse in';
        this.placeOrder = 'panel-collapse collapse  out';
        this.shippingProgress = 'bar done';
        this.shippingCircleProgress = 'circle done';
        this.billingOrderPanel = 'disable';
        this.shippingOrderPanel = '';
        this.placeOrder = 'disable';
        this.editBilling = true;
        this.shippingAddress = this.createShippingAddress(this.order);
        setTimeout(() => {
            window.scroll(0, 0);
        }, 100);
    }
    changeFeesFinished(value) {
        this.shipCountry = value.shipCountry;
        if (value.shipProvider) {
            this.shipProvider = value.shipProvider;
        }
        this.shipFee = value.shipFee;
        this.sub_totals = this.shipCountry == 'CA'
            ? this.orderLogicService.getSubTotalShipToCa() :
            this.orderLogicService.getSubTotal();
        this.getTotals();
        if (value.shippingAddress) {
            this.shippingAddress = value.shippingAddress;
            this.cdr.detectChanges();
            this.getTax();
        }
    }
    shippingOrderFormFinished(value) {
        this.comments = value.comments;
        this.sameAsBillingAddress = value.sameAsBillingAddress;
        this.shippingAddress = value.shippingAddress;
        if (value.shipProvider) {
            this.shipProvider = value.shipProvider;
        }
        this.shipFee = value.shipFee;
        this.getTax();
        this.billingAddress.email = this.email;
        this.shippingAddress.email = this.email;
        this.setPanel();
    }
    createOrderDetails() {
        let self = this;
        self.orderDetails = [];
        this.products = this.orderLogicService.getProducts();
        if (this.products) {
            this.products
                .forEach(item => {
                    if (this.shipCountry == 'CA') {
                        if (item.shipToCa && item.shipToCa == 'Y') {
                            let orderDetail = this.createOrderDetail(item);
                            self.orderDetails.push(orderDetail);
                        }
                    } else {
                        let orderDetail = this.createOrderDetail(item);
                        self.orderDetails.push(orderDetail);
                    }
                });
        }
    }
    createOrderDetail(item: Product) {
        let orderDetail = new OrderDetail();
        orderDetail.productSKU = item.productSKU;
        orderDetail.webPrice = item.webPrice;
        orderDetail.cartPrice = item.cartPrice;
        orderDetail.tax = 0;
        orderDetail.quantity = item.qty;
        return orderDetail;
    }
    editBillingOrderPanel() {
        this.billingOrderPanel = '';
        this.editBilling = true;
        this.billing = 'panel-collapse collapse in';
        this.shipping = 'panel-collapse collapse out';
        this.placeOrder = 'panel-collapse collapse out';
        this.placeOrder = 'disable';
        this.shippingOrderPanel = 'disable';
        this.placeOrderPanel = 'disable';
    }
    editShippingOrderPanel() {
        this.shippingOrderPanel = '';
        this.editShipping = true;
        this.shipping = 'panel-collapse collapse in';
        this.billing = 'panel-collapse collapse out';
        this.placeOrder = 'panel-collapse collapse out';
        this.placeOrder = 'disable';
        this.billingOrderPanel = 'disable';
        this.placeOrderPanel = 'disable';
    }
    getTax() {
        if (!this.orderDetails || this.orderDetails.length == 0) {
            this.createOrderDetails();
        }
        let data = {
            'billingaddress': this.billingAddress,
            'shippingaddress': this.shippingAddress,
            'orderdetails': this.orderDetails,
            'excludingshippingtotal': this.sub_totals,
            'shipfeetotal': this.shipFee
        };
        if (this.shippingAddress && this.billingAddress && this.orderDetails) {
            let operation = this.orderService.getOrderTax(data);
            operation.subscribe(
                result => {
                    if (result.errors) {
                        let errorMsg = JSON.parse(result.errors).detail;
                        this.modal.alert()
                            .size('sm')
                            .showClose(true)
                            .body(errorMsg)
                            .open().then(x=>{
                                this.document.body.classList.remove('modal-open');
                            });
                    }
                    else {
                        this.taxResponse = result;
                        this.tax = parseFloat(this.taxResponse.rate);
                        let currentUser = this.authenticationService.getCurrentUser();
                        this.showPass = !currentUser ? 1 : 0;
                        this.getTotals();
                    }
                });
        }
    }
    setPanel() {
        this.billing = 'panel-collapse collapse out';
        this.shipping = 'panel-collapse collapse out';
        this.placeOrder = 'panel-collapse collapse in';
        this.placeOrderProgress = 'bar done';
        this.placeOrderCircleProgress = 'circle done';
        this.billingOrderPanel = 'disable';
        this.shippingOrderPanel = 'disable';
        this.placeOrderPanel = '';
        this.editShipping = true;
    }
    showMsgShipToCa() {
        let products = this.orderLogicService.getProducts();
        if (products && products.length == 1 && this.shipCountry == 'CA'
            && (!products[0].shipToCa || products[0].shipToCa == 'N')) {
            let msg = 'We cannot ship this item out Canada';
            this.modal.alert()
                .size('sm')
                .isBlocking(true)
                .showClose(true)
                .body(msg)
                .open().then(x=>{
                    this.document.body.classList.remove('modal-open');
                });;
        }
    }
    createBillingAddress(order) {
        this.billingAddress = new BillingAddress();
        this.billingAddress.address = order.billingAddress;
        this.billingAddress.city = order.billingCity;
        this.billingAddress.company = order.billingCompany;
        this.billingAddress.country = order.billingCountry;
        this.email = order.billingEmail;
        this.billingAddress.phone = order.billingPhone;
        this.billingAddress.phone = order.billingFax;
        this.billingAddress.firstName = order.billingFirstName;
        this.billingAddress.lastName = order.billingLastName;
        this.billingAddress.fullName = this.billingAddress.firstName + ' ' + this.billingAddress.lastName;
        this.billingAddress.state = order.billingState;
        this.billingAddress.zip = order.billingZip;
        return this.billingAddress;
    }
    createShippingAddress(order) {
        if (this.order) {
            this.shippingAddress = new ShippingAddress();
            this.shippingAddress.address = order.shippingAddress;
            this.shippingAddress.city = order.shippingCity;
            this.shippingAddress.company = order.shippingCompany;
            this.shippingAddress.country = order.shippingCountry;
            this.shippingAddress.phone = order.shippingPhone;
            this.shippingAddress.phone = order.shippingFax;
            this.shippingAddress.firstName = order.shippingFirstName;
            this.shippingAddress.lastName = order.shippingLastName;
            this.shippingAddress.fullName = this.shippingAddress.firstName + ' ' + this.shippingAddress.lastName;
            this.shippingAddress.state = order.shippingState;
            this.shippingAddress.zip = order.shippingZip;
            return this.shippingAddress;
        }
    }
    @HostListener("window:scroll", ['$event'])
    onWindowScroll($event: Event) {
        if (document.getElementById('productList')) {
            let top = document.documentElement.scrollTop;
            if (top >= 300) {
                document.getElementById('productList').style.top = '50px';
            } else {
                document.getElementById('productList').style.top = '400px';
            }
        }
    }
    getParameterByName(name) {
        let url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
}