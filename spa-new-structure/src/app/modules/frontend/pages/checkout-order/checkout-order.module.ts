import {NgModule, Component, Pipe, PipeTransform} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ModalModule} from 'angular2-modal';
import {BootstrapModalModule} from 'angular2-modal/plugins/bootstrap';
import {CheckoutOrderComponent} from './checkout-order.component'; 
import { BillingOrderComponent } from './components/billing-order/billing-order.component';
import { FedExRateOptionsModal } from './components/fedExRateOptions/fedExRateOptions.component';
import { LoginCheckOutModal } from './components/login-check-out/login-check-out.component';
import { OrderReviewComponent } from './components/order-review/order-review.component';
import { PlaceOrderComponent } from './components/place-order/place-order.component';
import { ShippingOrderComponent } from './components/shipping-order/shipping-order.component';
// import { OffSaleComponent } from '../offsale/offsale.component';
@NgModule({
  declarations: [
    // OffSaleComponent,
    CheckoutOrderComponent,
    BillingOrderComponent,
    FedExRateOptionsModal,
    LoginCheckOutModal,
    OrderReviewComponent,
    PlaceOrderComponent,
    ShippingOrderComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    RouterModule.forChild([
      { path: '', component: CheckoutOrderComponent, pathMatch: 'full' }
    ])
  ],
  entryComponents: [
    FedExRateOptionsModal,
    LoginCheckOutModal
  ]
})
export class CheckoutOrderModule {}
