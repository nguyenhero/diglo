﻿import { Component, Input, Output, EventEmitter   } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/shared/services/api/models/user';
import { ExpressCheckoutDetails } from 'src/app/shared/services/api/models/expressCheckoutDetails';
import { ShipProvider } from 'src/app/shared/services/api/models/shipProvider';
import { Product } from 'src/app/shared/services/api/models/product';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { StateService } from 'src/app/shared/services/api/services/state.service';
import { OrderService } from 'src/app/shared/services/api/services/order.service';
import { BillingAddressService } from 'src/app/shared/services/api/services/billingAddress.service';
import { ShippingAddressService } from 'src/app/shared/services/api/services/shippingAddress.service';
import { ShippingChargeService } from 'src/app/shared/services/api/services/shippingCharge.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { CardProductService } from 'src/app/shared/services/api/services/cardProduct.service';
import { OrderLogicService } from 'src/app/shared/services/api/services/orderLogic.service';
import { ReviewOrder } from 'src/app/shared/services/api/models/reviewOrder';
import { OrderDetail } from 'src/app/shared/services/api/models/orderDetail';
import { OrderHead } from 'src/app/shared/services/api/models/orderHead';
import { BillingAddress } from 'src/app/shared/services/api/models/billingAddress';
import { ShippingAddress } from 'src/app/shared/services/api/models/shippingAddress';
import { FedExRateOptionsModalContext, FedExRateOptionsModal } from '../fedExRateOptions/fedExRateOptions.component';
 
@Component({
    selector: 'order-review',
    templateUrl: './order-review.component.html',
    styleUrls: ['./order-review.component.scss']
})
export class OrderReviewComponent {
    private PAY_PAL_COMPLETED: string = 'PayPalCompleted';
    CA_FEE: number = 25.00;
    currentUser: User;
    billingOrderForm: any;
    shippingOrderForm: any;
    states = [];
    shippingStates = [];
    addrID: number = 0;
    expressCheckoutDetails: ExpressCheckoutDetails;
    rate: any;
    shipFee: number = 0;
    shipProvider: ShipProvider;
    products: Product[];
    totals: number = 0;
    sub_totals: number = 0;
    cartPrice: number = 0;
    promoCode: string = '';
    agrreeTerm: number = 0;
    token: string;
    payerId: string;
    tax: number = 0;
    isFreeShip: boolean = true;
    moneyOver = 5000;
    stateList: string[] = ['AK', 'HI', 'PR'];
    shipCountry: string;
    shippingOrderPanel: string;
    constructor(private formBuilder: FormBuilder,
        private modal: Modal,
        private appState: AppState,
        private router: Router, private route: ActivatedRoute,
        private stateService: StateService,
        private orderService: OrderService,
        private billingAddressService: BillingAddressService,
        private shippingAddressService: ShippingAddressService,
        private shippingChargeService: ShippingChargeService,
        private authenticationService: AuthenticationService,
        private cardProductService: CardProductService,
        private orderLogicService: OrderLogicService) {
        this.token = this.getParameterByName('token'); // "lorem"
        this.payerId = this.getParameterByName('PayerID'); // "" (present with empty value)
        let order = this.orderLogicService.getOrder();
        if (order) {
            this.products = order.products;
            this.sub_totals = this.orderLogicService.getSubTotal();
            this.promoCode = order.promoCode;
            this.shipProvider = new ShipProvider();
            this.shipProvider.shipProvider = 'Fedex';
            let operation = this.orderService.getPalPalExpressCheckoutDetails(this.token);
            operation.subscribe(
                data => {
                    if (data.token) {
                        this.expressCheckoutDetails = data;
                        this.expressCheckoutDetails.token = this.token;
                        this.expressCheckoutDetails.payerId = this.payerId;
                        this.shipCountry = data.shipToCountryCode;
                        if (this.shipCountry == 'CA')
                            this.sub_totals = this.orderLogicService.getSubTotalShipToCa();
                        let stateOperation = this.stateService.get(this.shipCountry);
                        stateOperation.subscribe(result => {
                            this.shippingStates = result;
                        });                      
                        this.getRates();
                    }
                });
        }
    }
    showShippingChargesOver(total) {
        if (total > this.moneyOver) {
            let msg = `Please call for orders outside the U.S. or Canada.
                   Shipping charges for Internet orders over USD $5, 000 will be Actual Shipping`;
            this.modal.alert()
                .size('sm')
                .isBlocking(true)
                .showClose(true)
                .body(msg)
                .open();
        }
    }
    getCountry(country) {
        switch (country) {
            case 'US':
                return 'United States';
            case 'CA':
                return 'Canada';
        }
    }
    getShippingState(state) {
        for (let i = 0; i < this.shippingStates.length; i++) {
            if (this.shippingStates[i].stateID.toString() == state) {
                return this.shippingStates[i].stateDesc;
            }
        }
    }
    showRatesDialog(rates, state) {
        let context = new FedExRateOptionsModalContext(rates, state, this.shipProvider);
        context.dialogClass = 'fedExRateOptions';
        context.isBlocking = true;
        let dialog = this.modal.open(FedExRateOptionsModal, { context: context });
        dialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                if (result && result.totalNetCharge) {
                    this.shipProvider.shipType = result.serviceTypeCode;
                    this.shipFee = result.totalNetCharge;
                    this.getTax();
                } else {
                    this.shipProvider.shipProvider = '';
                    this.shipProvider.shipType = '';
                    this.shipFee = 0;
                }
            })
        });
    }
    getRatesDialog() {
        let products = this.orderLogicService.getProducts();
        let weightTotal = 0;
        for (let i = 0; i < this.products.length; i++) {
            weightTotal = weightTotal + this.products[i].qty * this.products[i].weight;
        }

        let shippingAddress = {
            'address': this.expressCheckoutDetails.shipToStreet,
            'city': this.expressCheckoutDetails.shipToCity,
            'state': this.expressCheckoutDetails.shipToState,
            'zip': this.expressCheckoutDetails.shipToZip,
            'country': this.expressCheckoutDetails.shipToCountryCode,
        };
        let data = {
            'shippingAddress': shippingAddress, 'weightTotal': weightTotal
        };
        this.orderService.getRates(data).subscribe(
            result => {
                if (result) {
                    this.showRatesDialog(result, shippingAddress.state);
                }
            });
    }
    getRates() {
        if (this.stateList.indexOf(this.expressCheckoutDetails.shipToState) == -1) {
            this.shipProvider.shipType = '';
            this.shipFee = 0;
            this.getTax();
            return;
        }
        let products = this.orderLogicService.getProducts();
        let weightTotal = 0;
        for (let i = 0; i < this.products.length; i++) {
            weightTotal = weightTotal + this.products[i].qty * this.products[i].weight;
        }
        let shippingAddress = {
            'Address': this.expressCheckoutDetails.shipToStreet,
            'City': this.expressCheckoutDetails.shipToCity,
            'State': this.expressCheckoutDetails.shipToState,
            'Zip': this.expressCheckoutDetails.shipToZip,
            'Country': this.expressCheckoutDetails.shipToCountryCode,
        };

        let data = {
            'shippingAddress': shippingAddress,
            'weightTotal': weightTotal,
            'serviceType': 'FEDEX_GROUND'
        };
        if (shippingAddress) {
            this.orderService.getRates(data).subscribe(
                result => {
                    let noAvailable = false;
                    if (result && result.rateOptions && result.rateOptions.length > 0) {
                        result.rateOptions.forEach(item => {
                            if (item.serviceTypeCode === 'FEDEX_GROUND') {
                                noAvailable = true;
                                this.shipProvider.shipType = item.serviceTypeCode;
                                this.shipFee = item.totalNetCharge;
                                this.getTax();
                                return;
                            }
                        });
                    }
                    if (!noAvailable) {
                        this.shipProvider.shipType = '';
                        this.shipFee = 0;
                        this.modal.alert()
                            .size('sm')
                            .isBlocking(true)
                            .showClose(true)
                            .body('FEDEX_GROUND services is not available')
                            .open();
                    }
                });
        }
    }
    changeQty(product, value) {
        product.qty = parseInt(value);
        this.orderLogicService.updateToCart(product);
        let order = this.orderLogicService.getOrder();
        if (order) {
            this.sub_totals = this.shipCountry == 'CA'
                ? this.orderLogicService.getSubTotalShipToCa() :
                this.orderLogicService.getSubTotal();
            this.promoCode = order.promoCode;
            this.getTax();
            this.showShippingChargesOver(this.sub_totals);
        }
    }
    agrreeTermClick() {
        if (this.agrreeTerm == 0) {
            this.agrreeTerm = 1;
        } else {
            this.agrreeTerm = 0;
        }
    }
    save() {
        if (this.sub_totals <= this.moneyOver) {
            let products = this.orderLogicService.getProducts();
            if (products && products.length > 0) {
                if (this.shipToCaProduct(products)) {
                    let data = this.createReviewOrder();
                    let operation = this.orderService.doExpressCheckoutPayment(data);
                    operation.subscribe(data => {
                        if (data.result > 0) {
                            this.orderLogicService.removeAll();
                            let order = { orderNumber: data.result };
                            this.router.navigate(['place-order-successfully', order]);
                        } else {
                            let msg = data.result == 0
                                ? `You have not successfully order. Thank you.`
                                : data.errorMsg;
                            this.showUnSuccessfullyOrder(msg);
                        }
                    });
                } else this.showMsgShipToCa();
            } else this.showMsgNoProduct();
        } else {
            this.showShippingChargesOver(this.sub_totals);
        }
    }
    createReviewOrder() {
        let reviewOrder = new ReviewOrder();
        reviewOrder.payerId = this.payerId;
        reviewOrder.token = this.token;
        reviewOrder.orderDetails = this.createOrderDetails();
        reviewOrder.orderHead = this.createOrderHead();
        return reviewOrder;
    }
    createOrderDetails() {
        let orderDetails: OrderDetail[] = [];
        this.products
            .forEach(item => {
                let orderDetail = this.createOrderDetail(item);
                orderDetails.push(orderDetail);
            });
        return orderDetails;
    }
    createOrderDetail(item: Product) {
        let orderDetail = new OrderDetail();
        orderDetail.productSKU = item.productSKU;
        orderDetail.webPrice = item.webPrice;
        orderDetail.cartPrice = item.cartPrice;
        orderDetail.specialFee = 0;
        orderDetail.tax = 0;
        orderDetail.quantity = item.qty;
        return orderDetail;
    }
    createOrderHead() {
        let currentUser = this.currentUser;
        let orderHead = new OrderHead();
        this.createOrderHeadBilling(orderHead);
        this.createOrderHeadShipping(orderHead);
        this.createOrderHeadPayment(orderHead);
        return orderHead;
    }
    createOrderHeadBilling(orderHead: OrderHead) {
        let currentUser = this.authenticationService.getCurrentUser();
        orderHead.custNo = currentUser ? currentUser.custNo : - 1;
        orderHead.billingEmail = this.expressCheckoutDetails.email;
        orderHead.billingFirstName = this.expressCheckoutDetails.firstName;
        orderHead.billingLastName = this.expressCheckoutDetails.lastName;
        orderHead.shippingFullName = this.expressCheckoutDetails.firstName + ' ' + this.expressCheckoutDetails.lastName;
        //orderHead.billingCountry = this.expressCheckoutDetails.countrycode;
        orderHead.billingAddress = this.expressCheckoutDetails.shipToStreet;
        orderHead.billingCity = this.expressCheckoutDetails.shipToCity;
        orderHead.billingCountry = this.expressCheckoutDetails.shipToCountryCode;
        orderHead.billingFullName = this.expressCheckoutDetails.shipToName
        orderHead.billingState = this.expressCheckoutDetails.shipToState;
        orderHead.billingZip = this.expressCheckoutDetails.shipToZip;
        orderHead.billingPhone = '111-111-1111';
    }
    createOrderHeadShipping(orderHead: OrderHead) {
        orderHead.shippingEmail = this.expressCheckoutDetails.email;
        orderHead.shippingAddress = this.expressCheckoutDetails.shipToStreet;
        orderHead.shippingCity = this.expressCheckoutDetails.shipToCity;
        orderHead.shippingCountry = this.expressCheckoutDetails.shipToCountryCode;
        orderHead.shippingFullName = this.expressCheckoutDetails.shipToName
        orderHead.shippingFirstName = orderHead.shippingFullName.split(' ').slice(0, -1).join(' ');
        orderHead.shippingLastName = orderHead.shippingFullName.split(' ').slice(-1).join(' ');
        orderHead.shippingState = this.expressCheckoutDetails.shipToState;
        orderHead.shippingZip = this.expressCheckoutDetails.shipToZip;
        orderHead.shipProvider = this.shipProvider.shipProvider;
        orderHead.shipType = this.shipProvider.shipType;
        orderHead.shipDesc = this.shipProvider.shipDesc;
    }
    createOrderHeadPayment(orderHead: OrderHead) {
        orderHead.paymentType = '33';
        orderHead.total = Math.round(this.totals * 100) / 100;
        orderHead.shipFee = this.shipFee;
        orderHead.status = this.PAY_PAL_COMPLETED;
        orderHead.promoCode = this.promoCode;
        orderHead.tax = this.tax * this.sub_totals;
        orderHead.shipProviderCode = this.orderLogicService.getShipProviderCode(this.shipProvider.shipType);
    }
    edit() {
        if (this.sub_totals <= this.moneyOver) {
            let data = {
                'shippingAddress': null,
                'amount': Math.round(this.totals * 100) / 100
            };
            let peration = this.orderService.getPalPalExpressCheckoutTokenByRequest(data);
            peration.subscribe(data => {
                if (data.result == 0) {
                    this.orderLogicService.createForm(data.url, data.secureToken);
                } else {
                    this.modal.alert()
                        .size('sm')
                        .isBlocking(true)
                        .showClose(true)
                        .body(data.errorMsg)
                        .open();
                }
            });
        } else {
            this.showShippingChargesOver(this.sub_totals);
        }
    }
    getTax() {
        let billingAddress = this.createBillingAddress();
        let shippingAddress = this.createShippingAddress();
        let orderDetails = this.createOrderDetails();

        let data = {
            'billingaddress': billingAddress,
            'shippingaddress': shippingAddress,
            'orderdetails': orderDetails,
            'excludingshippingtotal': this.sub_totals,
            'shipfeetotal': this.shipFee
        };
        let operation = this.orderService.getOrderTax(data);
        operation.subscribe(
            result => {
                if (result.errors) {
                    alert(result.errors);
                }
                else {
                    this.tax = parseFloat(result.rate);
                    let total = this.sub_totals + this.shipFee + this.tax * this.sub_totals;
                    this.totals = total;
                }

            });
    }
    removeProduct(product) {
        this.orderLogicService.remove(product);
        let order = this.orderLogicService.getOrder();
        if (order) {
            this.products = order.products;
            this.sub_totals = this.shipCountry == 'CA'
                ? this.orderLogicService.getSubTotalShipToCa() :
                this.orderLogicService.getSubTotal();
            this.promoCode = order.promoCode;
            this.getTax();
        } else {
            this.reset();
        }
    }
    reset() {
        this.products = [];
        this.sub_totals = 0;
        this.totals = 0;
        this.shipFee = 0;
        this.tax = 0;
    }
    createBillingAddress(): BillingAddress {
        let billingAddress = new BillingAddress();
        billingAddress.email = this.expressCheckoutDetails.email;
        billingAddress.firstName = this.expressCheckoutDetails.firstName;
        billingAddress.lastName = this.expressCheckoutDetails.lastName;
        billingAddress.fullName = this.expressCheckoutDetails.firstName + ' ' + this.expressCheckoutDetails.lastName;
        billingAddress.address = this.expressCheckoutDetails.shipToStreet;
        billingAddress.city = this.expressCheckoutDetails.shipToCity;
        billingAddress.country = this.expressCheckoutDetails.shipToCountryCode;
        billingAddress.fullName = this.expressCheckoutDetails.shipToName
        billingAddress.state = this.expressCheckoutDetails.shipToState;
        billingAddress.zip = this.expressCheckoutDetails.shipToZip;
        return billingAddress;
    }
    createShippingAddress(): ShippingAddress {
        let shippingAddress = new ShippingAddress();
        shippingAddress.email = this.expressCheckoutDetails.email;
        shippingAddress.firstName = this.expressCheckoutDetails.firstName;
        shippingAddress.lastName = this.expressCheckoutDetails.lastName;
        shippingAddress.fullName = this.expressCheckoutDetails.firstName + ' ' + this.expressCheckoutDetails.lastName;
        shippingAddress.address = this.expressCheckoutDetails.shipToStreet;
        shippingAddress.city = this.expressCheckoutDetails.shipToCity;
        shippingAddress.country = this.expressCheckoutDetails.shipToCountryCode;
        shippingAddress.fullName = this.expressCheckoutDetails.shipToName
        shippingAddress.state = this.expressCheckoutDetails.shipToState;
        shippingAddress.zip = this.expressCheckoutDetails.shipToZip;
        return shippingAddress;
    }
    shipToCaProduct(products) {
        if (products && products.length == 1) {
            let product = products[0];
            if (this.shipCountry =='CA' && product.shipToCa == 'N') return false;
        }
        return true;
    }
    showUnSuccessfullyOrder(msg = null) {
        msg = msg ? msg : 'You have not successfully order. Thank you';
        this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .body(msg)
            .open();
    }
    showMsgNoProduct() {
        let msg = 'No item in cart.';
        this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .body(msg)
            .open();
    }
    showMsgShipToCa() {
        let msg = 'We cannot ship this item out Canada';
        this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .body(msg)
            .open();
    }
    getParameterByName(name) {
        let url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
}