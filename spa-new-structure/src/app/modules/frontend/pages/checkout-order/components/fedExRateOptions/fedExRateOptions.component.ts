﻿import {Component,Inject, ViewEncapsulation} from '@angular/core';
import {DialogRef, ModalComponent, CloseGuard} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {DOCUMENT} from '@angular/platform-browser';
export class FedExRateOptionsModalContext extends BSModalContext {
    constructor(public rates: any = [], public state , public shipProvider: any) {
        super();
    }
}
@Component({
    selector: 'fedExRateOptions-modal',
    templateUrl: './fedExRateOptions.component.html',
    styleUrls: ['./fedExRateOptions.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class FedExRateOptionsModal implements CloseGuard, ModalComponent<FedExRateOptionsModalContext> {
    context: FedExRateOptionsModalContext;
    rates: any = [];
    showClear: boolean = true;
    rate: any;
    selected: any;
    stateList: string[] = ['AK', 'HI', 'PR'];
    rateCodeList: string[] = ['STANDARD_OVERNIGHT', 'FEDEX_2_DAY', 'FEDEX_EXPRESS_SAVER'];
    rateNameList: string[] = ['Standard Overnight', '2 Day', '3 Day'];
    rateCodeList3State: string[] = ['PRIORITY_OVERNIGHT', 'FEDEX_2_DAY', 'FEDEX_GROUND'];
    rateNameList3State: string[] = ['Priority Overnight', '2 Day', 'Standard Ground'];
    constructor(public dialog: DialogRef<FedExRateOptionsModalContext>, public modal: Modal,   @Inject(DOCUMENT) private document) {
        this.context = dialog.context;
        dialog.setCloseGuard(this);
        // if (this.dialog.context.rates.rateOptions) {
        //     if (this.stateList.indexOf(this.dialog.context.state) > -1) {
        //         this.showClear = false;
        //         dialog.context.rates.rateOptions.forEach((item, index) => {
        //             let i = this.rateCodeList3State.indexOf(item.serviceTypeCode);
        //             if (i > -1) {
        //                 item.serviceType = this.rateNameList3State[i];
        //                 this.rates.push(item);
        //             }
        //         });
        //     }
        //     else {
        //         dialog.context.rates.rateOptions.forEach((item, index) => {
        //             let i = this.rateCodeList.indexOf(item.serviceTypeCode);
        //             if (i > -1) {
        //                 item.serviceType = this.rateNameList[i];
        //                 this.rates.push(item);
        //             }
        //         });
        //     }
        // }

        // totalNetCharge: 27.53, 18.7, 16.49 
        // 5/4/2018 add manually Rates 
        this.rates = [
            {serviceType: 'Standard Overnight', 
            serviceTypeCode: 'STANDARD_OVERNIGHT', 
            totalNetCharge: 35.53, 
            deliveryTimestamp: '5/7/2018 3:00:00 PM', 
            transitTime: null},
            {serviceType: '2 Day', 
            serviceTypeCode: 'FEDEX_2_DAY', 
            totalNetCharge: 53.7, 
            deliveryTimestamp: '5/8/2018 4:30:00 PM', 
            transitTime: null},
            {serviceType: '3 Day', 
            serviceTypeCode: 'FEDEX_EXPRESS_SAVER', 
            totalNetCharge: 3553.49, 
            deliveryTimestamp: '5/9/2018 4:30:00 PM', 
            transitTime: null}
        ] ;

        if (dialog.context.shipProvider) {       
            this.rate = {};
            if (!this.rate.totalNetCharge) {
                this.rates.forEach((item, index) => {
                    if (item.serviceTypeCode == dialog.context.shipProvider.shipType) {
                        this.rate = item;
                        return;
                    }
                });
            }
            this.selected = this.rate.serviceTypeCode;
            //this.rate.totalNetCharge = dialog.context.shipProvider.shipFee;
            //this.rate.serviceTypeCode = dialog.context.shipProvider.shipType;
            //this.selected = this.rate.serviceTypeCode;
        }
    }
    chooseServiceType(rate) {
        this.rate = rate;
        this.selected = rate.serviceTypeCode;
    }
    clear() {
        this.selected = null;
        this.rate = null;
    }
    close() {
        this.dialog.close(this.rate);
        this.document.body.classList.remove('modal-open');
    }
    beforeDismiss(): boolean {
        return true;
    }
    beforeClose(): boolean {
        return false;
    }
}