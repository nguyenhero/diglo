﻿import {Component, Input, Output, OnChanges, SimpleChanges, EventEmitter} from '@angular/core';
import {DialogRef, ModalComponent, CloseGuard} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
 
import { BillingAddress } from 'src/app/shared/services/api/models/billingAddress';
import { User } from 'src/app/shared/services/api/models/user';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { StateService } from 'src/app/shared/services/api/services/state.service';
import { OrderService } from 'src/app/shared/services/api/services/order.service';
import { UserService } from 'src/app/shared/services/api/services/user.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
import { LoginCheckOutModalContext, LoginCheckOutModal } from '../login-check-out/login-check-out.component';
 
@Component({
    selector: 'billing-order',
    templateUrl: './billing-order.component.html',
    styleUrls: ['./billing-order.component.scss']
})
export class BillingOrderComponent implements OnChanges {
    @Output('billingOrderFinished') billingOrderFinished: EventEmitter<Object> = new EventEmitter<Object>();
    @Input()
    billingAddress: BillingAddress;
    isExistEmail = false;
    currentUser: User;
    billingOrderForm: any;
    states = [];
    paymentType = 0;
    addrID: number = 0;
    years = [];
    months = [];
    showEmail: string = 'show-class';
    showBillingOrder: string = 'hide-class';
    billingAddresses: BillingAddress[];
    requiredCreditCard: boolean = false;
    invalidCreditCard: boolean = false;
    requiredExpirationDateY: boolean = false;
    requiredExpirationDateM: boolean = false;
    requiredCreditCardNumer: boolean = false;
    requiredPaymentType: boolean = false;
    email: string;
    billingCountry: string;
    isChangeBillingAddress: boolean = false;
    constructor(private formBuilder: FormBuilder,
        private modal: Modal,
        private appState: AppState,
        private stateService: StateService,
        private orderService: OrderService,
        private userService: UserService,
        private authenticationService: AuthenticationService) {
        this.showEmailInfo();
        this.showBillingAddressInfo();
        this.createBillingOrderForm();
        this.createYears();
        this.createMonths();
    }
    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
            let change = changes[propName];
            if (propName === 'billingAddress') {
                if (change.currentValue) {
                    this.isChangeBillingAddress = true;
                    let billingAddress = change.currentValue;
                    if (!this.billingAddresses || this.billingAddresses.length == 0) {
                        this.currentUser = this.authenticationService.getCurrentUser();
                        if (this.currentUser && this.currentUser.custNo) {
                            let billingAddressOperation = this.orderService.getBillingAddress(this.currentUser.custNo.toString());
                            billingAddressOperation.subscribe(
                                data => {
                                    if (data.length > 0) {
                                        this.showEmail = 'hide-class';
                                        this.showBillingOrder = 'show-class';
                                        this.setBillingAddress(billingAddress);
                                        this.isChangeBillingAddress = true;
                                    }
                                });
                        }
                    } else if (this.billingAddress) {
                        this.showEmail = 'hide-class';
                        this.showBillingOrder = 'show-class';
                        this.setBillingAddress(billingAddress);
                        this.isChangeBillingAddress = true;
                    }
                }
            }
        }
    }
    setBillingAddress(billingAddress) {
        for (let i = 0; i < this.billingAddresses.length; i++) {
            let item = this.billingAddresses[i];
            item.company = item.company ? item.company : '';
            billingAddress.company = billingAddress.company ? billingAddress.company : '';
            if (item.firstName == billingAddress.firstName &&
                item.lastName == billingAddress.lastName &&
                item.address == billingAddress.address &&
                item.city == billingAddress.city &&
                item.country == billingAddress.country &&
                item.state == billingAddress.state &&
                item.zip == billingAddress.zip &&
                item.phone == billingAddress.phone &&
                item.company == billingAddress.company
            ) {
                this.billingOrderForm.controls['billingAddress'].setValue(item.addrID);
                this.setDataByEmail(this, item);
                break;
            }
        }
    }
    showEmailInfo() {
        this.currentUser = this.authenticationService.getCurrentUser();

        if (this.currentUser) {
            this.email = this.currentUser.custEmail;
            this.showEmail = 'hide-class';
            this.showBillingOrder = 'show-class';
        } else {
            this.showEmail = 'show-class';
            this.showBillingOrder = 'hide-class';
        }
    }
    showBillingAddressInfo() {
        this.currentUser = this.authenticationService.getCurrentUser();
        if (this.currentUser) {
            this.addEmail(this.currentUser.custEmail);
        }
    }
    createBillingOrderForm() {
        this.billingOrderForm = this.formBuilder.group({
            'billingAddress': [''],
            'firstName': ['', Validators.required],
            'lastName': ['', Validators.required],
            'phone': ['', [Validators.required, ValidationService.telephoneValidator]],
            'email': ['', [Validators.required, ValidationService.emailValidator]],
            'paymentType': [''],
            'company': [''],
            'country': ['', Validators.required],
            'address': ['', Validators.required],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zip': ['', Validators.required],
            'creditCardNumer': [''],
            'expirationDateM': [''],
            'expirationDateY': [''],
            'cardVerifyNumber': ['']
        });
    }
    createYears() {
        var year = new Date().getFullYear();
        this.years.push(year);
        for (var i = 1; i < 10; i++) {
            this.years.push(year + i);
        }
    }
    createMonths() {
        var month = 1;
        this.months.push(month);
        for (var i = 1; i < 12; i++) {
            this.months.push(month + i);
        }
    }
    onChangeCountry(value) {
        if (value) {
            this.billingCountry = value;
            let stateOperation = this.stateService.get(value);
            stateOperation.subscribe(result => {
                this.states = result;
            });
            let email = this.billingOrderForm.controls['email'].value;
            this.billingOrderForm.reset();
            this.billingOrderForm.controls['email'].setValue(email)
            this.billingOrderForm.controls['country'].setValue(value);
            this.billingOrderForm.controls['state'].setValue(-1);
            this.billingOrderForm.controls['expirationDateM'].setValue(-1);
            this.billingOrderForm.controls['expirationDateY'].setValue(-1);
            this.billingOrderForm.controls['billingAddress'].setValue(-1);
            this.showBillingAddress();
        }
    }
    addEmail(email) {
        if (email) {
            this.showBillingAddress();
        }

    }
    showBillingAddress() {
        if (this.currentUser && this.currentUser.custNo) {
            let self = this;
            let billingAddressOperation = this.orderService.getBillingAddress(this.currentUser.custNo.toString());
            billingAddressOperation.subscribe(
                data => {                
                    if (data.length > 0) {
                        if (self.billingOrderForm.controls['country'].value) {
                            self.billingAddresses = [];
                            data.forEach(x => {
                                if (x.country == self.billingOrderForm.controls['country'].value) {
                                    self.billingAddresses.push(x);
                                }
                            });
                            if (this.isChangeBillingAddress) {
                                this.setBillingAddress(this.billingAddress);
                            }
                        } else {
                            self.billingAddresses = data;
                            this.billingOrderForm.controls['state'].setValue(-1);
                            this.billingOrderForm.controls['expirationDateM'].setValue(-1);
                            this.billingOrderForm.controls['expirationDateY'].setValue(-1);
                            this.billingOrderForm.controls['billingAddress'].setValue(-1);
                        }
                        if (self.billingAddresses && self.billingAddresses.length > 0) {
                            this.showBillingOrder = 'hide-class';
                        } else {
                            self.showBillingOrder = 'show-class';
                        }
                    } else {
                        self.billingOrderForm.controls['firstName'].setValue(this.currentUser.custFirstName);
                        self.billingOrderForm.controls['lastName'].setValue(this.currentUser.custLastName);
                    }

                });
        }
    }
    chooseCreditCard(value) {
        this.paymentType = value;
        this.requiredPaymentType = false;
    }
    changeBillingAddress(addrID) {
        this.addrID = addrID;
        this.showBillingOrder = 'show-class';
        if (addrID) {
            this.findBillingAddress(addrID);
        } else {
            this.addrID = 0;
            let controls = this.billingOrderForm.controls;
            let email = this.billingOrderForm.controls['email'].value;
            this.billingOrderForm.reset();
            this.billingOrderForm.controls['email'].setValue(email);
            this.billingOrderForm.controls['country'].setValue(this.billingCountry);
            this.billingOrderForm.controls['billingAddress'].setValue('');
            this.billingOrderForm.controls['state'].setValue(-1);
            this.billingOrderForm.controls['expirationDateM'].setValue(-1);
            this.billingOrderForm.controls['expirationDateY'].setValue(-1);
        }
    }
    findBillingAddress(addrID) {
        for (let i = 0; i < this.billingAddresses.length; i++) {
            if (this.billingAddresses[i].addrID.toString() === addrID) {
                let item = this.billingAddresses[i];
                this.setDataByEmail(this, item);
                break;
            }
        }
    }
    validateEmail() {
        let email = this.billingOrderForm.controls['email'].value;
        this.email = email;
        if (email && !this.billingOrderForm.controls['email'].errors) {
            this.currentUser = this.authenticationService.getCurrentUser();
            if (this.currentUser) {
                this.addEmail(this.email);
            } else {
                let operation = this.userService.getCustomerByEmail(email);
                operation.subscribe(
                    data => {
                        if (data == 0) {
                            this.showEmail = 'hide-class';
                            this.showBillingOrder = 'show-class';
                        } else {
                            this.isExistEmail = true;
                            if (this.currentUser) {
                                this.addEmail(this.email);
                            } else {
                                this.showLoginCheckOutDialog();
                            }
                        }

                    });

            }
        } else {
            this.validateAllFormFields(this.billingOrderForm);
        }
    }
    setDataByEmail(self, data) {
        let controls = self.billingOrderForm.controls;
        controls['firstName'].setValue(data.firstName);
        controls['lastName'].setValue(data.lastName);
        controls['company'].setValue(data.company);
        controls['address'].setValue(data.address);
        controls['city'].setValue(data.city);
        controls['zip'].setValue(data.zip);
        controls['phone'].setValue(data.phone);
        controls['email'].setValue(this.email);
        controls['country'].setValue(data.country);
        let stateOperation = self.stateService.get(data.country);
        stateOperation.subscribe(result => {
            self.states = result;
            controls['state'].setValue(data.state);
        });
    }
    validateBillingOrderForm() {
        if (this.currentUser && this.currentUser.custEmail) {
            this.billingOrderForm.controls['email'].setValue(this.currentUser.custEmail);
            this.email = this.currentUser.custEmail;
        }
        if (this.billingOrderForm.valid) {
            this.requiredPaymentType = this.paymentType == 0 ? true : false;
            if (!this.requiredPaymentType) {
                if (this.paymentType == 24) {
                    this.validateCreditCard();
                } else {
                    this.billingOrderForm.value.paymentType = this.paymentType;
                    this.billingOrderForm.value.addrID = this.addrID;
                    this.createBillingOrderFinished();
                }
            }
        } else {
            this.validateAllFormFields(this.billingOrderForm);
            this.requiredPaymentType = this.paymentType == 0 ? true : false;
            if (this.paymentType == 24) {
                this.chooseCreditCard(this.paymentType);
            }
        }
    }
    validateAllFormFields(formGroup: FormGroup) {
        if (formGroup && formGroup.controls) {
            Object.keys(formGroup.controls).forEach(field => {
                const control = formGroup.get(field);
                if (control) {
                    if (control instanceof FormControl) {             //{4}
                        control.markAsTouched({ onlySelf: true });
                    } else if (control instanceof FormGroup) {        //{5}
                        this.validateAllFormFields(control);            //{6}
                    }
                }
            });
        }
    }
    createBillingOrderFinished() {
        this.billingOrderFinished.emit(
            {
                'email': this.email,
                'isExistEmail': this.isExistEmail,
                'billingOrder': this.billingOrderForm.value,
                'paymentType': this.billingOrderForm.value.paymentType
            }
        );
    }
    validateCreditCard() {
        this.requiredCreditCard = this.billingOrderForm.controls['creditCardNumer'].value ? false : true;
        this.requiredExpirationDateY = this.billingOrderForm.controls['expirationDateM'].value ? false : true;
        this.requiredExpirationDateM = this.billingOrderForm.controls['expirationDateY'].value ? false : true;
        this.requiredCreditCardNumer = this.billingOrderForm.controls['cardVerifyNumber'].value ? false : true;
        this.chooseCreditCard(this.paymentType);
        if (!this.requiredCreditCard &&
            !this.requiredExpirationDateY &&
            !this.requiredExpirationDateM &&
            !this.requiredCreditCardNumer) {
            this.billingOrderForm.value.paymentType = this.paymentType;
            this.billingOrderForm.value.addrID = this.addrID;
            this.createBillingOrderFinished();
        }
    }
    showLoginCheckOutDialog() {
        let email = this.billingOrderForm.controls['email'].value;
        let context = new LoginCheckOutModalContext(email);
        context.dialogClass = ' login-check-out-modal';
        context.isBlocking = true;
        let dialog = this.modal.open(LoginCheckOutModal, { context: context });
        dialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                if (result) {
                    if (result == 1) {
                        this.billingOrderForm.controls['email'].setValue('');
                    } else if (result == 2) {
                        this.showBillingOrder = 'show-class';
                        this.showEmail = 'hide-class';
                    } else {
                        this.currentUser = result;
                        this.email = result.custEmail;
                        this.addEmail(result.custEmail);
                        this.showBillingOrder = 'show-class';
                        this.showEmail = 'hide-class';
                    }
                }
            });
        });
    }
    setDataByUser(self, data) {
        let controls = self.billingOrderForm.controls;
        controls['firstName'].setValue(data.custFirstName);
        controls['lastName'].setValue(data.custLastName);
        controls['email'].setValue(data.custEmail);
    }
}
