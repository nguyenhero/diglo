import {NgModule, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {PrivacyComponent} from './privacy.component';
// import { OffSaleComponent } from './../OffSale/offsale.component';
@NgModule({
  declarations: 
  [PrivacyComponent
    // OffSaleComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: PrivacyComponent, pathMatch: 'full' }
    ])
  ]
})
export class PrivacyModule {}
