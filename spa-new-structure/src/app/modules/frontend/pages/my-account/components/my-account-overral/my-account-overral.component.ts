﻿import { Component} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'; 
import { Modal } from 'angular2-modal/plugins/bootstrap'; 
import { Router } from '@angular/router'; 
import { User } from 'src/app/shared/services/api/models/user';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { UserService } from 'src/app/shared/services/api/services/user.service';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
import { ChangePasswordModalComponent } from '../change-password-modal/change-password-modal.component';
import { MyAddressBookModalComponent } from '../my-address-book-modal/my-address-book-modal.component';
import { MyOrdersModalComponent } from '../my-orders-modal/my-orders-modal.component';
import { MyWishListModalComponent } from '../my-wish-list-modal/my-wish-list-modal.component';
import { MyAccountModalComponent } from '../my-account-modal/my-account-modal.component';
import { MyRecentsOrdersModalComponent } from '../my-recents-orders-modal/my-recents-orders-modal.component';
@Component({
    selector: 'my-account-overral',
    templateUrl: './my-account-overral.component.html',
    styleUrls: ['./my-account-overral.component.scss']
})
export class MyAccountOverralComponent {
    currentUser: User;
    shortcurrentUser: string;
    isLogin: boolean = false;
    userForm: any;
    isExistEmail = false;
    myAccount = true;
    myChangePassword = false;
    myAddressBook = false;
    myWishList = false;
    myOrders = false;
    myCurrentOrders = false;
    monthCount = '';
    constructor(public modal: Modal,
        private router: Router,
        private authenticationService: AuthenticationService,
        private formBuilder: FormBuilder,
        private userService: UserService,
        public appState: AppState) {
        appState.publish(this.router.url);
        this.userForm = this.formBuilder.group({
            'firstName': ['', Validators.required],
            'lastName': ['', Validators.required],
            'email': ['', [Validators.required, ValidationService.emailValidator]],
        });
        this.userForm.valueChanges.subscribe((value) => {
            this.isExistEmail = false;
        });
       this.setDataByUser();
    }
    changeNameFinished() {
        this.setDataByUser();
    }
    changeLink(name) {
        this.myChangePassword = false;
        this.myAccount = false;
        this.myAddressBook = false;
        this.myWishList = false;
        this.myOrders = false;
        this.myCurrentOrders = false;
        this.monthCount = '';
        switch (name) {
            case 'ACC':
                this.myAccount = true;
                break;
            case 'PW':
                this.myChangePassword = true;
                break;
            case 'AB':
                this.myAddressBook = true;
                break;
            case 'WL':
                this.myWishList = true;
                break;
            case 'O':
                this.myOrders = true;
                this.monthCount = '6';
                break;
            case 'COS':
                this.myCurrentOrders = true;
                this.monthCount = '';
                break;
        }
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
    setDataByUser() {
        this.currentUser = this.authenticationService.getCurrentUser();
        if (this.currentUser) {
            this.shortcurrentUser =
                this.currentUser.custFirstName.charAt(0).toUpperCase() +
                this.currentUser.custLastName.charAt(0).toUpperCase();
            let controls = this.userForm.controls;
            controls['firstName'].setValue(this.currentUser.custFirstName);
            controls['lastName'].setValue(this.currentUser.custLastName);
            controls['email'].setValue(this.currentUser.custEmail);
        }
    }
    openChangePasswordModal() {
        let dialog = this.modal.open(ChangePasswordModalComponent);
    }
    openMyAddressBookModal() {
        let dialog = this.modal.open(MyAddressBookModalComponent);
    }
    openMyOrdersModal() {
        let dialog = this.modal.open(MyOrdersModalComponent);
    }
    openMyRecentsOrdersModal() {
        let dialog = this.modal.open(MyRecentsOrdersModalComponent);
    }
    openMyWishListModal() {
        let dialog = this.modal.open(MyWishListModalComponent);
    }
    openMyAccountModal() {
        let dialog = this.modal.open(MyAccountModalComponent);
    }
}
