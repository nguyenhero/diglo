﻿import { Component, Output, EventEmitter, ViewEncapsulation  } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal'; 
import { Observable } from 'rxjs'; 
import { User } from 'src/app/shared/services/api/models/user';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { UserService } from 'src/app/shared/services/api/services/user.service';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
export class MyAccountModalContext extends BSModalContext {
    constructor(public link: string = '') {
        super();
    }
}
@Component({
    selector: 'my-account-modal',
    templateUrl: './my-account-modal.component.html',
    styleUrls: ['./my-account-modal.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class MyAccountModalComponent implements CloseGuard, ModalComponent<MyAccountModalContext> {
    currentUser: User;
    shortcurrentUser: string;
    isLogin: boolean = false;
    userForm: any;
    isExistEmail = false;
    context: MyAccountModalContext;
   @Output('changeNameFinished') changeNameFinished: EventEmitter<Object> = new EventEmitter<Object>();
    constructor(
        public dialog: DialogRef<MyAccountModalContext>,
        public modal: Modal,
        private authenticationService: AuthenticationService,
        private formBuilder: FormBuilder,
        private userService: UserService,
        public appState: AppState) {
        this.context = dialog.context;
        dialog.setCloseGuard(this);
        this.userForm = this.formBuilder.group({
            'firstName': ['', Validators.required],
            'lastName': ['', Validators.required],
            'email': ['', [Validators.required, ValidationService.emailValidator]],

        });
        this.userForm.valueChanges.subscribe((value) => {
            this.isExistEmail = false;
        });
        this.setDataByUser();
    }
    openLogin() {
        this.appState.publish('login');
    }
    saveUser() {
        if (this.userForm.dirty && this.userForm.valid) {
            let data = {
                'custNo': this.currentUser.custNo,
                'firstName': this.userForm.value.firstName,
                'lastName': this.userForm.value.lastName,
                'email': this.userForm.value.email,
            };
            let self = this;
            let registerOperation: Observable<number>;
            registerOperation = this.userService.updateAccount(this.currentUser.custNo, data);
            registerOperation.subscribe(
                result => {
                    let msg = result == 1
                        ? `<span class='successfully-message'> You have changed successfully account information.Thank you.</span>`
                        : `<span class='successfully-message'> You have changed unSuccessfully account information.Thank you.</span>`
                    if (result == 1) {
                        this.updateUserToAuthentication();
                        this.changeNameFinished.emit();
                    }
                    this.modal.alert()
                        .size('sm')
                        .showClose(true)
                        .isBlocking(true)
                        .body(msg)
                        .open();
                },
                err => { console.log(err); });
        } else {
            this.validateAllFormFields(this.userForm);
        }
    }
    updateUserToAuthentication() {
        this.currentUser.custFirstName = this.userForm.value.firstName;
        this.currentUser.custLastName = this.userForm.value.lastName;
        this.currentUser.custEmail = this.userForm.value.email;
        this.authenticationService.updateCurrentUser(this.currentUser);
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
    setDataByUser() {
        this.currentUser = this.authenticationService.getCurrentUser();
        if (this.currentUser) {
            this.shortcurrentUser =
                this.currentUser.custFirstName.charAt(0).toUpperCase() +
                this.currentUser.custLastName.charAt(0).toUpperCase();
            let controls = this.userForm.controls;
            controls['firstName'].setValue(this.currentUser.custFirstName);
            controls['lastName'].setValue(this.currentUser.custLastName);
            controls['email'].setValue(this.currentUser.custEmail);
        }
    }
    close() {
        this.dialog.close();
    }
    beforeDismiss(): boolean {
        return true;
    }
    beforeClose(): boolean {
        return false;
    }
}
