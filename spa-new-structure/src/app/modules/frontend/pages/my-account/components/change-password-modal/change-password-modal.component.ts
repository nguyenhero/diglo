﻿import { Component, ViewEncapsulation, Inject} from '@angular/core';
import { Router} from '@angular/router';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { Observable} from 'rxjs/Observable';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
import { UserService } from 'src/app/shared/services/api/services/user.service';
import { User }           from 'src/app/shared/services/api/models/user';
import { DOCUMENT } from '@angular/platform-browser';    //fixed popup scroll
import { PasswordValidation } from 'src/app/shared/directives/password-validation';
export class  ChangePasswordModalContext extends BSModalContext {
    constructor(public link: string = '') {
        super();
    }
}
@Component({
    selector: 'my-password-modal',
    templateUrl: './change-password-modal.component.html',
    styleUrls: ['./change-password-modal.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class ChangePasswordModalComponent implements CloseGuard, ModalComponent<ChangePasswordModalContext> {
    currentUser: User;
    myPassForm: any;
    context: ChangePasswordModalContext;
    constructor(public dialog: DialogRef<ChangePasswordModalContext>,
        public modal: Modal,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private router: Router,
        @Inject(DOCUMENT) private document,
        private authenticationService: AuthenticationService) {
        this.context = dialog.context;
        dialog.setCloseGuard(this);
        this.currentUser = this.authenticationService.getCurrentUser();
        this.myPassForm = this.formBuilder.group({
            'password': ['', [Validators.required, Validators.minLength(6), ValidationService.passwordValidator]],
            'confirmPassword': ['', Validators.required]
        },
            {
                validator: PasswordValidation.MatchPassword // your validation method
            });
    }
    save() {
        if (this.myPassForm.dirty && this.myPassForm.valid) {
            this.currentUser = this.authenticationService.getCurrentUser();
            let data = {
                'customerNo': this.currentUser.custNo,
                'password': this.myPassForm.value.password,
                'confirmPassword': this.myPassForm.value.confirmPassword
            };
            let changePasswordOperation: Observable<number>;
            changePasswordOperation = this.userService.resetPassword(data);
            changePasswordOperation.subscribe(
                register => {
                    let msg = '';
                    if (register == 0) {
                        this.router.navigate(['/home']);
                        msg = `<span class='successfully- message'>You have successfully reset your password please login with your new password. Thank you.</span>`;
                        this.updateUserToAuthentication();

                    } else if (register == 100) {
                        msg = `<span class='successfully- message'>Customer Does Not Exist.</span>`;
                    }
                    else msg = 'You have not successfully reset your password';
                    this.modal.alert()
                        .size('sm')
                        .showClose(true)
                        .isBlocking(true)
                        .body(msg)
                        .open().then(x=>{
                            this.document.body.classList.remove('modal-open');  //Added to fixed popup scroll
                        });
                },
                err => { console.log(err); });
        } else {
            this.validateAllFormFields(this.myPassForm);
        }
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
    updateUserToAuthentication() {
        this.currentUser.custPassword = this.myPassForm.value.password;
        this.authenticationService.updateUserPassword(this.currentUser);
        this.close();
    }
    close() {
        this.dialog.close();
        this.document.body.classList.remove('modal-open'); //fixed popup scroll
    }
    beforeDismiss(): boolean {
        return true;
    }
    beforeClose(): boolean {
        return false;
    }
}
