﻿import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { Router} from '@angular/router';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { Observable} from 'rxjs/Observable';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
import { UserService } from 'src/app/shared/services/api/services/user.service';
import { User }           from 'src/app/shared/services/api/models/user';
import { DOCUMENT } from '@angular/platform-browser';    //fixed popup scroll
import { PasswordValidation } from 'src/app/shared/directives/password-validation';
@Component({
    selector: 'my-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent {
    currentUser: User;
    myPassForm: any;
    constructor(public modal: Modal,
        private formBuilder: FormBuilder,
        @Inject(DOCUMENT) private document,
        private userService: UserService,
        private router: Router,
        private authenticationService: AuthenticationService) {
        this.currentUser = this.authenticationService.getCurrentUser();
        this.myPassForm = this.formBuilder.group({
            'password': ['', [Validators.required, Validators.minLength(6), ValidationService.passwordValidator]],
            'confirmPassword': ['', Validators.required]
        },
            {
                validator: PasswordValidation.MatchPassword // your validation method
            });
    }
    save() {
        if (this.myPassForm.dirty && this.myPassForm.valid) {
            this.currentUser = this.authenticationService.getCurrentUser();
            let data = {
                'customerNo': this.currentUser.custNo,
                'password': this.myPassForm.value.password,
                'confirmPassword': this.myPassForm.value.confirmPassword
            };
            let changePasswordOperation: Observable<number>;
            changePasswordOperation = this.userService.resetPassword(data);
            changePasswordOperation.subscribe(
                register => {
                    let msg = '';
                    if (register == 0) {
                        this.router.navigate(['/home']);
                        msg = `<span class='successfully- message'>You have successfully reset your password please login with your new password. Thank you.</span>`;
                        this.updateUserToAuthentication();

                    } else if (register == 100) {
                        msg = `<span class='successfully- message'>Customer Does Not Exist.</span>`;
                    }
                    else msg = 'You have not successfully reset your password';
                    this.modal.alert()
                        .size('sm')
                        .showClose(true)
                        .isBlocking(true)
                        .body(msg)
                        .open().then(x=>{
                            this.document.body.classList.remove('modal-open');
                        });
                },
                err => { console.log(err); });
        } else {
            this.validateAllFormFields(this.myPassForm);
        }
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
    updateUserToAuthentication() {
        this.currentUser.custPassword = this.myPassForm.value.password;
        this.authenticationService.updateUserPassword(this.currentUser);
    }
}
