﻿import { Component, Inject, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DOCUMENT } from '@angular/platform-browser';    //fixed popup scroll
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { Router, ActivatedRoute} from '@angular/router';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { WishListService } from 'src/app/shared/services/api/services/wishList.service';
import { User }           from 'src/app/shared/services/api/models/user';
export class ShareWishlistContext extends BSModalContext {
    constructor(public wishLists: any = []) {
        super();
    }
}
@Component({
    selector: 'share-wish-list',
    templateUrl: './share-wish-list.component.html',
    styleUrls: ['./share-wish-list.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class ShareWishlistModal implements CloseGuard, ModalComponent<ShareWishlistContext> {
    context: ShareWishlistContext;
    shareWishlist: any;
    wishLists: any;
    requiredShareWishlist: boolean = false;
    constructor(public dialog: DialogRef<ShareWishlistContext>,
        private formBuilder: FormBuilder,
        @Inject(DOCUMENT) private document,
        public modal: Modal, private router: Router,
        public appState: AppState, private _routeParams: ActivatedRoute,
        public authenticationService: AuthenticationService,
        public wishListService: WishListService
    ) {
        this.wishLists = dialog.context.wishLists;
        console.log(this.wishLists);
        dialog.setCloseGuard(this);
        this.shareWishlist = this.formBuilder.group({
            'emails': ['', []],
            'mesage': ['', []]
        });
    }
    shareWishList() {
        if (this.shareWishlist.controls['emails'].value) {
            let currentUser = this.authenticationService.getCurrentUser();
            this.requiredShareWishlist = false;
            let data = {
                'WishList': this.wishLists,
                'FromEmails': currentUser.custEmail,
                'Emails': this.shareWishlist.controls['emails'].value,
                'Messages': this.shareWishlist.controls['mesage'].value
            };
            let opretation = this.wishListService.sendshareWishList(data);
            opretation.subscribe(result => {
                this.dialog.close();
                this.document.body.classList.remove('modal-open'); //scroll fix
            });
        } else {
            this.requiredShareWishlist = true;
            return;
        }
    }
    continueShopping() {
        this.dialog.close();
        this.router.navigate(['all-products']);
    }
    close() {
        this.dialog.close();
        this.document.body.classList.remove('modal-open'); //scroll fix
    }
    beforeDismiss(): boolean {
        return true;
    }
    beforeClose(): boolean {
        return false;
    }
}
