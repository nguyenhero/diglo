 
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 
import { MyAccountOverralComponent } from './components/my-account-overral/my-account-overral.component';

const routes: Routes = [
    { path: '', component: MyAccountOverralComponent, pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MyAccountRoutingModule {}
