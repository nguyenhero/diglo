import { NgModule, Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MyAccountOverralComponent } from './components/my-account-overral/my-account-overral.component';
import { ShareWishlistModal } from './components/share-wish-list/share-wish-list.component';
import { MyOrdersComponent } from './components/my-orders/my-orders.component';
import { MyAccountComponent } from './my-account.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { MyAddressBookComponent } from './components/my-address-book/my-address-book.component';
import { MyWishListComponent } from './components/my-wish-list/my-wish-list.component';
import { MyAccountRoutingModule } from './my-account-routing.module';
import { MyRecentsOrdersComponent } from './components/my-recents-order/my-recents-orders.component';
@NgModule({
  declarations: [
    MyAccountOverralComponent,
    ShareWishlistModal,
    MyOrdersComponent,
    MyAccountComponent,
    ChangePasswordComponent,
    MyRecentsOrdersComponent,
    MyAddressBookComponent,
    MyWishListComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MyAccountRoutingModule
  ],
})
export class MyAccountModule { }
