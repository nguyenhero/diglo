﻿import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { Product } from 'src/app/shared/services/api/models/product';
import { ProductService } from 'src/app/shared/services/api/services/product.service';
@Component({
    selector: 'productRelatedMobi',
    templateUrl: './productRelatedMobi.component.html',
    styleUrls: ['./productRelatedMobi.component.scss']
})
export class ProductRelatedMobiComponent {
    productSKU: string;
    productRelateds: Product[] = [];
    public bufferSize: number = 4;
    constructor(private router: Router,
        private _routeParams: ActivatedRoute,
        private productService: ProductService) {
        this._routeParams.queryParams.subscribe(params => {
            this.productSKU = params['productSKU'];
            let operatetion = this.productService.getProductRelated(this.productSKU);
            operatetion.subscribe(result => {
                if (result) {
                    this.productRelateds = result;
                }
            }
            );
        });
    }
    chunks(size) {
        let number = Math.ceil(this.productRelateds.length / size);
        return this.createRange(number);
    };
    createRange(number) {
        var items: number[] = [];
        for (var i = 1; i <= number; i++) {
            items.push(i);
        }
        return items;
    }
}