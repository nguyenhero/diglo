 
 

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 
import { PlaceOrderSuccessfullyComponent } from './place-order-successfully.component';

const routes: Routes = [
    { path: '', component: PlaceOrderSuccessfullyComponent, pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PlaceOrderCuccessfullyRoutingModule {}
