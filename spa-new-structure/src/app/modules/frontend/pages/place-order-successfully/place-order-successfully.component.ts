﻿import { Component, Inject } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { ShipProvider } from 'src/app/shared/services/api/models/shipProvider';
import { Product } from 'src/app/shared/services/api/models/product';
import { OrderHead } from 'src/app/shared/services/api/models/orderHead';
import { OrderDetail } from 'src/app/shared/services/api/models/orderDetail';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { OrderLogicService } from 'src/app/shared/services/api/services/orderLogic.service';
import { OrderService } from 'src/app/shared/services/api/services/order.service';
import { StateService } from 'src/app/shared/services/api/services/state.service';
@Component({
    selector: 'place-order-successfully',
    templateUrl: './place-order-successfully.component.html',
    styleUrls: ['./place-order-successfully.component.scss'],
})
export class PlaceOrderSuccessfullyComponent {
    orderNumber: string = '';
    token: string;
    payerId: string;
    shipProvider: ShipProvider;
    products: Product[];
    totals: number = 0;
    sub_totals: number = 0;
    promoCode: string = '';
    promoCodeValid: number = 0;
    errorMsg: string = '';
    showsuccessfullyOrder: number = -1;
    order: OrderHead;
    orderDetails: OrderDetail[];
    states = [];
    shippingStates = [];
    billingStates = [];
    subTotal: number;
    constructor(private router: Router, private _routeParams: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private orderLogicService: OrderLogicService,
        private orderService: OrderService,
        private stateService: StateService
    ) {
        let affirm_checkout_token = this.getParameterByName('checkout_token');
        //if (this.token) { this.paypal(); }
        if (affirm_checkout_token) { this.affirm(affirm_checkout_token); }
        this._routeParams.params.subscribe(params => {
            this.orderNumber = params['orderNumber'] ? params['orderNumber'] : this.getParameterByName('orderNumber');
            this.showsuccessfullyOrder = this.orderNumber ? 1 : 0;
            this.orderLogicService.removeAll();
            this.getOrder();
        });
    }
    getOrder() {
        let currentUser = this.authenticationService.getCurrentUser();
        let cusNo = currentUser && currentUser.custNo ? currentUser.custNo.toString() : "-1";
        let operation = this.orderService.getOrder(cusNo, "-1", this.orderNumber);
        operation.subscribe(result => {
            if (result && result.length > 0) {
                this.order = result[0];
                this.loadDetailsOrder();
            }
        });
    }
    loadDetailsOrder() {
        let shippingStateOperation = this.stateService.get(this.order.shippingCountry);
        shippingStateOperation.subscribe(result => {
            this.shippingStates = result;});
        let billingStateOperation = this.stateService.get(this.order.billingCountry);
        billingStateOperation.subscribe(result => {
            this.billingStates = result;});
        let orderOperation = this.orderService.getOrderDetails(this.order.orderID.toString());
        orderOperation.subscribe(result => {
            this.orderDetails = result;
            this.getProductsPriceTotals();});
    }
    getProductsPriceTotals() {
        let total = 0;
        if (this.orderDetails) {
            for (let i = 0; i < this.orderDetails.length; i++) {
                if (this.orderDetails[i].cartPrice >= 0) {
                    total += this.orderDetails[i].cartPrice * this.orderDetails[i].quantity;
                } else {
                    total += this.orderDetails[i].webPrice * this.orderDetails[i].quantity;
                }
            }
        }
        this.subTotal = total;
    }
    getCountry(country) {
        switch (country) {
            case 'US':
                return 'United States';
            case 'CA':
                return 'Canada';
        }
    }
    getShippingState(state) {
        for (let i = 0; i < this.shippingStates.length; i++) {
            if (this.shippingStates[i].stateID.toString() == state) {
                return this.shippingStates[i].stateDesc;
            }
        }
    }
    getBillingState(state) {
        for (let i = 0; i < this.billingStates.length; i++) {
            if (this.billingStates[i].stateID.toString() == state) {
                return this.billingStates[i].stateDesc;
            }
        }
    }
    getShipFee(order) {
        return order.shipFee;
    }
    continueShopping() {
        this.router.navigate(['allproducts']);
    }
    affirm(affirm_checkout_token) {
        let order = this.orderService.getPayPalOrder(this.authenticationService.getCurrentUser().custNo);
        if (order) {
            order.token = affirm_checkout_token;
            let operation = this.orderService.addOrder(order);
            operation.subscribe(
                response => {
                    if (response.result > 0) {
                        this.showsuccessfullyOrder = 1;
                        this.orderLogicService.removeAll();
                        this.orderService.removePayPalOrder(this.authenticationService.getCurrentUser().custNo);
                        let order = { orderNumber: response.result };
                        this.router.navigate(['place-order-successfully', order]);
                    } else {
                        this.showsuccessfullyOrder = 0;
                        this.errorMsg = response.result == 0
                            ? `You have not successfully order. Thank you.`
                            : response.errorMsg;
                    }
                    this.orderService.removePayPalOrder(affirm_checkout_token);
                });
        }
    }
    getParameterByName(name) {
        let url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    print() {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }
}