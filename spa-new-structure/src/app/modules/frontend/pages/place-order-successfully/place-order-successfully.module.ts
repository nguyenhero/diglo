import {NgModule, Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlaceOrderSuccessfullyComponent} from './place-order-successfully.component';
import { PlaceOrderCuccessfullyRoutingModule } from './place-order-successfully-routing.module';
@NgModule({
  declarations: [
    PlaceOrderSuccessfullyComponent
  ],
  imports: [
    CommonModule,
    PlaceOrderCuccessfullyRoutingModule,
  ],
  entryComponents: [
  ]
})
export class PlaceOrderCuccessfullyModule {}
