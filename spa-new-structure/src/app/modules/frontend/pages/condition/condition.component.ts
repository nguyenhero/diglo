﻿import { Component} from '@angular/core';
import {Router} from '@angular/router';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
@Component({
    selector: 'condition',
    templateUrl: './condition.component.html',
    styleUrls: ['./condition.component.scss']
})

export class ConditionComponent {
    constructor(private router: Router, public appState: AppState) {
        window.scrollTo(0, 0);
        appState.publish(this.router.url);
    }
}