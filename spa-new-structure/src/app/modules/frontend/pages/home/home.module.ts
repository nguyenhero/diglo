import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module'; 
import { BannerComponent } from './components/banner/banner.component';
import { CategoryMenuComponent } from './components/category/category.component';
import { FreeCatalogMenuComponent } from './components/free-catalog/free-catalog.component';
import { GuaranteedMenuComponent } from './components/guaranteed/guaranteed.component';
import { TestimonialsMenuComponent } from './components/testimonials/testimonials.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home.component';

@NgModule({
  declarations: [HomeComponent, BannerComponent,CategoryMenuComponent,FreeCatalogMenuComponent,GuaranteedMenuComponent,TestimonialsMenuComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }