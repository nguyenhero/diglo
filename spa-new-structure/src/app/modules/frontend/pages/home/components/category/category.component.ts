import {Component} from '@angular/core';
@Component({
    selector: 'category',
    templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryMenuComponent {
  slide: number = 0;
prev(count) {
    if (this.slide > 0 && this.slide < count ) {
        this.slide--;
    } else {
        this.slide = 0;
    }
}
next(count) {
    if (this.slide < count - 1) {
        this.slide++;
    } else {
        this.slide = 0;
    }

}
}