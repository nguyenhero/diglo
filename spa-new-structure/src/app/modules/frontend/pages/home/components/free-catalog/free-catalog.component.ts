import {Component, Inject,ViewContainerRef, ViewEncapsulation} from '@angular/core';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {FreeCatalogRequestModal, FreeCatalogRequestModalContext} from './../free-catalog/free-catalog-request/free-catalog-request.component';
import {AppState} from 'src/app/shared/services/api/services/appState.service';
import {AuthenticationService} from 'src/app/shared/services/api/services/authentication.service';
import {DOCUMENT} from '@angular/platform-browser';
import { LoginModalContext, LoginModal } from 'src/app/modules/frontend/components/login/login.component';
@Component({
    selector: 'free-catalog',
    templateUrl: './free-catalog.component.html',
    styleUrls: ['./free-catalog.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class FreeCatalogMenuComponent {
    currentUser: any;
    isLogin: boolean = false;
    constructor(vcRef: ViewContainerRef,
        @Inject(DOCUMENT) private document,
        private modal: Modal,
        private appState: AppState,
      private authenticationService: AuthenticationService
    ) {}
    show_request_login() {
        let context = new LoginModalContext('free-catalog-request');
        context.dialogClass = 'login-modal';
        context.isBlocking = true; // now its blocking.        
        let dialog = this.modal.open(LoginModal, { context: context });
        dialog.then((resultPromise) => {
           return resultPromise.result.then((result) => {
               if (result) {                    
                   this.isLogin = true;
                   this.appState.publish('login');
                   this.validate_catalog_dialog(result);
               }
           });
        });
    }
    show_catalog_request() {
        let currentUser = this.authenticationService.getCurrentUser();
        if (currentUser) {
           this.validate_catalog_dialog(currentUser);
        } else {
           this.show_request_login();
        }
    }
    validate_catalog_dialog(currentUser) {
        if (currentUser.custCatStatus == 1) {
           this.show_message_requested_catalog();
        } else {
           let context = new FreeCatalogRequestModalContext(currentUser);
           context.isBlocking = true; // now its blocking.
           this.modal.open(FreeCatalogRequestModal, { context: context });
        }
    }
    show_message_requested_catalog() {
        this.modal.alert()
           .size('sm')
           .isBlocking(true)
           .showClose(true)
           .body(`<span class='successfully-message'> You already requested a catalog. For multiple catalog requests, email <a href="mailto:"><strong>info@harriscomm.com</strong></a>.<br /><br /> Thank you.</span>`)
           .open().then(x=>{
            this.document.body.classList.remove('modal-open');
        });;
    }
}
