﻿import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DOCUMENT } from '@angular/platform-browser';    //fixed popup scroll
import { User } from 'src/app/shared/services/api/models/user';
import { CustomerType } from 'src/app/shared/services/api/models/customerType';
import { CustomerCatService } from 'src/app/shared/services/api/services/customer.cat.service';
import { StateService } from 'src/app/shared/services/api/services/state.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
export class FreeCatalogRequestModalContext extends BSModalContext {
    constructor(public customer: User) {super();}
}
@Component({
    selector: 'free-catalog-request',
    templateUrl: './free-catalog-request.component.html',
    styleUrls: ['./free-catalog-request.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class FreeCatalogRequestModal implements CloseGuard, ModalComponent<FreeCatalogRequestModalContext> {
    context: FreeCatalogRequestModalContext;
    isExistEmail = false;
    customerCatForm: any;
    type: string = '';
    types: CustomerType[] = [];
    is_choose_customer_type: boolean = false;
    freeCatalogRequestClass: string = 'hide-class';
    states = [];
    constructor(public modal: Modal, public dialog: DialogRef<FreeCatalogRequestModalContext>, private formBuilder: FormBuilder,
        @Inject(DOCUMENT) private document, private customerCatService: CustomerCatService, private stateService: StateService,
        private authenticationService: AuthenticationService) {
        this.context = dialog.context;
        dialog.setCloseGuard(this);
        this.customerCatForm = this.formBuilder.group({
            'firstName': ['', Validators.required],
            'lastName': ['', Validators.required],
            'email': ['', [Validators.required, ValidationService.emailValidator]],
            'company': [''],
            'country': ['', Validators.required],
            'address': ['', Validators.required],
            'city': ['', Validators.required],
            'state': ['', Validators.required],
            'zipcode': ['', Validators.required]
        });
        this.customerCatForm.valueChanges.subscribe((value) => {
            this.isExistEmail = false;
        });
    }
    onChangeCountry(value) {
        if (value) {
            let stateOperation = this.stateService.get(value);
            stateOperation.subscribe(result => {
                this.states = result;
            });
        }
    }
    onChangeCustomerType(value) {
        if (value) {
            this.type = value;
            this.is_choose_customer_type = true;
            this.freeCatalogRequestClass = 'show-class';
        }
    }
    save() {
        if (this.customerCatForm.dirty && this.customerCatForm.valid) {
            let freeCatalogRequestOperation: Observable<number>;
            let request = this.customerCatForm.value;
            request.type = this.type;
            request.custNo = this.context.customer.custNo;
            freeCatalogRequestOperation = this.customerCatService.save(request);
            freeCatalogRequestOperation.subscribe(result => {
                if (result == 0) {
                    this.show_message_successfully();
                } else {
                    alert('Error!');
                }
            });
        } else {
            this.validateAllFormFields(this.customerCatForm);
        }
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
    show_message_successfully() {
        this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .body(`<span class='successfully-message'>You have successfully registered for a free catalog. <br /> <br />Thank you.</span>`)
            .open().then((resultPromise) => {
                let currentUser = this.authenticationService.getCurrentUser();
                if (currentUser) {
                    currentUser.custCatStatus = 1;
                    localStorage.setItem('currentUser', JSON.stringify(currentUser));
                }
                this.dialog.close();
                this.document.body.classList.remove('modal-open'); 
            });
    }
    validate() {
    }
    close() {
        this.dialog.close();
        this.document.body.classList.remove('modal-open'); 
    }
    beforeDismiss(): boolean {
        return true;
    }
}
