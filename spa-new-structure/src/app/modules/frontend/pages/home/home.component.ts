import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
//import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
//import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';

//import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
//import { AppState } from './../shared/services/appState.service';
//import { User }           from 'src/app/shared/services/api/models/user';
//import { ChangePasswordModalContext, ChangePasswordModal } from './../account//change-password//change-password.component';
//import { ForgotPasswordModalContext, ForgotPasswordModal} from './../account/forgot-password/forgot-password.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
    //providers: [Modal]
})
export class HomeComponent implements OnInit {
    //currentUser: User;
    //users: User[] = [];
    customerNo: string;
  requestAt: string;

  constructor() { }

    //constructor(public modal: Modal, private router: Router, private route: ActivatedRoute, public appState: AppState,
    //    private authenticationService: AuthenticationService) {
    //    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //    this.customerNo = this.route.snapshot.params['id'];
    //    this.requestAt = this.route.snapshot.params['requestAt'];

    //    if (this.customerNo && this.requestAt && this.currentUser) {
    //        this.authenticationService.logout();
    //    }
    //    appState.publish(this.router.url);
    //}

    ngOnInit() {
        //if (this.customerNo && this.requestAt) {
        //    this.validateChangePassword();
        //}
    }

    validateChangePassword() {

        var year =  parseInt(this.requestAt.substring(0, 4));
        var month = parseInt(this.requestAt.substring(4, 6));
        var day = parseInt(this.requestAt.substring(6, 8));
        var hour = parseInt(this.requestAt.substring(8, 10));
        var minute = parseInt(this.requestAt.substring(10, 12));
        var second = parseInt(this.requestAt.substring(12, 14));
        var expiresDate = new Date(year, month - 1, day, hour, minute, second);

        if ((new Date()).getTime() <= expiresDate.getTime()) {
            this.openChangePassword()
        } else {
            this.openForgotPassword()
        }
    }

    openChangePassword() {
        //let self = this;
        //let context = new ChangePasswordModalContext(this.customerNo);
        //context.isBlocking = true; // now its blocking.        
        //context.dialogClass = 'change-password-modal';

        //let changePasswordDialog = this.modal.open(ChangePasswordModal, { context: context });
        //changePasswordDialog.then((resultPromise) => {
        //    return resultPromise.result.then((result) => {
        //        self.router.navigate(['home']);
        //    });
        //});
    }

    openForgotPassword() {
        //let msg = 'Your Password reset link has expired.';
        //let context = new ForgotPasswordModalContext(msg);
        //context.isBlocking = true; // now its blocking.        
        //context.dialogClass = 'forgot-password-modal';
        //let self = this;
        //let forgotPasswordDialog = this.modal.open(ForgotPasswordModal, { context: context });
        //forgotPasswordDialog.then((resultPromise) => {
        //    return resultPromise.result.then((result) => {
        //        self.router.navigate(['home']);
        //    });
        //});
    }
}
