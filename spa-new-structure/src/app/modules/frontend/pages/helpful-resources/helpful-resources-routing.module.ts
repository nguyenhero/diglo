

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HelpfulResourcesComponent } from './helpful-resources.component';

const routes: Routes = [
  { path: '', component: HelpfulResourcesComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpfulResourcesRoutingModule { }
