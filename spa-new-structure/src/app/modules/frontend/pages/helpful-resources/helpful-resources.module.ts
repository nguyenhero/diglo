import {NgModule, Component} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HelpfulResourcesComponent} from './helpful-resources.component';
import { HelpfulResourcesRoutingModule } from './helpful-resources-routing.module';
@NgModule({
  declarations: [HelpfulResourcesComponent],
  imports: [
    HelpfulResourcesRoutingModule
  ]
})
export class HelpfulResourcesModule {}
