import {Component, Inject} from '@angular/core';
import {Modal} from 'angular2-modal/plugins/bootstrap';
import {Router} from '@angular/router';
import {AppState } from 'src/app/shared/services/api/services/appState.service';
import {DOCUMENT } from '@angular/platform-browser';    //fixed popup scroll
@Component({
    selector: 'helpful-resources',
    templateUrl: './helpful-resources.component.html',
    styleUrls: ['./helpful-resources.component.scss']
})
export class HelpfulResourcesComponent {
    constructor(private router: Router,  @Inject(DOCUMENT) private document, public appState: AppState, public modal: Modal) {
        window.scrollTo(0, 0);
        appState.publish(this.router.url);
    }
    what_is_ada() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                    <h1>What is ADA?</h1>
                </div>
                <div class="std">
                    <p>The Americans with Disabilities Act (ADA) of 1990 requires that public and private entities make reasonable accommodations to ensure their facilities are accessible to people with disabilities, including the deaf and hard of hearing. To view the full law, go to: <a href="http://www.ada.gov" target="_blank">www.ada.gov</a></p>
                <h2>What is available to help comply?</h2>
                <p>There are many different types of products that can help make workplaces, public venues or businesses ADA compliant. Since each state has different ADA laws, we recommend contacting your state's ADA office for specifics on what you need.</p>
                <p>All of the products we carry can be used to fulfill these requirements, below are some suggestions for different situations.</p>
                <h4>ADA Kits:</h4>
                <p>We have a many ADA kits that are perfect for hotels, hospitals and other places of public accommodation that are required to equip a certain amount of rooms with ADA accommodations. These kits include a variety of products including signaling devices, phone amplifiers and smoke alarms. If you don't see a pre-packaged kit that fits your needs, give us a call and we will be happy to customize one for you. Please contact us for a quote if you need to buy multiple units.</p>
                <h4>FM Systems:</h4>
                <p>These systems have a transmitter that is either wired into the venue's sound system or that the speaker wears. Each hard-of-hearing audience member has their own receiver, which they can plug in a neckloop or headphone.</p>
                <p>FM systems work well for auditoriums, churches, courtrooms, workplaces, classrooms and for tour guides.</p>
                <h2><a [routerLink]="['helpful-resources']">Read Our FM System Guide</a></h2>
                <h4>Loop Systems:</h4> 
                <p>These systems include a loop amplifier and a loop wire. The loop wire is placed around the listening area and then plugged into the amplifier. Plug your desired sound source into the loop amplifier, like a sound system or a microphone. T-coil hearing aid wearers or people using a receiver and headphone can than hear the sounds once they enter the looped area.</p>
                <p>Loop systems work well for auditoriums, churches and workplaces and classrooms.</p>
                <h2><a [routerLink]="['helpful-resources']">Read Our Loop System Guide</a></h2>
                <h4>Personal Amplifiers:</h4><p>These have a microphone that picks up and amplifies conversations that the user is having.</p>
                <p>Personal amplifiers are useful for hospitals, nursing homes or places that hard of hearing patients need to be able to communicate with staff and family. They can also be helpful in the workplace where communication with co-workers is necessary.</p>
                <h2><a [routerLink]="['helpful-resources']">Read Our Personal Amplifiers Guide</a></h2>
                <h4>Emergency Products:</h4><p>We carry loud and flashing smoke and CO detectors that will ensure deaf and hard of hearing employees, patients and tenants are safe in an emergency.</p>
                <h2><a [routerLink]="['helpful-resources']">Read Our Emergency Products Guide</a></h2>
                <h4>Office Phone solutions:</h4>
                <p>We have many solutions that allow you keep an existing office phone, while still providing the benefits of amplification.</p>
                <h2><a [routerLink]="['helpful-resources']">Read Our Telephones Guide</a></h2>
                <h4>Stethoscopes:</h4>
                <p>We have a variety of amplified stethoscopes that allow hard of hearing medical professionals better hear and diagnose patients.</p>
                <h2><a [routerLink]="['helpful-resources']">Read Our Stethoscopes Guide</a></h2>
                <h4>Sign Language:</h4>
                <p>It is important to be able to have basic communication with the Deaf coworkers, customers and patients. We have a few inexpensive guides that cover basic signs to help in different situations.</p>
                <h4><a [routerLink]="['helpful-resources']">Shop The Guides (N268 &amp; N269)</a></h4>
                <h1>Disabled Access Credit (DAC) for Small Businesses</h1>
                <p>If you are a small business you may qualify for the DAC (Disabled Access Credit) tax incentive to help you cover costs in becoming ADA compliant. The DAC is available to eligible small businesses with 30 or fewer full-time employees, or gross receipts less than $1 million/year. The credit can help out with eligible expenses between $250 and $10,250, for a maximum credit of $5,000 a year. <a href="http://askjan.org/media/tax.html" target="_blank">Learn more about DAC</a>.</p>
                <p>You can contact The Job Accommodation Network (JAN) for help, it is a national, toll-free, consulting service that provides information regarding job accommodations. <a href="http://askjan.org/" target="_blank">http://askjan.org/</a></p></div>
            </div>
        `;
        this.show_pop_up(body);
    }
    how_can_i_amplify_outgoing_speach() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                   <h1>How Can I Amplify Outgoing Speech </h1>
                </div>
                <div class="std">
                    <p>We carry a variety of speech assistance products that amplify the speaker's voice, allowing those with soft voices or those who work in noisy environments to be heard properly.</p>
                <p><strong>Speech Amplifiers</strong> include a portable speaker and a microphone. A person talks into the microphone, and the speaker amplifies the sound. The speaker is generally worn around the waist or placed on a table.</p>
                <p><strong>Electronic Larynxes</strong> are for people who have lost their voicebox. These have features like volume and tone control for a more natural sounding voice.</p>
                <p><strong>Inline Voice Amplifiers</strong> amplify the user's voice during phone conversations. Simply connect to a corded phone between the receiver and the base.</p>
                <p><strong>Telephones</strong> in this section amplify the outgoing AND incoming voice during phone conversations. They are the same as our amplified phones, and include many features like tone control and a loud/visual ringer. <link> Read our guide on Amplified Telephones</p>
                <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Outgoing Speech Amplification</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    how_can_i_hear_better_on_my_cellphone() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                   <h1>How Can I Hear Better On My Cell Phone? </h1>
                </div>
               <div class="std">
                <p>We carry many products that help the deaf and hard of hearing get the most out of their cell phone. Read our guide to find out how to be alerted to calls or texts, have clearer conversations and even what products have apps.</p>
                <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Cell Phone Products</a></p>
                <h2>How Do I Know When I'm Getting A Call Or Text?</h2>
                <p>Are you always missing calls and texts from your cell phone? There are a few solutions that can help you out! These signalers can also alert you to apps, such as Skype calls, emails or Facebook messages.</p>
                <h2>Vibration/Light Sensing Signalers</h2>
                <p>These detect incoming calls/messages by sensing vibration or light from your cell phone's screen. You'll be notified by bright flashing lights or loud alarm. Some have an optional bed shaker you can use for nighttime notification.</p>
                <p>Some models are stand-alone signalers, meaning they will just alert you to your cell phone. Others can connect to a signaling system, which allows you to use receivers to alert in multiple rooms and transmitters to alert to different events, such as the doorbell. (<link>Read our signaling system guide).</p>
                <h2>Smart Watches</h2>
                <p>Smart watches connect to your cell phone via Bluetooth. When you receive a phone call, text or email the watch will vibrate and display the caller or what the text or email says. You can decline calls and reply to texts or emails right from the watch. You can also choose to receive notification from many apps including your calendar, Skype, Facebook and much more!</p>
                <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Cell Phone Signalers</a></p>
                <h2>How Can I Hear Better On My Cell Phone?</h2>
                <p>There are many products available that provide clearer, more enjoyable cell phone conversations for those with hearing loss. They reduce distracting background noise and many boost the volume of the caller's voice. Some can be beneficial for listening to MP3 players or watching videos on your cell phone or tablet as well.</p>
                <h2>Bluetooth Amplifiers</h2>
                <p>These pair with your Bluetooth-enabled cell phone and can be used with or without hearing aids. They have many features that traditional amplified phones have, such as tone control, speakerphone or loud ringer. We have handsets that amplify cell phone calls or Bluetooth amplified phones that amplify both cell and landline calls.</p>
                <h2>Neckloops/Silhouettes</h2>
                <p>Theseare specifically designed for people who have t-coil equipped hearing aids, though some allow you to plug in a headphone. Learn more about t-coils in our <link>guide.</p>
                <p>Neckloops/Silhouettes work by generating an electromagnetic signal that sends the conversation directly to your hearing aids. This cuts out background noise and produces a much clearer sound. Both neckloops and silhouettes tend to work well for most people, but if you have an "automatic" telecoil you may find that silhouettes work better.</p>
                <ul>
                <li><p><Strong>Neckloops</strong> are worn around your neck and wirelessly sends the conversation to your hearing aids. They have many unique features, such as control of your phone's functions, detachable mics for conversation amplification or even the option to connect it to a TV.</p></li>
                <li><p><strong>Silhouettes</strong> have a "hook" that hooks around your ear next to the hearing aid, and a wire connects the hook to your cellphone. Some people prefer Silhouettes since they are placed so close to the hearing aid that the signal can be stronger.</p></li>
                </ul>
                <h2>Headphones / Headsets</h2>
                <p>Headsets reduce all the distracting noise around you, allowing you to focus on your call, movie or music. We have Bone-Conduction headphones that use the cheekbones to transmit sound to the inner ears, bypassing the eardrums completely, which many people with hearing loss find extremely beneficial. These are handy for those who enjoy exercising outside since it allows you to hear other sounds, such as a car coming.</p>
                <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Cell Phone Products</a></p>
                <h2>App Controlled Devices</h2>
                <p>We have quite a few products that come with free app downloads, allowing you to monitor or change the settings of the product on your phone.</p>
                <p><strong>Bluetooth Bed Shakers</strong> wirelessly connect to your smartphone and the alarm is set through an app. Once the alarm goes off, it vibrates and sounds an alarm to wake you up. You can set up to 10 different alarms for any day of the week. Many people use these for cooking timers or medication reminders as well.</p>
                <p><strong>Smart Watches</strong> have apps continuously being created for them by developers from around the world. In addition to monitoring your calls and texts, you can download apps for calendar, fitness tracking, games and even weather reports!</p>
                <p><strong>Emergency Devices</strong> will send an alert to your cell phone letting you know when it detects smoke or carbon monoxide. This gives you great peace of mind when you are away from the house, but should not be relied on for primary notification of an emergency. Read our <link>emergency products guide to learn about your options.</p>
                <p>Some products send alerts to your phone when a transmitter from your <strong>signaling system</strong> goes off, alerting you events such as a door knock or your baby crying. Learn more about signaling systems in our <a [routerLink]="['helpful-resources']">guide</a>.</p>
                <p>A few of our <strong>in-ear personal amplifiers</strong> allow you to customize settings, like volume and tone, right from your cell phone. This comes in really handy when you want to discreetly adjust your amplifier to better hear in social situations.</p>
                <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop App Controlled Devices</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    how_can_i_keep_my_hearing_ads_running_smoothl() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                   <h1>How to Buy a Personal Amplifier</h1>
                </div>
                <div class="std">
                    <p>Hearing aids are a huge investment, but without proper care you can face expensive repairs or may even need to get them replaced. We have many inexpensive products that will help maintain the condition of your hearing aids by keeping them clean and dry, ensuring they operate at their finest for many years to come.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Hearing Aid ACcessories</a></p>
                    <h2>Hearing Aid Protectors and Sweatbands</h2>
                    <p>It is important to keep your hearing aid free of dirt, dust and moisture. Hearing aid protectors and sweatbands are an effective, inexpensive solution. They come in many different sizes and simply slip over your hearing aid without interference to sound quality. Some models have a cord that clips to your clothing so you won't lose them while active.</p>
                    <h2>Hearing Aid Dryers</h2>
                    <p>Hearing aid dryers are great for removing moisture and ear wax, killing germs and deodorizing your hearing aid or cochlear implant. It is highly recommended to put your hearing aid in a dryer each night while you sleep.</p>
                    <h2>Desiccant</h2>
                    <p>Some hearing aid dryers use desiccant, which are little moisture absorbent beads or a brick. The beads start off as orange, and turn green once they wear out. They can then be recharged in the microwave. Generally, desiccant needs to be replaced every 2 months.</p>
                    <p>Many of these models are very inexpensive and require no electricity, making them easy to slip in your suitcase while traveling!</p>
                    <h2>Heat</h2>
                    <p>These dryers are initially more expensive than desiccant dryers, but in addition to removing moisture, they use heat to melt away earwax. Models with a UV light also kill germs and deodorize your hearing aid.</p>
                    <p>Most of these dryers don't require desiccant, which saves you the hassle and expense of having to replace every few months. These do require electricity, but many are still small enough to travel and won't take up much space on your bedside table.</p>
                    <h2>Hearing Aid Cleaners</h2>
                    <p>We have many products that can give your hearing aid a good cleaning, such as vacuums and cleaning kits. We also carry disinfectant wipes that don't dry out or damage the silicone in your hearing aid like standard wipes might.</p>
                    <h2>Ear Care</h2>
                    <p>While taking care of your hearing aids, don't forget about your own ears! We have many healing and anti-itch creams to sooth irritated ears. We also have ear lubricant to help your hearing aids go in smoothly.</p>
                    <h2>Hearing Aid Batteries</h2>
                    <p>We have batteries for hearing aids and cochlear implants, as well as an array of battery testers and holders.</p>
                    <p>Traditional hearing aid batteries are have mercury, which can contaminate the water supply. The batteries we sell are mercury free, making them more environmentally friendly and legal in all 50 states. Mercury free hearing aid batteries are also the longest lasting!</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Hearing Aid Accessories</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    what_is_a_t_coil() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                   <h1>What is a T-coil?</h1>
                </div>
                <div class="std">
                <p>A t-coil or telecoil is one of the most helpful features of your hearing aid, but many people don't know if they have one or how it is used. It picks up signals from a loop or a t-coil compatible product and sends the sound directly to your hearing aid, providing clear sound while shutting out background noise. It's like turning your hearing aid into a personal receiver!</p>
                <h2>How do I know if I have a t-coil?</h2>
                <p>T-coils are an optional feature of your hearing aid. Check with your hearing health care provider to see if you have one, as not all hearing aids are compatible with t-coils.</p>
                <p>Some of our Personal Amplifiers have a t-coil receiver built into them. Shop Personal Amplifiers</p>
                <h2>How do t-coils work?</h2>
                <p>A t-coil is a small coil of wire inside a hearing aid that is designed to pick up sounds in the form of electromagnetic signals. When your t-coil is turned on, it shuts off the microphone on your hearing aid and begins "listening" for an electromagnetic signal.</p>
                <p>Loops and other t-coil compatible products turn sounds into electromagnetic signals, which the t-coil on your hearing aid then picks up.</p>
                <p>This allows you to hear better in a variety of situations because it allows you to shut out unwanted background noise and focus in on only the sounds you truly want to hear. T-coils can also help eliminate the annoying buzzing or feedback that hearing aid users can sometimes experience when using the phone.</p>
                <p>Some hearing aids can be equipped to allow you to use both your t-coil and hearing aid microphone at the same time. This allows you to hear both sounds around you (such as your own voice) and sounds coming through your t-coil at the simultaneously.</p>
                <h2>When would I want to use my t-coil?</h2>
                <p>When using your t-coil along with compatible products, you will have a much easier time hearing in many everyday situations.</p>
                <h2>Watching TV</h2>
                <p>You have two options for using your t-coil while watching TV:</p>
                <p><strong>TV Listening System with a Neckloop Style Receiver:</strong> These are very simple to set-up. Simply plug the transmitter into your TV and place the neckloop receiver around your neck, much like a necklace. The transmitter sends the sound from your TV to the neckloop, which sends it right to your hearing aids. <a href="/products/amplification-devices/loop-systems">Browse TV Listening Systems with a Neckloop Style Receiver</a></p>
                <p><strong>Home Loop Systems:</strong> These systems include the loop amplifier and your choice of a loop wire or loop pad. The loop wire is placed around your room and the pad is placed underneath your chair cushion. Plug the pad or wire into the loop amplifier, and then plug the amplifier into the audio output jacks on your TV. Your t-coil hearing aid then acts as a receiver for the TV whenever you walk into the looped area! These are a little more complicated to set-up, but many people enjoy the convenience of not wearing the receiver around their neck. <a href="/products/amplification-devices/loop-systems">Browse Home Loop Systems</a></p>
                <h2>Talking on the Phone</h2>
                <p>All of the amplified phones we offer are t-coil compatible. They virtually eliminate feedback and interference that many standard phones cause when using with hearing aids. <a href="/products/amplification-devices/telephones">Browse Amplified Phones</a></p>
                <h2>Talking on the Cell Phone</h2>
                <p><strong>Neckloops:</strong> These are worn around your neck and connect to your phone by either Bluetooth or a plug. They transmit conversations and music directly to your t-coil. <a href="/products/amplification-devices/accessories/neckloops">Browse Neckloops</a></p>
                <p><strong>Silhouettes:</strong> These have a "hook" that hooks around your ear next to the hearing aid and a wire plugs into your cell phone. This style may work better for some people, depending on the position of the actual telecoil in your hearing aid. <a [routerLink]="['helpful-resources']">Browse Silhouettes for Cell Phones</a></p>
                <p><strong>Hand-Held Amplifier:</strong> These amplifiers connect to your cell phone through Bluetooth and are used like a handset, the t-coil mode sends the conversation right to your t-coil. <a href="/products/amplification-devices">Browse Hand-Held Amplifiers</a></p>
                <h2>Listening to Music or Radio</h2>
                <p>If you would like to listen to music or the radio through a stereo system, you can use the same products as you would for watching TV.</p>
                <p><a [routerLink]="['helpful-resources']">Browse Listening Systems with a Neckloop Style Receiver</a></p>
                <p><a [routerLink]="['helpful-resources']">Browse Home Loop Systems</a></p>
                <p>To listen to an MP3 player you can use the same Neckloops or Silhouettes as you would for your Cell Phone Conversations. <a [routerLink]="['helpful-resources']">Browse Neckloops/Silhouettes</a></p>
                <h2>Conversations</h2>
                <p><strong>Personal Amplifiers with Neckloop:</strong> The neckloop is worn around your neck and plugs into the amplifier, which is hand held or attached to clothing. The amplifier picks up the conversation you are having and the neckloop sends it to your t-coil. <a [routerLink]="['helpful-resources']">Browse Personal Amplifiers with Neckloop</a></p>
                <p><strong>Personal Amplifiers with T-Coil Capabilities Built In:</strong> These amplifiers have a t-coil receiver built-in. If you enter into a looped area, the personal amplifier will send the conversation to your t-coil without the need of a neckloop.</p>
                <h2>Classrooms and Meetings</h2>
                <p><strong>Personal FM System with Neckloop:</strong> FM Systems consist of a transmitter, which the speaker wears and a receiver, which the listener wears. A neckloop is worn around your neck and plugs into the receiver. The transmitter picks up the speakers voice and sends it to the receiver, which the neckloop then sends to your t-coil. <a [routerLink]="['helpful-resources']">Browse FM Systems with Neckloop</a></p>
                <p><strong>Loop Systems:</strong> These systems include a loop amplifier and a loop wire. The loop wire is placed around the listening area and then plugged into the amplifier. Plug your desired sound source into the loop amplifier, like a sound system or a microphone. Your t-coil hearing aid then acts as a receiver whenever you walk into the looped area! <a [routerLink]="['helpful-resources']">Browse Loop Systems</a></p>
                <h2>Auditoriums, Churches or Theaters</h2>
                <p><strong>Large Area/Multiple User FM Systems:</strong> These systems have a transmitter that is either wired into the venue's sound system or that the speaker wears. Each hard-of-hearing audience member has their own receiver, which they can plug in a neckloop or headphone. The transmitter picks up the sound source and sends it to the receiver, and if a neckloop is plugged in, it will send to your t-coil. <a [routerLink]="['helpful-resources']">Browse Large Area/Multiple User FM Systems </a></p>
                <p><strong>Loop Systems:</strong> These systems include a loop amplifier and a loop wire. The loop wire is placed around the listening area and then plugged into the amplifier. Plug your desired sound source into the loop amplifier, like a sound system or a microphone. Your t-coil hearing aid then acts as a receiver whenever you walk into the looped area! <a [routerLink]="['helpful-resources']">Browse Loop Systems</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    what_is_a_loop_system() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                 <h1>How To Buy A Loop System</h1>
                </div>
               <div class="std">
                    <p>A loop system picks up and transmits the desired sound to your telecoil-equipped hearing aid, bridging the gap between you and the sound source, virtually eliminating any unwanted background noise. It's like turning your hearing aid into a personal receiver!</p>
                    <p>Talk with your audiologist to see if you have a t-coil installed in your hearing aid. If you don't have t-coil equipped hearing aids, you can use a t-coil receiver with a pair of headphones plugged in. <link>Read our t-coil guide</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Loop Systems</a></p>
                    <h2>What Is Included?</h2>
                    <p>A loop system typically consists of a thin wire, amplifier, and a microphone.</p>
                    <h2>How Does It Work?</h2>
                    <p>First, the wire is looped around your desired listening area and then plugged into the amplifier. Some systems offer a loop pad, which can be placed underneath the listener's chair.</p>
                    <p>The amplifier is then hooked up to whatever source(s) you'd like to listen to, such as a TV or sound system. A microphone can be plugged into the amplifier to pick up a speaker's voice.</p>
                    <p>Once the amplifier is turned on, any sound signals that pass through the amplifier are then amplified and circulated throughout the loop, generating an invisible magnetic field that can be detected and amplified wirelessly by the telecoil in your hearing aid.</p>
                    <p>Once you enter the looped area, turn your telecoil on and your hearing aid becomes a personal loudspeaker! Turning the telecoil on will simultaneously turn the microphone in your hearing aid off, which will eliminate unwanted background noise. If you have a Mic + T Coil setting, you can choose to use the microphone and telecoil at the same time.</p>
                    <h2>Where Would I Use One?</h2>
                    <p>Loop systems can be used in many different settings. They can be used for watching TV, hearing lectures in classrooms, co-workers at meetings, etc.</p>
                    <p>Portable loop systems are also available and are great for use in the car, one-on-one conversations, or at reception desks. They have a shorter range, making conversations more private.</p>
                    <p>We also have a clipboard loop system which is really handy for coaches, doctors or teachers.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Loop Systems</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    how_to_buy_an_alarm_clock() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                    <h1>How To Buy An Alarm Clock</h1>
                </div>
              <div class="std">
                    <h2>Alarm Clocks</h2>
                    <p>The alarm clocks we carry work much the same as standard alarm clocks, but are designed with a combination of special features that are ideal if you have a hearing loss, are deaf, or are just a deep sleeper.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Alarm Clocks</a></p>
                    <h2>What Are My Notification Options?</h2>
                    <p>All of our clocks will wake you up by at least one of these methods, but many will use a combination of two or more. Generally, you can turn off any notification option you do not want to use.</p>
                    <p><strong>Bed Shaker</strong></p>
                    <ul>
                    <li><p><strong>Alarm Clock:</strong> almost all of our alarm clocks come with a bed shaker that you can place under your mattress or pillow. When the alarm goes off, the bed shaker will vibrate your bed to wake you up. This tends to be most the effective way to wake up, but if you don't like it you can simply unplug the shaker from the clock.</p></li>
                    <li><p><strong>Bluetooth:</strong> these bed shakers wirelessly connect to your smartphone and the alarm is set through an app. Once the alarm goes off it vibrates and sounds an alarm to wake you up. You can set up to 10 different alarms for any day of the week. Many people use these for cooking timers or medication reminders as well.</p></li>
                    </ul>
                    <p><strong>Extra Loud Audible Alarm</strong></p>
                    <p>All of our alarm clocks have an extra loud audible alarm. Depending on the clock, you can adjust it up to 113dB, which is comparable to what a rock concert sounds like from your seat! Many also come equipped with tone control, allowing you to adjust the alarm to a frequency that is easier for you to hear in the morning.</p>
                    <p><strong>Wrist Vibration</strong></p>
                    <p>Some clocks come with a wristband that vibrates when the alarm goes off. These are useful when you want to wake up without disturbing your partner or roommate.</p>
                    <p><strong>Lamp Flasher</strong></p>
                    <p>Clocks that have this function allow you to plug a lamp into an outlet on the back of the clock or into the end of the cord, depending on the clock. When the alarm goes off, the lamp will flash.</p>
                    <h2>What Else Should I Consider?</h2>
                    <p><strong>Dual Alarm</strong></p>
                    <p>Some of our alarm clocks give you the option of setting more than one alarm. This is really helpful if you and your partner wake up at different times.</p>
                    <p><strong>Portable/Travel</strong></p>
                    <p>We have many travel clocks that are a perfect size to throw in your suitcase and take with you on the go!</p>
                    <h2>Watches/Timers</h2>
                    <p>We sell an array of vibrating watches and timers that operate like normal watches and timers, with the added functionality of vibration to alert you when the timer or alarm goes off. Not only is this useful for waking up, but you can set multiple alarms for medication, cooking and other reminders.</p>
                    <p>SmartWatches, such as the Pebble, connect to your cell phone via Bluetooth. They will alert you to notifications on your phone, such as the alarm going off, calls or texts.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Clocks and Watches</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    how_to_buy_an_amplified_telephone() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                 <h1>How to Buy an Amplified Telephone</h1>
                </div>
               <div class="std">
                    <p>There are many unique phones available that help make conversations with hearing loss a headache free experience. Many of our phones have additional features that help out with memory loss, low mobility or low vision as well.</p>
                    <p>In this guide, you will learn about:</p>
                    <ul>
                    <li><p><a [routerLink]="['helpful-resources']">Amplified Phones</a></p></li>
                    <li><p><a [routerLink]="['helpful-resources']">Captioned Phones</a></p></li>
                    <li><p><a [routerLink]="['helpful-resources']">Products To Use With Your Existing Phone</a></p></li>
                    <li><p><a [routerLink]="['helpful-resources']">Office Phone Solutions</a></p></li>
                    <li><p><a [routerLink]="['helpful-resources']">Photo Phones</a></p></li>
                    <li><p><a [routerLink]="['helpful-resources']">Emergency Response Systems</a></p></li>
                    <li><p><a [routerLink]="['helpful-resources']">Low Vision/Mobility Phones</a></p></li>
                    <li><p><a [routerLink]="['helpful-resources']">TTYs</a></p></li>
                    </ul>
                    <h2>Amplified Phones</h2>
                    <p>These phones are just like a standard landline phone, and have many of the same elements like Caller ID, answering machine and speakerphone, but offer extra features to help you hear better.</p>
                    <h2>Amplification</h2>
                    <p>This is one of the most important features in choosing an amplified phone. Generally, hearing loss can be categorized as mild, moderate or severe.</p>
                    <ul>
                    <li><P><strong>Mild:</strong> If you miss occasional words or strain to hear on the phone but still can get through a conversation, we recommend a phone with up to 30dB of amplification.</p></li>
                    <li><P><strong>Moderate:</strong> If you find yourself constantly asking the person on the other line to repeat themselves, we recommend a phone with 30-45dB of amplification.</p></li>
                    <li><P><strong>Severe:</strong> If youbtn btn-brandve decided to stop using the phone because you can hardly hear anything at all, we recommend 50dB or more of amplification. Keep in mind that hearing loss can be degenerative. It may be a good idea to get a phone with a little more amplification than you currently need and turn down the volume if it is too loud.</p></li>
                    </ul>
                    <h2>Tone Control</h2>
                    <p>This feature allows you to adjust the frequency (or pitch) of sounds to better fit your hearing loss. It helps define the amplified sound better, making speech much clearer and intelligible. We highly recommend purchasing a phone with both volume and tone control for the best possible experience.</p>
                    <h2>Loud and Visual Ringer</h2>
                    <p>Most of our phones come with a loud ringer that also flashes a light, making sure you never miss a call.</p>
                    <h2>Hearing Aid Compatible</h2>
                    <p>All of our amplifiedphones are Hearing Aid Compatible (HAC), which means they are designed to reduce feedback and interference sometimes caused by hearing aids. Some of our cordless phones are TIA-1083 complaint, which reduces interference even more and is the best option for the clearest experience.</p>
                    <h2>What Else Should I Consider?</h2>
                    <p><strong>Bluetooth</strong></p>
                    <p>A few of our phones are Bluetooth enabled, so you can pair your cell phone to the amplified phone and experience the same high quality sound on both your landline and cell phone calls.</p>
                    <p><strong>Boost Override</strong></p>
                    <p>A lot of our phones have a feature called "boost override." When this feature is turned on, it will save the volume and tone settings from your last phone call. This is a great feature if you are the main user of the phone, but if you have other people that will be using the phone, we recommend turning this feature off.</p>
                    <p><strong>Low Vision, Dexterity and Memory Loss</strong></p>
                    <p>Many of our phones have features to help those with low vision, dexterity and memory loss as well as hearing loss. Look for features like extra-large keypad, photo memory buttons, high contrast screens and talking features.</p>
                    <p><strong>Outgoing Speech Amplification</strong></p>
                    <p>In addition to amplifying incoming speech, some of our phones amplify outgoing speech as well. This is really helpful for those with soft voices.</p>
                    <p><strong>Expansion Handsets</strong></p>
                    <p>Have multiple phones throughout your home at a fraction of the cost! Many of our phones allow you to add additional expansion handsets to the base. These expansion handsets are far less expensive than the base phone and have many of the same features. They come with their own charging dock, making them convenient to place in many rooms.</p>
                    <p><a class="button btn-gren" [routerLink]="['helpful-resources']">Shop Amplified Phones</a></p>
                    <h2>Captioned Phones</h2>
                    <p>A captioned phone works just like an amplified phone, but with one very cool exception�it comes with a large, built-in screen that displays captions of everything the other caller is saying. This allows you to read along and see everything the caller is saying at the same time youbtn btn-brandre having the conversation.</p>
                    <p>Whenever you place or receive a phone call on a captioned phone, the phone automatically connects to a 3rd party captioning service that operates in the background. A trained operator listens to the person on the other line and re-voices everything they say into a speech recognition program that transcribes their voice into captions. These captions are then displayed on the screen of your phone.</p>
                    <p>The captioning service is funded by the FCC and provided 100% free of charge! All you need is a regular home phone line, an electrical outlet and high speed internet.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Captioned Phones</a></p>
                    <h2>Phone Add-Ons</h2>
                    <p>If you arenbtn btn-brandt ready to purchase an amplified phone, or need a portable option, we have many inexpensive alternatives that you can use with your existing phone.</p>
                    <p><strong>Telephone Amplifiers</strong></p>
                    <p>These are a simple and inexpensive way to amplify your existing phone and are small enough to fit into a bag or purse! They come in two different styles:</p>
                    <ul>
                    <li><p><strong>In-Line:</strong> These amplify incoming sounds up to 40dB and have tone control. They connect directly between the handset and the base of a corded phone. In-line amplifiers are compatible with all analog and most digital phones, making them a great option for amplifying both your home and work phone.</p></li>
                    <li><p><strong>Portable:</strong> These amplify incoming sounds up to 30dB. They attach to the handset on a standard corded or cordless phone and have a built-in volume control. All they take is a few batteries and theybtn btn-brandre ready to go.</p></li>
                    </ul>
                    <p><strong>Amplified Answering Machines</strong></p>
                    <p>These are similar to regular answering machines, but include much louder speakers and the ability to control the speed at which the message is played back for maximum clarity. Some models have tone control which produces much clearer, intelligible speech when used with amplification.</p>
                    <p><strong>Ring Signalers</strong></p>
                    <p>These plug into any phone jack in your home, and when the phone rings they will notify you through an extra loud ringer, flashing strobe light, and/or vibration.</p>
                    <p><strong>Phone Pads</strong></p>
                    <p>For hearing aid users that experience annoying feedback when using their phone, simply sticking one of these to the earpiece of the phone can help solve the problem!</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Phone Add-Ons</a></p>
                    <h2>Office Phone Solutions</h2>
                    <p>With the widespread use of digital and VoIP telephone systems in the office today, finding a solution to help you hear better on the phone at work isnbtn btn-brandt as simple as just buying a new phone. You need a solution that lets you keep your existing phone, while still providing the benefits of amplification.</p>
                    <p><strong>Headset/Neckloop Office Systems</strong> amplify your calls through an inline amplifier which connects between your phone and a headset or neckloop. Neckloops are a great hands free option for those with a t-coil equipped hearing aid; it sends sounds directly from the phone to the hearing aid <link>Read our guide to learn about t-coils. We also have Bluetooth solutions which pairs your phone to a Bluetooth headset.</p>
                    <p>Many of our <strong>Headsets/Handsets</strong> are ergonomically designed andhelp cancel out background noise. We even have a few options that plug into a USB port to use with Skype or internet calling software.</p>
                    <p>We also have amplified <strong>Home Office Phones</strong> which provide loud and clear conference-grade speakerphone.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Office Phone Solutions</a></p>
                    <h2>Photo Phones</h2>
                    <p>These phones come with photo memory buttons that make for easy speed dialing. Great for seniors, those with Alzheimerbtn btn-brands, or those with cognitive impairments that may have difficulty remembering phone numbers. Many have amplification, tone control and other features you find on amplified phones.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Photo Phones</a></p>
                    <h2>Emergency Response Systems</h2>
                    <p>These systems offer independence and peace of mind for users and their families, with no monthly fees! These phones have an "emergency connect" function that can help keep users safe if the emergency prevents them from reaching the phone.</p>
                    <p>In the event of an emergency, simply activate the included wireless pendant and the system will begin dialing through a list of preprogrammed numbers, or calls 911. Once someone picks up, they will be able to begin a two-way conversation with you, sometimes after a short pre-programmed message informing them of an emergency.</p>
                    <p><strong>Amplified Emergency Phones</strong> are great for those who want to combine the everyday benefits of an amplified phone with the security of an emergency response system</p>
                    <p><strong>Personal Emergency Response Devices</strong> are designed for emergency purposes and some cannot be used for regular phone calls. The greatest benefit to these systems is the ability to communicate directly through the pendant itself, rather than relying on your phonebtn btn-brands speakerphone for communication</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Emergency Response Systems</a></p>
                    <h2>Low Vision/Mobility Phones</h2>
                    <p>These phones make using the phone easier for those with low vision or mobility.Most include amplification, tone control, loud ringer and other features commonly found on amplified phones.</p>
                    <p><strong>Low Vision</strong></p>
                    <p>Many of our phones have features to help those with low vision, including extra-large buttons, high contrast screens, talking features and braille on the buttons.</p>
                    <p><strong>Low Mobility</strong></p>
                    <p>These phones are voice activated and some have accessories that let you control functions with your body or mouth.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Low Vision / Mobility PhoneS</a></p>
                    <h2>TTYs</h2>
                    <p>TTY, or text telephone, allows you to have typed conversations sent through the phone line. The conversation is typed using a full keyboard and displayed on the screen. Many TTYs let you print out your conversations for future reference.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Ttys</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    how_to_buy_an_fm_digital_system() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                <h1>How to Buy an FM/Digital System</h1>
                </div>
              <div class="std">
                    <p>FM or Digital systems are great for helping you hear in situations where the person or people you're trying to listen to are further away (up to 150 feet), such as the teacher in a classroom or the pastor at a church.</p>
                    <p><a  class="btn btn-brand" [routerLink]="['helpful-resources']">Shop FM / DigitaL Systems</a></p>
                    <h2>Personal FM Systems</h2>
                    <p>Personal FM Systems are used when there is one listener. They consist of a transmitter and receiver. The transmitter is worn on the speaker's clothes or placed near the sound source. The microphone on the transmitter picks up sound and sends a wireless signal to the receiver, which reproduces the sound to the listener through the connected headphones, neckloop etc.</p>
                    <p>FM Systems are truly versatile. The receiver can be used as a personal amplifier, picking up and amplifying sounds around you. The transmitter can also be plugged into a TV, computer or other audio source, and the sound will be sent to the receiver.</p>
                    <h2>Large Area/Multiple User Systems</h2>
                    <p>These systems function the same way as personal FM systems except they include many receivers per transmitter, so multiple listeners can hear what the speaker is saying. These systems are either portable with a transmitter that is worn by the speaker (perfect for tour guides), or stationary, where the transmitter is wired into a sound system (perfect for churches or theaters). Some systems have transceivers, which allows you to listen and talk through the same device, which can be helpful in team teaching or for stagehands.</p>
                    <h2>Wearing Styles:</h2>
                    <ul>
                    <li>Headphones can be used with or without hearing aids.</li>
                    <li>Earphones can be used without hearing aids.</li>
                    <li>Neckloops are for those with <a [routerLink]="['helpful-resources']">t-coil enabled hearing aids</a>. It is worn comfortably around the neck, and sends sounds directly to the hearing aid.</li>
                    </ul>
                    <h2>What Else Should I Consider?</h2>
                    <ul>
                    <li><strong>Microphones:</strong> All of transmitters come one or both of these types of microphones: directional and omnidirectional. Omnidirectional picks up sound from every direction, while directional allows you to focus in on a specific sound source, which can be really helpful in noisy environments. Some receivers also include a microphone which can be handy if you want to focus on the speaker as well as hear people around you.</li>
                    <li><strong>Range:</strong> FM Systems typically have up to 100 feet of range, depending on the environment you are using it in.</li>
                    <li><strong>Secure Encryption:</strong> Some of our FM systems are able to prevent electronic eavesdropping, which is useful in courtrooms or other places privacy is a concern.</li>
                    <li><strong>Conference Microphone:</strong> This is an optional accessory that can be used to extend the range of the sound that is picked up.</li>
                    </ul>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop FM / DigitaL Systems</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    how_to_buy_an_emergency_notification_device() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                    <h1>How to buy an Emergency Notification Device</h1>
                </div>
               <div class="std">
                     <p>The emergency products we carry are specially designed to alert deaf and hard of hearing to a smoke, carbon monoxide (co) or severe weather emergency.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop emergeny alerts</a></p>
                    <h2>Smoke/CO Alarms</h2>
                    <h2>What type of device do I need?</h2>
                    <p>There are four types of alarms you can choose from: Listen and Alert, Hard-Wired, AC-Powered and Signaling System Compatible.</p>
                    <p><strong>Signaling System Compatible:</strong> Signaling systems consist of two parts: transmitters and receivers. Smoke or CO transmitters monitor for that specific event. Once the transmitter detects the emergency, it will let the receiver know, which will then alert you, usually by flashing lights, vibration or loud sound.</p>
                    <p>Depending on the system, the transmitter will be hard-wired into your electrical system or it can be placed next an existing alarm.</p>
                    <p>We highly recommend purchasing products that are signaling system compatible since they are very customizable to your needs. You can add transmitters to alert you to different events like doorbell, phone calls or baby cries. <link>See our Signaling System Buying Guide</p>
                    <p><strong>Listen and Alert:</strong> Rather than detecting smoke or CO itself, these fire alarms are designed to "listen" to your home's existing T3 or T4 alarms (see "what else should I consider?" for T3/T4 definition). If one becomes activated, the Listen and Alert device will then alert you in different ways depending on the device.</p>
                    <p><strong>Hard-Wired Alarms:</strong> These are recommended if you live in a home containing different floors and rooms. You can purchase multiple alarms and strobes to be placed around your home. Once one is activated, all the alarms in your home will go off. These require a licensed electrician to install.</p>
                    <p><strong>A/C-Powered Alarms:</strong> These are recommended if you live in a studio apartment or dorm. Plug into any wall outlet and you will be notified when smoke reaches that room. These cannot be interconnected like hard-wired alarms, so if you place one in your bedroom and a fire starts in your living room, you may not be alerted until it's too late. This option isn't available for CO.</p>
                    <h2>What are my notification options?</h2>
                    <p>Typical notification options are flashing strobe, loud alarm and/or vibration. When used simultaneously, these notification methods have been proven to significantly increase the chances that a hard of hearing or deaf person is woken up from their sleep in an emergency.</p>
                    <p><strong>Strobe Light:</strong> Many of our emergency products can alert you with a flashing strobe light. Some have built-in strobes, while others will send a signal wirelessly to a remote strobe light placed elsewhere in your home.</p>
                    <p><strong>Loud Alarm:</strong> Almost all of our emergency devices have standard loud audible alarms that put out up to 90dB of sound.</p>
                    <p><strong>Vibration:</strong> Bed shakers are a popular way to be woken up at night. Simply connected the bed shaker to a device by your bed and then placed under your mattress or pillow. When the alarm goes off, it sends a signal to the bed shaker which then vibrates and shakes your bed.</p>
                    <p>We also carry vibrating watches and pagers that offer a more portable solution, though we do not recommend using these to wake you up at night.</p>
                    <p><strong>Cell Phone:</strong> Some of our emergency products can also alert you on your cell phone! This method is used by Listen and Alert or signaling system devices. Download the device's app, and once the device picks up a smoke or CO alarm, you will be alerted via your cell phone. This is great for when you are awake, but many not strong enough to wake you up. We highly recommend using an option with flashing lights or vibration while you sleep.</p>
                    <h2>What Else Should I Consider?</h2>
                    <p><strong>T3, T4 and Continuous Sound:</strong></p>
                    <ul>
                    <li><strong>T3:</strong> These are smoke alarms that make a beep-beep-beep-pause sound. It is required alarm type in residential homes and includes any alarm made after 1996.</li>
                    <li><strong>T4:</strong> These are CO alarms that make a beep-beep-beep-beep-pause sound. Many states have requirements for CO alarm installation. We recommend checking your local regulations since they vary by state.</li>
                    <li><strong>Continuous:</strong> A continuous smoke alarm's siren continues to play without pauses. This type of sound would be present in older alarms.</li>
                    </ul>
                    <p><strong>Photoelectric vs Ionization Smoke Detector:</strong> Photoelectric alarms respond slightly faster to smoldering fires and are best suited for rooms with large pieces of furniture. Ionization alarms respond slightly faster to flaming fires and are best suited for rooms that contain highly combustible materials such as newspapers or flammable liquids. Ionization alarms are often more quicker to react but may cause more false alarms.</p>
                    <p><strong>Combination Alarms:</strong> We do have a few alarms that detect for both Smoke and CO. These can save you money instead of buying two separate alarms.</p>
                    <p><strong>Battery Back-up:</strong> Products with battery back-up keep you safe even during a power outage. Battery back-up works only for the audible aspect of the alarm unless otherwise specified.</p>
                    <h2>Weather Alert</h2>
                    <p>Another type of emergency that is commonly overlooked is severe weather. We offer a few weather alert systems that receive updates from NOAA weather bureau channels, and will indicate the specific type of weather alert and what level it is at for your area. Our systems are unique in that they also include components (strobe lights and bed shakers) to ensure you are alerted immediately and have time to get to safety before severe weather hits.</p>
                    <p>Most of the severe weather notification devices are signaling system compatible. A transmitter plugs into the weather alert radio, and when NOAA issues a warning, it will send a signal to all your receivers.</p>
                    <p>We do have a stand-alone option, in which the strobe and bed shaker are plugged into the radio. Once the NOAA issues a warning, the strobe and bed shaker will activate.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Emergeny Alerts</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    how_to_buy_amplified_stethoscopes() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                    <h1>How to buy an amplified stethoscope </h1>
                </div>
               <div class="std">
                    <p>Standard stethoscopes make it difficult for help hard of hearing medical professionals to do their job properly. We carry a variety of amplified stethoscopes that will help you diagnose your patients with confidence.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Stethoscopes</a></p>
                    <h2>What Should I Consider?</h2>
                    <p><strong>Amplification</strong></p>
                    <p>All of our stethoscopes are amplified, making it easier to hear critical body sounds.</p>
                    <p>&nbsp;</p>
                    <p><strong>Visual Display</strong></p>
                    <p>A few of our stethoscopes also come with software that allows you to see sounds on a monitor or cell phone and you can record for later analysis.</p>
                    <p>&nbsp;</p>
                    <p><strong>Bluetooth</strong></p>
                    <p>Some stethoscope models connect to a Bluetooth-enabled phone or computer. This allows you to transmit, view and save recordings easily.</p>
                    <p>&nbsp;</p>
                    <p><strong>Hearing Aid Use</strong></p>
                    <p>For those who wear hearing aids, standard stethoscope style headsets are very uncomfortable. We offer a few models that allow you to plug in your own headphones or neckloop.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Stethoscopes</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    how_to_but_a_tv_listening_system() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                    <h1>How to Buy a TV Amplifier</h1>
                </div>
               <div class="std">
                    <h2>What is a TV Amplifier?</h2>
                    <p>With a TV Amplifier, you can listen to the TV (or radio) at a volume that's comfortable for you, without disturbing anyone else!</p>
                    <p>A TV Amplifier consists of two parts-a transmitter and a receiver. The transmitter connects to your TV and transmits audio to the receiver which is worn by you.</p>
                    <p>TV Amplifiers work independently from your TV's speakers, so everyone in the room can still hear.</p>
                    <p>When purchasing a TV amplifier, the two main things to consider are style and technology type.</p>
                   <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop TV Amplifiers</a></p>
                    <h2>Styles:</h2>
                    <p><strong>Speaker TV Amplifiers:</strong> The speaker is placed near you and amplifies sound from the TV. These systems are nice if you find receivers worn in the ear uncomfortable or isolate you from conversation.</p>
                    <p><strong>Headphone TV Amplifiers:</strong> These are worn like a traditional pair of headphones and can be used with or without hearing aids.</p>
                    <p><strong>Stethoscope TV Amplifiers:</strong> The earpiece is placed directly into the ear canal. This style is great if you don't have hearing aids or take them out while watching TV.</p>
                    <p><strong>Neckloop TV Amplifier:</strong> These can be used by those with telecoil (t-coil) enabled hearing aids. It is worn comfortably around the neck, and sends sounds directly to the hearing aid (see our t-coil guide for more information). They also have a 3.5mm jack so you can use your own headphone.</p>
                    <p><strong>Home Loop Systems:</strong> These are for those who have telecoil (t-coil) equipped hearing aids. These systems include a loop amplifier and a loop wire or pad. Read our t-coil guide.</p>
                    <p>Place the loop wire around the desired listening area, or place the loop pad under your seat cushion. Plug the loop wire/pad into the amplifier and then plug the amplifier into the audio output jacks on your TV. Your telecoil-equipped hearing aid then acts as a receiver for the TV whenever you walk into a looped area!</p>
                    <h2>Technology</h2>
                    <p><strong>Radio Frequency (RF)/ FM Systems:</strong> These systems can send signals up to 300ft, even through walls and ceilings! You can hear the TV no matter what room you are in. Many people find the ability to hear the TV in different rooms very valuable.</p>
                    <p><strong>Infrared (IR) Systems:</strong> To get a clear sound, IR systems need an unblocked signal between the transmitter and receiver, giving them a limited range of around 40ft. You may be able to take your system to the movies, please check with your local theater to see what frequency they use.</p>
                    <p><strong>Bluetooth Systems:</strong> As well as listening to the TV, these systems allow you to answer cell phones calls through the headset or neckloop. They can connect to most bluetooth-enabled audio devices. Bluetooth has a range of about 30ft.</p>
                    <h2>What else should I consider?</h2>
                    <p><strong>Tone Control</strong></p>
                    <p>This feature allows you to adjust the frequency (or pitch) of sounds to better fit your hearing loss. It helps define the amplified sound better, making speech much clearer and intelligible.</p>
                    <p><strong>Balance Control</strong></p>
                    <p>This feature allows you to adjust the volume of the right or left ear independently. This can be beneficial if you experience more hearing loss in one ear over the other.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop TV Amplifiers</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    how_to_buy_a_signaling_device() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                 <h1>How to Buy a Signaling Device</h1>
                </div>
              <div class="std">
                    <h2>What type of device do I need?</h2>
                    <p>There are two types of signaling devices you can choose from: Stand-Alone and Signaling System Compatible.</p>
                    <p><strong>Stand-Alone Devices:</strong> These will only alert you to one type of sound, such as a door knock or phone ringing. They are less expensive than an entire signaling system, but are far more limited in what they can do. If you will only ever need to be one type of event, Stand-Alone devices are for you.</p>
                    <p>If you imagine yourself needing to be alerted to different events and in multiple rooms in the future, we highly recommend you purchase a signaling system compatible device. You can always start out small and add to the system later.</p>
                    <p><strong>Signaling System Compatible:</strong> Signaling systems consist of two parts: transmitters and receivers. Transmitters monitor for a specific sound or event, such as a door knock, phone call or baby crying. Once the transmitter detects the sound, it will let the receiver know, which will then alert you, usually by flashing lights, vibration or loud sound.</p>
                    <p>We highly recommend purchasing products that are signaling system compatible since they are very customizable to your needs. You can start off small and purchase only for events you need to know of now, and add on later. The rest of this guide will focus on signaling system compatible devices.</p>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Signaling &amp; Alerts</a></p>
                    <h2>What Signaling System is right for me?</h2>
                    <p>Transmitters and receivers will only communicate with each other if they are from the same system, so it's important to take time before your initial purchase and choose the right system for you.</p>
                    <p><strong>Step one:</strong> Which sounds and events do I want to be notified of?</p>
                    <p>Generally, you will need to buy a transmitter for each event, though some alert you to multiple. Typical events are:</p>
                    <ul>
                    <li>Doorbell/Door knock</li>
                    <li>Landline/Videophone calls</li>
                    <li>Cell phone calls/texts</li>
                    <li>Baby Crying and other loud sounds</li>
                    <li>Smoke/CO alarm</li>
                    <li>Severe Weather</li>
                    <li>Alarm Clock</li>
                    <li>Getting out of Bed</li>
                    <li>Opening a door or window</li>
                    <li>Entering a room</li>
                    <li>Pager/Help button</li>
                    <li>Step two: How do I want to be notified?Which events do I want to be notified of?</li>
                    </ul>
                    <p>Each receiver has different ways of alerting you. Common ways are flashing strobe, vibration or loud alarm. Being able to plug in a household lamp for flashing notification is less commonly available but preferred by many customers. Some receivers will even scroll the text of the event across the screen!</p>
                    <p><strong>Step three:</strong> How many rooms do I need to be notified in?Which events do I want to be notified of?</p>
                    <p>Typically you would buy a receiver for for each room where you need to receive notifications. Some systems have body worn receivers if you prefer to carry it around with you.</p>
                    <p><strong>Step four:</strong> What technology do I need?</p>
                    <p><strong>Radio Frequency:</strong> Most of our systems use this technology. It sends signals from the transmitter to the receiver through radio waves. A unique frequency can be set for your own system, allowing you to use multiple systems in apartments or dorms without interference. Radio frequency can travel through ceilings and walls, but concrete or metal studs can block the signals.</p>
                    <p><strong>Line carrier:</strong> This technology sends signals from your transmitters to your receivers through your homes electrical circuits. The transmitters and receivers must be on the same circuit to operate. You can tell this by looking at your home's fuse box, the switches on the left column are on one circuit and the switches on the right are on another.</p>
                    <h2>Which system should I choose?</h2>
                    <p>We've created this chart to give you an overview of each system. We know purchasing a signaling system can be confusing, so please don't hesitate to contact us with any questions. Many of the systems offer a value pack of transmitters and receivers, which is an excellent way to get started.Which events do I want to be notified of?</p>
                    <p>Radio Frequency</p>
                    <table class="data-table">
                    <thead>
                    <tr><th class='white-space'>Signaling System</th><th>Transmitters</th><th class='white-space'>Receiver Alerting Options</th><th>Warranty</th><th>Technology</th><th>Overview</th></tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td><a [routerLink]="['helpful-resources']">Sonic Alert HomeAware</a></td>
                    <td>
                    <ul>
                    <li>Door</li>
                    <li>Telephone/VP</li>
                    <li>Smoke/CO</li>
                    <li>Baby Cry/Sound</li>
                    <li>Smart Phone</li>
                    <li>Alarm Clock</li>
                    <li>Security System</li>
                    <li>Pager/Help Button</li>
                    </ul>
                    </td>
                    <td>
                    <ul>
                    <li>Scrolling Words</li>
                    <li>Flashing Strobe</li>
                    <li>Lamp Outlet</li>
                    <li>LED Icons</li>
                    <li>Loud Alarm</li>
                    <li>Bed Shaker</li>
                    <li>Smart Phone</li>
                    </ul>
                    </td>
                    <td>One year</td>
                    <td>Radio Frequency</td>
                    <td>
                    <ul>
                    <li>Broken link/Battery monitoring</li>
                    <li>Each receiver adds 1,000ft to range</li>
                    <li>SonicBridge connects Traditional receivers to HomeAware Master Unit</li>
                    <li>Need to pair transmitters/receivers to HA360 Master Unit before use</li>
                    <li>Purchase of the HA360 Master Package is required</li>
                    </ul>
                    </td>
                    </tr>
                    <tr>
                    <td><a [routerLink]="['helpful-resources']">Sonic Alert Traditional</a></td>
                    <td>
                    <ul>
                    <li>Door</li>
                    <li>Telephone/VP</li>
                    <li>Baby Cry/Sound</li>
                    <li>Alarm Clock</li>
                    </ul>
                    </td>
                    <td>
                    <ul>
                    <li>Flashing Strobe</li>
                    <li>Lamp Outlet</li>
                    <li>Loud Alarm</li>
                    <li>Bed Shaker</li>
                    </ul>
                    </td>
                    <td>Five year</td>
                    <td>Line Carrier</td>
                    <td>
                    <ul>
                    <li>Some transmitters alert you buy strobe or lamp outlet, saving money on receivers</li>
                    <li>Unique flash pattern per transmitter</li>
                    <li>Value Packs available</li>
                    </ul>
                    </td>
                    </tr>
                    <tr>
                    <td> <a [routerLink]="['helpful-resources']">Bellman & Symfon Visit</a> </td>
                    <td>
                    <ul>
                    <li>Door</li>
                    <li>Telephone/VP</li>
                    <li>Smoke</li>
                    <li>Baby Cry/Sound</li>
                    <li>Smart Phone</li>
                    <li>Alarm Clock</li>
                    <li>Opening a door or window</li>
                    <li>Entering a room</li>
                    <li>Pager/Help Button</li>
                    </ul>
                    </td>
                    <td>
                    <ul>
                    <li>Flashing Strobe</li>
                    <li>Loud Alarm</li>
                    <li>LED Icons</li>
                    <li>Bed Shaker</li>
                    <li>Body worn pager with vibration</li>
                    </ul>
                    </td>
                    <td>Two Year</td>
                    <td>Radio Frequency</td>
                    <td>
                    <ul>
                    <li>Known for high quality &amp; reliability</li>
                    <li>Ready out-of-box, no pairing required</li>
                    <li>Up to 260ft range</li>
                    <li>Clock and portable receiver alert by sound to match transmitter (i.e. sounds doorbell when someone presses doorbell)</li>
                    <li>Value Packs available</li>
                    </ul>
                    </td>
                    </tr>
                    <tr>
                    <td><a [routerLink]="['helpful-resources']">Serene Innovations Central Alert</a></td>
                    <td>
                    <ul>
                    <li>Door</li>
                    <li>Telephone/VP</li>
                    <li>Smart Phone</li>
                    <li>Smoke/CO</li>
                    <li>Severe Weather</li>
                    <li>Baby Cry/Sound</li>
                    <li>Pager/Help Button</li>
                    <li>Motion</li>
                    <li>Alarm Clock</li>
                    </ul>
                    </td>
                    <td>
                    <ul>
                    <li>Words</li>
                    <li>Flashing Strobe</li>
                    <li>Lamp Outlet</li>
                    <li>Loud Alarm</li>
                    <li>LED Icons</li>
                    <li>Bed Shaker</li>
                    <li>Body worn pager with vibration</li>
                    </ul>
                    </td>
                    <td>One year</td>
                    <td>Radio Frequency</td>
                    <td>
                    <ul>
                    <li>Versatility makes it a popular choice</li>
                    <li>Need to pair transmitters/ receivers before use</li>
                    <li>Up to 200ft range</li>
                    </ul>
                    </td>
                    </tr>
                    <tr>
                    <td><a [routerLink]="['helpful-resources']">Clarity AlertMaster</a></td>
                    <td>
                    <ul>
                    <li>Door</li>
                    <li>Telephone/VP</li>
                    <li>Baby Cry/Sound</li>
                    <li>Alarm Clock</li>
                    </ul>
                    </td>
                    <td>
                    <ul>
                    <li>Flashing Strobe</li>
                    <li>Lamp Outlet</li>
                    <li>Loud Alarm</li>
                    <li>LED Icons</li>
                    <li>Bed Shaker</li>
                    <li>Body worn pager with vibration</li>
                    </ul>
                    </td>
                    <td>One year</td>
                    <td>Radio Frequency</td>
                    <td>
                    <ul>
                    <li>Ready out-of-box, no pairing required</li>
                    <li>Up to 100ft range</li>
                    </ul>
                    </td>
                    </tr>
                    <tr>
                    <td><a [routerLink]="['helpful-resources']">Silent Call Signature</a></td>
                    <td>
                    <ul>
                    <li>Door</li>
                    <li>Telephone/VP</li>
                    <li>Smoke/CO</li>
                    <li>Severe Weather</li>
                    <li>Baby Cry/Sound</li>
                    <li>Opening a door or window</li>
                    <li>Entering a room</li>
                    <li>Getting out of bed</li>
                    <li>Alarm Clock</li>
                    </ul>
                    </td>
                    <td>
                    <ul>
                    <li>Flashing Strobe</li>
                    <li>Loud Alarm</li>
                    <li>LED Icons</li>
                    <li>Bed Shaker</li>
                    <li>Body worn pager with vibration</li>
                    </ul>
                    </td>
                    <td>Five year</td>
                    <td>Radio Frequency</td>
                    <td>
                    <ul>
                    <li>Ready out-of-box, no pairing required</li>
                    <li>Broken link monitoring between smoke detector and receivers</li>
                    <li>Up to 800ft range</li>
                    <li>Value Packs available</li>
                    <li>Made in America</li>
                    </ul>
                    </td>
                    </tr>
                    <tr>
                    <td><a [routerLink]="['helpful-resources']">Silent Call Legacy</a></td>
                    <td>
                    <ul>
                    <li>Door</li>
                    <li>Telephone/VP</li>
                    <li>Smoke/CO</li>
                    <li>Severe Weather</li>
                    <li>Baby Cry/Sound</li>
                    <li>Pager/Help Button</li>
                    <li>Opening a door or window</li>
                    <li>Getting out of bed</li>
                    <li>Entering a room</li>
                    <li>Alarm Clock</li>
                    </ul>
                    </td>
                    <td>
                    <ul>
                    <li>Flashing Strobe</li>
                    <li>Lamp Outlet</li>
                    <li>Loud Alarm</li>
                    <li>LED Icons</li>
                    <li>Bed Shaker</li>
                    <li>Body worn pager with vibration</li>
                    </ul>
                    </td>
                    <td>Five year</td>
                    <td>Radio Frequency</td>
                    <td>
                    <ul>
                    <li>Ready out-of-box, no pairing required</li>
                    <li>Up to 500ft range</li>
                    <li>Lamp outlet receiver available separate from main clock receiver</li>
                    <li>Made in America</li>
                    </ul>
                    </td>
                    </tr>
                    <tr>
                    <td><a [routerLink]="['helpful-resources']">Krown KA1000</a></td>
                    <td>
                    <ul>
                    <li>Door</li>
                    <li>Telephone/VP</li>
                    <li>Baby Cry/Sound</li>
                    <li>Alarm Clock</li>
                    </ul>
                    </td>
                    <td>
                    <ul>
                    <li>Flashing Strobe</li>
                    <li>Lamp Outlet</li>
                    <li>Loud Alarm</li>
                    <li>Bed Shaker</li>
                    </ul>
                    </td>
                    <td>One year</td>
                    <td>Radio Frequency</td>
                    <td>
                    <ul>
                    <li>Up to 100ft range</li>
                    <li>Need to pair transmitters/ receivers before use</li>
                    <li>Lamp outlet receiver available separate from main clock receiver</li>
                    </ul>
                    </td>
                    </tr>
                    <tr>
                    <td><a [routerLink]="['helpful-resources']">Krown KA300</a></td>
                    <td>
                    <ul>
                    <li>Smoke/CO</li>
                    <li>Severe Weather</li>
                    </ul>
                    </td>
                    <td>
                    <ul>
                    <li>Flashing Strobe</li>
                    <li>Loud Alarm</li>
                    <li>Bed Shaker</li>
                    </ul>
                    </td>
                    <td>One year</td>
                    <td>Radio Frequency</td>
                    <td>
                    <ul>
                    <li>Up to 100ft range</li>
                    <li>Need to pair transmitters/ receivers before use</li>
                    <li>Bed shaker receiver available separate from main clock receiver</li>
                    </ul>
                    </td>
                    </tr>
                    </tbody>
                    </table>
                    <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Signaling Systems</a></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    how_to_buy_a_personal_amplifier() {
        let body = `
            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="title">
                 <h1>How to Buy a Personal Amplifier</h1>
                </div>
              <div class="std">
                <h2>What is a Personal Amplifier?</h2>
                <p>Personal Amplifiers reduce background noise while amplifying the sounds you truly want to hear. They are really helpful in restaurants, family gatherings, small group meetings and even in cars! They can be used together with hearing aids to further improve your hearing, or as an alternative depending on your needs.</p>
                <p>They come with an array of customizable features that give you a much better listening experience than amplifiers you may have seen advertised on TV.</p>
                <p>There are two styles of Personal Amplifiers to choose from, In-Ear or Hand-Held/Body Worn.</p>
                <p><a class="btn btn-brand" [routerLink]="['helpful-resources']">Shop PersonaL Amplifiers</a></p>
                <h2>In-Ear</h2>
                <p>In-Ear Personal Amplifiers, also known as PSAPs (Personal Sound Amplification Products) are very discreet and can amplify sounds 15-30dB, perfect for those needing a little extra boost. Some models even allow you to customize settings through a cell phone app. Many come with built-in t-coil options, which pick up sounds from hearing aid compatible phones, venues with a loop system, or sound from your cell phone when used with a neckloop. <link>Read our t-coil guide</p>
                <p>Even though these amplifiers can help you hear in many situations, they cannot be tailored to your specific hearing loss like hearing aids can. They are, however, far less expensive and can be purchased without need for a professional exam. To explore all your options, have your hearing evaluated by a professional.</p>
                <p><strong>Wearing Styles:</strong></p>
                <ul>
                <li><strong>In-Ear</strong> models slip into your ear much like an earbud. You don't have to worry about tubes or fitting the unit behind your ear. It can be more comfortable for those who wear glasses.</li>
                <li><strong>Behind the Ear</strong> models are worn behind the ear and a tube connects to a tip that is placed inside your ear. You can use different kinds of tips and tubes to increase the amplification of the sounds you hear.</li>
                </ul>
                <h2>Hand-Held/Body Worn</h2>
                <p>The larger size of the hand-held/body worn personal amplifiers allow them to amplify sound up to 60dB, louder than the more discreet in-ear personal amplifiers. They are also less expensive.</p>
                <p>The base unit (which is held or clipped to your clothing) picks up sound through the built-in microphone, amplifies it, and sends it through headphones, earphones or a neckloop.</p>
                <p><strong>Wearing Styles:</strong></p>
                <ul>
                <li><strong>Headphones</strong> can be used with or without hearing aids. These come in single or double sided options.</li>
                <li><strong>Earphones</strong> can be used without hearing aids. These come in single or double sided options.</li>
                <li><strong>Neckloops</strong> are for those with t-coil enabled hearing aids (link to t-coil page). It is worn comfortably around the neck, and sends sounds directly to the hearing aid.</li>
                </ul>
                <h2>What Else Should I Consider?</h2>
                <p><strong>Microphones</strong></p>
                <p>All of the personal amplifiers come with one or both of these types of microphones: directional and omnidirectional. Omnidirectional picks up sound from every direction, while directional allows you to focus in on a specific sound source, which can be really helpful in noisy environments.</p>
                <p>Microphones can be either internal or external. Internal microphones are more discreet whereas external microphones tend to pick up more sound.</p>
                <p><strong>Batteries</strong></p>
                <p>Our personal amplifiers use either standard, rechargeable or hearing aid batteries. Many people prefer rechargeable batteries, especially if they have dexterity issues that would make changing batteries difficult. Some amplifiers come with standard batteries, but have the option of purchasing rechargeable.</p>
                <p><strong>Balance Control</strong></p>
                <p>This feature allows you to adjust the volume of the right or left ear independently. This can be beneficial if you experience more hearing loss in one ear over the other.</p>
                <p><button class="btn btn-brand" [routerLink]="['helpful-resources']">Shop Personal Amplifiers</button></p></div>
            </div>`;
        this.show_pop_up(body);
    }
    show_pop_up(body) {
        this.modal.alert()
            .size('lg')
            .bodyClass('content-infomation')
            .showClose(true)
            .okBtn('Close')
            .footerClass('helpful-resources-footer')
            .isBlocking(true)
            .body(body)
            .open().then(x=>{
                this.document.body.classList.remove('modal-open');
            });
    }
}
