import {NgModule, Component} from '@angular/core';
import {RouterModule} from '@angular/router'; 
import { AboutUsComponent } from './about-us.component';
@NgModule({
  declarations: [AboutUsComponent],
  imports: [
    RouterModule.forChild([
      { path: '', component: AboutUsComponent, pathMatch: 'full' }
    ])
  ]
})
export class AboutUsModule {}
