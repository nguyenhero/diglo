import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AppState} from 'src/app/shared/services/api/services/appState.service';
@Component({
    selector: 'about-us',
    templateUrl: './about-us.component.html',
    styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent {
    display_card_info = 'hide-class';
    visible = false;
    constructor(private router: Router,private appState:AppState) {
        appState.publish(this.router.url);
    }
    show_card_info(id) {
        this.visible = true;
        let elements = document.getElementsByClassName("card-info show-class");
        while (elements.length)
            elements[0].className = 'card-info hide-class';
        document.getElementById(id).className = 'card-info show-class';
    }
    close_card_info(id) {
        this.visible = false;
        document.getElementById(id).className = 'card-info hide-class';
    }
}

