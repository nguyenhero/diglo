﻿import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { Product } from 'src/app/shared/services/api/models/product';
import { ProductService } from 'src/app/shared/services/api/services/product.service';
@Component({
    selector: 'productRelated',
    templateUrl: './productRelated.component.html',
    styleUrls: ['./productRelated.component.scss']
})
export class ProductRelatedComponent {
    productSKU: string;
    productRelateds: Product[] = [];
    public bufferSize: number = 0;
    public number: number = 0;
    constructor(private router: Router,
        private _routeParams: ActivatedRoute,
        private productService: ProductService) {
        this._routeParams.queryParams.subscribe(params => {
            this.productSKU = params['productSKU'];
            let operatetion = this.productService.getProductRelated(this.productSKU);
            operatetion.subscribe(result => {
                if (result) {
                    this.productRelateds = result;
                }
            }
            );
        });
    }
    chunks(size) {
        this.number = Math.ceil(this.productRelateds.length / size);
        return this.createRange(this.number);
    };
    createRange(number) {
        var items: number[] = [];
        for (var i = 1; i <= number; i++) {
            items.push(i);
        }
        return items;
    }
    prev() {
        if (this.bufferSize > 0 && this.bufferSize < this.number ) {
            this.bufferSize--;
        } else {
            this.bufferSize = 0;
        }
    }
    next() {
        if (this.bufferSize < this.number-1) {
            this.bufferSize++;
        } else {
            this.bufferSize = 0;
        }
    }
}
