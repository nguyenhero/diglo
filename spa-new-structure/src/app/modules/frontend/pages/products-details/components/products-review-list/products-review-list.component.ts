﻿import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { ProductReview } from 'src/app/shared/services/api/models/productReview';
import { ProductReviewService } from 'src/app/shared/services/api/services/productReview.service';
@Component({
    selector: 'products-review-list',
    templateUrl: './products-review-list.component.html',
    styleUrls: ['./products-review-list.component.scss']
})
export class ProductsReviewListComponent {
    productSKU: string;
    productReviews: ProductReview[] = [];
    start: number = 0;
    totalItems: number = 0;
    public bufferSize: number = 3;
    constructor(private router: Router,
        private _routeParams: ActivatedRoute,
        private productReviewService: ProductReviewService) {
        this._routeParams.queryParams.subscribe(params => {
            this.productSKU = params['productSKU'];
            this.get(this.start, this.bufferSize);
        });
    }
    get(start, end) {
        this.productReviewService.get(this.productSKU, start, end)
            .subscribe(result => {
                if (result) {
                    this.productReviews = this.productReviews.concat(result);
                    this.start = this.productReviews.length;
                    if (this.productReviews && this.productReviews.length > 0) {
                        this.totalItems = this.productReviews[0].overallCount;
                    }
                }
            });
    }
    loadMore() {
        this.get(this.start, this.bufferSize);
    }
}