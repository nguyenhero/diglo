﻿import {Component} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';  
import { Observable } from 'rxjs'; 
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { ProductReviewService } from 'src/app/shared/services/api/services/productReview.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { LoginModalContext, LoginModal } from 'src/app/modules/frontend/components/login/login.component';
@Component({
    selector: 'products-review',
    templateUrl: './products-review.component.html',
    styleUrls: ['./products-review.component.scss']
})
export class ProductsReviewComponent {
    productSKU: string;
    productDesc: string;
    productReviewForm: any;
    star: number = 0;
    currentUser: any;
    isLogin: boolean = false;
    constructor(private router: Router,
        public appState: AppState,
        public modal: Modal,
        private formBuilder: FormBuilder,
        private _routeParams: ActivatedRoute,
        private productReviewService: ProductReviewService,
        public authenticationService: AuthenticationService) {
        this.productReviewForm = this.formBuilder.group({
            'nickName': ['', [Validators.required]],
            'review': ['', [Validators.required]]
        });
        this._routeParams.queryParams.subscribe(params => {
            this.productSKU = params['productSKU'];
            this.productDesc = params['productDesc'];
        });
    }
    submit() {
        if (this.productReviewForm.dirty && this.productReviewForm.valid) {
            this.currentUser = this.authenticationService.getCurrentUser();
            if (this.currentUser && this.currentUser.custNo) {
                var data = {
                    'productSKU': this.productSKU,
                    'star': this.star,
                    'nickName': this.productReviewForm.value.nickName,
                    'review': this.productReviewForm.value.review,
                    'custNo': this.currentUser.custNo.toString()
                }
                let submitReviewsOperation: Observable<number>;
                submitReviewsOperation = this.productReviewService.save(data);
                // Subscribe to observable
                submitReviewsOperation.subscribe(
                    register => {
                        let msg = '';
                        if (register > 0) {
                            this.star = 0;
                            this.productReviewForm.reset();
                            msg = `<span class='successfully- message'>You have successfully submit review. Thank you.</span>`;
                        }
                        else msg = 'You have not successfully submit review';
                        // this.modal.alert()
                        //     .size('sm')
                        //     .showClose(true)
                        //     .isBlocking(true)
                        //     .body(msg)
                        //     .open();
                    });
            }
        } else {
            this.validateAllFormFields(this.productReviewForm);
        }
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
    show_request_login() {
        let context = new LoginModalContext('free-catalog-request');
        context.dialogClass = 'login-modal';
        context.isBlocking = true; // now its blocking.        
        let dialog = this.modal.open(LoginModal, { context: context });
        dialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                if (result) {
                    this.isLogin = true;
                    this.appState.publish('updateCurrentUser');     
                    this.submit();                                 
                }
            });
        });
    }
    validateSubmit() {
        let currentUser = this.authenticationService.getCurrentUser();
        if (currentUser) {
            this.submit();
        } else {
            this.show_request_login();
        }
    }
}
