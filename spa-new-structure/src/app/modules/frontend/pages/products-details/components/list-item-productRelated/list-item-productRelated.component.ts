﻿import {Product} from 'src/app/shared/services/api/models/product';
import {Router} from '@angular/router';
import { Component, ChangeDetectionStrategy, Input, NgZone, ChangeDetectorRef } from '@angular/core';
@Component({
    selector: 'list-item-product-related',
    templateUrl: './list-item-productRelated.component.html',
    styleUrls: ['./list-item-productRelated.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListItemProductRelatedComponent {
    @Input()
    item: Product;
    productRelated: number;
    constructor(private router: Router, private zone: NgZone, private changeDetectorRef: ChangeDetectorRef) {
        this.router = router;
        this.productRelated = this.router.url.indexOf('productDetails') > -1 ? 1 : 0;     
    }
    selectDetails(item) {
        let queryParams = { productSKU: item.productSKU, parent: item.color ? true : false };
        this.router.navigate(['productDetails'], { queryParams: queryParams });
        return false;
    }
}
