﻿import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ProductAdditionInfoService} from 'src/app/shared/services/api/services/productAdditionInfo.service';
import {ProductAdditionInfo} from 'src/app/shared/services/api/models/productAdditionInfo';
@Component({
    selector: 'product-addition-info',
    templateUrl: './product-addition-info.component.html',
    styleUrls: ['./product-addition-info.component.scss']
})
export class ProductAdditionInfoComponent {
    productAdditionInfos: ProductAdditionInfo[];
    productSKU: string;
    constructor(private _routeParams: ActivatedRoute,
                private productAdditionInfoService: ProductAdditionInfoService,
    ) {
        this._routeParams.queryParams.subscribe(params => {
            this.productSKU = params['productSKU'];
        });
        this.productAdditionInfoService.get(this.productSKU).subscribe(result => {
            if (result && result.length > 0) {
                this.productAdditionInfos = result;
            }
        });
    }
}
