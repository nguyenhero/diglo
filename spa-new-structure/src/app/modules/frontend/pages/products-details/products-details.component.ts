﻿declare var affirm: any;
import {Component, Inject, NgZone} from '@angular/core';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms'
import {Router, ActivatedRoute} from '@angular/router';
import {DOCUMENT, DomSanitizer} from '@angular/platform-browser';    //scroll
import { Product } from 'src/app/shared/services/api/models/product';
import { User } from 'src/app/shared/services/api/models/user';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
import { WishListService } from 'src/app/shared/services/api/services/wishList.service';
import { ProductDetailsService } from 'src/app/shared/services/api/services/productDetails.service';
import { ProductsImageMobiContext, ProductsImageMobi } from './components/products-image-mobi/products-image-mobi.component';
import { AddOrderModalContext, AddOrderModal } from '../../components/add-order/add-order.component';
import { LoginModalContext, LoginModal } from '../../components/login/login.component';
@Component({
    selector: 'products',
    templateUrl: './products-details.component.html',
    styleUrls: ['./products-details.component.scss']
})
export class ProductDetailsComponent {
    affirm: any;
    public product: Product;
    public products: Product[];
    private productSKU: string;
    productSpeCollapse: string = 'glyphicon glyphicon-plus';
    productReviewCollapse: string = 'glyphicon glyphicon-plus';
    productInfoCollapse: string = 'glyphicon glyphicon-plus';
    url: any;
    counterValue: number = 0;
    scaleX: number = 1;
    scaleY: number = 1;
    transformOrigin: string = 'unset';
    SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
    avatars = [];
    zoomProduct: string = 'hidden';
    contents: string = 'visible';
    currentUser: User;
    isLogin: boolean = false;
    showAddToCart: boolean = false;
    availabilities: string[] = ['Coming Soon', 'Discontinued', 'Temporarily out of stock'];
    productForm: any;
    inValidQty: boolean = false;
    isReview: boolean = true;
    displayProductSpec: string;
    displayReview: string;
    displayProductInfo: string;
    constructor(private zone: NgZone,
        public modal: Modal,
        @Inject(DOCUMENT) private document,
        private sanitizer: DomSanitizer, private router: Router,
        public appState: AppState,
        private _routeParams: ActivatedRoute,
        private formBuilder: FormBuilder, public authenticationService: AuthenticationService,
        public wishListService: WishListService,
        private productDetailsService: ProductDetailsService) {
        this._routeParams.queryParams.subscribe(params => {
            this.reset();
            this.productSKU = this.getParameterByName('productSKU');
            let parent = this.getParameterByName('parent');
            this.productForm = this.formBuilder.group({
                'qty': ['', [Validators.required]]
            });
            this.productForm.controls['qty'].setValue(1);
            let operation = this.productDetailsService.get(this.productSKU, parent);
            operation.subscribe(result => {
                this.zone.run(() => { // <== added
                    if (result && result.length > 0) {
                        this.products = result;
                        this.product = result[0];
                        this.product.qty = 1;
                        this.showAddToCart = !(this.availabilities.indexOf(this.product.availability) > 0);
                        this.createAvatars();
                        if (this.product.media) {
                            this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.product.media);
                        }
                    }
                });
            });
        });
        appState.publish(this.router.url);
        appState.event.subscribe((data) => {
            if (data == 'addWishListBack') {
                this.addToWishList();
            }
        });

    }
    learnMore() {
        affirm.ui.openModal();
    }
    reset() {
        this.product = null;
        this.products = [];
        this.avatars = [];
        this.transformOrigin = 'unset';
        this.SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
        this.zoomProduct = 'hidden';
        this.contents = 'visible';
        this.showAddToCart = false;
        this.scaleX = 1;
        this.scaleY = 1;
        this.counterValue = 0;
        this.productSpeCollapse = 'glyphicon glyphicon-plus';
        this.productReviewCollapse = 'glyphicon glyphicon-plus';
        this.productInfoCollapse = 'glyphicon glyphicon-plus';
    }
    changeImage2() {
        let temp = this.product.image1;
        this.product.image1 = this.product.image2;
        this.product.image2 = temp;
        this.showAddToCart = !(this.availabilities.indexOf(this.product.availability) > 0);
        this.product.qty = 1;
        this.productForm.controls['qty'].setValue(1);
    }
    changeImage3() {
        let temp = this.product.image1;
        this.product.image1 = this.product.image3;
        this.product.image3 = temp;
        this.showAddToCart = !(this.availabilities.indexOf(this.product.availability) > 0);
        this.product.qty = 1;
        this.productForm.controls['qty'].setValue(1);
    }
    changeImage4() {
        let temp = this.product.image1;
        this.product.image1 = this.product.image4;
        this.product.image4 = temp;
        this.showAddToCart = !(this.availabilities.indexOf(this.product.availability) > 0);
        this.product.qty = 1;
        this.productForm.controls['qty'].setValue(1);
    }
    changeProduct(item) {
        this.product = item;
        this.product.qty = 1;
        this.productForm.controls['qty'].setValue(1);
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.product.media);
        this.showAddToCart = !(this.availabilities.indexOf(this.product.availability) > 0);
    }
    getProductReviewCollapse() {
        if (this.productReviewCollapse == 'glyphicon glyphicon-plus') {
            this.productReviewCollapse = 'glyphicon glyphicon-minus';
            this.productSpeCollapse = 'glyphicon glyphicon-plus';
            this.productInfoCollapse = 'glyphicon glyphicon-plus';
            this.displayReview = this.displayReview == 'block' ? 'none' : 'block';
            this.displayProductSpec = 'none';
            this.displayProductInfo = 'none';
        } else {
            this.productReviewCollapse = 'glyphicon glyphicon-plus';
            this.displayReview = 'none';
        }
    }
    getProductSpeCollapse() {
                if (this.productSpeCollapse == 'glyphicon glyphicon-plus') {
            this.productSpeCollapse = 'glyphicon glyphicon-minus';
            this.productReviewCollapse = 'glyphicon glyphicon-plus';
            this.displayProductSpec = this.displayProductSpec == 'block' ? 'none' : 'block';
            this.displayReview = 'none';
            this.displayProductInfo = 'none';
        } else {
            this.productSpeCollapse = 'glyphicon glyphicon-plus';
            this.displayProductSpec = 'none';
        }
    }
    getProductInfoCollapse() {
        if (this.productInfoCollapse == 'glyphicon glyphicon-plus') {
            this.productInfoCollapse = 'glyphicon glyphicon-minus';
            this.productReviewCollapse = 'glyphicon glyphicon-plus';
            this.productSpeCollapse = 'glyphicon glyphicon-plus';
            this.displayProductInfo = this.displayProductInfo == 'block' ? 'none' : 'block';
            this.displayProductSpec = 'none';
            this.displayReview = 'none';
        } else {
            this.productInfoCollapse = 'glyphicon glyphicon-plus';
            this.displayProductInfo = 'none';
        }
    }
    addToWishList() {
        if (this.productForm.valid && this.inValidQty == false) {
            this.currentUser = this.authenticationService.getCurrentUser();
            if (this.currentUser) {
                let request = {
                    'CustNo': this.currentUser.custNo,
                    'ProductSKU': this.product.productSKU,
                    'WishItemQty': this.product.qty,
                    'Status': '1',
                    'Email': this.currentUser.custEmail,
                    'UpdateUser': this.currentUser.custFirstName + ' ' + this.currentUser.custLastName
                };
                let opetation = this.wishListService.add(request);
                opetation.subscribe(
                    result => {
                        let msg = result == 0
                            ? `You have not added successfully  wish list. Thank you.`
                            : `You have added successfully wish list. Thank you.`;
                        this.modal.alert()
                            .size('sm')
                            .isBlocking(true)
                            .showClose(true)
                            .body(msg)
                            .open().then(x=>{
                                this.document.body.classList.remove('modal-open');
                            });
                    }
                );
            } else {
                this.show_request_login();
            }
        } else {
            this.validateAllFormFields(this.productForm);
        }
    }
    decrement(product) {
        let qty = product.qty - 1;
        if (qty <= 0) return;
        product.qty = qty;
        this.productForm.controls['qty'].setValue(qty);
    }
    increment(product) {
        let qty = product.qty + 1;
        product.qty = qty;
        this.productForm.controls['qty'].setValue(qty);
        this.inValidQty = false;
    }
    mouseMove(e) {
        let element = document.getElementById('photo');
        if (e && element) {
            this.transformOrigin = ((e.clientX - element.offsetLeft) / element.clientWidth) * 100 + '% ' + ((e.offsetY - element.offsetTop) / element.clientHeight) * 100 + '%';
            this.scaleX = 1.5;
            this.scaleY = 1.5;
        }
    }
    mouseOut() {
        this.scaleX = 1;
        this.scaleY = 1;
    }
    mouseEnter() {
        let context = new ProductsImageMobiContext(this.product, this.products);
        let dialog = this.modal.open(ProductsImageMobi, { context: context });
    }
    zoomIn(event) {
        var element = document.getElementById("overlay");
        if (event && element) {
            element.style.display = "inline-block";
            var img = document.getElementById("photo");
            if (img) {
                var posX = event.offsetX ? (event.offsetX) : event.pageX - img.offsetLeft;
                var posY = event.offsetY ? (event.offsetY) : event.pageY - img.offsetTop;
                element.style.backgroundPosition = (-posX * 2) + "px " + (-posY * 4) + "px";
            }
        }
    }
    zoomOut() {
        var element = document.getElementById("overlay");
        element.style.display = "none";
    }
    swipe(currentIndex: number, action = this.SWIPE_ACTION.RIGHT) {
        if (currentIndex > this.avatars.length || currentIndex < 0) return;
        let nextIndex = 0;
        // next
        if (action === this.SWIPE_ACTION.LEFT) {
            const isLast = currentIndex === this.avatars.length - 1;
            nextIndex = isLast ? 0 : currentIndex + 1;
        }
        // previous
        if (action === this.SWIPE_ACTION.RIGHT) {
            const isFirst = currentIndex === 0;
            nextIndex = isFirst ? this.avatars.length - 1 : currentIndex - 1;
        }
        // toggle avatar visibility
        this.avatars.forEach((x, i) => x.visible = (i === nextIndex));
    }
    changeProductMobile(item) {
        this.product = item;
        this.avatars = [];
        this.createAvatars();
        this.showAddToCart = !(this.availabilities.indexOf(this.product.availability) > 0);
    }
    createAvatars() {
        if (this.product.image1) {
            let image1 = {
                name: 'image1',
                image: this.product.image1,
                visible: true
            }
            this.avatars.push(image1);
        }
        if (this.product.image2) {
            let image2 = {
                name: 'image2',
                image: this.product.image2,
                visible: false
            }
            this.avatars.push(image2);
        }
        if (this.product.image3) {
            let image3 = {
                name: 'image3',
                image: this.product.image3,
                visible: false
            }
            this.avatars.push(image3);
        }
        if (this.product.image4) {
            let image4 = {
                name: 'image4',
                image: this.product.image4,
                visible: false
            }
            this.avatars.push(image4);
        }
    }
    changeQty(value) {
        if (value) {
            this.product.qty = parseInt(value);
            if (this.product.qty > 0) {
                this.productForm.controls['qty'].setValue(this.product.qty);
                this.validateAllFormFields(this.productForm);
                this.inValidQty = false;
            } else {
                this.inValidQty = true;
            }
        }
    }
    showAddOrderDialog() {
        if (this.productForm.valid && this.inValidQty == false) {
            let context = new AddOrderModalContext(this.product);
            context.dialogClass = 'add-order-modal';
            context.isBlocking = true;
            this.modal.open(AddOrderModal, { context: context });
        }
        else {
            this.validateAllFormFields(this.productForm);
        }
    }
    show_request_login() {
        let context = new LoginModalContext('free-catalog-request');
        context.dialogClass = 'login-modal';
        context.isBlocking = true;
        let dialog = this.modal.open(LoginModal, { context: context });
        dialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                if (result) {
                    this.isLogin = true;
                    this.appState.publish('addWishList');
                }
            });
        });
    }
    productDepartmentLink() {
        let queryParams = {
            departmentId: this.product.departmentID,
            title: this.product.departmentDesc
        };
        this.router.navigate(['allproducts'], { queryParams: queryParams });
    }
    productClassLink() {
        let queryParams = {
            departmentId: this.product.departmentID,
            classId: this.product.classID,
            title: this.product.classDesc
        };
        this.router.navigate(['allproducts'], { queryParams: queryParams });
    }
    productSubClassLink() {
        let queryParams = {
            departmentId: this.product.departmentID,
            classId: this.product.classID,
            subclassId: this.product.subclassID,
            title: this.product.subclassDesc
        };
        this.router.navigate(['allproducts'], { queryParams: queryParams });
    }
    getParameterByName(name) {
        let url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
}

