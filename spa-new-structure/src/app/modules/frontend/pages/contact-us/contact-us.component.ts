import { Component, NgZone, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Router} from '@angular/router'; 
import { ValidationService } from 'src/app/shared/services/api/services/validation.service';
declare var google: any;
@Component({
    selector: 'contact-us',
    templateUrl: './contact-us.component.html',
    styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
    public latitude: number;
    public longitude: number;
    public zoom: number;
    ContactUsForm: any;

    constructor(private formBuilder: FormBuilder, private router: Router) {
        window.scrollTo(0, 0);
        // appState.publish(this.router.url);

        this.ContactUsForm = this.formBuilder.group({
            'name': ['', Validators.required],
            'email': ['', [Validators.required, ValidationService.emailValidator]],
            'telephone': ['', Validators.required],
            'comment': ['', Validators.required],
        });

    }
    ngOnInit() {                    
        var address = '15155 Technology Drive Eden Prairie, MN 55344';
        var map = new google.maps.Map(document.getElementById('map'), {
            mapTypeId: google.maps.MapTypeId.TERRAIN,
            zoom: 16
        });

        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({
            'address': address
        },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map
                    });
                    map.setCenter(results[0].geometry.location);
                }
            });


    }


   
}

