﻿import { Component, OnDestroy, NgZone, ChangeDetectorRef, Inject } from '@angular/core'; 
import { Product } from 'src/app/shared/services/api/models/product';
import { FilterGroup } from 'src/app/shared/services/api/models/filterGroup';
import { Router, ActivatedRoute } from '@angular/router';
import { AppState } from 'src/app/shared/services/api/services/appState.service';
import { DOCUMENT } from '@angular/platform-browser';
import { ProductService } from 'src/app/shared/services/api/services/product.service';
import { FilterGroupService } from 'src/app/shared/services/api/services/filterGroup.service';
import { Subscription } from 'rxjs';
import * as rxjsNumeric from "rxjs/util/isNumeric"


@Component({
    selector: 'search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnDestroy {
    private subscription: Subscription;

    public currentPage: number = 1;
    public lastPage: number = 0;
    public totalItems: number = 0;
    public readyTotalItems: boolean = false;
    public maxSize: number = 5;
    public buffer: Product[];
    public bufferSize: number = 12;
    loading: boolean = false;
    lastScrollTop: number = 0;
    start: number = 0;
    blockHeight: number = 0;
    direction: string = '';
    sortBy: number = 4;
    sortByDropdown: string = 'hidden';
    filter: string = 'hidden';
    departmentId: string;
    classId: string;
    subClassId: string;
    q: string = '';
    qDisp: string = '';
    newArrivalInd = '';
    fromPrice: number = 0;
    toPrice: number = 0;

    filterGroups: FilterGroup[];
    departments: any = [];
    tempGroups: any = [];
    filterGroupId: number = 0;
    filterDetails: any = [];
    tickFilterGroup: string = 'hidden';
    page: number = 1;
    limit: number = 5;
    title: string = "All Products";
    category: boolean = false;
    showPrice: boolean = false;
    productRelated: number;

    constructor(private zone: NgZone,
        private cdr: ChangeDetectorRef,
        private router: Router,
        private routeParams: ActivatedRoute,
        public appState: AppState, @Inject(DOCUMENT) private document,
        private productService: ProductService,
        private filterGroupService: FilterGroupService) {
        this.router = router;
        this.productRelated = this.router.url.indexOf('product-details') > -1 ? 1 : 0;

        appState.publish(this.router.url);
        this.buffer = [];
        this.start = 0;
        this.sortBy = 0;

        this.subscription = this.routeParams.queryParams.subscribe(
            (queryParam: any) => {
                this.buffer = [];
                this.start = 0;
                this.sortBy = 0;
                this.filterDetails = [];
                this.showPrice = false;
                this.departmentId = queryParam['departmentId'];
                this.classId = queryParam['classId'];
                this.subClassId = queryParam['subClassId'];
                this.q = queryParam['q'];
                this.qDisp = this.q.replace(/''/g, "'");
                this.title = queryParam['title'] ? queryParam['title'] : 'Search Result for ';
                this.newArrivalInd = queryParam['newArrivalInd'] ? 'new'
                    : (queryParam['clearance'] ? 'clearance' : '');
                if (!queryParam['category']) {
                    this.departments = [];
                }
                this.loadData();
                this.getFilterGroup();
                //this.getCategory();
            }
        );
    }

    subclassFillter(departmentID, classItem, subclass) {
        this.departmentId = departmentID;
        this.classId = classItem.classID;
        this.subClassId = subclass.subclassID;
        this.link(subclass);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    public setPage(pageNo: number): void {
        this.currentPage = pageNo;
    }

    public pageChanged(event: any): void {
        if (event.direction) {
            this.direction = event.direction;
        }
        let request = this.createRequest();
        this.get(request);

    };

    public get(request) {
        this.loading = true;
        let operatetion = this.productService.get(request, this.filterDetails);
        operatetion.subscribe(result => {
            this.zone.run(() => {
                if (result) {
                    this.buffer = this.buffer.concat(result);
                    this.start = this.buffer.length;
                    this.loading = false;
                    this.cdr.detectChanges();
                }
            });
        });
        if (!this.readyTotalItems) {
            let getCountOpt = this.productService.getCount(request, this.filterDetails);
            getCountOpt.subscribe(result => {
                this.zone.run(() => {
                    this.totalItems = result;
                    this.loading = false;
                });
            });
        }
    }

    public getFilterGroup() {
        let operatetion = this.filterGroupService.get(this.departmentId, this.classId, this.subClassId, this.newArrivalInd, '', this.q);
        operatetion.subscribe(result => {
            if (result) {
                this.filterGroups = result;
                this.cdr.detectChanges();
            }
        });

    }

    public getCategory() {
        if (!this.departmentId && !this.classId && !this.subClassId) {
            let operatetion = this.productService.getCategory();
            operatetion.subscribe(result => {
                if (result) {
                    this.departments = result;
                }
            });
        }

    }

    loadMore(value) {
        this.limit = this.limit + 5;
    }

    chooseFilter(filterGroupId) {
        this.filterGroupId = this.filterGroupId == filterGroupId ? 0 : filterGroupId;
    }

    filterTick(filterDetail) {
        this.fromPrice = 0;
        this.toPrice = 0;
        let add = false;
        if (this.filterDetails != null && this.filterDetails.length > 0) {
            for (let i = 0; i < this.filterDetails.length; i++) {
                let item = this.filterDetails[i];
                if (item.filterDetailId == filterDetail.filterDetailId) {
                    this.filterDetails.splice(i, 1);
                    add = true;
                    break;
                }
            }
        }
        if (!add) {
            this.filterDetails.push(filterDetail);
        }
        this.buffer = [];
        this.start = 0;
        this.loadData();
    }

    filterMobiTick(filterDetail) {
        this.fromPrice = 0;
        this.toPrice = 0;

        let add = false;
        if (this.filterDetails != null && this.filterDetails.length > 0) {
            for (let i = 0; i < this.filterDetails.length; i++) {
                let item = this.filterDetails[i];
                if (item.filterDetailId == filterDetail.filterDetailId) {
                    this.filterDetails.splice(i, 1);
                    add = true;
                    break;
                }
            }
        }
        if (!add) {
            this.filterDetails.push(filterDetail);
        }

    }

    clearFilterByGroupId(filterGroup, moblie = false) {
        this.fromPrice = 0;
        this.toPrice = 0;
        this.showPrice = false;
        filterGroup.filterDetails.forEach((item, index) => {
            item.show = false;
        });
        if (this.filterDetails != null && this.filterDetails.length > 0) {
            var i = this.filterDetails.length;
            while (i--) {
                if (this.filterDetails[i].filterGroupId == filterGroup.filterGroupId)
                    this.filterDetails.splice(i, 1);
            }
        }
        if (!moblie) {
            this.buffer = [];
            this.start = 0;
            this.loadData();
        }
    }

    goPriceFilter(filterGroup, fromPrice, toPrice) {
        filterGroup.filterDetails.forEach((item, index) => {
            item.show = false;
        });
        this.filterDetails = [];
        if (!rxjsNumeric.isNumeric(fromPrice) || !rxjsNumeric.isNumeric(toPrice)) {
            alert('Please enter number!');
            return;
        }

        let filterDetail = {
            filterGroupId: filterGroup.filterGroupId,
            filterDetailId: 0,
            tableName: 'Products',
            searchcolumn: 'webprice',
            operation: 'between',
            filterDetailValue: fromPrice + ' and ' + toPrice
        };
        let add = false;
        if (this.filterDetails != null && this.filterDetails.length > 0) {
            for (let i = 0; i < this.filterDetails.length; i++) {
                let item = this.filterDetails[i];
                if (item.filterDetailId == filterDetail.filterDetailId) {
                    item.tableName = filterDetail.tableName;
                    item.searchcolumn = filterDetail.searchcolumn;
                    item.operation = filterDetail.operation;
                    item.filterDetailValue = filterDetail.filterDetailValue;
                    add = true;
                    break;
                }
            }
        }
        if (!add) {
            this.filterDetails.push(filterDetail);
        }

        this.buffer = [];
        this.start = 0;
        this.loadData();
        this.filterDetails = [];
    }

    viewProducts() {
        this.buffer = [];
        this.start = 0;
        this.loadData();
    }

    loadData() {
        let request = this.createRequest();
        this.get(request);
    }

    changeSortBy(value) {
        this.readyTotalItems = true;
        this.sortBy = value;
        this.start = 0;
        this.buffer = [];
        //this.totalItems = 0;
        let request = this.createRequest();
        this.sortByDropdown = 'hidden';
        this.get(request);
        this.readyTotalItems = false;
    }

    createRequest() {
        let loadIteamTotal = this.bufferSize * this.maxSize;
        let request =
            {
                'sortBy': this.sortBy,
                'start': this.start,
                'end': loadIteamTotal,
                'departmentId': this.departmentId,
                'classId': this.classId,
                'subClassId': this.subClassId,
                'q': this.q,
                'newArrivalInd': this.newArrivalInd
            };

        return request;
    }

    showSortByDropdown() {
        if (this.sortByDropdown == 'hidden') {
            this.sortByDropdown = 'visible';
            this.filter = 'hidden';
        } else {
            this.sortByDropdown = 'hidden';
        }
    }

    showFilter() {
        if (this.filter == 'hidden') {
            this.filter = 'visible';
            this.sortByDropdown = 'hidden';
        } else {
            this.filter = 'hidden';
        }
    }

    clear() {
        if (this.filterGroups && this.filterGroups.length > 0) {
            this.filterGroups.forEach((item, index) => {
                this.clearFilterByGroupId(item, true);
            });
        }
    }

    link(subclass) {
        if (subclass.subclassDesc == 'Clearance') {
            this.clearanceProductLink(subclass);
        }
        else {
            this.productLink(subclass);
        }
    }
    newProductLink(departmentID, subclass) {
        let queryParams = {
            departmentId: departmentID,
            title: subclass.subclassDesc,
            category: true,
            newArrivalInd: 'Y'
        };
        this.router.navigate(['all-products'], { queryParams: queryParams });
    }

    clearanceProductLink(subclass) {
        let queryParams = {
            departmentId: this.departmentId,
            title: subclass.subclassDesc,
            category: true,
            clearance: 'Y'
        };
        this.router.navigate(['all-products'], { queryParams: queryParams });
    }

    productLink(subclass) {
        let queryParams = {};
        if (subclass.subclassDesc == 'All Phone' ||
            subclass.subclassDesc == 'All Education' ||
            subclass.subclassDesc == 'All Alarms' ||
            subclass.subclassDesc == 'All Devices') {
            queryParams = {
                departmentId: this.departmentId,
                title: subclass.subclassDesc,
                category: true
            };

        } else {
            queryParams = {
                departmentId: this.departmentId,
                classId: subclass.classID,
                subClassId: subclass.subclassID,
                title: subclass.subclassDesc,
                category: true
            };

        }
        this.router.navigate(['allproducts'], { queryParams: queryParams });
    }

    selectDetails(item) {
        let queryParams = { productSKU: item.productSKU, parent: item.color ? true : false };
        this.router.navigate(['productDetails'], { queryParams: queryParams });
        return false;
    }

    getParameterByName(name) {
        let url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
}

