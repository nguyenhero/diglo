﻿import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {DialogRef, ModalComponent, CloseGuard} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Router} from '@angular/router';  
import {DOCUMENT } from '@angular/platform-browser';    // scroll
import { Product } from 'src/app/shared/services/api/models/product';
import { CardProductService } from 'src/app/shared/services/api/services/cardProduct.service';
import { PromoCodeService } from 'src/app/shared/services/api/services/promoCode.service';
import { OrderService } from 'src/app/shared/services/api/services/order.service';
import { OrderLogicService } from 'src/app/shared/services/api/services/orderLogic.service';
import { AuthenticationService } from 'src/app/shared/services/api/services/authentication.service';
export class AddOrderModalContext extends BSModalContext {
    constructor(public product: Product) {
        super();
    }
}
@Component({
    selector: 'add-order',
    templateUrl: './add-order.component.html',
    styleUrls: ['./add-order.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class AddOrderModal implements CloseGuard, ModalComponent<AddOrderModalContext> {
    context: AddOrderModalContext;
    public products: Product[];
    totals: number = 0;
    sub_totals: number = 0;
    countProduct: number = 0;
    cartPrice: number = 0;
    promoCode: string = '';
    promoCodeValid: number = -1;
    secureToken: string;
    promotionCodeError: boolean = false;
    promotionCodeErrorMsg: string;
    disabled: boolean = false;
    moneyOver = 5000;
    constructor(private router: Router, public modal: Modal,
        public dialog: DialogRef<AddOrderModalContext>, private formBuilder: FormBuilder,
        private cardProductService: CardProductService,
        private promoCodeService: PromoCodeService,
        private orderService: OrderService,
        private orderLogicService: OrderLogicService,
        @Inject(DOCUMENT) private document,
        private authenticationService: AuthenticationService
    ) {
        this.context = dialog.context;
        if (this.context.product) {
            this.orderLogicService.addToCart(this.context.product);
        }

        let order = this.orderLogicService.getOrder();
        if (order) {
            this.products = order.products;
            this.sub_totals = this.orderLogicService.getSubTotal(order);
            this.countProduct = this.orderLogicService.countProducts();
            this.promoCode = order.promoCode;
            this.promoCodeValid = this.promoCode ? 1 : -1;
            this.totals = this.sub_totals;
        }
        dialog.setCloseGuard(this);
        this.router = router;
    }
    linkCheckoutPage() {
        if (this.sub_totals > 0) {
            if (this.sub_totals <= this.moneyOver) {
                this.close();
                if (this.products) {
                    this.products.forEach(item => {
                        if (item.qty <= 0) {
                            this.removeProduct(item);
                        }
                    });
                }
                this.router.navigate(['checkout-order']);
            } else {
                this.showShippingChargesOver(this.sub_totals);
            }
        } else {
            this.showValidateOrder();
        }
    }
    close() {
        this.dialog.close();
        this.document.body.classList.remove('modal-open'); //scroll
    }
    beforeDismiss(): boolean {
        return true;
    }
    changeQty(product, value) {
        if (value) {
            product.required = false;
            product.qty = parseInt(value);
            if (product.qty > 0) {
                product.validQty = false;
            } else {
                product.validQty = true;
            }
            this.orderLogicService.updateToCart(product);
            let order = this.orderLogicService.getOrder();
            this.countProduct = this.orderLogicService.countProducts();
            if (order) {
                this.sub_totals = this.orderLogicService.getSubTotal(order);
                this.showShippingChargesOver(this.sub_totals);
            }
        } else {
            product.required = true;
        }
    }
    removeProduct(product) {
        this.orderLogicService.remove(product);
        let order = this.orderLogicService.getOrder();
        if (order) {
            this.products = order.products;
            this.countProduct = this.orderLogicService.countProducts();
            this.sub_totals = this.orderLogicService.getSubTotal(order);
        } else {
            this.products = [];
            this.sub_totals = 0;
            this.totals = 0;
        }
    }
    applyPromoCode() {
        if (this.promoCode) {
            let checkPromoCodeOperation: Observable<number>;
            let self = this;
            checkPromoCodeOperation = this.promoCodeService.checkPromoCode(this.promoCode);
            checkPromoCodeOperation.subscribe(result => {
                this.promoCodeValid = result;
                if (this.promoCodeValid == 0) {
                    this.promotionCodeError = true;
                    this.promotionCodeErrorMsg = 'Coupon code ' + this.promoCode + ' is not valid';
                }
                this.setPromotionCode();
                return;
            });
        } else {
            this.setPromotionCode();
            this.promotionCodeError = true;
            this.promotionCodeErrorMsg = 'Please enter your promotion code.';
            this.promoCodeValid = -1;
        }
    }
    setPromotionCode() {
        let promoCode = this.promoCodeValid == 0 ? '' : this.promoCode;
        this.orderLogicService.updatePromoCode(promoCode);
        let order = this.orderLogicService.getOrder();
        if (order.promoCode) {
            this.promoCodeValid == 1;
        } else {
            this.promoCodeValid == 0;
        }
        this.sub_totals = this.orderLogicService.getSubTotal(order);
        this.totals = this.sub_totals;
    }
    getProductsPriceByPromotionCode(products) {
        let total = 0;
        if (products) {
            for (let i = 0; i < products.length; i++) {
                if (products[i].cartPrice != '-1.00') {
                    total += products[i].cartPrice * products[i].qty;
                } else {
                    total += products[i].webPrice * products[i].qty;
                }
            }
        }
        this.totals = total;
    }
    blurpromoCode() {
        this.promotionCodeError = false;
        this.promotionCodeErrorMsg = '';
    }
    decrement(product) {
        let qty = product.qty - 1;
        if (qty <= 0) return;
        product.qty = qty;
        this.changeQty(product, product.qty);
    }
    increment(product) {
        product.qty++;
        this.changeQty(product, product.qty);
    }
    showShippingChargesOver(total) {
        if (total > this.moneyOver) {
            this.disabled = false;
            let msg = `To order online your order has to be under $5,000.00. Please call our customer service: 800-825-6758 if your order is above $5,000.00`;
            this.modal.alert()
                .size('sm')
                .isBlocking(true)
                .showClose(true)
                .body(msg)
                .open().then(x=>{
                    this.document.body.classList.remove('modal-open');  //scroll
                });
        } else {
            this.disabled = true;
        }
    }
    showValidateOrder() {
        if (this.sub_totals <= 0) {
            this.disabled = true;
            let msg = `Please enter your quatity.`;
            this.modal.alert()
                .size('sm')
                .isBlocking(true)
                .showClose(true)
                .body(msg)
                .open().then(x=>{
                    this.document.body.classList.remove('modal-open');  //scroll
                });
        } else {
            this.disabled = false;
        }
    }
    payPalExpressCheckout() {
        if (this.sub_totals > 0) {
            if (this.sub_totals <= this.moneyOver) {
                let data = {
                    'shippingAddress': null,
                    'amount': this.sub_totals
                };
                if (this.products) {
                    this.products.forEach(item => {
                        if (item.qty <= 0) {
                            this.removeProduct(item);
                        }
                    });
                }
                let peration = this.orderService.getPalPalExpressCheckoutTokenByRequest(data);
                peration.subscribe(data => {
                    this.dialog.close();
                    if (data.result == 0) {
                        this.orderLogicService.createForm(data.url, data.secureToken);
                    } else {
                        this.modal.alert()
                            .size('sm')
                            .isBlocking(true)
                            .showClose(true)
                            .body(data.errorMsg)
                            .open().then(x=>{
                                this.document.body.classList.remove('modal-open');  //scroll
                            });
                    }
                });
            } else {
                this.showShippingChargesOver(this.sub_totals);
            }
        } else {
            this.showValidateOrder();
        }
    }
}
