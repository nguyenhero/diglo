import {NgModule, Component} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ModalModule} from 'angular2-modal';
import {BootstrapModalModule} from 'angular2-modal/plugins/bootstrap';
import {AddOrderModal} from './add-order.component';
@NgModule({
  declarations: [
    AddOrderModal
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ModalModule.forRoot(),
    BootstrapModalModule   
  ],
  providers: [
  ],
  entryComponents: [
    AddOrderModal
  ]
})
export class AddOrderModule {}
