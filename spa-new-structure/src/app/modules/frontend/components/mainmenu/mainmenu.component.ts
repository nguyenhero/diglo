import {Component, ViewEncapsulation, OnInit, ElementRef} from '@angular/core';
import {Modal} from 'angular2-modal/plugins/bootstrap';
import {Department} from 'src/app/shared/services/api/models/department';
import {MenuService} from 'src/app/shared/services/api/services/menu.service';
import {AppState} from 'src/app/shared/services/api/services/appState.service';
import {OrderLogicService} from 'src/app/shared/services/api/services/orderLogic.service';
import {AuthenticationService} from 'src/app/shared/services/api/services/authentication.service';
import {User} from 'src/app/shared/services/api/models/user';
import {Router} from '@angular/router';
import { AddOrderModalContext, AddOrderModal } from '../add-order/add-order.component';
@Component({
    selector: 'main-menu',
    host: {
        '(document:click)': 'handleSearchClick($event)',
    },
    templateUrl: './mainmenu.component.html',
    styleUrls: ['./mainmenu.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class MainMenuComponent implements OnInit {
    menu: Department[];
    subMenuCollapse: string;
    navGroup: string;
    activeId: number;
    subActiveId: number = 0;
    show: boolean = false;    
    url: any;
    countProducts: number = 0;
    private ADD_ORDER_CARD: string = 'ADD_ORDER_CARD';
    currentUserStorage = localStorage['currentUser'];
    currentUser: User;
    /* Begin Search */
    showSearchMb: boolean = false;
    onSearchClick: boolean = false;
    querySearch: string  = '';
    itemListSearch: string[] = [];
    filteredListSearch: string[] = [];
    elementRefSearch: ElementRef;
    /* End Search */
    displayMenu: string;
    displayContact: string;
    displaySort: string;
    displayFilter: string;
    constructor( public modal: Modal, 
        public myElementSearch: ElementRef, 
        private orderLogicService: OrderLogicService,
        private router: Router, private authenticationService: AuthenticationService,
        // private suggestionSearchService: SuggestionSearchService,
        public menuService: MenuService, public appState: AppState) {
        this.elementRefSearch = myElementSearch;
        this.countProducts = this.orderLogicService.countProducts();
        this.navGroup = 'nav-group with-menu';
        appState.event.subscribe((data) => {
            this.url = data;
            switch (data) {
                case 'login':
                    this.currentUser = this.authenticationService.getCurrentUser();
                    if (this.currentUser) {
                        this.orderLogicService.addCartToUser();
                    } else {
                        localStorage.clear();
                    }
                    break;
                case 'logout':
                    this.countProducts = this.orderLogicService.countProducts();
                    break;
                case this.ADD_ORDER_CARD:
                    this.countProducts = this.orderLogicService.countProducts();
                    break;
            }
        });
    }
    linkFinished() {
        this.closeAllMenu();
    }
    showAddOrderDialog() {
        this.displayContact = 'none';
        this.displayMenu = 'none';
        let context = new AddOrderModalContext(null);
        context.dialogClass = 'add-order-modal';
        context.isBlocking = true;
        this.modal.open(AddOrderModal, { context: context });
    }
    subCollapse(item) {
        if (document.getElementById('department' + item.departmentID).className.indexOf('in') > 0) {
            item.classHtml = 'glyphicon glyphicon-plus';
        } else {   
            [].forEach.call(this.menu, function (el) {
                el.classHtml = 'glyphicon glyphicon-plus';
            });

            item.classHtml = 'glyphicon glyphicon-minus';
        }
    }
    closeMenu() {
        let body = document.getElementsByTagName("body");
        if (body && body.length > 0) {
            body[0].style.overflowY = "scroll";
        }   
        [].forEach.call(this.menu, function (item) {
            if (item && item.classHtml) {
                item.classHtml = 'glyphicon glyphicon-plus';
            }
        });
    }
    closeAllMenu() {
        this.displayMenu = 'none';
        this.displayContact = 'none';
        this.closeMenu();
    }
    openMenu(){
        this.displayContact = 'none';
        this.displayMenu = this.displayMenu == 'block'? 'none' : 'block';
    }
    openContact(){
        this.displayMenu = 'none';
        this.menu.forEach( x=>{
            x.show = false;
        });
        this.displayContact = this.displayContact == 'block'? 'none' : 'block';
    }
    openDepartemnt(departemnt){
        let show = !departemnt.show;
        this.menu.forEach( x=>{
            x.show = false;
        });
        departemnt.show = show;
    }
    ngOnInit() {
        this.menuService.get().subscribe(result => {
            if (result) {
                this.menu = result;
            }
        });
    }
    overMenu(id) {
        this.activeId = id;
    }
    leaveMenu() {
        this.activeId = 0;
    }
    // search
    searchProduct() {
        if (this.querySearch == null) {
            return false;
        }
        let qStr: string = this.querySearch;
        qStr = qStr.trim();
        qStr = qStr.replace(/\s+/g, ' ');

        if (qStr == "") {
            return false;
        }
        let queryParams = {
            q: qStr.replace(/'/g, "''")
        };
        this.querySearch = "";
        this.filteredListSearch = [];
        this.router.navigate(['search'], { queryParams: queryParams });
    }
    // show/hide search mobile
    showhideSearchMb() {
        this.showSearchMb = this.showSearchMb ? false : true;
    }
    // suggestion filter
    filterSearch() {}
    // suggestion select
    selectSearch(item) {
        this.querySearch = item;
        this.filteredListSearch = [];
        this.searchProduct();
    }
    // layout suggestion list
    showSuggestionItem(item) {
        let strDispTmp = this.querySearch.trim();
        return item.replace(strDispTmp, '<b>' + strDispTmp+'</b>');
    }
    // handle click event for search
    handleSearchClick(event) {
        var clickedElemSearch = event.target;
        var inside = false;
        do {
            if (clickedElemSearch === this.elementRefSearch.nativeElement) {
                inside = true;
            }
            clickedElemSearch = clickedElemSearch.parentNode;
        } while (clickedElemSearch);
        if (!inside || (inside && !this.onSearchClick)) {
            this.filteredListSearch = [];
            this.querySearch = "";
            this.showSearchMb = false;            
        }
        this.onSearchClick = false;
    }
    // check click on search
    checkInSearch() {
        this.onSearchClick = true;
    }
    filter(){
        this.displaySort = 'none';
        this.displayFilter = this.displayFilter == 'block'? 'none' : 'block';
    }
    sort(){
        this.displayFilter = 'none';
        this.displaySort = this.displaySort == 'block'? 'none' : 'block';
    }
}