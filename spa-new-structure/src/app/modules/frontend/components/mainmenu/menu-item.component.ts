import {Component, Input, Output, EventEmitter} from '@angular/core';
import {las} from 'src/app/shared/services/api/models/las';
import {Router} from '@angular/router';
import {AppState} from 'src/app/shared/services/api/services/appState.service';
@Component({
   selector: 'menu-item',
   templateUrl: './menu-item.component.html',
   styleUrls: ['./menu-item.component.scss']
})
export class ClassItemComponent {
    @Input()
    departmentId: number;
    @Input()
    item: las;
    @Output('linkFinished') linkFinished: EventEmitter<Object> = new EventEmitter<Object>();
    newArrivalInd = '';
    public constructor(private router: Router) { }
    link(subclass) {
        if (subclass.subclassDesc == 'Clearance') {
            this.clearanceProductLink(subclass);
        } else {
            this.productLink(subclass);
        }
        this.linkFinished.emit();
    }
    productLink(subclass) {
        let queryParams = {};
        if (subclass.subclassDesc == 'All Phone' ||
            subclass.subclassDesc == 'All Education' ||
            subclass.subclassDesc == 'All Alarms' ||
            subclass.subclassDesc == 'All Listening') {
            queryParams = {
                departmentId: this.departmentId,
                title: subclass.subclassDesc
            };
        } else {
            queryParams = {
                departmentId: this.departmentId,
                classId: subclass.classID,
                subClassId: subclass.subclassID,
                title: subclass.subclassDesc
            };
        }
        this.router.navigate(['allproducts'], { queryParams: queryParams });
    }
    newProductLink(subclass) {
        let queryParams = {
            departmentId: this.departmentId,
            title: subclass.subclassDesc,
            newArrivalInd: 'Y'
        };
        this.router.navigate(['allproducts'], { queryParams: queryParams });
        this.linkFinished.emit();
    }
    clearanceProductLink(subclass) {
        let queryParams = {
            departmentId: this.departmentId,
            title: subclass.subclassDesc,
            clearance: 'Y'
        };
        this.router.navigate(['allproducts'], { queryParams: queryParams });
    }
}
