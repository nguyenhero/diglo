﻿import {Component, Inject,ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {DOCUMENT} from '@angular/platform-browser';    //fixed popup scroll
import {DialogRef, ModalComponent, CloseGuard} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {ValidationService} from 'src/app/shared/services/api/services/validation.service';
import {AuthenticationService} from 'src/app/shared/services/api/services/authentication.service';
import {User} from 'src/app/shared/services/api/models/user';
import {AppState } from 'src/app/shared/services/api/services/appState.service';
import { RegisterModal } from '../register/register.component';
import { FreeCatalogRequestModalContext, FreeCatalogRequestModal } from '../../pages/home/components/free-catalog/free-catalog-request/free-catalog-request.component';
import { ForgotPasswordModal } from '../forgot-password/forgot-password.component';
export class LoginModalContext extends BSModalContext {
    constructor(public link: string = '') {
        super();
    }
}
@Component({
    selector: 'login-modal',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class LoginModal implements 
    CloseGuard, 
    ModalComponent<LoginModalContext> {
    userForm: any;
    context: LoginModalContext;
    public errorMsg = '';
    isExistEmail = true;
    isExistPassword = true;
    constructor(public dialog: DialogRef<LoginModalContext>, private formBuilder: FormBuilder, public modal: Modal,
        @Inject(DOCUMENT) private document, private authenticationService: AuthenticationService, 
        private router: Router, public appState: AppState) {
        this.context = dialog.context;
        dialog.setCloseGuard(this);
        this.userForm = this.formBuilder.group({
            'email': ['', [Validators.required, ValidationService.emailValidator]],
            'password': ['', [Validators.required, Validators.minLength(6), ValidationService.passwordValidator]]
        });
        this.userForm.valueChanges.subscribe((value) => {
            this.isExistEmail = true;
            this.isExistPassword = true;
        });
    }
    login() {
        if (this.userForm.dirty && this.userForm.valid) {

            this.authenticationService.login(this.userForm.value)
                .subscribe(
                data => {
                    let currentUser = null;
                    if (data) {
                        if (data.errorNumber == 100) this.isExistEmail = false;
                        else if (data.errorNumber == 101) this.isExistPassword = false;
                        else if (data.custEmail && data.custPassword) {
                            currentUser = data;
                            this.dialog.close(currentUser);
                            this.document.body.classList.remove('modal-open');  //popup croll fixed
                        }
                    }
                },
                error => {
                    // this.alertService.error(error);
                    // this.loading = false;
                });
        } else {
            this.validateAllFormFields(this.userForm);
        }
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }
    openRegister() {
        this.dialog.close();
        let registerDialog = this.modal.open(RegisterModal);
        registerDialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                if (result) {                    
                    this.appState.publish('login');
                    if (this.context.link && this.context.link == "free-catalog-request") {
                        let context = new FreeCatalogRequestModalContext(result);
                        context.isBlocking = true; // now its blocking.
                        this.modal.open(FreeCatalogRequestModal, { context: context });
                    }
                }
            });
        });
    }
    openForgotPassword() {
        this.dialog.close();
        let forgotPasswordDialog = this.modal.open(ForgotPasswordModal);
        forgotPasswordDialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                if (result) {

                }
            });
        });
    }
    close() {
        this.dialog.close();
        this.document.body.classList.remove('modal-open'); 
    }

    beforeDismiss(): boolean {
        return true;
    }

    beforeClose(): boolean {
        return false;
    }
}