﻿import {Component, ViewContainerRef, ViewEncapsulation} from '@angular/core';
import {BSModalContext,Modal, BSModalContextBuilder} from 'angular2-modal/plugins/bootstrap'; 
import {User} from 'src/app/shared/services/api/models/user';
import {Product} from 'src/app/shared/services/api/models/product';
import {Router} from '@angular/router';
import {AppState} from 'src/app/shared/services/api/services/appState.service';
import {CardProductService} from 'src/app/shared/services/api/services/cardProduct.service';
import {OrderLogicService} from 'src/app/shared/services/api/services/orderLogic.service';
import {AuthenticationService } from 'src/app/shared/services/api/services/authentication.service'; 
import { LoginModal } from '../login/login.component';
import { RegisterModal } from '../register/register.component';
import { AddOrderModalContext, AddOrderModal } from '../add-order/add-order.component';
@Component({
    selector: 'header-menu',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    providers: [Modal],
    encapsulation: ViewEncapsulation.None
})
export class HeaderMenuComponent {
    private ADD_ORDER_CARD: string = 'ADD_ORDER_CARD';
    currentUserStorage = localStorage['currentUser'];
    currentUser: User;
    isLogin: boolean = false;
    countProducts: number = 0;
    constructor(vcRef: ViewContainerRef, 
        public modal: Modal, 
        private router: Router,
        public appState: AppState, private authenticationService: AuthenticationService,
        private orderLogicService: OrderLogicService,
        private cardProductService: CardProductService) {             
        this.currentUser = this.authenticationService.getCurrentUser();
        this.isLogin = this.currentUser ? true : false;
        this.countProducts = this.orderLogicService.countProducts();
        appState.event.subscribe((data) => {
            switch (data) {
                case 'login':
                    this.currentUser = this.authenticationService.getCurrentUser();
                    if (this.currentUser) {
                        this.isLogin = true;
                        this.orderLogicService.addCartToUser();
                    } else {
                        localStorage.clear();
                        this.openLogin();
                    }
                    break;
                case 'logout':
                    this.isLogin = false;               
                    this.countProducts = this.orderLogicService.countProducts();     
                    break;
                case this.ADD_ORDER_CARD:
                    this.countProducts = this.orderLogicService.countProducts();
                    break;
                case 'updateCurrentUser':
                    this.isLogin = true;    
                    this.currentUser = this.authenticationService.getCurrentUser();
                    break;
                case 'addWishList':
                    this.currentUser = this.authenticationService.getCurrentUser();
                    if (this.currentUser) {
                        this.isLogin = true;
                        this.appState.publish('addWishListBack');   
                    } 
                    break;

            }
        });
    }
    logout() {
        this.authenticationService.logout();
        localStorage.clear();
        this.router.navigate(['/home']);
        this.currentUser = null;
        localStorage.clear();
        this.isLogin = false;
    }
    openLogin() {
        let dialog = this.modal.open(LoginModal);
        dialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                this.currentUser = result;
                if (this.currentUser) {
                    this.isLogin = true;
                    //this.orderLogicService.addCartToUser();
                }
            });
        });
    }
    openRegister() {
        let dialog = this.modal.open(RegisterModal);
        dialog.then((resultPromise) => {
            return resultPromise.result.then((result) => {
                this.currentUser = result;
                if (this.currentUser) {
                    this.isLogin = true;
                    //this.orderLogicService.addCartToUser();
                }
            });
        });
    }   
    showAddOrderDialog() {
        let context = new AddOrderModalContext(null);
        context.dialogClass = 'add-order-modal';
        context.isBlocking = true;
        this.modal.open(AddOrderModal, { context: context });
    }
}
